package models

import (
	"crypto/rand"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"golang.org/x/crypto/bcrypt"
)

// UserWithProject ...
type UserWithProject struct {
	ID            int       `json:"id"`
	Username      string    `json:"username"`
	Firstname     string    `json:"firstname"`
	Lastname      string    `json:"lastname"`
	Email         string    `json:"email"`
	OsUserID      string    `json:"os_user_id"`
	OsTenantID    string    `json:"os_tenant_id"`
	OsTenantName  string    `json:"os_tenant_name"`
	LastLoginDate time.Time `json:"last_login_date"`
	IsActive      bool      `json:"is_active"`
	MobileNum     string    `json:"mobile_num"`
	RoleID        int       `json:"role_id"`
	RoleName      string    `json:"role_name"`
}

//IndexAll ...
func IndexAll() ([]database.SysUser, error) {
	o := orm.NewOrm()
	var us []database.SysUser

	count, e := o.QueryTable("SysUser").All(&us, "MobileNum", "Email")
	if e != nil {
		return nil, e
	}

	if count <= 0 {
		return nil, errors.New("There are no users")
	}
	return us, nil
}

// CreateNewUser ...
func CreateNewUser(email, password, username string) (int64, error) {
	o := orm.NewOrm()

	passHash, hashErr := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if hashErr != nil {
		return -1, errors.New("Cannot generate hash from password")
	}

	user := database.SysUser{}
	user.Email = email
	user.Password = string(passHash)
	user.Username = username

	_, err := o.Raw("SELECT * FROM sys_user WHERE email OR username ? and ?", email, username).QueryRows(&user) // TODO
	if err != nil {
		return -1, errors.New("Invalid username or email")
	}

	uid, insertErr := o.Insert(&user)
	if insertErr != nil {
		return -1, errors.New("Failed to insert user to database")
	}

	return uid, nil
}

// FindByID ...
func FindByID(id uint32) (*database.SysUser, error) {
	o := orm.NewOrm()
	u := database.SysUser{Base: database.Base{
		ID: uint32(id),
	}}
	err := o.Read(&u)

	if err != orm.ErrNoRows {
		return nil, errors.New("User not found")
	} else if err == nil {
		return &u, nil
	} else {
		return nil, errors.New("Unknown error occurred")
	}
}

// GetUserByEmail ...
func GetUserByEmail(email string) (user database.SysUser, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("SysUser").Filter("email", email).One(&user)
	return
}

// GetUserByName ...
func GetUserByName(username string) (*database.SysUser, error) {
	o := orm.NewOrm()
	u := database.SysUser{Username: username}
	err := o.Read(&u, "Username")

	if err != orm.ErrNoRows {
		return nil, errors.New("User not found")
	} else if err == nil {
		return &u, nil
	} else {
		return nil, errors.New("Unknown error occurred")
	}
}

// ForgotPassword ...
func ForgotPassword(email string) (database.SysUser, string, error) {
	o := orm.NewOrm()
	var token = ""
	var userData database.SysUser

	// _, err := o.Raw("UPDATE sys_user SET password = ? WHERE email = ? ", "-1", email).Exec() // TODO

	// if err == nil {
	n := 5
	b := make([]byte, n)

	if _, err := rand.Read(b); err != nil {
		panic(err)
	}

	token = fmt.Sprintf("%X", b)

	err := o.QueryTable("SysUser").Filter("Email", email).One(&userData)

	forgottenPassword := database.SysForgotPwd{}

	forgottenPassword.Email = email
	forgottenPassword.Token = token

	_, insertError := o.Insert(&forgottenPassword)

	if insertError == nil {
		return userData, token, err
	}
	return userData, "", err
}

// ResetPassword ...
func ResetPassword(token, newPassword string) (string, error) {
	o := orm.NewOrm()

	pass := database.SysForgotPwd{Token: token}
	err := o.Read(&pass, "Token")

	if pass.IsActive == false {

		passHash, _ := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)

		_, err = o.Raw("UPDATE sys_user SET password = ? WHERE email = ? ", string(passHash), pass.Email).Exec()

		if err == nil {
			_, err = o.Raw("UPDATE sys_forgot_pwd SET is_active = ? WHERE token = ? ", true, pass.Token).Exec()
			return "OK", err
		}
		return "", err
	}
	return "", fmt.Errorf("ALREADY USED")
}

// LoginByUser ...
func LoginByUser(user database.SysUser, password string) (string, error) {

	o := orm.NewOrm()
	var userDat database.SysUser
	err := o.QueryTable("SysUser").Filter("Email", user.Email).One(&userDat)
	if err != nil {
		if err.Error() == "invalid connection" {
			for i := 0; i < 5; i++ {
				err = o.QueryTable("SysUser").Filter("Email", user.Email).One(&userDat)
				if err != nil {
					if err.Error() != "invalid connection" {
						break
					}
					str := fmt.Sprintf("Connect to db : RETRY %d", i)
					fmt.Println(str + err.Error())
				} else {
					break
				}
			}
		}
	}

	if err != nil {
		fmt.Println(err.Error())
		return "email", err // TODO
	}
	if err := bcrypt.CompareHashAndPassword([]byte(userDat.Password), []byte(password)); err != nil {
		fmt.Println(err.Error())
		return "password", err
	}
	if userDat.Role != "member" {
		fmt.Println(err.Error())
		return "user", err
	}
	return "ok", err
}

// SignInUser ...
func SignInUser(email, password, userType string) (userData database.SysUser, msg string, err error) {
	// db adapter
	o := orm.NewOrm()
	//
	o.Using("default")

	// var userData User
	err = o.QueryTable("SysUser").Filter("Email", email).One(&userData)
	if err == nil && userData != (database.SysUser{}) {
		t, e := CheckAccount(userData, password, userType)
		msg = t
		err = e
		return
	}

	if err.Error() == "invalid connection" {
		for i := 0; i < 5; i++ {
			err := o.QueryTable("SysUser").Filter("Email", email).One(&userData)
			if err != nil {
				if err.Error() != "invalid connection" {
					break
				}
			} else {
				break
			}
		}
		t, e := CheckAccount(userData, password, userType)
		msg = t
		err = e
		return
	}
	if userData == (database.SysUser{}) && err == nil {
		msg = "email"
		return
	}
	return
}

// UserEmailSearch ...
func UserEmailSearch(email string) (userData database.SysUser, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("SysUser").Filter("Email", email).One(&userData)
	return userData, err
}

// UsersAllProject ...
func UsersAllProject() (userProject []UserWithProject, err error) {
	o := orm.NewOrm()
	_, err = o.Raw(`SELECT su.id as i_d, su.username, su.firstname as firstname, su.lastname as lastname, su.email, su.os_user_id as os_user_i_d, upl.os_tenant_id as os_tenant_i_d, upl.os_tenant_name, su.last_login_date, su.is_active, su.mobile_num, upl.role_id as role_i_d, sur.name as role_name
	FROM sys_user su 
LEFT JOIN user_project_role upl 
	ON su.os_user_id = upl.os_user_id 
LEFT JOIN sys_user_roles sur 
	ON upl.role_id = sur.id`).QueryRows(&userProject)
	return userProject, err
}

// CheckEmail ...
func CheckEmail(email string) (retVal bool, err error) {
	var userData database.SysUser
	o := orm.NewOrm()
	err = o.QueryTable("SysUser").Filter("Email", email).One(&userData)
	if err != nil {
		if strings.Contains(err.Error(), "no row found") {
			return false, nil
		}
		if strings.Contains(err.Error(), "invalid connection") {
			for i := 0; i < 5; i++ {
				err := o.QueryTable("SysUser").Filter("Email", email).One(&userData)
				if err != nil {
					if err.Error() != "invalid connection" {
						break
					}
				} else {
					break
				}
			}
		}
		err = o.QueryTable("SysUser").Filter("Email", email).One(&userData)
		return true, err
	}
	return true, err
}

// CheckAccount ...
func CheckAccount(user database.SysUser, password, userType string) (string, error) {
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		fmt.Println(err.Error())
		return "password", err
	}

	if userType == "finance" {
		if user.Role != "reader" {
			return "user", nil
		}
	}
	return "ok", nil
}

// LoginByAdmin ...
func LoginByAdmin(user database.SysUser, password string) string {
	o := orm.NewOrm()
	var email database.SysUser

	o.QueryTable("SysUser").Filter("Email", user.Email).All(&email)
	err := o.Read(&user, "Email")

	if err != nil {
		return "email"
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return "password"
	}
	if email.Role == "user" || email.Role == "finance" {
		return "user"
	}
	return "ok"
}

// LoginByFinance ...
func LoginByFinance(user database.SysUser, password string) string {
	o := orm.NewOrm()
	var email database.SysUser

	o.QueryTable("SysUser").Filter("Email", user.Email).All(&email)
	err := o.Read(&user, "Email")

	if err != nil {
		return "email"
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return "password"
	}
	if email.Role != "finance" {
		return "user"
	}
	return "ok"
}

// UserListDatabase ...
func UserListDatabase() ([]*database.SysUser, error) {
	o := orm.NewOrm()
	var users []*database.SysUser

	_, err := o.QueryTable("SysUser").All(&users)
	return users, err
}

// GetUserFromDatabase ...
func GetUserFromDatabase(osUserID string) (database.SysUser, error) {
	o := orm.NewOrm()
	var user database.SysUser

	err := o.QueryTable("SysUser").Filter("OsUserID", osUserID).One(&user) // TODO
	return user, err
}

// UpdateUserFromDatabase ...
func UpdateUserFromDatabase(user *database.SysUser) error {
	o := orm.NewOrm()
	u := database.SysUser{Base: database.Base{
		ID: uint32(user.ID),
	}}

	if err := o.Read(&u); err == nil {
		if _, err = o.Update(&user, "Firstname", "Lastname", "Email"); err == nil {
			return err
		}
	}

	return errors.New("Error")
}

// DeleteUserFromDatabase ...
func DeleteUserFromDatabase(osUserID string) error {
	o := orm.NewOrm()
	_, err := o.QueryTable("SysUser").Filter("os_user_id", osUserID).Delete()
	return err
}

// ReturnUserData ...
func ReturnUserData(openstackID string) database.SysUser {
	var user database.SysUser
	o := orm.NewOrm()
	err := o.QueryTable("SysUser").Filter("OsUserID", openstackID).One(&user)
	if err == nil {

		return user
	}
	return database.SysUser{}
}

// GetUserDefTenantID ...
func GetUserDefTenantID(username string) string {
	var user database.SysUser
	o := orm.NewOrm()
	err := o.QueryTable("SysUser").Filter("Username", username).One(&user)
	if err == nil {
		return user.OsTenantID
	}
	return ""
}

// VerifyUserMobile ...
func VerifyUserMobile(o orm.Ormer, osUserID, code, mobileNum string) (database.SysUser, error) {
	var mobileVal database.SysMobileVal
	var user database.SysUser
	selectMobileErr := o.Raw("SELECT * FROM sys_mobile_val spv where os_user_id = ? AND code = ?", osUserID, code).QueryRow(&mobileVal)

	if selectMobileErr == orm.ErrNoRows {
		return user, errors.New("Wrong code")
	}

	if mobileVal.IsUsed {
		return user, errors.New("It's already used")
	}

	if selectMobileErr != nil {
		return user, selectMobileErr
	}

	_, updateErr1 := o.QueryTable("sys_user").Filter("os_user_id", osUserID).Filter("mobile_num", mobileNum).Update(orm.Params{
		"is_active_mobile": true,
		"quota_plan":       helper.QuotaPlanBasic,
	})

	if updateErr1 != nil {
		return user, updateErr1
	}

	_, updateErr2 := o.QueryTable("sys_mobile_val").Filter("code", code).Filter("mobile_num", mobileNum).Update(orm.Params{
		"is_used": true,
	})

	if updateErr2 != nil {
		return user, updateErr2
	}

	selectErr := o.QueryTable("sys_user").Filter("os_user_id", osUserID).One(&user)
	return user, selectErr
}

// UpdateLoginDate ...
// Update login date time after login
func UpdateLoginDate(user database.SysUser) (err error) {
	o := orm.NewOrm()

	user.LastLoginDate = time.Now()
	if _, err := o.Update(&user); err != nil {
		return err
	}

	return
}

// InsertPhoneVerifyCode ...
func InsertPhoneVerifyCode(o orm.Ormer, randCode, osUserID, mobile string) (sysMobileVal database.SysMobileVal, err error) {
	sysMobileVal.Code = randCode
	sysMobileVal.OsUserID = osUserID
	sysMobileVal.MobileNum = mobile

	_, err = o.Insert(&sysMobileVal)
	return sysMobileVal, err
}

// UpdateUserMobile ...
func UpdateUserMobile(o orm.Ormer, osUserID, mobile string) (err error) {
	_, err = o.QueryTable("sys_user").Filter("os_user_id", osUserID).Update(orm.Params{
		"mobile_num":       mobile,
		"is_active_mobile": false,
	})
	return err
}

// InsertSysMsg ...
func InsertSysMsg(o orm.Ormer, smsTxt, smsType, mobile, osUserID, errorTxt string) (err error) {
	var isError bool = false
	if errorTxt != "" {
		isError = true
	}

	var sysMsg database.SysMsg

	sysMsg.Body = smsTxt
	sysMsg.Type = smsType
	sysMsg.OsUserID = osUserID
	sysMsg.ToNumber = mobile
	sysMsg.IsError = isError
	sysMsg.ErrorTxt = errorTxt

	_, err = o.Insert(&sysMsg)
	return err
}

// CheckAdmin role...
func CheckAdmin(osUserID string) (isAdmin bool, err error) {
	var user database.SysUser
	o := orm.NewOrm()

	err = o.QueryTable("SysUser").Filter("os_user_id", osUserID).One(&user)

	if user.Role == helper.Admin {
		isAdmin = true
	} else {
		isAdmin = false
	}
	return isAdmin, err
}

// UpdateUserActived ...
func UpdateUserActived(osUserID string, enabled bool) (err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("sys_user").Filter("os_user_id", osUserID).Update(orm.Params{
		"is_active": enabled,
	})
	return err
}
