package models

import (
	"errors"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// CreateQuotas ...
func CreateQuotas(quota database.SysUserQuota) (newQuota database.SysUserQuota, err error) {
	o := orm.NewOrm()
	id, err := o.Insert(&quota)
	quota.ID = uint32(id)
	newQuota = quota
	return newQuota, err
}

// ListQuotas ...
func ListQuotas() (Quotas []database.SysUserQuota, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("SysUserQuota").RelatedSel().All(&Quotas)
	return
}

// GetQuota ...
func GetQuota(quotaID uint32) (role database.SysUserQuota, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("SysUserQuota").Filter("id", quotaID).One(&role)
	if err == orm.ErrNoRows {
		err = errors.New("No row fount")
	}

	if err == nil {
		// o.LoadRelated(&role, "permissions")
	}

	return
}

// DeleteQuota ...
func DeleteQuota(o orm.Ormer, quotaID uint32) error {
	_, err := o.QueryTable("SysUserQuota").Filter("id", quotaID).Delete()
	return err
}

// UpdateQuota ...
func UpdateQuota(quotaID uint32, Instances, CPU, RAM, Keypair, Volume, Snapshot, VolumeSize, ExternalIP, SecurityGroup float64) error {
	o := orm.NewOrm()
	_, err := o.QueryTable("SysUserQuota").Filter("id", quotaID).Update(orm.Params{
		"instances":      Instances,
		"cpu":            CPU,
		"ram":            RAM,
		"keypair":        Keypair,
		"volume":         Volume,
		"snapshot":       Snapshot,
		"volume_size":    VolumeSize,
		"external_ip":    ExternalIP,
		"security_group": SecurityGroup,
	})
	return err
}

// CheckUserQuota ...
func CheckUserQuota(projectID, osUserID string) (quota database.SysUserQuota, quotaErr error) {
	o := orm.NewOrm()
	var projectQuota database.UserProjectRole
	projectErr := o.QueryTable("UserProjectRole").Filter("os_tenant_id", projectID).Filter("os_user_id", osUserID).One(&projectQuota)
	if projectErr != nil {
		return quota, projectErr
	}

	quotaErr = o.QueryTable("SysUserQuota").Filter("id", projectQuota.QuotaID).One(&quota)
	return quota, quotaErr
}

// AddUsageQuotaInstance ...
func AddUsageQuotaInstance(quotaID uint32, CPU, RAM, VolumeSize int) (err error) {
	o := orm.NewOrm()

	if VolumeSize != 0 {
		_, err = o.QueryTable("SysUserQuota").Filter("id", quotaID).Update(orm.Params{
			"used_cpu":         CPU,
			"used_ram":         RAM,
			"used_volume_size": VolumeSize,
		})
	} else {
		_, err = o.QueryTable("SysUserQuota").Filter("id", quotaID).Update(orm.Params{
			"used_instances": orm.ColValue(orm.ColAdd, 1),
			"used_cpu":       CPU,
			"used_ram":       RAM,
			"used_volume":    orm.ColValue(orm.ColAdd, 1),
		})
	}
	return err
}

// MinusUsageQuotaInstance ...
func MinusUsageQuotaInstance(quotaID uint32, CPU, RAM, VolumeSize int) (err error) {
	o := orm.NewOrm()

	_, err = o.QueryTable("SysUserQuota").Filter("id", quotaID).Update(orm.Params{
		"used_instances":   orm.ColValue(orm.ColMinus, 1),
		"used_cpu":         orm.ColValue(orm.ColMinus, CPU),
		"used_ram":         orm.ColValue(orm.ColMinus, RAM),
		"used_volume":      orm.ColValue(orm.ColMinus, 1),
		"used_volume_size": orm.ColValue(orm.ColMinus, VolumeSize),
	})

	return err
}

// AddUsageQuotaKPAndSG ...
func AddUsageQuotaKPAndSG(projectID, osUserID string, isKP, isSG bool) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}

	if isKP {
		_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
			"used_keypair": orm.ColValue(orm.ColAdd, 1),
		})
	}
	if isSG {
		_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
			"used_security_group": orm.ColValue(orm.ColAdd, 1),
		})
	}

	return err
}

// MinusUsageQuotaKPAndSG ...
func MinusUsageQuotaKPAndSG(projectID, osUserID string, isKP, isSG bool) (err error) {
	o := orm.NewOrm()

	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}

	if isKP {
		_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
			"used_keypair": orm.ColValue(orm.ColMinus, 1),
		})
	}
	if isSG {
		_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
			"used_security_group": orm.ColValue(orm.ColMinus, 1),
		})
	}
	return err
}

// AddUsageQuotaSnapshot ...
func AddUsageQuotaSnapshot(projectID, osUserID string) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}
	_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
		"used_snapshot": orm.ColValue(orm.ColAdd, 1),
	})
	return err
}

// MinusUsageQuotaSnapshot ...
func MinusUsageQuotaSnapshot(projectID, osUserID string) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}
	_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
		"used_snapshot": orm.ColValue(orm.ColMinus, 1),
	})
	return err
}

// AddUsageQuotaFIP ...
func AddUsageQuotaFIP(projectID, osUserID string) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}
	_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
		"used_external_ip": orm.ColValue(orm.ColAdd, 1),
	})
	return err
}

// MinusUsageQuotaFIP ...
func MinusUsageQuotaFIP(projectID, osUserID string) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}
	_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
		"used_external_ip": orm.ColValue(orm.ColMinus, 1),
	})
	return err
}

// AddUsageQuotaVolume ...
func AddUsageQuotaVolume(projectID, osUserID string, volumeSize int) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}
	_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
		"used_volume":      orm.ColValue(orm.ColAdd, 1),
		"used_volume_size": orm.ColValue(orm.ColAdd, volumeSize),
	})
	return err
}

// MinusUsageQuotaVolume ...
func MinusUsageQuotaVolume(projectID, osUserID string, volumeSize int) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}
	_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
		"used_volume":      orm.ColValue(orm.ColMinus, 1),
		"used_volume_size": orm.ColValue(orm.ColMinus, volumeSize),
	})
	return err
}

// AddUsageQuotaVolumeSize ...
func AddUsageQuotaVolumeSize(projectID, osUserID string, volumeSize int) (err error) {
	o := orm.NewOrm()
	quota, quotaErr := UserQuota(projectID, osUserID)
	if quotaErr != nil {
		return quotaErr
	}
	_, err = o.QueryTable("SysUserQuota").Filter("id", quota.ID).Update(orm.Params{
		"used_volume":      orm.ColValue(orm.ColMinus, 1),
		"used_volume_size": orm.ColValue(orm.ColMinus, volumeSize),
	})
	return err
}
