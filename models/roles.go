package models

import (
	"errors"
	"fmt"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// CreateRoles ...
func CreateRoles(role database.SysUserRoles) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(&role)
	role.ID = uint32(id)
	return id, err
}

// ListRoles ...
func ListRoles() (roles []database.SysUserRoles, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("SysUserRoles").RelatedSel().All(&roles)
	for _, role := range roles {
		o.LoadRelated(&role, "Permissions")
	}
	return
}

// GetRole ...
func GetRole(roleID uint32) (role database.SysUserRoles, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("SysUserRoles").Filter("id", roleID).One(&role)
	o.LoadRelated(&role, "Permissions")
	if err == orm.ErrNoRows {
		err = errors.New("No row fount")
	}
	return
}

// GetRoleName ...
func GetRoleName(roleName string) (role database.SysUserRoles, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("SysUserRoles").Filter("name", roleName).One(&role)
	o.LoadRelated(&role, "Permissions")
	if err == orm.ErrNoRows {
		err = errors.New("No row fount")
	}
	return
}

// DeleteRole ...
func DeleteRole(o orm.Ormer, roleID uint32) error {
	_, err := o.QueryTable("SysUserRoles").Filter("id", roleID).Delete()
	return err
}

// UpdateRole ...
func UpdateRole(o orm.Ormer, roleID uint32, name string) error {
	_, err := o.QueryTable("SysUserRoles").Filter("id", roleID).Update(orm.Params{
		"Name": name,
	})
	return err
}

// RoleAddPermission ...
func RoleAddPermission(o orm.Ormer, roleID uint32, permission database.SysUserPermission) (err error) {
	role := database.SysUserRoles{ID: roleID}
	m2m := o.QueryM2M(&role, "Permissions")
	_, err = m2m.Add(permission)
	return
}

// RoleAddPermissions ...
func RoleAddPermissions(o orm.Ormer, roleID string, permissions []interface{}) (err error) {

	sql := "INSERT INTO sys_user_roles_sys_user_permissions (sys_user_roles_id, sys_user_permission_id) VALUES "

	for index, per := range permissions {
		sql = sql + "(" + roleID + "," + per.(string) + ")"
		if index != len(permissions)-1 {
			sql = sql + ","
		}
	}

	fmt.Println(sql)

	_, err = o.Raw(sql).Exec()

	// role := database.SysUserRoles{ID: roleID}
	// m2m := o.QueryM2M(&role, "Permissions")
	// _, err = m2m.Add(permission)
	return
}

// RoleRemoveAllPermission ...
func RoleRemoveAllPermission(o orm.Ormer, roleID uint32) (err error) {
	role := database.SysUserRoles{ID: roleID}
	m2m := o.QueryM2M(&role, "Permissions")
	_, err = m2m.Clear()
	return
}

// RoleCheck ...
func RoleCheck(email, tenantID, api string) (isGranted bool) {
	o := orm.NewOrm()
	isGranted = false
	var per []database.SysUserPermission
	sql := "SELECT * FROM sys_user_permission sup WHERE API = ? AND EXISTS (SELECT * FROM sys_user_roles_sys_user_permissions sursup WHERE sys_user_roles_id = (SELECT role_id FROM user_project_role upr WHERE os_tenant_id = ? AND email = ?) AND sup.id = sursup.sys_user_permission_id)"

	count, errProRole := o.Raw(sql, api, tenantID, email).QueryRows(&per)

	if errProRole != nil {
		fmt.Println(errProRole.Error())
	}

	if count > 0 {
		isGranted = true
	}

	return
}
