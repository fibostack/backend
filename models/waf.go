package models

import (
	"errors"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// CreateWAF ...
func CreateWAF(waf database.WAFs) (database.WAFs, error) {
	o := orm.NewOrm()
	id, err := o.Insert(&waf)
	waf.ID = uint32(id)
	return waf, err
}

// ListWAFs ...
func ListWAFs() (wafs []database.WAFs, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("WAFs").All(&wafs)
	return
}

// ListInstanceWAFs ...
func ListInstanceWAFs() (map[string]database.WAFs, error) {
	o := orm.NewOrm()
	var wafs []database.WAFs
	result := make(map[string]database.WAFs)
	_, err := o.QueryTable("WAFs").All(&wafs)
	for _, waf := range wafs {
		result[waf.InstanceID] = waf
	}
	return result, err
}

// GetWAF ...
func GetWAF(id uint32) (waf database.WAFs, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("WAFs").Filter("id", id).One(&waf)
	if err == orm.ErrNoRows {
		err = errors.New("No row fount")
	}
	return
}

// GetInstanceWAFs ...
func GetInstanceWAFs(id string) (waf database.WAFs, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("WAFs").Filter("instance_id", id).One(&waf)
	if err == orm.ErrNoRows {
		err = errors.New("No row fount")
	}
	return
}

// DeleteWAF ...
func DeleteWAF(id uint32) error {
	o := orm.NewOrm()
	_, err := o.QueryTable("WAFs").Filter("id", id).Delete()
	return err
}
