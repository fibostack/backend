package models

import (
	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// ListLapp ...
// Ticket Controller оос гаргана
func ListLapp() []*database.LappMaster {
	o := orm.NewOrm()
	var lapp []*database.LappMaster
	o.QueryTable(new(database.LappMaster)).All(&lapp)
	return lapp
}

// CreateLapp нь script үүсгэх
func CreateLapp(name, path string) error {
	o := orm.NewOrm()
	s := database.LappMaster{Name: name, PathYml: path}
	_, err := o.Insert(&s)
	return err
}

// GetLappByID нь 1 буцаах
func GetLappByID(id uint32) database.LappMaster {
	s := database.LappMaster{Base: database.Base{ID: id}}
	o := orm.NewOrm()
	err := o.Read(&s)
	if err != nil {
		return s
	}
	return s
}
