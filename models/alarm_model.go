package models

import (
	"time"
)

type (
	// AlarmNotification struct
	AlarmNotification struct {
		ID     string `json:"id"`
		Name   string `json:"name"`
		Period int    `json:"period"`
		Email  string `json:"email"`
	}

	// AlarmDefinition struct
	AlarmDefinition struct {
		ID                  string   `json:"id"`
		Name                string   `json:"name"`
		Severity            string   `json:"severity"`
		Expression          string   `json:"expression"`
		AlarmAction         []string `json:"alarmAction"`
		OkAction            []string `json:"okAction"`
		MatchBy             []string `json:"matchBy"`
		UndeterminedActions []string `json:"undetermined_actions"`
		Description         string   `json:"description"`
		//totalCnt         int      `json:"totalCnt"`
	}

	// AlarmDefinitionDetail struct
	AlarmDefinitionDetail struct {
		ID                string              `json:"id"`
		Name              string              `json:"name"`
		Severity          string              `json:"severity"`
		Expression        string              `json:"expression"`
		AlarmNotification []AlarmNotification `json:"alarmAction"`
		//OkAction   	 []string `json:"okAction"`
		MatchBy             []string `json:"matchBy"`
		UndeterminedActions []string `json:"undetermined_actions"`
		Description         string   `json:"description"`
	}

	// AlarmAction struct
	AlarmAction struct {
		ID     string `json:"id"`
		Name   string `json:"name"`
		Period int    `json:"period"`
		Email  string `json:"email"`
	}

	// AlarmStatus struct
	AlarmStatus struct {
		ID                  string `json:"id"`
		HostName            string `json:"hostname"`
		AlarmDefinitionName string `json:"alarmDefinitionName"`
		AlarmDefinitionID   string `json:"alarmDefinitionId"`
		MetricName          string `json:"metricName"`
		Expression          string `json:"expression"`
		Type                string `json:"type"`
		Zone                string `json:"zone"`
		Severity            string `json:"severity"`
		State               string `json:"state"`
		UpdateTime          string `json:"updateTime"`
	}

	// Alarm struct
	Alarm struct {
		ID                string
		AlarmDefinitionID string
		Name              string
		Expression        string
		Severity          string
	}

	// AlarmHistory struct
	AlarmHistory struct {
		ID       string `json:"alarmId"`
		Time     string `json:"time"`
		NewState string `json:"newState"`
		OldState string `json:"oldState"`
		Reason   string `json:"reason"`
	}

	// AlarmActionHistory struct
	AlarmActionHistory struct {
		ID              uint      `gorm:"primary_key"`
		AlarmID         string    `gorm:"type:varchar(36);not null;"`
		AlarmActionDesc string    `gorm:"type:text;"`
		RegDate         time.Time `gorm:"type:datetime;DEFAULT:current_timestamp;not null;"`
		RegUser         string    `gorm:"type:varchar(36);DEFAULT:'system';not null;"`
		ModiDate        time.Time `gorm:"type:datetime;DEFAULT:current_timestamp;null;"`
		ModiUser        string    `gorm:"type:varchar(36);DEFAULT:'system';null;"`
	}

	// AlarmActionRequest struct
	AlarmActionRequest struct {
		ID              uint
		AlarmID         string
		AlarmActionDesc string
		RegDate         time.Time
		RegUser         string
		ModiDate        time.Time
		ModiUser        string
	}

	// AlarmActionResponse struct
	AlarmActionResponse struct {
		ID              uint   `json:"id"`
		AlarmID         string `json:"alarmId"`
		AlarmActionDesc string `json:"alarmActionDesc"`
		RegDate         string `json:"regDate"`
		RegUser         string `json:"regUser"`
		ModiDate        string `json:"ModiDate"`
		ModiUser        string `json:"modiUser"`
	}
)
