package models

import (
	"errors"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// CreatePermissions ...
func CreatePermissions(permission database.SysUserPermission) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(&permission)
	permission.ID = uint32(id)
	return id, err
}

// ListPermissions ...
func ListPermissions() (permissions []database.SysUserPermission, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("SysUserPermission").RelatedSel().All(&permissions)
	return
}

// GetPermission ...
func GetPermission(permissionID uint32) (permission database.SysUserPermission, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("SysUserPermission").Filter("id", permissionID).One(&permission)
	// o.LoadRelated(&permission, "SysUserRoles")
	if err == orm.ErrNoRows {
		err = errors.New("No row fount")
	}
	return
}

// DeletePermission ...
func DeletePermission(permissionID uint32) error {
	o := orm.NewOrm()
	_, err := o.QueryTable("SysUserPermission").Filter("id", permissionID).Delete()
	return err
}
