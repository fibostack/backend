package models

import (
	"fmt"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
)

// TotalLog ...
type TotalLog struct {
	Count int `json:"count"`
}

// LogStruct ...
type LogStruct struct {
	ID           uint      `json:"id"`
	CreatedAt    time.Time `json:"create_at"`
	Hostname     string    `json:"hostname"`
	OsResourceID string    `json:"os_resource_id"`
	Flavor       string    `json:"flavor"`
	DiskSize     int       `json:"disk_size"`
	Service      string    `json:"service"`
	Status       string    `json:"status"`
	IP           string    `json:"ip"`
	Action       string    `json:"action"`
	IsError      bool      `json:"is_error"`
	ErrorMsg     string    `json:"error_msg"`
}

// ActionLogStruct ...
type ActionLogStruct struct {
	Id            int       `json:"id"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	OsUserId      string    `json:"os_user_id"`
	CloudProvider string    `json:"cloud_provider"`
	Service       string    `json:"service"`
	ResourceName  string    `json:"resource_name"`
	ResourceId    string    `json:"resource_id"`
	EventName     string    `json:"event_name"`
	EventDate     string    `json:"event_date"`
	ErrorMsg      string    `json:"error_msg"`
	SourceIp      string    `json:"source_ip"`
	OsTenantId    string    `json:"os_tenant_id"`
	OsTenantName  string    `json:"os_tenant_name"`
	Email         string    `json:"email"`
	RequestBody   string    `json:"request_body"`
	RequestHeader string    `json:"request_header"`
}

// CreateUsageAction struct
func CreateUsageAction(sysUserID uint32, osUserID, Type, hostname, osResourceID, osInstanceID, flavor, status, ip string, lastLogActionID int64, startDate, endDate time.Time, diskSize, cpu, ram int) {
	o := orm.NewOrm()
	var usageObj database.UsgHistory

	var endDateStr string
	if (endDate != time.Time{}) {
		endDateStr = endDate.Format(helper.TimeFormatYYYYMMDDHHMMSS)
	}

	usageObj = database.UsgHistory{
		SysUserID:       sysUserID,
		OsUserID:        osUserID,
		Type:            Type,
		Hostname:        hostname,
		OsResourceID:    osResourceID,
		OsInstanceID:    osInstanceID,
		Flavor:          flavor,
		DiskSize:        diskSize,
		CPU:             cpu,
		RAM:             ram,
		Status:          status,
		StartDate:       startDate.Format(helper.TimeFormatYYYYMMDDHHMMSS),
		EndDate:         endDateStr,
		IP:              ip,
		LastLogActionID: uint32(lastLogActionID),
	}
	_, err := o.Insert(&usageObj)
	fmt.Println(err)
	return
}

// SafeLogColumnName ...
func SafeLogColumnName(name string) string {
	switch name {
	case "cloud_provider":
		return "la." + name
	case "created_at":
		return "la." + name
	case "resource_name":
		return "la." + name
	case "service":
		return "la." + name
	case "event_name":
		return "la." + name
	case "source_ip":
		return "la." + name
	case "error_msg":
		return "la." + name
	case "email":
		return "su." + name
	case "os_tenant_name":
		return "upl." + name
	default:
		return name
	}
}

// ActionLogList ...
func ActionLogList(osUserID, osTenantID string, pageNum, pageSize int, sortField, sortOrder, fieldName, keyword string) (logs []ActionLogStruct, total int64, err error) {
	o := orm.NewOrm()
	var logsCount []database.LogAction

	var count int64
	var errCount error

	if osTenantID != beego.AppConfig.String("tenantID") {
		count, errCount = o.QueryTable("LogAction").Filter("os_user_id", osUserID).Filter("os_tenant_id", osTenantID).All(&logsCount)

	} else {
		count, errCount = o.QueryTable("LogAction").All(&logsCount)
	}

	if errCount != nil {
		return logs, count, errCount
	}

	offset := pageNum*pageSize - pageSize
	// filterColumns := [8]string{"event_name", "service", "resource_name", "cloud_provider", "os_tenant_name", "email", "error_msg", "source_ip"}

	sql := "SELECT la.*, su.email, upl.os_tenant_name FROM log_action la LEFT JOIN sys_user su on la.os_user_id = su.os_user_id LEFT JOIN user_project_role upl on la.os_tenant_id = upl.os_tenant_id"

	if fieldName != "" && keyword != "" {
		sql += " WHERE " + SafeLogColumnName(fieldName) + " like '%" + keyword + "%'"

		if osTenantID != beego.AppConfig.String("tenantID") {
			sql += " AND " + "la.os_user_id" + " = '" + osUserID + "'"
			sql += " AND " + "la.os_tenant_id" + " = '" + osTenantID + "'"
		}
	} else {
		if osTenantID != beego.AppConfig.String("tenantID") {
			sql += " WHERE " + "la.os_user_id" + " = '" + osUserID + "'"
			sql += " AND " + "la.os_tenant_id" + " = '" + osTenantID + "'"
		}
	}

	if sortField != "" && sortOrder != "" {
		sql += " ORDER BY " + SafeLogColumnName(sortField) + " " + sortOrder
	} else {
		sql += " ORDER BY created_at DESC"
	}
	sql += " LIMIT ? OFFSET ?"

	_, err = o.Raw(sql, pageSize, offset).QueryRows(&logs)

	return logs, count, err
}
