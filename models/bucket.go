package models

// Bucket ...
type Bucket struct {
	Name    string `json:"name"`
	IsCloud bool   `json:"is_cloud"`
}
