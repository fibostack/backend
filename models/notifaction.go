package models

import (
	"time"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// GetNotification ...
func GetNotification(id int) (database.SysNotif, error) {
	notif := database.SysNotif{Base: database.Base{ID: uint32(id)}}
	o := orm.NewOrm()
	err := o.Read(&notif)
	if err != nil {
		return notif, err
	}
	return notif, nil
}

// ChangeNotificationStatus ...
func ChangeNotificationStatus(notifid, isnew int) (bool, error) {
	o := orm.NewOrm()
	notif := database.SysNotif{Base: database.Base{ID: uint32(notifid)}}
	if err := o.Read(&notif); err == nil {
		notif.IsViewed = isnew
		if _, err := o.Update(&notif, "IsViewed"); err == nil {
			return true, err
		}
		return false, err
	}
	return false, nil
}

// ListNotification ...
func ListNotification(userid string) (notification []database.SysNotif, err error) {
	o := orm.NewOrm()
	o.QueryTable("SysNotif").Filter("OsUserID", userid).All(&notification)
	return notification, err
}

// CreateNotification ...
func CreateNotification(userid, topicname, notifname, tenantid, url, iserror string, isnew int, date time.Time) (database.SysNotif, error) {
	o := orm.NewOrm()

	notif := database.SysNotif{
		OsUserID:   userid,
		Title:      topicname,
		Text:       notifname,
		IsErrored:  iserror,
		Base:       database.Base{CreatedAt: date},
		OsTenantID: tenantid,
		IsViewed:   isnew,
		URL:        url,
	}
	_, err := o.Insert(&notif)

	if err == nil {
		return notif, nil
	}
	return notif, err
}
