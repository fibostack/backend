package models

import (
	"errors"
	"unicode"

	// "github.com/alexedwards/scs"
	"github.com/cihub/seelog"
	// monascagopher "github.com/gophercloud/gophercloud"
)

// constants
const (
	CsrfTokenName            = "X-XSRF-TOKEN"
	TestTokenName            = "TestCase"
	TestTokenValue           = "TestCase"
	UserSessionName          = "info"
	MetricNameCPUUsage       = "cpu"
	MetricNameCPULoad1M      = "1m"
	MetricNameCPULoad5M      = "5m"
	MetricNameCPULoad15M     = "15m"
	MetricNameMemorySwap     = "swap"
	MetricNameMemoryUsage    = "memory"
	MetricNameNetworkEthIn   = "InEth"
	MetricNameNetworkVxIn    = "InVxlan"
	MetricNameNetworkEthOut  = "OutEth"
	MetricNameNetworkVxOut   = "OutVxlan"
	MetricNameDiskReadKbyte  = "read"
	MetricNameDiskWriteKbyte = "write"
	MetricNameNetworkIn      = "in"
	MetricNameNetworkOut     = "out"
	ResultCnt                = "totalCnt"
	ResultProjectID          = "projectId"
	ResultName               = "name"
	ResultData               = "data"
	ResultDataName           = "metric"
	VMStatusNo               = "noStatus"
	VMStatusRunning          = "running"
	VMStatusIdle             = "idle/blocked"
	VMStatusPaused           = "paused"
	VMStatusShutdown         = "shutDown"
	VMStatusShutoff          = "shutOff"
	VMStatusCrashed          = "crashed"
	VMStatusPoewrOff         = "powerOff"
)

// ErrMessage map
type ErrMessage map[string]interface{}

// GmtTimeGap int
var GmtTimeGap int

// TestUserName string
var TestUserName string

// TestPassword string
var TestPassword string

// TestTenantID string
var TestTenantID string

// TestDomainName string
var TestDomainName string

// TestIdentityEndpoint string
var TestIdentityEndpoint string

type (

	// DetailReq struct
	DetailReq struct {
		HostName         string
		InstanceID       string
		MetricName       string
		MountPoint       string
		DefaultTimeRange string
		TimeRangeFrom    string
		TimeRangeTo      string
		GroupBy          string
	}

	// NodeReq struct
	NodeReq struct {
		HostName string
	}

	// InstanceReq struct
	InstanceReq struct {
		InstanceID string
	}

	// ProjectReq struct
	ProjectReq struct {
		ProjectID   string
		ProjectName string
		Limit       string
		Marker      string
		HostName    string
		Username    string
	}
	// AlarmReq struct
	AlarmReq struct {
		AlarmID   string
		State     string
		TimeRange string
	}
	// ErrorMessageStruct struct
	ErrorMessageStruct struct {
		Message    string `json:"message"`
		HTTPStatus int    `json:"HttpStatus"`
	}
	// TopProcess struct
	TopProcess struct {
		Index       int     `json:"index"`
		ProcessName string  `json:"processName"`
		Usage       float64 `json:"usage"`
	}
	// LogMessage struct
	LogMessage struct {
		Hostname     string   `json:"hostname"`
		PageIndex    int      `json:"pageIndex"`
		PageItems    int      `json:"pageItems"`
		LogType      string   `json:"logType"`
		Keyword      string   `json:"keyword"`
		Index        string   `json:"logstashIndex"`
		TargetDate   string   `json:"targetDate"`
		Period       int64    `json:"period"`
		StartTime    string   `json:"startTime"`
		EndTime      string   `json:"endTime"`
		CurrentItems int      `json:"currentItems"`
		TotalCount   int      `json:"totalCount"`
		Messages     []string `json:"messages"`
	}
)

// var OpenStackClient []map[string]*gophercloud.ProviderClient

// MonitLogger LoggerInterface
var MonitLogger seelog.LoggerInterface

// ProjectInstanceRequestValidate ...
func (bm ProjectReq) ProjectInstanceRequestValidate(req ProjectReq) error {

	if req.Limit == "" {
		return errors.New("Required input value does not exist. [limit]")
	}
	if isInt(req.Limit) == false {
		return errors.New("Required input value require Number. [limit]")
	}

	if req.ProjectID == "" {
		return errors.New("Required input value does not exist. [projectId]")
	}
	return nil
}

// isInt ...
func isInt(s string) bool {
	for _, c := range s {
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}

// var SessionManager scs.Manager // = scs.NewCookieManager("u46IpCV9y5Vlur8YvODJEhgOY8m9JVE4")

// MetricRequestValidate ...
func (bm DetailReq) MetricRequestValidate(req DetailReq) error {

	if req.HostName == "" {
		return errors.New("Required input value does not exist. [hostname]")
	}

	//조회 조건 Validation Check
	if req.TimeRangeFrom == "" && req.TimeRangeTo == "" {
		if req.DefaultTimeRange == "" {
			return errors.New("Required input value does not exist. [defaultTimeRange]")
		}
		if req.GroupBy != "" {
			return nil
		}
		return errors.New("Required input value does not exist. [groupBy]")
		// return errors.New("Required input value does not exist. [timeRangeFrom, timeRangeTo]")
	}
	if req.TimeRangeFrom == "" || req.TimeRangeTo == "" {
		if req.TimeRangeFrom == "" {
			return errors.New("Required input value does not exist. [timeRangeFrom]")
		} else if req.TimeRangeTo == "" {
			return errors.New("Required input value does not exist. [timeRangeTo]")
		}
	}

	if req.GroupBy == "" {
		return errors.New("Required input value does not exist. [groupBy]")
	}

	return nil

}

// InstanceMetricRequestValidate ...
func (bm DetailReq) InstanceMetricRequestValidate(req DetailReq) error {

	if req.InstanceID == "" {
		return errors.New("Required input value does not exist. [instanceId]")
	}

	//조회 조건 Validation Check
	if req.TimeRangeFrom == "" && req.TimeRangeTo == "" {
		if req.DefaultTimeRange == "" {
			return errors.New("Required input value does not exist. [defaultTimeRange]")
		}
		if req.GroupBy != "" {
			return nil
		}
		return errors.New("Required input value does not exist. [groupBy]")

		// return errors.New("Required input value does not exist. [timeRangeFrom, timeRangeTo]")
	}
	if req.TimeRangeFrom == "" || req.TimeRangeTo == "" {
		if req.TimeRangeFrom == "" {
			return errors.New("Required input value does not exist. [timeRangeFrom]")
		} else if req.TimeRangeTo == "" {
			return errors.New("Required input value does not exist. [timeRangeTo]")
		}
	}

	if req.GroupBy == "" {
		return errors.New("Required input value does not exist. [groupBy]")
	}

	return nil

}

// DefaultLogValidate ...
func (bm LogMessage) DefaultLogValidate(req LogMessage) error {
	if req.Hostname == "" {
		return errors.New("Required input value does not exist. [hostname]")
	}
	if req.LogType == "" {
		return errors.New("Required input value does not exist. [logType]")
	}
	if req.PageIndex == 0 {
		return errors.New("Required input value does not exist. [pageIndex]")
	}
	if req.PageItems == 0 {
		return errors.New("Required input value does not exist. [pageItems]")
	}
	if req.Period == 0 {
		return errors.New("Required input value does not exist. [period]")
	}

	return nil
}

// SpecificTimeRangeLogValidate ...
func (bm LogMessage) SpecificTimeRangeLogValidate(req LogMessage) error {
	if req.Hostname == "" {
		return errors.New("Required input value does not exist. [hostname]")
	}
	if req.LogType == "" {
		return errors.New("Required input value does not exist. [logType]")
	}
	if req.TargetDate == "" {
		return errors.New("Required input value does not exist. [targetDate]")
	}

	return nil
}
