package models

import (
	"fmt"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/utils/gnocchi"
	"github.com/monasca/golang-monascaclient/monascaclient"
	"gitlab.com/fibo-stack/backend/database"

	// mysql ...
	_ "github.com/go-sql-driver/mysql"
)

// StartTime ...

// provider ...
var provider *gophercloud.ProviderClient

// OpenStackClient ...
var OpenStackClient []map[string]*gophercloud.ProviderClient

type (
	// BaseResponse struct
	BaseResponse struct {
		StatusCode int         `json:"status_code"`
		ErrorMsg   string      `json:"error_msg"`
		Body       interface{} `json:"body"`
	}

	// BaseResponseBody ...
	BaseResponseBody struct {
		Body interface{} `json:"body"`
	}
)

// InitDB ...
func InitDB() {
	// StartTime = startTime
	dbhost := beego.AppConfig.String("db.host")
	dbport := beego.AppConfig.String("db.port")
	dbuser := beego.AppConfig.String("db.user")
	dbpassword := beego.AppConfig.String("db.password")
	dbname := beego.AppConfig.String("db.name")
	if dbport == "" {
		dbport = "3306"
	}
	dsn := dbuser + ":" + dbpassword + "@tcp(" + dbhost + ":" + dbport + ")/" + dbname + "?charset=utf8&loc=Asia%2FUlaanbaatar"
	fmt.Println(dsn)
	orm.RegisterDataBase("default", "mysql", dsn)
	orm.RegisterModel(
		new(database.SysUser),
		new(database.SysNotif),
		new(database.SysForgotPwd),
		new(database.SysMobileVal),
		new(database.LappMaster),
		new(database.UsgHistory),
		new(database.LogAction),
		new(database.SysMsg),
		new(database.SysUserRoles),
		new(database.SysUserQuota),
		new(database.SysUserPermission),

		new(database.UserProjectRole),
		// ---------------------
		new(database.SysSettings),
		new(database.WAFs),
	)
	orm.RunSyncdb(
		"default",
		false, // if its true, force drop and create
		true,  // verbose = log
	)
	if beego.AppConfig.String("runmode") == "dev" {
		orm.Debug = true
	}

	o := orm.NewOrm()
	var users []database.SysUser
	_, err := o.QueryTable("SysUser").All(&users)

	if err != nil {
		fmt.Print("error: ", err.Error())
		return
	}

	if len(users) == 0 {
		print("needeeed to make")
	}
}

// InitOpenstackProvider ...
func InitOpenstackProvider(userName string, password string, tenantid string) (*gophercloud.ProviderClient, error) {

	opts := gophercloud.AuthOptions{
		IdentityEndpoint: beego.AppConfig.String("identityEndpoint"),
		Username:         userName,
		Password:         password,
		DomainID:         "default",
		TenantID:         tenantid,
		AllowReauth:      true,
	}
	var err error
	provider, err = openstack.AuthenticatedClient(opts)
	if err != nil {
		fmt.Print(err.Error())
		return nil, err
	}
	SetProvider(provider, userName)
	return provider, err
}

// TableName ...
func TableName(name string) string {
	return beego.AppConfig.String("db.prefix") + name
}

// GetProvider ...
func GetProvider(username string) *gophercloud.ProviderClient {
	for _, data := range OpenStackClient {
		if data[username] != nil {

			return data[username]
		}
	}
	return nil
}

// DeleteProvider ...
func DeleteProvider(username string) int {
	retVal := 0
	var newClient []map[string]*gophercloud.ProviderClient
	for _, data := range OpenStackClient {
		if data[username] == nil {
			newClient = append(newClient, data)
			retVal = 1
		}
	}
	OpenStackClient = newClient
	return retVal
}

// SetProvider ...
func SetProvider(provider *gophercloud.ProviderClient, username string) {
	clientProvider := map[string]*gophercloud.ProviderClient{
		username: provider,
	}
	OpenStackClient = append(OpenStackClient, clientProvider)
}

// GetClientBlockstorage ...
func GetClientBlockstorage(osProvider *gophercloud.ProviderClient) (blockClient *gophercloud.ServiceClient) {
	blockClient, _ = openstack.NewBlockStorageV3(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return
}

// GetClientImage ...
func GetClientImage(osProvider *gophercloud.ProviderClient) (imageClient *gophercloud.ServiceClient) {
	imageClient, _ = openstack.NewImageServiceV2(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return
}

// GetClientIdentity ...
func GetClientIdentity(osProvider *gophercloud.ProviderClient, email string) *gophercloud.ServiceClient {
	identityClient, _ := openstack.NewIdentityV3(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return identityClient
}

// GetClientCompute ...
func GetClientCompute(osProvider *gophercloud.ProviderClient) (computeClient *gophercloud.ServiceClient) {
	computeClient, _ = openstack.NewComputeV2(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return
}

// GetClientNetwork ...
func GetClientNetwork(osProvider *gophercloud.ProviderClient) (networkClient *gophercloud.ServiceClient) {
	networkClient, _ = openstack.NewNetworkV2(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return
}

// GetClientObjectStroge ...
func GetClientObjectStroge(osProvider *gophercloud.ProviderClient) (objClient *gophercloud.ServiceClient) {
	objClient, _ = openstack.NewObjectStorageV1(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return
}

// GetClientService ...
func GetClientService(osProvider *gophercloud.ProviderClient) (objClient *gophercloud.ServiceClient) {
	objClient, _ = openstack.NewOrchestrationV1(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return
}

// GetClientMonasca ...
func GetClientMonasca(username string) (monascaClient monascaclient.Client) {
	userData, err := GetUserByName(username)
	if err != nil {
		return
	}
	authOpt := new(gophercloud.AuthOptions)
	authOpt.Username = userData.Username
	authOpt.Password = userData.OsPwd
	authOpt.DomainName = beego.AppConfig.String("domainname")
	authOpt.TenantID = userData.OsTenantID
	authOpt.IdentityEndpoint = beego.AppConfig.String("identityEndpoint")

	// fmt.Println("authInfo=========+>", userSession.MonAuth)
	monascaClient.SetKeystoneConfig(authOpt)
	return
}

// GetGnocchi ...
func GetGnocchi(osProvider *gophercloud.ProviderClient) (gnocchiClient *gophercloud.ServiceClient) {
	gnocchiClient, _ = gnocchi.NewGnocchiV1(osProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})
	return
}
