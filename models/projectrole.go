package models

import (
	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// CreateProjectRole ...
func CreateProjectRole(projectRole database.UserProjectRole) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(&projectRole)
	projectRole.ID = uint32(id)
	return id, err
}

// ListCreateProjectRole ...
func ListCreateProjectRole() (projectRoles []database.UserProjectRole, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("UserProjectRole").All(&projectRoles)
	return
}

// GetProjectRole ...
func GetProjectRole(projectRoleID string) (projectRoles []database.UserProjectRole, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("UserProjectRole").Filter("os_tenant_id", projectRoleID).All(&projectRoles)
	return
}

// DeleteCreateProjectRole ...
func DeleteCreateProjectRole(projectRoleID uint32) error {
	o := orm.NewOrm()
	_, err := o.QueryTable("UserProjectRole").Filter("id", projectRoleID).Delete()
	return err
}

// GetProjectWithTenantAndUser ...
func GetProjectWithTenantAndUser(osTenantID, osUserID string) (projectRole database.UserProjectRole, err error) {
	o := orm.NewOrm()
	err = o.QueryTable("UserProjectRole").Filter("os_tenant_id", osTenantID).Filter("os_user_id", osUserID).One(&projectRole)
	return projectRole, err
}

// DeleteCreateProjectRoleTenant ...
func DeleteCreateProjectRoleTenant(o orm.Ormer, osTenantID, osUserID string) error {
	_, err := o.QueryTable("UserProjectRole").Filter("os_tenant_id", osTenantID).Filter("os_user_id", osUserID).Delete()
	return err
}

// UpdateProjectRole ...
func UpdateProjectRole(projectID, osUserID string, roleID uint32) error {
	o := orm.NewOrm()
	_, err := o.QueryTable("UserProjectRole").Filter("os_tenant_id", projectID).Filter("os_user_id", osUserID).Update(orm.Params{
		"role_id": roleID,
	})
	return err
}

// UserQuota ...
func UserQuota(projectID, osUserID string) (quota database.SysUserQuota, err error) {
	o := orm.NewOrm()
	var projectRole database.UserProjectRole
	errProject := o.QueryTable("UserProjectRole").Filter("os_tenant_id", projectID).Filter("os_user_id", osUserID).One(&projectRole)
	if errProject != nil {
		return
	}

	err = o.QueryTable("SysUserQuota").Filter("id", projectRole.QuotaID).One(&quota)
	return
}
