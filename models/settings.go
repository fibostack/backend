package models

import (
	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
)

// ChangeHostNames ...
func ChangeHostNames(data string) error {
	o := orm.NewOrm()
	if o.QueryTable("sys_settings").Filter("key", "hostnames").Exist() {
		if _, err := o.QueryTable("sys_settings").Filter("key", "hostnames").Update(orm.Params{"value": data}); err != nil {
			return err
		}
	} else {
		if _, err := o.Insert(&database.SysSettings{Key: "hostnames", Value: data}); err != nil {
			return err
		}
	}
	return nil
}

// GetHostNames ...
func GetHostNames() (database.SysSettings, error) {
	var settings database.SysSettings
	err := orm.NewOrm().QueryTable("sys_settings").Filter("key", "hostnames").One(&settings)
	if err != nil {
		if err == orm.ErrNoRows {
			return settings, nil
		}
		return settings, err
	}
	return settings, nil
}
