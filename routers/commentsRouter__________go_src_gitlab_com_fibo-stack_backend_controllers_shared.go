package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:InitAllController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:InitAllController"],
        beego.ControllerComments{
            Method: "InitAll",
            Router: "/initall",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:InitAllController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:InitAllController"],
        beego.ControllerComments{
            Method: "New",
            Router: "/new",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"],
        beego.ControllerComments{
            Method: "InitPermission",
            Router: "/init",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:PermissionsController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:ProjectRoleController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:ProjectRoleController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:ProjectRoleController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:ProjectRoleController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:ProjectRoleController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:ProjectRoleController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"],
        beego.ControllerComments{
            Method: "InitQuota",
            Router: "/init",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:QuotasContoller"],
        beego.ControllerComments{
            Method: "Update",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"],
        beego.ControllerComments{
            Method: "InitRole",
            Router: "/init",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"],
        beego.ControllerComments{
            Method: "Update",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/shared:RolesController"],
        beego.ControllerComments{
            Method: "UpdatePermission",
            Router: "/update/permission",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
