package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "AddHost",
            Router: "/addHost",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "CreateAggregate",
            Router: "/createAggregate",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "CreateOrUpdateMetadata",
            Router: "/createOrUpdateMetadata",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "DeleteAggregate",
            Router: "/deleteAggregate",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "ListAggregates",
            Router: "/listAggregate",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "RemoveAggregate",
            Router: "/removeAggregate",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "ShowAggregate",
            Router: "/showAggregate",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AggregateController"],
        beego.ControllerComments{
            Method: "UpdateAggregate",
            Router: "/updateAggregate",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"],
        beego.ControllerComments{
            Method: "ForgotPassword",
            Router: "/forgotPassword",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"],
        beego.ControllerComments{
            Method: "InitProject",
            Router: "/initProject",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"],
        beego.ControllerComments{
            Method: "InitAdminUser",
            Router: "/initUser",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"],
        beego.ControllerComments{
            Method: "Login",
            Router: "/login",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"],
        beego.ControllerComments{
            Method: "Logout",
            Router: "/logout",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:AuthController"],
        beego.ControllerComments{
            Method: "ResetPassword",
            Router: "/resetPassword",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "GrantAccess",
            Router: "/grantAccess",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "ListFlavorAccess",
            Router: "/listFlavorAccess",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "ListFlavorExtra",
            Router: "/listFlavorExtra",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:FlavorController"],
        beego.ControllerComments{
            Method: "RemoveAccess",
            Router: "/removeAccess",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"],
        beego.ControllerComments{
            Method: "Show",
            Router: "/show",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"],
        beego.ControllerComments{
            Method: "ShowUptime",
            Router: "/showUptime",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"],
        beego.ControllerComments{
            Method: "Statistics",
            Router: "/statistics",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:HypervisorController"],
        beego.ControllerComments{
            Method: "TenantUsage",
            Router: "/tenantUsage",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"],
        beego.ControllerComments{
            Method: "CreateServerFromImage",
            Router: "/createServerFromImage",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"],
        beego.ControllerComments{
            Method: "DeleteImage",
            Router: "/deleteImage",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"],
        beego.ControllerComments{
            Method: "GetImage",
            Router: "/getImage",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageController"],
        beego.ControllerComments{
            Method: "ImageList",
            Router: "/listImage",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "AddMemberToImage",
            Router: "/addMemberToImage",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "CreateImage",
            Router: "/createImage",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "CreateImageImport",
            Router: "/createImageImport",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "CreateTask",
            Router: "/createTask",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "DeleteImage",
            Router: "/deleteImage",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "DeleteMemberFromImage",
            Router: "/deleteMemberFromImage",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "DownloadImageData",
            Router: "/downloadImageData",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "GetInfoAboutImportAPI",
            Router: "/downloadImageData",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "GetTask",
            Router: "/getTask",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "ListDiskFormat",
            Router: "/listImageData",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "ListImages",
            Router: "/listImages",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "ListMembers",
            Router: "/listMembers",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "ListTasks",
            Router: "/listTasks",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "StageImageData",
            Router: "/stageImageData",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "UpdateImage",
            Router: "/updateImage",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "UpdateStatusOfMember",
            Router: "/updateStatusOfMember",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ImageServiceController"],
        beego.ControllerComments{
            Method: "UploadImageData",
            Router: "/uploadImageData",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Action",
            Router: "/action",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "AddSecGroup",
            Router: "/addSecGroup",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Clone",
            Router: "/clone",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "ConfirmResize",
            Router: "/confirmResize",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Console",
            Router: "/console",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "CreateSnapshot",
            Router: "/createSnapshot",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "ListAZ",
            Router: "/listAZ",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "OffWAF",
            Router: "/off_waf",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "OnWAF",
            Router: "/on_waf",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Reboot",
            Router: "/reboot",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Resize",
            Router: "/resize",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Resume",
            Router: "/resume",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "RevertResize",
            Router: "/revertResize",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "ShowConsole",
            Router: "/showConsole",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Start",
            Router: "/start",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Stop",
            Router: "/stop",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InstanceController"],
        beego.ControllerComments{
            Method: "Suspend",
            Router: "/suspend",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"],
        beego.ControllerComments{
            Method: "CreateServerInterface",
            Router: "/createServerInterface",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"],
        beego.ControllerComments{
            Method: "DeleteServerInterface",
            Router: "/deleteServerInterface",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"],
        beego.ControllerComments{
            Method: "GetServerInterface",
            Router: "/getServerInterface",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:InterfaceController"],
        beego.ControllerComments{
            Method: "ListServerInterface",
            Router: "/listServerInterface",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"],
        beego.ControllerComments{
            Method: "Import",
            Router: "/import",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:KeyPairController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "CreateLoadBlancer",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "DeleteLoadBalancer",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "GetLoadBalancer",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "GetLoadBalancerStatus",
            Router: "/getStatus",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "CreateListener",
            Router: "/listener/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "DeleteListener",
            Router: "/listener/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "GetListener",
            Router: "/listener/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "ListListener",
            Router: "/listener/list",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "UpdateListener",
            Router: "/listener/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "CreateMonitor",
            Router: "/monitor/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "DeleteMonitor",
            Router: "/monitor/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "GetMonitor",
            Router: "/monitor/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "ListMonitors",
            Router: "/monitor/list",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "UpdateMonitor",
            Router: "/monitor/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "CreatePool",
            Router: "/pool/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "DeletePool",
            Router: "/pool/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "ListPools",
            Router: "/pool/list",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "UpdatePool",
            Router: "/pool/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "CreatePoolMember",
            Router: "/pool_member/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "DeletePoolMember",
            Router: "/pool_member/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "GetPoolMember",
            Router: "/pool_member/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "ListplMembers",
            Router: "/pool_member/list",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "UpdatePoolMember",
            Router: "/pool_member/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:LoadBalancerController"],
        beego.ControllerComments{
            Method: "UpdateLoadBalancer",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "GetInstanceNetworkPacketsList",
            Router: "/GetInstanceNetworkPacketsList",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "GetInstanceCPUUsageList",
            Router: "/getInstanceCpuUsageList",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "GetInstanceDiskReadList",
            Router: "/getInstanceDiskReadList",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "GetInstanceDiskWriteList",
            Router: "/getInstanceDiskWriteList",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "GetInstanceMemoryUsageList",
            Router: "/getInstanceMemoryUsageList",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "GetInstanceNetworkIoList",
            Router: "/getInstanceNetworkIoList",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "GetProjectInstanceList",
            Router: "/getProjectInstanceList",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:MonascaController"],
        beego.ControllerComments{
            Method: "ProjectSummary",
            Router: "/projectSummary",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "GetNetWorkFloatingIps",
            Router: "/GetNetWorkFloatingIps",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "AssociateFIP",
            Router: "/associateFIP",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "CreateNetwork",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "CreateFloatingIP",
            Router: "/createFloatingIP",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "DeleteNetwork",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "DeleteFLoatingIP",
            Router: "/deleteFloatingIP",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "DisassociateFIP",
            Router: "/disassociatedFIP",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "GetNetwork",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "GetPort",
            Router: "/getPort",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "ListNetworks",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "ListFloatingIPs",
            Router: "/listFloatingIPs",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "ListSubnetFromPrivate",
            Router: "/listSubnetFromPrivate",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "CreatePort",
            Router: "/port/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "DeletePorts",
            Router: "/port/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "ListPorts",
            Router: "/port/list",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "UpdatePort",
            Router: "/port/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "AddInterfacePortToRouter",
            Router: "/router/addInterfacePort",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "AddInterfaceSubnetToRouter",
            Router: "/router/addInterfaceSubnet",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "AddRoutesToRouter",
            Router: "/router/addRoute",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "CreateRouter",
            Router: "/router/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "DeleteRouter",
            Router: "/router/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "GetRouter",
            Router: "/router/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "InterfaceList",
            Router: "/router/interface/list",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "ListRouters",
            Router: "/router/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "RemoveAllRoutesFromRouter",
            Router: "/router/removeAllRoutes",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "RemoveInterfacePortFromRouter",
            Router: "/router/removeInterfacePort",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "RemoveInterfaceSubnetFromRouter",
            Router: "/router/removeInterfaceSubnet",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "RemoveRoutesFromRouter",
            Router: "/router/removeRoute",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "UpdateRouter",
            Router: "/router/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "CreateSubnet",
            Router: "/subnet/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "DeleteSubnet",
            Router: "/subnet/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "GetSubnet",
            Router: "/subnet/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "ListSubnets",
            Router: "/subnet/list",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "RemoveGatewayFromSubnet",
            Router: "/subnet/removeGateway",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "UpdateSubnet",
            Router: "/subnet/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NetworkController"],
        beego.ControllerComments{
            Method: "UpdateNetwork",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NotificationController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NotificationController"],
        beego.ControllerComments{
            Method: "ChangeNotificationStatus",
            Router: "/changeNotificationStatus",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NotificationController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NotificationController"],
        beego.ControllerComments{
            Method: "GetNotification",
            Router: "/getNotification",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NotificationController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:NotificationController"],
        beego.ControllerComments{
            Method: "ListNotification",
            Router: "/listNotification",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "ChangePasswordUser",
            Router: "/changePasswordUser",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "GetPersonalInfo",
            Router: "/getPersonalInfo",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "UserProjects",
            Router: "/projects",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "UserRoleList",
            Router: "/rolelist",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "SendCodeMobile",
            Router: "/sendCodeMobile",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "UpdateUser",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "UpdatePassword",
            Router: "/updatePassword",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "UpdateUserFromDatabase",
            Router: "/updateUserFromDatabase",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:OsUsersController"],
        beego.ControllerComments{
            Method: "VerifyMobile",
            Router: "/verifyMobile",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "ManageMembersCreate",
            Router: "/member/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "ManageMembersDelete",
            Router: "/member/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "ManageMembersUpdate",
            Router: "/member/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "Update",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ProjectsController"],
        beego.ControllerComments{
            Method: "ProjectUserQuota",
            Router: "/user/quota",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:RolesController"],
        beego.ControllerComments{
            Method: "Update",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ScriptController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ScriptController"],
        beego.ControllerComments{
            Method: "CreateScript",
            Router: "/createScript",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ScriptController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:ScriptController"],
        beego.ControllerComments{
            Method: "ListScript",
            Router: "/listScript",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "AddSecurityGroupto",
            Router: "/addSecGrouptoServer",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "ListSecurityGroupServer",
            Router: "/listSecurityGroupServer",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "Remove",
            Router: "/remove",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "RuleCreate",
            Router: "/rule/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "RuleDelete",
            Router: "/rule/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SecurityGroupController"],
        beego.ControllerComments{
            Method: "Update",
            Router: "/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SnapshotController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SnapshotController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SnapshotController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SnapshotController"],
        beego.ControllerComments{
            Method: "Launch",
            Router: "/launch",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SnapshotController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SnapshotController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ActionLog",
            Router: "/actionLog",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ListBlockQuotas",
            Router: "/blockstorage/quotas",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ListBlockstorage",
            Router: "/blockstorage/service",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ListComputeQuotas",
            Router: "/compute/quotas",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ListCompute",
            Router: "/compute/service",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ListAgents",
            Router: "/network/agent",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ProjectLimit",
            Router: "/projectLimit",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:SystemController"],
        beego.ControllerComments{
            Method: "ListServices",
            Router: "/services",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "GetUserDetail",
            Router: "/GetUserDetail",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "ChangePasswordUser",
            Router: "/changePasswordUser",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "DeleteUser",
            Router: "/deleteUser",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "DeleteUserFromDatabase",
            Router: "/deleteUserFromDatabase",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "EnableUser",
            Router: "/enableUser",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "GetPersonalInfo",
            Router: "/getPersonalInfo",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "SendCodeMobile",
            Router: "/sendCodeMobile",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "UpdatePassword",
            Router: "/updatePassword",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "UpdateUserFromDatabase",
            Router: "/updateUserFromDatabase",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:UsersController"],
        beego.ControllerComments{
            Method: "VerifyMobile",
            Router: "/verifyMobile",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "Attach",
            Router: "/attach",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "Create",
            Router: "/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "CreateSnapshotFromVolume",
            Router: "/createSnapshot",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "Detach",
            Router: "/detach",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "Extend",
            Router: "/extend",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "CreateVolumeType",
            Router: "/volume_type/create",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "DeleteVolumeType",
            Router: "/volume_type/delete",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "GetVolumeType",
            Router: "/volume_type/get",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "ListVolumeType",
            Router: "/volume_type/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"] = append(beego.GlobalControllerRouter["gitlab.com/fibo-stack/backend/controllers/ostack:VolumeController"],
        beego.ControllerComments{
            Method: "UpdateVolumeType",
            Router: "/volume_type/update",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
