// Package routers ...
// @APIVersion 1.0.0
// @Title FIBO STACK
// @Description API SERVICE
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"github.com/astaxie/beego"
	"gitlab.com/fibo-stack/backend/controllers/ostack"
	"gitlab.com/fibo-stack/backend/controllers/shared"
)

func init() {

	beego.Router("/", &ostack.MainController{}, "*:Get")
	routes := beego.NewNamespace("/api/v1",
		beego.NSNamespace("/auth",
			beego.NSInclude(
				&ostack.AuthController{},
			),
		),
		beego.NSNamespace("/initall",
			beego.NSInclude(
				&shared.InitAllController{},
			),
		),
		beego.NSNamespace("/permissions",
			beego.NSInclude(
				&shared.PermissionsController{},
			),
		),
		beego.NSNamespace("/roles",
			beego.NSInclude(
				&shared.RolesController{},
			),
		),
		beego.NSNamespace("/quotas",
			beego.NSInclude(
				&shared.QuotasContoller{},
			),
		),
		beego.NSNamespace("/instance",
			beego.NSInclude(
				&ostack.InstanceController{},
			),
		),
		beego.NSNamespace("/volume",
			beego.NSInclude(
				&ostack.VolumeController{},
			),
		),
		beego.NSNamespace("/snapshot",
			beego.NSInclude(
				&ostack.SnapshotController{},
			),
		),
		beego.NSNamespace("/keypair",
			beego.NSInclude(
				&ostack.KeyPairController{},
			),
		),
		beego.NSNamespace("/interface",
			beego.NSInclude(
				&ostack.InterfaceController{},
			),
		),
		beego.NSNamespace("/hypervisor",
			beego.NSInclude(
				&ostack.HypervisorController{},
			),
		),
		beego.NSNamespace("/projects",
			beego.NSInclude(
				&ostack.ProjectsController{},
			),
		),
		beego.NSNamespace("/image",
			beego.NSInclude(
				&ostack.ImageController{},
			),
		),
		beego.NSNamespace("/security_group",
			beego.NSInclude(
				&ostack.SecurityGroupController{},
			),
		),
		beego.NSNamespace("/flavor",
			beego.NSInclude(
				&ostack.FlavorController{},
			),
		),
		beego.NSNamespace("/imageService",
			beego.NSInclude(
				&ostack.ImageServiceController{},
			),
		),
		beego.NSNamespace("/script",
			beego.NSInclude(
				&ostack.ScriptController{},
			),
		),
		beego.NSNamespace("/system",
			beego.NSInclude(
				&ostack.SystemController{},
			),
		),
		beego.NSNamespace("/network",
			beego.NSInclude(
				&ostack.NetworkController{},
			),
		),
		beego.NSNamespace("/aggregate",
			beego.NSInclude(
				&ostack.AggregateController{},
			),
		),
		beego.NSNamespace("/test",
			beego.NSInclude(
				&ostack.TestController{},
			),
		),
		beego.NSNamespace("/integration",
			beego.NSInclude(
				&ostack.IntegrationController{},
			),
		),

		beego.NSNamespace("/notification",
			beego.NSInclude(
				&ostack.NotificationController{},
			),
		),
		beego.NSNamespace("/monitor-monasca",
			beego.NSInclude(
				&ostack.MonascaController{},
			),
		),
		beego.NSNamespace("/public",
			beego.NSInclude(
				&ostack.PublicController{},
			),
		),
		beego.NSNamespace("/user",
			beego.NSInclude(
				&ostack.OsUsersController{},
			),
		),
	)
	beego.AddNamespace(routes)

}
