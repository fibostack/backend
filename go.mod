module gitlab.com/fibo-stack/backend

go 1.15

require (
	github.com/astaxie/beego v1.12.2
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gophercloud/gophercloud v0.13.0
	github.com/gophercloud/utils v0.0.0-20201016024308-5fc12d2a573d
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/influxdata/influxdb1-client v0.0.0-20200827194710-b269163b24ab
	github.com/jinzhu/gorm v1.9.16
	github.com/monasca/golang-monascaclient v0.0.0-20170719154857-bd0d6bd06da0
	github.com/sacOO7/go-logger v0.0.0-20180719173527-9ac9add5a50d // indirect
	github.com/sacOO7/gowebsocket v0.0.0-20180719182212-1436bb906a4e
	github.com/stretchr/testify v1.5.1 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/olivere/elastic.v3 v3.0.75
)
