# initialize fibo database
mysql -uroot -pfibo123 -e "CREATE DATABASE IF NOT EXISTS fibo_stack CHARACTER SET utf8;"

cd /opt/stack/devstack

# admin user connection
. openrc admin admin

TENANT_ID=$(openstack project list --user admin -c ID -f value | head -n 1)
PUBLIC_NETWORK_ID=$(openstack network list --name public -c ID -f value)
MEMBER_ROLE_ID=$(openstack role list -f value | grep member | cut -d ' ' -f 1)

cat > ./app.conf <<EOL
appname = fibo
httpport = 8081
runmode = prod

# database
db.host = 127.0.0.1
db.user = root
db.password = "fibo123"
db.port = 3306
db.name = "fibo_stack"
db.prefix = fc_
#openstack
identityEndpoint = "http://127.0.0.1:8888/identity"
username = "admin"
password = "fibo123"
domainname = "default"
tenantName = "admin"
tenantID = "$TENANT_ID"
public.network.name = "public"
public.network.id = "$PUBLIC_NETWORK_ID"
member.role.id = "$MEMBER_ROLE_ID"

#Openstack Nova
nova.target.url=http://127.0.0.1:8888/compute/v2.1
nova.target.version=v2.1

#Openstack Keystone
keystone.target.url=http://127.0.0.1:8888/identity
keystone.target.version=v3

#Openstack Neutron
neutron.target.url=http://127.0.0.1:9696
neutron.target.version=v2.0

#Openstack Cinder
cinder.target.url=http://127.0.0.1:8888
cinder.target.version=v2
#Openstack Glance
glance.target.url=http://127.0.0.1:8888/image
glance.target.version=v2

#Time difference(hour)
gmt.time.gap=0

#Log file destination
log.openstak.error = "/logs/openstackerror/"

# autorender = false
copyrequestbody = true
EnableDocs = true
EOL

mv app.conf /opt/stack/fibostack/backend/conf/app.conf
cd /opt/stack/fibostack/backend/
sudo apt-get update
sudo apt-get install -y git gcc g++ libc-dev 

sh ./shells/goinstall.sh
