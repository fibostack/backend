# Stage 0: Build base reactjs
FROM library/golang:1.14-alpine AS go_build
MAINTAINER infinityhazard@gmail.com

# Linux packages required for build
RUN apk add bash ca-certificates git gcc g++ libc-dev
RUN apk add --update tzdata
ENV TZ=Asia/Ulaanbaatar

WORKDIR /home/
COPY . .

# Enable go module
ENV GO111MODULE=on

# Install dependencies
# RUN go mod download
RUN go get -d -v

# Stage 2: Build beego application
FROM go_build AS beego_build
# Copy whole source tree
COPY . .
# Build beego project
# RUN go install
RUN go build
#CMD ["backuser"]

# Stage 3: Build fresh container image without golang
FROM alpine:3.7
RUN apk add --no-cache tzdata ca-certificates 
WORKDIR /home/

ENV TZ Asia/Ulaanbaatar

COPY --from=beego_build /home/ /home/
RUN ls /home/
# COPY --from=beego_build /go/src/gitlab.com/ndc-stack/backend/back-user /go/src/gitlab.com/ndc-stack/backend/
# COPY --from=beego_build /go/src/gitlab.com/ndc-stack/backend/log_config.xml /go/src/gitlab.com/ndc-stack/backend/

# ENV MN=Asia/Ulaanbaatar
# RUN ln -snf /usr/share/zoneinfo/$MN /etc/localtime && echo $MN > /etc/timezone

EXPOSE 8081
ENTRYPOINT ["/home/backend"]