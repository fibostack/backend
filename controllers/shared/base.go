package shared

import (
	"sync"

	"github.com/astaxie/beego"
)

// BaseController operations for Auth
type BaseController struct {
	beego.Controller
	sync.RWMutex
}
