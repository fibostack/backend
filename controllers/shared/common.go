package shared

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"reflect"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/integration/networking"
	"gitlab.com/fibo-stack/backend/utils"

	"github.com/dgrijalva/jwt-go"
)

// CommonController ...
type CommonController struct {
	BaseController
}

// Claims ...
//JW token custom claims
type Claims struct {
	SysUserID uint32 `json:"sysUserID"`
	Username  string `json:"username"`
	UserID    string `json:"userID"`
	TenantID  string `json:"tenantID"`
	UserRole  string `json:"userRole"`
	Email     string `json:"email"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Test      bool   `json:"test"`
	jwt.StandardClaims
}

// ChildKeyValue ...
type ChildKeyValue struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

//jwt secret key
var jwtKey = []byte("zzkAdVzUv2cMWxExdH9cHWecGgYe39pdsJEE6eXhVqMThFVGJAb5CB83SaGtspJezrVxxEpc7JLLDfsyZhDCCux6JqfLveuPaBZD3YtPKPWksbFrXCnDjaCVFhdhef8P")

//letter key
var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// Push notification app id
var appid = "630e2aa8-947b-4615-9f33-ce487eb95d9c"

// Event ...
type Event struct {
	EventType string `json:"eventType"`
	Data      struct {
		InvoiceNumber string `json:"invoiceNumber"`
		Description   string `json:"description"`
		Status        int    `json:"status"`
		Amount        int    `json:"amount"`
		TrackingData  string `json:"trackingData"`
		CreatedAt     string `json:"createdAt"`
		ExpireDate    string `json:"expireDate"`
		PaidDate      string `json:"paidDate"`
	} `json:"data"`
	Signature string `json:"signature,omitempty"`
}

// CheckToken ...
func (c *BaseController) CheckToken() (jwt.MapClaims, error) {
	reqToken := c.Ctx.Request.Header.Get("Authorization")
	retClaim := jwt.MapClaims{}
	JwtToken, err := jwt.ParseWithClaims(reqToken, retClaim, func(t *jwt.Token) (interface{}, error) {
		return []byte(jwtKey), nil
	})

	if err == nil {
		if !JwtToken.Valid {
			c.Ctx.ResponseWriter.WriteHeader(http.StatusUnauthorized)
			return nil, nil
		}
	} else {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusUnauthorized)
		return nil, err
	}
	return retClaim, err
}

// GetHeaderValue ...
func (c *BaseController) GetHeaderValue(name string) string {
	projectID := c.Ctx.Request.Header.Get(name)
	if projectID == "" {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusUnauthorized)
		return ""
	}

	return projectID
}

// VerifySignature ...
func VerifySignature(pub *rsa.PublicKey, data []byte) (*Event, bool) {
	parsedData := new(Event)
	if err := json.Unmarshal(data, &parsedData); err != nil {
		return nil, false
	}

	signatureBytes, err := base64.StdEncoding.DecodeString(parsedData.Signature)
	if err != nil {
		return nil, false
	}

	parsedData.Signature = ""
	toVerify, err := json.Marshal(parsedData)
	if err != nil {
		return nil, false
	}

	hash := sha256.Sum256(toVerify)
	return parsedData, rsa.VerifyPKCS1v15(pub, crypto.SHA256, hash[:], signatureBytes) == nil
}

// ErrorWriter ...
//Get error message from error code and return map
func ErrorWriter(code int) map[string]interface{} {
	retJSON := make(map[string]interface{})
	retJSON["Code"] = code
	retJSON["Message"] = utils.StatusText(code)
	return retJSON
}

// randSeq ...
func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// CalculateNetworkByte ...
func CalculateNetworkByte(networkByte map[int](map[string]float64)) float64 {
	var amount float64
	amount = 0

	//loop except cpu_util measure
	for i := 0; i <= len(networkByte); i++ {
		for _, v := range networkByte[i] {
			amount += v
		}
	}

	return amount
}

// CalculateMeasureFromMap ...
//vcpus, memory to inInterface, cpu_util to outInterface
func CalculateMeasureFromMap(inInterface, activeTimeInterface map[int](map[string]float64)) float64 {
	var prevTime time.Time
	var prevVal float64
	var amount float64

	amount = 0

	//loop except cpu_util measure
	for i := 0; i <= len(inInterface); i++ {
		if prevVal != 0 && i < len(inInterface) {
			for k, v := range inInterface[i] {
				t, _ := time.Parse(time.RFC3339, k)
				//loop cpu_util
				for l := 0; l < len(activeTimeInterface); l++ {
					for k := range activeTimeInterface[l] {
						t1, _ := time.Parse(time.RFC3339, k)
						//count Between 2 date from memory or vcpus, disk root size
						if t1.Sub(prevTime).Minutes() >= 0 && t.Sub(t1).Minutes() > 0 {
							amount += prevVal
						}
					}
				}
				prevTime = t
				prevVal = v
			}
		} else if i == len(inInterface) {
			lastTime := time.Date(prevTime.Year(), prevTime.Month(), prevTime.Day(), 23, 59, 59, 59, time.UTC)
			for l := 0; l < len(activeTimeInterface); l++ {
				for k := range activeTimeInterface[l] {
					t1, _ := time.Parse(time.RFC3339, k)
					if t1.Sub(prevTime).Minutes() >= 0 && lastTime.Sub(t1).Minutes() > 0 {
						amount += prevVal
					}
				}
			}
		} else {
			for k, v := range inInterface[i] {
				prevTime, _ = time.Parse(time.RFC3339, k)
				prevVal = v
			}
		}
	}

	return amount
}

const uuidLen = 12

// GenerateUUID ...
func GenerateUUID() (string, error) {
	buf, err := GenerateRandomBytes(uuidLen)
	if err != nil {
		return "", err
	}
	return FormatUUID(buf)
}

// GenerateRandomBytes is used to generate random bytes of given size.
func GenerateRandomBytes(size int) ([]byte, error) {
	buf := make([]byte, size)
	if _, err := rand.Read(buf); err != nil {
		return nil, fmt.Errorf("failed to read random bytes: %v", err)
	}
	return buf, nil
}

// FormatUUID ...
func FormatUUID(buf []byte) (string, error) {
	if buflen := len(buf); buflen != uuidLen {
		return "", fmt.Errorf("wrong length byte slice (%d)", buflen)
	}

	return fmt.Sprintf("%x%x%x%x%x",
		buf[0:2],
		buf[2:4],
		buf[4:6],
		buf[6:8],
		buf[8:12]), nil
}

// ParseUUID ...
func ParseUUID(uuid string) ([]byte, error) {
	if len(uuid) != 2*uuidLen+4 {
		return nil, fmt.Errorf("uuid string is wrong length")
	}

	if uuid[3] != '-' ||
		uuid[7] != '-' ||
		uuid[10] != '-' ||
		uuid[12] != '-' {
		return nil, fmt.Errorf("uuid is improperly formatted")
	}

	hexStr := uuid[0:8] + uuid[9:13] + uuid[14:18] + uuid[19:23] + uuid[24:36]

	ret, err := hex.DecodeString(hexStr)
	if err != nil {
		return nil, err
	}
	if len(ret) != uuidLen {
		return nil, fmt.Errorf("decoded hex is the wrong length")
	}

	return ret, nil
}

// InterfaceToMap ...
//interface{} to map[] with filter only granularity=60
func InterfaceToMap(interf []interface{}) (map[int](map[string]float64), error) {
	retAll := make(map[int](map[string]float64))

	if interf != nil {
		k := 0

		for _, t := range interf {
			s := reflect.ValueOf(t)

			if s.Kind() != reflect.Slice {
				return nil, errors.New("Interface is empty")
			}

			ret := make(map[int]interface{})

			for i := 0; i < s.Len(); i++ {
				ret[i] = s.Index(i).Interface()
			}

			if ret[1].(float64) == 60 {
				tmp := make(map[string]float64)
				tmp[ret[0].(string)] = ret[2].(float64)
				retAll[k] = tmp
				k++
			}

		}

		return retAll, nil
	}

	return nil, errors.New("Array of interface is empty")
}

// InterfaceToMap2 ...
func InterfaceToMap2(interf interface{}) map[int]interface{} {
	s := reflect.ValueOf(interf)
	if s.Kind() != reflect.Slice {
		return nil
	}
	ret := make(map[int]interface{})
	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}
	return ret
}

// PushNotification ...
func PushNotification(userid, title, content string) bool {
	jsons := make(map[string]interface{})
	url := "https://onesignal.com/api/v1/notifications"

	payload := strings.NewReader("{\n    \"app_id\": \"" + appid + "\",\n    \"include_external_user_ids\": [\n        \"" + userid + "\"\n    ],\n    \"headings\": {\n        \"en\": \"" + title + "\"\n    },\n    \"contents\": {\n        \"en\": \"" + content + "\"\n    }\n}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)

	fmt.Println(err)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	json.Unmarshal(body, &jsons)

	return true
}

// LogWriteOpenstackError ...
func LogWriteOpenstackError(stackID string, err error) bool {
	if err != nil {
		dir := beego.AppConfig.String("log.openstack.error")
		t := time.Now()
		tod := t.Format("2000-01-01")
		dir = dir + stackID + "/" + tod + ".txt"
		fmt.Println(dir)
		utils.CreateFile(dir)
		utils.WriteFile(err, dir)
		return true
	}
	return false
}

// CatchTraceError ...
func CatchTraceError() (err error) {
	if r := recover(); r != nil {
		err = r.(error)
	}
	return
}

// RetrieveDataFromBody ...
func RetrieveDataFromBody(body io.ReadCloser) (result map[string]interface{}) {
	var bodyBytes []byte
	if body != nil {
		bodyBytes, _ = ioutil.ReadAll(body)
	}
	json.Unmarshal(bodyBytes, &result)
	return result
}

// RetrieveDataFromHeader ...
func RetrieveDataFromHeader(requesthead http.Header) map[string]string {
	result := make(map[string]string)

	for name, headers := range requesthead {
		fmt.Println(name, headers)
		name = strings.ToLower(name)
		for _, h := range headers {
			fmt.Println("h", h)
			result[name] = h
		}
	}

	fmt.Println("result", result)
	return result
}

// CheckBodyResult check request body variables
func CheckBodyResult(bodyResult map[string]interface{}, names []string) (statusCode int, ErrMsg string) {
	for _, n := range names {
		if bodyResult[n] == nil {
			statusCode = 100
			ErrMsg = n + ","
		}
	}

	if len(ErrMsg) != 0 {
		statusCode = 100
		ErrMsg = ErrMsg[:len(ErrMsg)-1]
		ErrMsg += " [value was missed]"
		return
	}
	statusCode = 0
	return
}

// GenerateJWTString ...
func GenerateJWTString(user database.SysUser) string {
	expirationTime := time.Now().Add(60 * time.Minute)

	claims := &Claims{
		SysUserID: user.Base.ID,
		Username:  user.Username,
		UserID:    user.OsUserID,
		TenantID:  user.OsTenantID,
		UserRole:  user.Role,
		Email:     user.Email,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, _ := token.SignedString(jwtKey)
	return tokenString
}

// ExtractJWTString ...
func ExtractJWTString(tokenString string) jwt.MapClaims {
	retClaim := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, retClaim, func(t *jwt.Token) (interface{}, error) {
		return []byte(jwtKey), nil
	})
	if err != nil {
		return nil
	}
	return retClaim
}

// EmailForgotPassword ...
func EmailForgotPassword(firstname, lastname, email, token string) bool {
	url := "Your endpoint for user password forget email"

	payloadObject := make(map[string]interface{})
	payloadObject["firstname"] = firstname
	payloadObject["lastname"] = lastname
	payloadObject["email"] = email
	payloadObject["token"] = token
	payloadString, _ := json.Marshal(payloadObject)
	payload := strings.NewReader(string(payloadString))
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	fmt.Println(err)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Println(body)
	return true
}

// AuthOSAdmin ...
func AuthOSAdmin() (provider *gophercloud.ProviderClient, err error) {
	opts := gophercloud.AuthOptions{
		IdentityEndpoint: beego.AppConfig.String("identityEndpoint"),
		Username:         beego.AppConfig.String("username"),
		Password:         beego.AppConfig.String("password"),
		TenantID:         beego.AppConfig.String("tenantID"),
		DomainID:         "default",
	}
	//authenticate to openstack
	provider, err = openstack.AuthenticatedClient(opts)
	return
}

// BasicAuthHeader ...
func BasicAuthHeader(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

// RandomNumber ...
func RandomNumber(min int, max int) int {
	return rand.Intn(max-min) + min
}

// GeneratePassword ...
func GeneratePassword() string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	bbb := make([]rune, 10)
	for i := range bbb {
		bbb[i] = letters[rand.Intn(len(letters))]
	}

	return string(bbb)
}

// CreateLog struct
func (c *BaseController) CreateLog(cloudProvider, service, resourceName, resourceID, eventName string, errorMsg error) int64 {
	o := orm.NewOrm()
	osTenantID := c.GetHeaderValue("OS-Tenant-ID")
	sourcePublicIP := c.GetHeaderValue("SourceIP")

	jwtToken := c.GetHeaderValue("Authorization")

	retClaim := jwt.MapClaims{}
	jwt.ParseWithClaims(jwtToken, retClaim, func(t *jwt.Token) (interface{}, error) {
		return []byte(jwtKey), nil
	})

	requestBody := string(c.Ctx.Input.RequestBody)
	requestHeader := RetrieveDataFromHeader(c.Ctx.Request.Header)
	jsonHeader, _ := json.Marshal(requestHeader)

	var logObj database.LogAction

	logObj = database.LogAction{
		OsUserID:      retClaim["userID"].(string),
		OsTenantID:    osTenantID,
		CloudProvider: cloudProvider,
		Service:       service,
		ResourceName:  resourceName,
		ResourceID:    resourceID,
		EventName:     eventName,
		EventDate:     time.Now(),
		SourceIP:      sourcePublicIP,
		RequestBody:   requestBody,
		RequestHeader: string(jsonHeader),
	}

	if errorMsg != nil {
		logObj.ErrorMsg = errorMsg.Error()
	}

	id, err := o.Insert(&logObj)
	fmt.Println(err)
	return id
}

// FindSubnetFromIPAddress ...
func FindSubnetFromIPAddress(ports []networking.PortWithBinding, IPAddress string) string {
	for _, port := range ports {
		for _, fixedIPs := range port.FixedIPs {
			if IPAddress == fixedIPs.IPAddress {
				return fixedIPs.SubnetID
			}
		}
	}
	return ""
}
