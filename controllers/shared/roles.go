package shared

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/utils"

	"gitlab.com/fibo-stack/backend/models"
)

// RolesStruct ...
type RolesStruct []struct {
	ID            float64       `json:"id"`
	Name          string        `json:"name"`
	PermissionIds []interface{} `json:"permission_ids"`
}

// RolesController operations for Auth
type RolesController struct {
	BaseController
}

// URLMapping ...
func (t *RolesController) URLMapping() {
	t.Mapping("Init", t.InitRole)
	t.Mapping("Create", t.Create)
	t.Mapping("Get", t.Get)
	t.Mapping("List", t.List)
	t.Mapping("Delete", t.Delete)
	t.Mapping("Update", t.Update)
}

// InitRole ...
// @Title InitRole
// @Success 200 {string} login success
// @Failure 403 user not exist
// @router /init  [post]
func (t *RolesController) InitRole() {
	logger := utils.GetLogger()
	o := orm.NewOrm()
	var request RolesStruct
	var response models.BaseResponse

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	core, _ := ioutil.ReadFile("files/RBAC/roles.json")
	json.Unmarshal(core, &request)
	fmt.Println(request)

	for _, eachPer := range request {
		var role database.SysUserRoles
		role.ID = uint32(eachPer.ID)
		role.Name = eachPer.Name

		roleID, creatError := models.CreateRoles(role)
		if creatError != nil {
			fmt.Println(creatError.Error())
			t.RWMutex.Lock()
			defer t.RWMutex.Unlock()
			logger.Error(creatError.Error())
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = creatError.Error()
			response.Body = false
			return
		}

		for _, permission := range eachPer.PermissionIds {
			per, perError := models.GetPermission(uint32(permission.(float64)))
			if perError != nil {
				fmt.Println(perError.Error())
				t.RWMutex.Lock()
				defer t.RWMutex.Unlock()
				logger.Error(perError.Error())
				response.StatusCode = utils.StatusBadRequest
				response.ErrorMsg = perError.Error()
				response.Body = false
				return
			}
			updateErr := models.RoleAddPermission(o, uint32(roleID), per)
			if updateErr != nil {
				t.RWMutex.Lock()
				defer t.RWMutex.Unlock()
				logger.Error(updateErr.Error())
				response.StatusCode = 1
				response.ErrorMsg = updateErr.Error()
				response.Body = false
				return
			}
		}
	}

	response.StatusCode = 0
	// response.Body = placeID
	return
}

// Create role create ...
// @Title role create
// @Success 200 {string} login success
// @Param   name     body   string true       "name"
// @Failure 403 user not exist
// @router /create  [post]
func (t *RolesController) Create() {
	logger := utils.GetLogger()
	var response models.BaseResponse
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	bodyString := []string{
		"name",
	}

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	name := bodyResult["name"].(string)
	var role database.SysUserRoles
	role.Name = name

	roleID, creatError := models.CreateRoles(role)
	t.CreateLog(helper.OPENSTACK, helper.Role, name, strconv.Itoa(int(roleID)), helper.Create, creatError)
	if creatError != nil {
		fmt.Println(creatError.Error())
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(creatError.Error())
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = creatError.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	response.Body = roleID
	return
}

// List ...
// @Title role list
// @Success 200
// @Failure 403
// @router /list  [get]
func (t *RolesController) List() {
	var response models.BaseResponse
	logger := utils.GetLogger()

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	roles, getErr := models.ListRoles()
	if getErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(getErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = getErr.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.Body = roles
	return
}

// Delete ...
// @Title role delete
// @Success 200
// @Failure 403
// @Param   role_id     body   int true       "role_id"
// @router /delete  [delete]
func (t *RolesController) Delete() {
	claims, _ := t.CheckToken()
	logger := utils.GetLogger()
	o := orm.NewOrm()
	utils.SetLumlog(claims["email"].(string))
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		if response.StatusCode == 0 {
			o.Commit()
		} else {
			o.Rollback()
		}
		t.Data["json"] = response
		t.ServeJSON()
	}()

	bodyString := []string{
		"role_id",
	}

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	roleID := bodyResult["role_id"].(float64)

	role, err := models.GetRole(uint32(roleID))
	if err != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = false
		return
	}

	perErr := models.RoleRemoveAllPermission(o, uint32(role.ID))
	if perErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(perErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = perErr.Error()
		response.Body = false
		return
	}

	delErr := models.DeleteRole(o, uint32(role.ID))
	t.CreateLog(helper.OPENSTACK, helper.Role, role.Name, strconv.Itoa(int(roleID)), helper.Create, delErr)
	if delErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(delErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = delErr.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	response.Body = true
	return
}

// Get ...
// @Title role detail
// @Success 200
// @Failure 403
// @Param   role_id     body   int true       "role_id"
// @router /get  [post]
func (t *RolesController) Get() {
	logger := utils.GetLogger()
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)
	var response models.BaseResponse
	bodyString := []string{
		"role_id",
	}

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	roleID := bodyResult["role_id"].(float64)

	role, delErr := models.GetRole(uint32(roleID))
	if delErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(delErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = delErr.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	response.Body = role
	return
}

// UpdatePermission ...
// @Title Update
// @Success 200
// @Failure 403
// @Param   role_id     body   int true       "role_id"
// @Param   permission_ids     body   []string true       "permission_ids"
// @router /update/permission  [put]
func (t *RolesController) UpdatePermission() {
	logger := utils.GetLogger()
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)
	var response models.BaseResponse
	o := orm.NewOrm()
	o.Begin()

	bodyString := []string{
		"role_id",
		"permission_ids",
	}

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	roleID := bodyResult["role_id"].(float64)

	defer func() {
		if response.StatusCode == 0 {
			o.Commit()
		} else {
			o.Rollback()
		}

		t.Data["json"] = response
		t.ServeJSON()
	}()

	role, getErr := models.GetRole(uint32(roleID))
	if getErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(getErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = getErr.Error()
		response.Body = false
		return
	}

	errorDelete := models.RoleRemoveAllPermission(o, uint32(roleID))
	if errorDelete != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(errorDelete.Error())
		response.StatusCode = 100
		response.ErrorMsg = errorDelete.Error()
		response.Body = false
		return
	}

	pers := bodyResult["permission_ids"].([]interface{})

	updateErr := models.RoleAddPermissions(o, strconv.Itoa(int(roleID)), pers)

	if updateErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(updateErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = updateErr.Error()
		response.Body = false
		return
	}
	t.CreateLog(helper.OPENSTACK, helper.Role, role.Name, strconv.Itoa(int(roleID)), helper.RoleUpdatePermission, nil)

	response.StatusCode = 0
	response.Body = true
	return
}

// Update ...
// @Title Update
// @Success 200
// @Failure 403
// @Param   role_id     body   int true       "role_id"
// @Param   name     body   string true       "name"
// @router /update  [put]
func (t *RolesController) Update() {
	logger := utils.GetLogger()
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)
	var response models.BaseResponse
	o := orm.NewOrm()
	o.Begin()

	defer func() {
		if response.StatusCode == 0 {
			o.Commit()
		} else {
			o.Rollback()
		}

		t.Data["json"] = response
		t.ServeJSON()
	}()

	bodyString := []string{
		"role_id",
		"name",
	}

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	roleID := bodyResult["role_id"].(float64)
	name := bodyResult["name"].(string)

	role, getErr := models.GetRole(uint32(roleID))
	if getErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(getErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = getErr.Error()
		response.Body = false
		return
	}

	updateErr := models.UpdateRole(o, uint32(roleID), name)
	t.CreateLog(helper.OPENSTACK, helper.Role, role.Name, strconv.Itoa(int(roleID)), helper.Update, updateErr)
	if updateErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(updateErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = updateErr.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	response.Body = true
	return
}
