package shared

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/utils"

	"gitlab.com/fibo-stack/backend/models"
)

// ProjectRoleController operations for Auth
type ProjectRoleController struct {
	BaseController
}

// URLMapping ...
func (t *ProjectRoleController) URLMapping() {
	t.Mapping("Create", t.Create)
	t.Mapping("Get", t.Get)
	t.Mapping("List", t.List)
	t.Mapping("Delete", t.Delete)
}

// Create trip types ...
// @Title place
// @Success 200 {string} login success
// @Failure 403 user not exist
// @router /create  [post]
func (t *ProjectRoleController) Create() {
	claims, _ := t.CheckToken()
	utils.SetLumlog(claims["email"].(string))
	logger := utils.GetLogger()
	o := orm.NewOrm()
	var request RolesStruct
	var response models.BaseResponse

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	core, _ := ioutil.ReadFile("files/RBAC/roles.json")
	json.Unmarshal(core, &request)
	fmt.Println(request)

	for _, eachPer := range request {

		// quota, _ := models.GetQuota(uint32(eachPer.QuotaID))

		var role database.SysUserRoles
		role.ID = uint32(eachPer.ID)
		role.Name = eachPer.Name
		// role.Quota = &quota

		roleID, creatError := models.CreateRoles(role)
		if creatError != nil {
			fmt.Println(creatError.Error())
			t.RWMutex.Lock()
			defer t.RWMutex.Unlock()
			logger.Error(creatError.Error())
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = creatError.Error()
			response.Body = false
			return
		}

		for _, permission := range eachPer.PermissionIds {
			per, perError := models.GetPermission(uint32(permission.(float64)))
			if perError != nil {
				fmt.Println(perError.Error())
				t.RWMutex.Lock()
				defer t.RWMutex.Unlock()
				logger.Error(perError.Error())
				response.StatusCode = utils.StatusBadRequest
				response.ErrorMsg = perError.Error()
				response.Body = false
				return
			}
			updateErr := models.RoleAddPermission(o, uint32(roleID), per)
			if updateErr != nil {
				t.RWMutex.Lock()
				defer t.RWMutex.Unlock()
				logger.Error(updateErr.Error())
				response.StatusCode = 1
				response.ErrorMsg = updateErr.Error()
				response.Body = false
				return
			}
		}
	}

	response.StatusCode = 0
	// response.Body = placeID
	return
}

// List ...
// @Title permissions
// @Success 200
// @Failure 403
// @router /list  [get]
func (t *ProjectRoleController) List() {
	var response models.BaseResponse
	claims, _ := t.CheckToken()
	logger := utils.GetLogger()
	utils.SetLumlog(claims["email"].(string))

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	roles, getErr := models.ListRoles()
	if getErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(getErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = getErr.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.Body = roles
	return
}

// Delete ...
// @Title Zurag ustgah
// @Success 200
// @Failure 403
// @Param   permission_id     body   int true       "permission_id"
// @router /delete  [delete]
func (t *ProjectRoleController) Delete() {
	claims, _ := t.CheckToken()
	logger := utils.GetLogger()
	utils.SetLumlog(claims["email"].(string))
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)
	var response models.BaseResponse
	bodyString := []string{
		"permission_id",
	}

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	permissionID := bodyResult["permission_id"].(float64)

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	delErr := models.DeletePermission(uint32(permissionID))
	if delErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(delErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = delErr.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	response.Body = true
	return
}
