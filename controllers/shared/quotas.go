package shared

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/utils"

	"gitlab.com/fibo-stack/backend/models"
)

// QuotasStruct ...
type QuotasStruct []struct {
	ID            int `json:"id"`
	Instances     int `json:"instances"`
	CPU           int `json:"cpu"`
	RAM           int `json:"ram"`
	Keypair       int `json:"keypair"`
	Volume        int `json:"volume"`
	Snapshot      int `json:"snapshot"`
	VolumeSize    int `json:"volume_size"`
	ExternalIP    int `json:"external_ip"`
	SecurityGroup int `json:"security_group"`
}

// QuotasContoller operations for Auth
type QuotasContoller struct {
	BaseController
}

// URLMapping ...
func (t *QuotasContoller) URLMapping() {
	t.Mapping("Create", t.Create)
	t.Mapping("Get", t.Get)
	t.Mapping("List", t.List)
	t.Mapping("Delete", t.Delete)
}

// InitQuota quotas ...
// @Title place
// @Success 200
// @Failure 403
// @router /init  [post]
func (t *QuotasContoller) InitQuota() {
	logger := utils.GetLogger()
	var request QuotasStruct
	var response models.BaseResponse

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	core, _ := ioutil.ReadFile("files/RBAC/quotas.json")
	json.Unmarshal(core, &request)
	fmt.Println(request)

	for _, eachPer := range request {
		var quota database.SysUserQuota
		quota.ID = uint32(eachPer.ID)
		quota.Instances = (eachPer.Instances)
		quota.CPU = (eachPer.CPU)
		quota.RAM = (eachPer.RAM)
		quota.Keypair = (eachPer.Keypair)
		quota.Volume = (eachPer.Volume)
		quota.Snapshot = (eachPer.Snapshot)
		quota.VolumeSize = (eachPer.VolumeSize)
		quota.ExternalIP = (eachPer.ExternalIP)
		quota.SecurityGroup = (eachPer.SecurityGroup)
		_, creatError := models.CreateQuotas(quota)

		if creatError != nil {
			fmt.Println(creatError.Error())
			t.RWMutex.Lock()
			defer t.RWMutex.Unlock()
			logger.Error(creatError.Error())
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = creatError.Error()
			response.Body = false
			return
		}
	}

	response.StatusCode = 0
	// response.Body = placeID
	return
}

// Create quotas ...
// @Title place
// @Success 200
// @Param   instance     body   int true       "instance"
// @Param   cpu     body   int true       "cpu"
// @Param   ram     body   int true       "ram"
// @Param   Keypair     body   int true       "Keypair"
// @Param   volume     body   int true       "volume"
// @Param   snapshot     body   int true       "snapshot"
// @Param   volume_size     body   int true       "volume_size"
// @Param   external_IP     body   int true       "external_IP"
// @Param   security_group     body   int true       "security_group"
// @Failure 403
// @router /create  [post]
func (t *QuotasContoller) Create() {
	logger := utils.GetLogger()
	var response models.BaseResponse
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)

	bodyString := []string{
		"instance",
		"cpu",
		"ram",
		"Keypair",
		"volume",
		"snapshot",
		"volume_size",
		"external_IP",
		"security_group",
	}

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	quotas, listerr := models.ListQuotas()
	if listerr != nil {
		fmt.Println(listerr.Error())
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(listerr.Error())
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = listerr.Error()
		response.Body = false
		return
	}

	Instances := bodyResult["instance"].(float64)
	CPU := bodyResult["cpu"].(float64)
	RAM := bodyResult["ram"].(float64)
	Keypair := bodyResult["Keypair"].(float64)
	Volume := bodyResult["volume"].(float64)
	Snapshot := bodyResult["snapshot"].(float64)
	VolumeSize := bodyResult["volume_size"].(float64)
	ExternalIP := bodyResult["external_IP"].(float64)
	SecurityGroup := bodyResult["security_group"].(float64)

	var quota database.SysUserQuota
	quota.ID = uint32(len(quotas) + 1)
	quota.Instances = int(Instances)
	quota.CPU = int(CPU)
	quota.RAM = int(RAM)
	quota.Keypair = int(Keypair)
	quota.Volume = int(Volume)
	quota.Snapshot = int(Snapshot)
	quota.VolumeSize = int(VolumeSize)
	quota.ExternalIP = int(ExternalIP)
	quota.SecurityGroup = int(SecurityGroup)
	_, creatError := models.CreateQuotas(quota)

	if creatError != nil {
		fmt.Println(creatError.Error())
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(creatError.Error())
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = creatError.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	// response.Body = placeID
	return
}

// List ...
// @Title quotas
// @Success 200
// @Failure 403
// @router /list  [get]
func (t *QuotasContoller) List() {
	var response models.BaseResponse
	claims, _ := t.CheckToken()
	logger := utils.GetLogger()
	utils.SetLumlog(claims["email"].(string))

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	quota, getErr := models.ListQuotas()
	if getErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(getErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = getErr.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.Body = quota
	return
}

// Update quotas ...
// @Title place
// @Success 200
// @Param   quota_id     body   int true       "quota_id"
// @Param   instance     body   int true       "instance"
// @Param   cpu     body   int true       "cpu"
// @Param   ram     body   int true       "ram"
// @Param   Keypair     body   int true       "Keypair"
// @Param   volume     body   int true       "volume"
// @Param   snapshot     body   int true       "snapshot"
// @Param   volume_size     body   int true       "volume_size"
// @Param   external_IP     body   int true       "external_IP"
// @Param   security_group     body   int true       "security_group"
// @Failure 403
// @router /update  [put]
func (t *QuotasContoller) Update() {
	logger := utils.GetLogger()
	var response models.BaseResponse
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)

	bodyString := []string{
		"instance",
		"cpu",
		"ram",
		"Keypair",
		"volume",
		"snapshot",
		"volume_size",
		"external_IP",
		"security_group",
		"quota_id",
	}

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	QuotaID := bodyResult["quota_id"].(float64)
	Instances := bodyResult["instance"].(float64)
	CPU := bodyResult["cpu"].(float64)
	RAM := bodyResult["ram"].(float64)
	Keypair := bodyResult["Keypair"].(float64)
	Volume := bodyResult["volume"].(float64)
	Snapshot := bodyResult["snapshot"].(float64)
	VolumeSize := bodyResult["volume_size"].(float64)
	ExternalIP := bodyResult["external_IP"].(float64)
	SecurityGroup := bodyResult["security_group"].(float64)

	creatError := models.UpdateQuota(uint32(QuotaID), Instances, CPU, RAM, Keypair, Volume, Snapshot, VolumeSize, ExternalIP, SecurityGroup)

	if creatError != nil {
		fmt.Println(creatError.Error())
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(creatError.Error())
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = creatError.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	// response.Body = placeID
	return
}
