package shared

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	math "math/rand"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/projects"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/roles"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/users"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/identity"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
	"golang.org/x/crypto/bcrypt"
)

// InitAllController ...
type InitAllController struct {
	BaseController
}

// InitAll ...
// @Description New user register openstack
// @Success 200 {string} id
// @router /initall [get]
func (a *InitAllController) InitAll() {
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	// #region [ User ]
	email := "demo@fibo.cloud"
	firstname := "demo"
	lastname := "demo"
	password := "fibo123"

	checkEmail, _ := models.CheckEmail(email)
	if checkEmail {
		response.StatusCode = 100
		response.ErrorMsg = "Registered email address"
		response.Body = true
		return
	}

	math.Seed(time.Now().UnixNano())
	passHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	bbb := make([]rune, 10)
	for i := range bbb {
		bbb[i] = letters[math.Intn(len(letters))]
	}

	OsPwd := string(bbb)

	// create tenant (project)
	//authenticate to openstack
	adminProvider, err := AuthOSAdmin()
	if err != nil {
		fmt.Println("err admin", err)
	}
	//create client to identity service
	client, err := openstack.NewIdentityV3(adminProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})

	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	opts1 := users.CreateOpts{
		Name:             email,
		DomainID:         "default",
		DefaultProjectID: beego.AppConfig.String("tenantID"),
		Enabled:          gophercloud.Enabled,
		Password:         OsPwd,
		Extra: map[string]interface{}{
			"email": email,
		},
	}

	user, err := users.Create(client, opts1).Extract()
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	//Get roles from service
	var roleID string
	listOpts := roles.ListOpts{
		DomainID: "",
	}
	allPages, err := roles.List(client, listOpts).AllPages()
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	allRoles, err := roles.ExtractRoles(allPages)
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	//Set user role member
	for _, role := range allRoles {
		fmt.Printf("%+v\a", role.Name)
		if role.Name == "admin" {
			roleID = role.ID
		}
	}

	roleAssignError := roles.Assign(client, roleID, roles.AssignOpts{
		UserID:    user.ID,
		ProjectID: beego.AppConfig.String("tenantID"),
	}).ExtractErr()

	if roleAssignError != nil {
		response.StatusCode = 100
		response.ErrorMsg = roleAssignError.Error()
		response.Body = true
		return
	}

	_, providerError := models.InitOpenstackProvider(email, OsPwd, beego.AppConfig.String("tenantID"))
	if providerError != nil {
		response.StatusCode = 100
		response.ErrorMsg = providerError.Error()
		response.Body = true
		return
	}

	o := orm.NewOrm()
	newUser := database.SysUser{
		Username:      email,
		Password:      string(passHash),
		Email:         email,
		Firstname:     firstname,
		Lastname:      lastname,
		Role:          "admin",
		OsPwd:         OsPwd,
		LastLoginDate: time.Time{},
		OsTenantID:    beego.AppConfig.String("tenantID"),
		OsUserID:      user.ID,
		QuotaPlan:     helper.QuotaPlanSandbox,
		IsActive:      *gophercloud.Enabled,
	}

	_, inserError := o.Insert(&newUser)
	if inserError != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	projectRole := database.UserProjectRole{
		Email:        email,
		OsUserID:     user.ID,
		OsTenantID:   beego.AppConfig.String("tenantID"),
		OsTenantName: "admin",
		RoleID:       uint32(1),
		QuotaID:      uint32(1),
	}

	_, errRole := models.CreateProjectRole(projectRole)
	if errRole != nil {
		response.StatusCode = 100
		response.ErrorMsg = errRole.Error()
		response.Body = true
		return
	}

	// #endregion

	// #region [ Projects ]
	// ---------------------------------- Project -------------------------------

	project, err := identity.GetKeystone("admin").CreateProject("userdefault", "default users project", adminProvider)
	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	_, quotaErr := blockstorage.GetBlockstorage("admin").InitialQuotaset(adminProvider, project.ID)
	if quotaErr != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		response.StatusCode = 100
		response.ErrorMsg = quotaErr.Error()
		return
	}

	_, quotaErrCompute := compute.GetCompute("admin").InitialQuotaset(adminProvider, project.ID)
	if quotaErrCompute != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		response.StatusCode = 100
		response.ErrorMsg = quotaErrCompute.Error()
		return
	}
	// #endregion

	// #region [ Permissions ]
	// ---------------------------------- Permissions -------------------------------
	// PermissionStruct ...
	type PermissionStruct []struct {
		Entity string  `json:"entity"`
		Action float64 `json:"action"`
		API    string  `json:"api"`
		IsAll  bool    `json:"is_all"`
		ID     float64 `json:"id"`
	}

	var requestPermissions PermissionStruct

	core, _ := ioutil.ReadFile("files/RBAC/permissions.json")
	json.Unmarshal(core, &requestPermissions)
	fmt.Println(requestPermissions)

	for _, eachPer := range requestPermissions {
		var permission database.SysUserPermission
		permission.ID = uint32(eachPer.ID)
		permission.Entity = eachPer.Entity
		permission.API = "/api/v1" + eachPer.API
		_, creatError := models.CreatePermissions(permission)
		if creatError != nil {
			fmt.Println(creatError.Error())
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = creatError.Error()
			response.Body = false
			return
		}
	}

	// #endregion

	// #region [ Quotas ]
	// QuotasStruct ...
	type QuotasStruct []struct {
		ID            int `json:"id"`
		Instances     int `json:"instances"`
		CPU           int `json:"cpu"`
		RAM           int `json:"ram"`
		Keypair       int `json:"keypair"`
		Volume        int `json:"volume"`
		Snapshot      int `json:"snapshot"`
		VolumeSize    int `json:"volume_size"`
		ExternalIP    int `json:"external_ip"`
		SecurityGroup int `json:"security_group"`
	}

	quotaCore, _ := ioutil.ReadFile("files/RBAC/quotas.json")
	var requestquota QuotasStruct
	json.Unmarshal(quotaCore, &requestquota)

	for _, eachPer := range requestquota {
		var quota database.SysUserQuota
		quota.ID = uint32(eachPer.ID)
		quota.Instances = (eachPer.Instances)
		quota.CPU = (eachPer.CPU)
		quota.RAM = (eachPer.RAM)
		quota.Keypair = (eachPer.Keypair)
		quota.Volume = (eachPer.Volume)
		quota.Snapshot = (eachPer.Snapshot)
		quota.VolumeSize = (eachPer.VolumeSize)
		quota.ExternalIP = (eachPer.ExternalIP)
		quota.SecurityGroup = (eachPer.SecurityGroup)
		_, creatError := models.CreateQuotas(quota)

		if creatError != nil {
			fmt.Println(creatError.Error())
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = creatError.Error()
			response.Body = false
			return
		}
	}
	// #endregion

	// #region [ Roles ]
	// ---------------------------------- Role -------------------------------
	// RolesStruct ...
	type RolesStruct []struct {
		ID            float64       `json:"id"`
		Name          string        `json:"name"`
		PermissionIds []interface{} `json:"permission_ids"`
	}

	var requestRoles RolesStruct

	coreRole, _ := ioutil.ReadFile("files/RBAC/roles.json")
	json.Unmarshal(coreRole, &requestRoles)
	fmt.Println(requestRoles)

	for _, eachPer := range requestRoles {
		var role database.SysUserRoles
		role.ID = uint32(eachPer.ID)
		role.Name = eachPer.Name

		roleID, creatError := models.CreateRoles(role)
		if creatError != nil {
			fmt.Println(creatError.Error())
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = creatError.Error()
			response.Body = false
			return
		}

		for _, permission := range eachPer.PermissionIds {
			per, perError := models.GetPermission(uint32(permission.(float64)))
			if perError != nil {
				fmt.Println(perError.Error())
				response.StatusCode = utils.StatusBadRequest
				response.ErrorMsg = perError.Error()
				response.Body = false
				return
			}
			updateErr := models.RoleAddPermission(o, uint32(roleID), per)
			if updateErr != nil {
				response.StatusCode = 1
				response.ErrorMsg = updateErr.Error()
				response.Body = false
				return
			}
		}
	}
	// #endregion

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// New ...
// @Description New user register openstack
// @Success 200 {string} id
// @router /new [get]
func (a *InitAllController) New() {
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	// #region [ User ]
	email := "demo53@fibo.cloud"
	firstname := "demo53"
	lastname := "demo53"
	password := "fibo123"

	checkEmail, _ := models.CheckEmail(email)
	if checkEmail {
		response.StatusCode = 6
		response.ErrorMsg = "Registered email address"
		response.Body = true
		return
	}

	math.Seed(time.Now().UnixNano())
	passHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	bbb := make([]rune, 10)
	for i := range bbb {
		bbb[i] = letters[math.Intn(len(letters))]
	}

	OsPwd := string(bbb)

	// create tenant (project)
	//authenticate to openstack
	adminProvider, err := AuthOSAdmin()
	//create client to identity service
	client, err := openstack.NewIdentityV3(adminProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})

	if err != nil {
		response.StatusCode = 5
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	project, err := projects.Create(client, projects.CreateOpts{
		Name:        email,
		Description: "Default project of " + email,
	}).Extract()

	if err != nil {
		response.StatusCode = 3
		response.ErrorMsg = err.Error()
		response.Body = nil
		return
	}

	opts1 := users.CreateOpts{
		Name:             email,
		DomainID:         "default",
		DefaultProjectID: project.ID,
		Enabled:          gophercloud.Enabled,
		Password:         OsPwd,
		Extra: map[string]interface{}{
			"email": email,
		},
	}

	user, err := users.Create(client, opts1).Extract()
	if err != nil {
		response.StatusCode = 4
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	//Get roles from service
	var roleID string
	listOpts := roles.ListOpts{
		DomainID: "",
	}
	allPages, err := roles.List(client, listOpts).AllPages()
	if err != nil {
		response.StatusCode = 2
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	allRoles, err := roles.ExtractRoles(allPages)
	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	//Set user role member
	for _, role := range allRoles {
		fmt.Printf("%+v\a", role.Name)
		if role.Name == "member" {
			roleID = role.ID
		}
	}

	roleAssignError := roles.Assign(client, roleID, roles.AssignOpts{
		UserID:    user.ID,
		ProjectID: project.ID,
	}).ExtractErr()

	if roleAssignError != nil {
		response.StatusCode = 101
		response.ErrorMsg = roleAssignError.Error()
		response.Body = true
		return
	}

	_, providerError := models.InitOpenstackProvider(email, OsPwd, project.ID)
	if providerError != nil {
		response.StatusCode = 102
		response.ErrorMsg = providerError.Error()
		response.Body = true
		return
	}

	o := orm.NewOrm()
	newUser := database.SysUser{
		Username:      email,
		Password:      string(passHash),
		Email:         email,
		Firstname:     firstname,
		Lastname:      lastname,
		Role:          "member",
		OsPwd:         OsPwd,
		LastLoginDate: time.Time{},
		OsTenantID:    project.ID,
		OsUserID:      user.ID,
		QuotaPlan:     helper.QuotaPlanSandbox,
		IsActive:      *gophercloud.Enabled,
	}

	_, inserError := o.Insert(&newUser)
	if inserError != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	projectRole := database.UserProjectRole{
		Email:        email,
		OsUserID:     user.ID,
		OsTenantID:   project.ID,
		OsTenantName: project.Name,
		RoleID:       uint32(1),
		QuotaID:      uint32(1),
	}

	_, errRole := models.CreateProjectRole(projectRole)
	if errRole != nil {
		response.StatusCode = 100
		response.ErrorMsg = errRole.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}
