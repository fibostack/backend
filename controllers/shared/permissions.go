package shared

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"

	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/utils"

	"gitlab.com/fibo-stack/backend/models"
)

// PermissionStruct ...
type PermissionStruct []struct {
	Entity string  `json:"entity"`
	Action float64 `json:"action"`
	API    string  `json:"api"`
	IsAll  bool    `json:"is_all"`
	ID     float64 `json:"id"`
}

// PermissionsController operations for Auth
type PermissionsController struct {
	BaseController
}

// URLMapping ...
func (t *PermissionsController) URLMapping() {
	t.Mapping("Init", t.InitPermission)
	t.Mapping("Create", t.Create)
	t.Mapping("Get", t.Get)
	t.Mapping("List", t.List)
	t.Mapping("Delete", t.Delete)
}

// InitPermission ...
// @Title Init
// @Success 200
// @Failure 403
// @router /init  [post]
func (t *PermissionsController) InitPermission() {
	logger := utils.GetLogger()
	var request PermissionStruct
	var response models.BaseResponse

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	core, _ := ioutil.ReadFile("files/RBAC/permissions.json")
	json.Unmarshal(core, &request)
	fmt.Println(request)

	for _, eachPer := range request {
		var permission database.SysUserPermission
		permission.ID = uint32(eachPer.ID)
		permission.Entity = eachPer.Entity
		permission.API = "/api/v1" + eachPer.API
		_, creatError := models.CreatePermissions(permission)
		if creatError != nil {
			fmt.Println(creatError.Error())
			t.RWMutex.Lock()
			defer t.RWMutex.Unlock()
			logger.Error(creatError.Error())
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = creatError.Error()
			response.Body = false
			return
		}
	}

	response.StatusCode = 0
	// response.Body = placeID
	return
}

// Create ...
// @Title create
// @Success 200 {string} login success
// @Failure 403 user not exist
// @Param   entity     body   string true       "entity"
// @Param   api     body   string true       "api"
// @router /create  [post]
func (t *PermissionsController) Create() {
	logger := utils.GetLogger()
	var response models.BaseResponse
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)
	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	bodyString := []string{
		"entity",
		"api",
	}

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	entity := bodyResult["entity"].(string)
	api := bodyResult["api"].(string)

	var permission database.SysUserPermission
	permission.Entity = entity
	permission.API = api
	perID, creatError := models.CreatePermissions(permission)
	t.CreateLog(helper.OPENSTACK, helper.Permission, api, strconv.Itoa(int(perID)), helper.Create, creatError)
	if creatError != nil {
		fmt.Println(creatError.Error())
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(creatError.Error())
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = creatError.Error()
		response.Body = false
		return
	}

	response.StatusCode = 0
	response.Body = perID
	return
}

// List ...
// @Title permissions
// @Success 200
// @Failure 403
// @router /list  [get]
func (t *PermissionsController) List() {
	var response models.BaseResponse
	claims, _ := t.CheckToken()
	logger := utils.GetLogger()
	utils.SetLumlog(claims["email"].(string))

	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	permissions, getErr := models.ListPermissions()
	if getErr != nil {
		t.RWMutex.Lock()
		defer t.RWMutex.Unlock()
		logger.Error(getErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = getErr.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.Body = permissions
	return
}

// Delete ...
// @Title Zurag ustgah
// @Success 200
// @Failure 403
// @Param   permission_ids     body   []int true       "permission_ids"
// @router /delete  [delete]
func (t *PermissionsController) Delete() {
	claims, _ := t.CheckToken()
	logger := utils.GetLogger()
	utils.SetLumlog(claims["email"].(string))
	bodyResult := RetrieveDataFromBody(t.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		t.Data["json"] = response
		t.ServeJSON()
	}()

	bodyString := []string{
		"permission_ids",
	}

	if sCod, eMsg := CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	permissionIDs := bodyResult["permission_ids"].([]interface{})

	for _, i := range permissionIDs {
		permission, getErr := models.GetPermission(uint32(i.(float64)))
		if getErr != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = getErr.Error()
			response.Body = false
			return
		}

		delErr := models.DeletePermission(uint32(i.(float64)))
		t.CreateLog(helper.OPENSTACK, helper.Permission, permission.API, strconv.Itoa(int(permission.ID)), helper.Delete, delErr)
		if delErr != nil {
			t.RWMutex.Lock()
			defer t.RWMutex.Unlock()
			logger.Error(delErr.Error())
			response.StatusCode = 100
			response.ErrorMsg = delErr.Error()
			response.Body = false
			return
		}
	}

	response.StatusCode = 0
	response.Body = true
	return
}
