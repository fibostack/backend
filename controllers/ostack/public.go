package ostack

import "gitlab.com/fibo-stack/backend/controllers/shared"

// PublicController struct
type PublicController struct {
	shared.BaseController
}
