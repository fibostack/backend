package ostack

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// VolumeController struct
type VolumeController struct {
	shared.BaseController
}

// URLMapping ...
func (v *VolumeController) URLMapping() {
	v.Mapping("ListVolume", v.List)
	v.Mapping("GetVolume", v.Get)
	v.Mapping("CreateVolume", v.Create)
	v.Mapping("DeleteVolume", v.Delete)
	v.Mapping("AttachVolumeInstance", v.Attach)
	v.Mapping("DetachVolumeInstance", v.Detach)
	v.Mapping("ListVolumeType", v.ListVolumeType)
	v.Mapping("CreateVolumeType", v.CreateVolumeType)
	v.Mapping("DeleteVolumeType", v.DeleteVolumeType)
	v.Mapping("GetVolumeType", v.GetVolumeType)
	v.Mapping("UpdateVolumeType", v.UpdateVolumeType)
}

// Prepare ...
func (v *VolumeController) Prepare() {
	_, tokenErr := v.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		v.Data["Data"] = jsons
		v.ServeJSON()
	}
}

// List ...
// @Title List
// @Description get List
// @Failure 403
// @router /list [get]
func (v *VolumeController) List() {
	claims, _ := v.CheckToken()
	OsTenantID := v.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	volumeList, err := blockstorage.GetBlockstorage(claims["username"].(string)).ListVolumes(OsTenantID)
	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = volumeList
	return
}

// Get ...
// @Title Get
// @Description get Get
// @Param    volumeID    string    false    "volumeID"
// @Failure 403
// @router /get [post]
func (v *VolumeController) Get() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"volumeID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	getVolume, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolume(bodyResult["volumeID"].(string))

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = getVolume
	return
}

// Attach ...
// @Title AttachInstance
// @Description hint AttachInstance
// @Param    serverID    string    false    "serverID"
// @Param    volumeID    string    false    "volumeID"
// @Failure 403
// @router /attach [post]
func (v *VolumeController) Attach() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"serverID",
		"volumeID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	volume, volumeErr := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolume(bodyResult["volumeID"].(string))
	if volumeErr != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(volumeErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = volumeErr.Error()
		return
	}

	attachedVolume, err := compute.GetCompute(claims["username"].(string)).AttachVolume(bodyResult["serverID"].(string), bodyResult["volumeID"].(string))
	v.CreateLog(helper.OPENSTACK, helper.Volume, volume.Name, volume.ID, helper.VolumeAttach, err)

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = attachedVolume
	return
}

// Detach ...
// @Title DetachInstance
// @Description hint DetachInstance
// @Param    serverID	string    false    "serverID"
// @Param    attachmentID	string    false    "attachmentID"
// @Failure 403
// @router /detach [post]
func (v *VolumeController) Detach() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"serverID",
		"attachmentID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	volume, volumeErr := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolume(bodyResult["volumeID"].(string))
	if volumeErr != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(volumeErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = volumeErr.Error()
		return
	}

	err := compute.GetCompute(claims["username"].(string)).DetachVolume(bodyResult["serverID"].(string), bodyResult["attachmentID"].(string))
	v.CreateLog(helper.OPENSTACK, helper.Volume, volume.Name, volume.ID, helper.VolumeDetach, err)

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Create ...
// @Title Create
// @Description hint Create
// @Param    size		int    false    "size"
// @Param    volumeType	string    false    "volumeType"
// @Param    name	string    false    "name"
// @Param    description	string    false    "description"
// @Failure 403
// @router /create [post]
func (v *VolumeController) Create() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	osTenantID := v.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	osUserID := claims["userID"].(string)

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"name",
		"description",
		"size",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	SysUserID := claims["sysUserID"].(float64)
	volSize := int(bodyResult["size"].(float64))
	name := bodyResult["name"].(string)
	desc := bodyResult["description"].(string)

	quota, quataErr := models.UserQuota(osTenantID, osUserID)
	if quataErr != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quataErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = quataErr.Error()
		response.Body = true
		return
	}

	if quota.Volume < quota.UsedVolume+1 || quota.VolumeSize < quota.UsedVolumeSize+volSize {
		response.StatusCode = 1
		response.ErrorMsg = "Your volume quota has been exceeded"
		response.Body = true
		return
	}

	volume, err := blockstorage.GetBlockstorage(claims["username"].(string)).CreateVolume(volSize, name, desc, "")
	logID := v.CreateLog(helper.OPENSTACK, helper.Volume, volume.Name, volume.ID, helper.Create, err)
	models.AddUsageQuotaVolume(osTenantID, osUserID, volSize)

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	go func(volumeID, username string, logID uint32) {
		for {
			volumeGet, errGet := blockstorage.GetBlockstorage(username).GetVolume(volumeID)
			if errGet != nil {
				v.RWMutex.Lock()
				defer v.RWMutex.Unlock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errGet.Error())
				return
			}

			//  “available”, “error”, “creating”, “deleting”, and “error_deleting”.
			if volumeGet.Status == "available" {
				o := orm.NewOrm()
				var usageObj database.UsgHistory
				usageObj = database.UsgHistory{
					SysUserID:       uint32(SysUserID),
					OsUserID:        osUserID,
					Type:            "Volume",
					Hostname:        volumeGet.Name,
					OsResourceID:    volumeGet.ID,
					OsInstanceID:    "",
					Flavor:          "",
					DiskSize:        volumeGet.Size,
					CPU:             0,
					RAM:             0,
					Status:          "AVAILABLE",
					StartDate:       time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
					EndDate:         "",
					IP:              "",
					LastLogActionID: uint32(logID),
				}
				_, errTable := o.Insert(&usageObj)
				if errTable != nil {
					v.RWMutex.Lock()
					defer v.RWMutex.Unlock()
					utils.SetLumlog(claims["email"].(string))
					logger.Error(errTable.Error())
				}
				return
			}

			if volumeGet.Status == "error" {
				return
			}

			time.Sleep(3 * time.Second)
		}

	}(volume.ID, claims["username"].(string), uint32(logID))

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = volume
	return
}

// CreateVolumeType ...
// @Title CreateVolumeType
// @Description hint CreateVolumeType
// @Param    name		string    false    "name"
// @Param    description	string    false    "description"
// @Param    isPublic	bool    false    "isPublic"
// @Failure 403
// @router /volume_type/create [post]
func (v *VolumeController) CreateVolumeType() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"name",
		"description",
		"isPublic",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	volume, err := blockstorage.GetBlockstorage(claims["username"].(string)).CreateVolumeType(bodyResult["name"].(string), bodyResult["description"].(string), bodyResult["isPublic"].(bool))

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = volume
	return
}

// ListVolumeType ...
// @Title ListVolumeType
// @Description hint ListVolumeType
// @Failure 403
// @router /volume_type/list [get]
func (v *VolumeController) ListVolumeType() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	volume, err := blockstorage.GetBlockstorage(claims["username"].(string)).ListVolumeTypes()

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = volume
	return
}

// Delete ...
// @Title Delete
// @Description get Delete
// @Param    vol_ids		[]string    false    "vol_ids"
// @Failure 403
// @router /delete [delete]
func (v *VolumeController) Delete() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	osTenantID := v.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse
	var err error

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"vol_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	for _, volID := range bodyResult["vol_ids"].([]interface{}) {
		volume, volumeErr := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolume(volID.(string))
		if volumeErr != nil {
			v.RWMutex.Lock()
			defer v.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(volumeErr.Error())
			response.StatusCode = 100
			response.ErrorMsg = volumeErr.Error()
			return
		}

		err = blockstorage.GetBlockstorage(claims["username"].(string)).DeleteVolume(volID.(string))
		models.MinusUsageQuotaVolume(osTenantID, OsUserID, volume.Size)
		logID := v.CreateLog(helper.OPENSTACK, helper.Volume, volume.Name, volume.ID, helper.Delete, err)

		if err != nil {
			v.RWMutex.Lock()
			defer v.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 100
		} else {
			response.StatusCode = 0

			go func(volumeID, username string, logID uint32) {
				for {
					volumeGet, errGet := blockstorage.GetBlockstorage(username).GetVolume(volumeID)
					if errGet != nil {
						if errGet.Error() == "Resource not found" {
							o := orm.NewOrm()
							_, errTable := o.QueryTable("usg_history").Filter("os_resource_id", volumeID).Filter("end_date", "").Update(orm.Params{
								"sys_user_id":        SysUserID,
								"os_user_id":         OsUserID,
								"status":             "DELETED",
								"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
								"ip":                 "",
								"last_log_action_id": logID,
							})
							if errTable != nil {
								v.RWMutex.Lock()
								defer v.RWMutex.Unlock()
								utils.SetLumlog(claims["email"].(string))
								logger.Error(errTable.Error())
							}
							return
						}
						v.RWMutex.Lock()
						defer v.RWMutex.Unlock()
						utils.SetLumlog(claims["email"].(string))
						logger.Error(errGet.Error())
					}

					if volumeGet.Status == "error" {
						return
					}
					time.Sleep(1 * time.Second)
				}
			}(volID.(string), claims["username"].(string), uint32(logID))
		}
	}

	if err != nil {
		response.ErrorMsg = err.Error()
		return
	}
	response.ErrorMsg = ""
	response.Body = true
	return
}

// DeleteVolumeType ...
// @Title DeleteVolumeType
// @Description hint DeleteVolumeType
// @Param    volumeTypeID		[]string    false    "volumeTypeID"
// @Failure 403
// @router /volume_type/delete [delete]
func (v *VolumeController) DeleteVolumeType() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	var err error

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"volumeTypeID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	for _, volID := range bodyResult["volumeTypeID"].([]interface{}) {
		err = blockstorage.GetBlockstorage(claims["username"].(string)).DeleteVolumeType(volID.(string))

		if err != nil {
			v.RWMutex.Lock()
			defer v.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 100
		} else {
			response.StatusCode = 0
		}
	}

	if err != nil {
		response.ErrorMsg = err.Error()
		return
	}
	response.ErrorMsg = ""
	response.Body = true
	return
}

// GetVolumeType ...
// @Title GetVolumeType
// @Description hint GetVolumeType
// @Param    volumeTypeID		string    false    "volumeTypeID"
// @Failure 403
// @router /volume_type/get [post]
func (v *VolumeController) GetVolumeType() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"volumeTypeID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	volume, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolumeType(bodyResult["volumeTypeID"].(string))

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = volume
	return
}

// UpdateVolumeType ...
// @Title UpdateVolumeType
// @Description hint UpdateVolumeType
// @Param    volumeTypeID		string    false    "volumeTypeID"
// @Param    newName		string    false    "newName"
// @Param    description		string    false    "description"
// @Param    isPublic		bool    false    "isPublic"
// @Failure 403
// @router /volume_type/update [put]
func (v *VolumeController) UpdateVolumeType() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"volumeTypeID",
		"newName",
		"description",
		"isPublic",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	volume, err := blockstorage.GetBlockstorage(claims["username"].(string)).UpdateVolumeType(bodyResult["volumeTypeID"].(string), bodyResult["newName"].(string), bodyResult["description"].(string), bodyResult["isPublic"].(bool))

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = volume
	return
}

// CreateSnapshotFromVolume ...
// @Title Create snapshot from Volume
// @Description hint Create snapshot from Volume
// @Param    volumeID		string    false    "volumeID"
// @Param    name		string    false    "name"
// @Failure 403
// @router /createSnapshot [post]
func (v *VolumeController) CreateSnapshotFromVolume() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	osTenantID := v.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"volumeID",
		"name",
		"volumeID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	osUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	quota, quataErr := models.UserQuota(osTenantID, osUserID)
	if quataErr != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quataErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = quataErr.Error()
		response.Body = true
		return
	}

	if quota.Snapshot < quota.UsedSnapshot+1 {
		response.StatusCode = 1
		response.ErrorMsg = "Your snapshot quota has been exceeded"
		response.Body = true
		return
	}

	volume, volumeErr := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolume(bodyResult["volumeID"].(string))
	if volumeErr != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(volumeErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = volumeErr.Error()
		return
	}

	snapshot, err := blockstorage.GetBlockstorage(claims["username"].(string)).CreateSnapshot(bodyResult["volumeID"].(string), bodyResult["name"].(string), "Snapshot from "+bodyResult["volumeID"].(string)+")")
	logID := v.CreateLog(helper.OPENSTACK, helper.Volume, volume.Name, volume.ID, helper.Snapshot, err)
	models.AddUsageQuotaSnapshot(osTenantID, osUserID)

	go func(snapID, username string, logID uint32) {
		for {
			snapGet, errGet := blockstorage.GetBlockstorage(username).GetSnapshot(snapID)
			if errGet != nil {
				v.RWMutex.Lock()
				defer v.RWMutex.Unlock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errGet.Error())
				return
			}

			//  “available”, “error”, “creating”, “deleting”, and “error_deleting”.
			if snapGet.Status == "available" {
				o := orm.NewOrm()
				var usageObj database.UsgHistory
				usageObj = database.UsgHistory{
					SysUserID:       uint32(SysUserID),
					OsUserID:        osUserID,
					Type:            "SnapShot",
					Hostname:        snapGet.Name,
					OsResourceID:    snapGet.ID,
					OsInstanceID:    "",
					Flavor:          "",
					DiskSize:        snapGet.Size,
					CPU:             0,
					RAM:             0,
					Status:          "AVAILABLE",
					StartDate:       time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
					EndDate:         "",
					IP:              "",
					LastLogActionID: uint32(logID),
				}
				_, errTable := o.Insert(&usageObj)
				if errTable != nil {
					v.RWMutex.Lock()
					defer v.RWMutex.Unlock()
					utils.SetLumlog(claims["email"].(string))
					logger.Error(errTable.Error())
				}
				return
			}

			if snapGet.Status == "error" {
				return
			}

			time.Sleep(3 * time.Second)
		}

	}(snapshot.ID, claims["username"].(string), uint32(logID))

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = snapshot
	return
}

// Extend ...
// @Title Extend
// @Description Extend
// @Param    volumeID		string    false    "volumeID"
// @Param    newSize		int    false    "newSize"
// @Failure 403
// @router /extend [post]
func (v *VolumeController) Extend() {
	claims, _ := v.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	osTenantID := v.GetHeaderValue("OS-Tenant-ID")

	defer func() {
		v.Data["json"] = response
		v.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(v.Ctx.Request.Body)
	bodyString := []string{
		"newSize",
		"volumeID",
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	volume, volumeErr := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolume(bodyResult["volumeID"].(string))
	if volumeErr != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(volumeErr.Error())
		response.StatusCode = 100
	}

	quota, quotaErr := models.UserQuota(osTenantID, OsUserID)
	if quotaErr != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quotaErr.Error())
		response.StatusCode = 100
	}

	if quota.VolumeSize < quota.UsedVolumeSize-volume.Size+int(bodyResult["newSize"].(float64)) {
		response.StatusCode = 1
		response.ErrorMsg = "Your volume size quota has been exceeded"
		response.Body = true
		return
	}

	err := blockstorage.GetBlockstorage(claims["username"].(string)).ExtendVolumeSize(int(bodyResult["newSize"].(float64)), bodyResult["volumeID"].(string))
	logID := v.CreateLog(helper.OPENSTACK, helper.Volume, volume.Name, volume.ID, helper.VolumeExtend, err)

	if err == nil {
		go func(volID, username string, logID int64) {
			for {
				getVolume, errVolume := blockstorage.GetBlockstorage(username).GetVolume(volID)

				if errVolume == nil {
					if getVolume.Status == "available" && getVolume.Size == int(bodyResult["newSize"].(float64)) {
						o := orm.NewOrm()

						_, errTableUpdate := o.QueryTable("usg_history").Filter("os_resource_id", getVolume.ID).Filter("end_date", "").Update(orm.Params{
							"sys_user_id":        SysUserID,
							"os_user_id":         OsUserID,
							"status":             "EXTENTED",
							"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
							"ip":                 "",
							"last_log_action_id": logID,
						})

						if errTableUpdate != nil {
							fmt.Print("-------")
							fmt.Print("-------")
							fmt.Print("-------")
							fmt.Print(errTableUpdate.Error())
							v.RWMutex.Lock()
							defer v.RWMutex.Unlock()
							utils.SetLumlog(claims["email"].(string))
							logger.Error(errTableUpdate.Error())
						}

						var usageObj database.UsgHistory
						usageObj = database.UsgHistory{
							SysUserID:       uint32(SysUserID),
							OsUserID:        OsUserID,
							Type:            "Volume",
							Hostname:        getVolume.Name,
							OsResourceID:    getVolume.ID,
							OsInstanceID:    "",
							Flavor:          "",
							DiskSize:        getVolume.Size,
							CPU:             0,
							RAM:             0,
							Status:          "AVAILABLE",
							StartDate:       time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
							EndDate:         "",
							IP:              "",
							LastLogActionID: uint32(logID),
						}
						_, errTable := o.Insert(&usageObj)
						if errTable != nil {
							v.RWMutex.Lock()
							defer v.RWMutex.Unlock()
							utils.SetLumlog(claims["email"].(string))
							logger.Error(errTable.Error())
						}
						return
					}

				}
				if getVolume.Status == "error" {
					return
				}
				time.Sleep(3 * time.Second)
			}
		}(bodyResult["volumeID"].(string), claims["username"].(string), logID)
	}

	if err != nil {
		v.RWMutex.Lock()
		defer v.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}
