package ostack

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/snapshots"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
)

// SnapshotController operations for Snapshot
type SnapshotController struct {
	shared.BaseController
}

// URLMapping ...
func (c *SnapshotController) URLMapping() {
	c.Mapping("List", c.List)
	c.Mapping("Delete", c.Delete)
	c.Mapping("Launch", c.Launch)
}

// SnapshotWithName ...
type SnapshotWithName struct {
	Snap       snapshots.Snapshot
	VolumeName string `json:"volumeName"`
}

// Prepare ...
func (c *SnapshotController) Prepare() {
	_, tokenErr := c.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		c.Data["Data"] = jsons
		c.ServeJSON()
	}
}

// List ...
// @Title List
// @Description get Snapshot
// @Success 200 {object} Snapshot
// @Failure 403
// @router /list [get]
func (c *SnapshotController) List() {
	claims, err := c.CheckToken()
	logger := utils.GetLogger()
	OsTenantID := c.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse

	defer func() {
		c.Data["json"] = response
		c.ServeJSON()
	}()

	o := orm.NewOrm()

	listsSnapshots, err := blockstorage.GetBlockstorage(claims["username"].(string)).ListSnapshots(OsTenantID)

	var resData []interface{}
	for _, snapshot := range listsSnapshots {
		var result database.UsgHistory
		o.QueryTable("usg_history").Filter("os_resource_id", snapshot.ID).One(&result)
		tmp := make(map[string]interface{})
		dtmp, _ := json.Marshal(&snapshot)
		_ = json.Unmarshal(dtmp, &tmp)
		tmp["created_date"] = result.StartDate
		resData = append(resData, tmp)
	}

	if err != nil {
		c.RWMutex.Lock()
		defer c.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = resData
	return
}

// Launch ...
// @Title Launch
// @Description create server from snapshot
// @Param	snapshotID		string	false	"Snapshot ID"
// @Param	serverName		string	false	"New Server Name"
// @Param	flavorID	    string	false	"New Server FlavorID"
// @Param	customFlavor	string	false	"customFlavor"
// @Success 200 {server} server
// @Failure 403 body is empty
// @router /launch [post]
func (c *SnapshotController) Launch() {
	claims, _ := c.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		c.Data["json"] = response
		c.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(c.Ctx.Request.Body)
	bodyString := []string{
		"customFlavor",
		"snapshotID",
		"serverName",
		"keypairName",
		"volumeSize",
		"instance_password",
		"instance_root",
		"flavorID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	instancePassword := bodyResult["instance_password"].(string)
	instanceRoot := bodyResult["instance_root"].(string)

	core, _ := ioutil.ReadFile("files/root.txt")
	rootName, _ := ioutil.ReadFile("files/rootName.txt")
	rootPass, _ := ioutil.ReadFile("files/rootPass.txt")

	var userData []byte

	if instancePassword != "" && instanceRoot != "" {
		userData = make([]byte, 0)
		userData = append(userData, core...)
		if instanceRoot != "" && instancePassword != "" {
			userData = append(userData, []byte(rootName)...)
			userData = append(userData, []byte(instanceRoot)...)
			userData = append(userData, rootPass...)
			userData = append(userData, []byte(instancePassword)...)
		}
	}

	fmt.Println("--------------------")
	fmt.Println("--------------------")
	fmt.Println(string(userData))
	fmt.Println("--------------------")
	fmt.Println("--------------------")

	data := bodyResult["customFlavor"].(map[string]interface{})
	length := len(bodyResult["customFlavor"].(map[string]interface{}))
	// network, _ := networking.GetNetworking(claims["username"].(string)).IDFromName("default-private-network")
	network := beego.AppConfig.String("public.network.id")
	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)
	flavorID := bodyResult["flavorID"].(string)

	if length != 0 {

		flavor, err := compute.GetCompute(claims["username"].(string)).CreateFlavor(bodyResult["serverName"].(string)+"_flavor", int(data["ram"].(float64)), int(data["vcpu"].(float64)), false)
		snapshot, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetSnapshot(bodyResult["snapshotID"].(string))
		volume, err := blockstorage.GetBlockstorage(claims["username"].(string)).CreateVolumeFromSnapshot(int(bodyResult["volumeSize"].(float64)), bodyResult["snapshotID"].(string), "Volume for "+snapshot.Name, "Volume used for "+snapshot.Name)
		server, serverErr := compute.GetCompute(claims["username"].(string)).CreateServerFromExistVolume(volume.ID, bodyResult["serverName"].(string), flavor.ID, network, bodyResult["keypairName"].(string), userData)
		c.CreateLog(helper.OPENSTACK, helper.Snapshot, snapshot.Name, snapshot.ID, helper.SnapshotLaunch, serverErr)

		if serverErr != nil {
			c.RWMutex.Lock()
			defer c.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 100
			response.ErrorMsg = err.Error()
			return
		}
		response.StatusCode = 0
		response.ErrorMsg = ""
		response.Body = server
		return
	}
	snapshot, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetSnapshot(bodyResult["snapshotID"].(string))
	volume, err := blockstorage.GetBlockstorage(claims["username"].(string)).CreateVolumeFromSnapshot(int(bodyResult["volumeSize"].(float64)), bodyResult["snapshotID"].(string), "Volume for "+snapshot.Name, "Volume used for "+snapshot.Name)
	server, serverErr := compute.GetCompute(claims["username"].(string)).CreateServerFromExistVolume(volume.ID, bodyResult["serverName"].(string), bodyResult["flavorID"].(string), network, bodyResult["keypairName"].(string), userData)
	logID := c.CreateLog(helper.OPENSTACK, helper.Snapshot, snapshot.Name, snapshot.ID, helper.SnapshotLaunch, serverErr)
	flavorRes, errGetFlavor := compute.GetCompute(claims["username"].(string)).GetFlavor(flavorID)
	if errGetFlavor != nil {
		c.RWMutex.Lock()
		defer c.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
	}
	if serverErr == nil {
		go func(serverid string, username string, logID int64) {
			provider := models.GetProvider(username)
			for {
				server, _ := servers.Get(models.GetClientCompute(provider), serverid).Extract()
				if server.Status == "ACTIVE" && len(server.Addresses[beego.AppConfig.String("public.network.name")].([]interface{})) > 0 && len(server.AttachedVolumes) > 0 {
					models.CreateUsageAction(uint32(SysUserID), OsUserID, "Instance", server.Name, server.ID, server.ID, flavorRes.Name, "ACTIVE", "", logID, time.Now(), time.Time{}, 0, flavorRes.VCPUs, flavorRes.RAM)

					for _, l := range server.Addresses[beego.AppConfig.String("public.network.name")].([]interface{}) {
						tmp := l.(map[string]interface{})
						models.CreateUsageAction(uint32(SysUserID), OsUserID, "IP", tmp["addr"].(string), "", server.ID, "", "ACTIVE", tmp["addr"].(string), logID, time.Now(), time.Time{}, 0, 0, 0)
					}

					for _, volume := range server.AttachedVolumes {
						models.CreateUsageAction(uint32(SysUserID), OsUserID, "Volume", volume.ID, volume.ID, server.ID, "", "ACTIVE", "", logID, time.Now(), time.Time{}, snapshot.Size, 0, 0)
					}

					fmt.Println(server.AttachedVolumes)
					return
				}
				if server.Status == "ERROR" {
					return
				}
				time.Sleep(3 * time.Second)
			}
		}(server.ID, claims["username"].(string), logID)
	}

	if err != nil {
		c.RWMutex.Lock()
		defer c.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = server
	return
}

// Delete ...
// @Title Delete Snapshot
// @Description DeleteSnapshot
// @Param	snapshotID	body	string	false	"Snapshot ID"
// @Success 201 {server} server
// @Failure 403
// @router /delete [delete]
func (c *SnapshotController) Delete() {
	claims, err := c.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	osTenantID := c.GetHeaderValue("OS-Tenant-ID")
	osUserID := claims["userID"].(string)

	defer func() {
		c.Data["json"] = response
		c.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(c.Ctx.Request.Body)
	bodyString := []string{
		"snapshotID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	SysUserID := claims["sysUserID"].(float64)

	snapIds := bodyResult["snapshotID"].([]interface{})

	for _, id := range snapIds {
		snapshot, errSnapshot := blockstorage.GetBlockstorage(claims["username"].(string)).GetSnapshot(id.(string))
		if errSnapshot != nil {
			c.RWMutex.Lock()
			defer c.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errSnapshot.Error())
			response.StatusCode = 100
			response.ErrorMsg = errSnapshot.Error()
			return
		}

		errDelete := blockstorage.GetBlockstorage(claims["username"].(string)).DeleteSnapshot(id.(string))
		models.MinusUsageQuotaSnapshot(osTenantID, osUserID)

		logID := c.CreateLog(helper.OPENSTACK, helper.Snapshot, snapshot.Name, snapshot.ID, helper.Create, errDelete)

		if errDelete == nil {
			go func(snapID, username string, logID uint32) {
				for {
					snapGet, errGet := blockstorage.GetBlockstorage(username).GetSnapshot(snapID)
					if errGet != nil {
						if errGet.Error() == "Resource not found" {
							o := orm.NewOrm()
							_, errTable := o.QueryTable("usg_history").Filter("os_resource_id", snapID).Filter("end_date", "").Update(orm.Params{
								"sys_user_id":        SysUserID,
								"os_user_id":         osUserID,
								"status":             "DELETED",
								"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
								"ip":                 "",
								"last_log_action_id": logID,
							})
							if errTable != nil {
								c.RWMutex.Lock()
								defer c.RWMutex.Unlock()
								utils.SetLumlog(claims["email"].(string))
								logger.Error(errTable.Error())
							}
							return
						}
						c.RWMutex.Lock()
						defer c.RWMutex.Unlock()
						utils.SetLumlog(claims["email"].(string))
						logger.Error(errGet.Error())
					}

					if snapGet.Status == "error" {
						return
					}
					time.Sleep(1 * time.Second)
				}
			}(id.(string), claims["username"].(string), uint32(logID))
		}

		if err != nil {
			c.RWMutex.Lock()
			defer c.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 100
			response.ErrorMsg = err.Error()
			return
		}
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}
