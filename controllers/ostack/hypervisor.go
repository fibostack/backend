package ostack

import (
	"time"

	"github.com/astaxie/beego"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/hypervisors"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// HypervisorList ...
type HypervisorList struct {
	CPUInfo            hypervisors.CPUInfo `json:"-"`
	CurrentWorkload    int                 `json:"current_workload"`
	Status             string              `json:"status"`
	State              string              `json:"state"`
	DiskAvailableLeast int                 `json:"disk_available_least"`
	HostIP             string              `json:"host_ip"`
	FreeDiskGB         int                 `json:"free_disk_gb"`
	FreeRAMMB          int                 `json:"free_ram_mb"`
	HypervisorHostname string              `json:"hypervisor_hostname"`
	HypervisorType     string              `json:"hypervisor_type"`
	HypervisorVersion  int                 `json:"-"`
	ID                 string              `json:"id"`
	LocalGB            int                 `json:"local_gb"`
	LocalGBUsed        int                 `json:"local_gb_used"`
	MemoryMB           int                 `json:"memory_mb"`
	MemoryMBUsed       int                 `json:"memory_mb_used"`
	RunningVMs         int                 `json:"running_vms"`
	Service            hypervisors.Service `json:"service"`
	VCPUs              int                 `json:"vcpus"`
	VCPUsUsed          int                 `json:"vcpus_used"`
}

// HypervisorController struct
type HypervisorController struct {
	shared.BaseController
}

// URLMapping ...
func (h *HypervisorController) URLMapping() {
	h.Mapping("List", h.List)
	h.Mapping("Show", h.Show)
	h.Mapping("Statistics", h.Statistics)
	h.Mapping("ShowUptime", h.ShowUptime)
	h.Mapping("TenantUsage", h.TenantUsage)
}

// Prepare ...
func (h *HypervisorController) Prepare() {
	_, tokenErr := h.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		h.Data["Data"] = jsons
		h.ServeJSON()
	}
}

// List ...
// @Title ListHypervisors
// @Description hint ListHypervisors
// @Failure 403
// @router /list [get]
func (h *HypervisorController) List() {

	var response models.BaseResponse
	logger := utils.GetLogger()

	opts := gophercloud.AuthOptions{
		IdentityEndpoint: beego.AppConfig.String("identityEndpoint"),
		Username:         beego.AppConfig.String("username"),
		Password:         beego.AppConfig.String("password"),
		TenantID:         beego.AppConfig.String("tenantID"),
		DomainID:         "default",
	}
	//authenticate to openstack
	adminProvider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	defer func() {
		h.Data["json"] = response
		h.ServeJSON()
	}()

	var resMain []HypervisorList
	hypervisorList, err := compute.GetCompute(beego.AppConfig.String("username")).ListHypervisors(adminProvider)
	for _, i := range hypervisorList {
		each := HypervisorList{
			CPUInfo:            i.CPUInfo,
			CurrentWorkload:    i.CurrentWorkload,
			Status:             i.Status,
			State:              i.State,
			DiskAvailableLeast: i.DiskAvailableLeast,
			HostIP:             i.HostIP,
			FreeDiskGB:         i.FreeDiskGB,
			FreeRAMMB:          i.FreeRamMB,
			HypervisorHostname: i.HypervisorHostname,
			HypervisorType:     i.HypervisorType,
			HypervisorVersion:  i.HypervisorVersion,
			ID:                 i.ID,
			LocalGB:            i.LocalGB,
			LocalGBUsed:        i.LocalGBUsed,
			MemoryMB:           i.MemoryMB,
			MemoryMBUsed:       i.MemoryMBUsed,
			RunningVMs:         i.RunningVMs,
			Service:            i.Service,
			VCPUs:              i.VCPUs,
			VCPUsUsed:          i.VCPUsUsed,
		}

		resMain = append(resMain, each)
	}

	if err != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = resMain
	return
}

// Show ...
// @Title Show
// @Description hint Show
// @Param	hypervisor_id 	string	true	hypervisor_id"
// @Failure 403
// @router /show [post]
func (h *HypervisorController) Show() {
	claims, _ := h.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(h.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		h.Data["json"] = response
		h.ServeJSON()
	}()

	opts := gophercloud.AuthOptions{
		IdentityEndpoint: beego.AppConfig.String("identityEndpoint"),
		Username:         beego.AppConfig.String("username"),
		Password:         beego.AppConfig.String("password"),
		TenantID:         beego.AppConfig.String("tenantID"),
		DomainID:         "default",
	}
	//authenticate to openstack
	adminProvider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	bodyString := []string{
		"hypervisor_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	hypervisorList, err := compute.GetCompute(beego.AppConfig.String("username")).ShowHypervisor(adminProvider, bodyResult["hypervisor_id"].(string))

	if err != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = hypervisorList
	return
}

// Statistics ...
// @Title Statistics
// @Description hint Statistics
// @Failure 403
// @router /statistics [get]
func (h *HypervisorController) Statistics() {
	claims, _ := h.CheckToken()
	logger := utils.GetLogger()

	var response models.BaseResponse

	defer func() {
		h.Data["json"] = response
		h.ServeJSON()
	}()

	adminProvider, adminError := shared.AuthOSAdmin()
	if adminError != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(adminError.Error())
		response.StatusCode = 100
		response.ErrorMsg = adminError.Error()
		return
	}

	hypervisorList, err := compute.GetCompute(claims["username"].(string)).ShowHypervisorStatistics(adminProvider)

	if err != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = hypervisorList
	return
}

// ShowUptime ...
// @Title ShowUptime
// @Description hint Update
// @Param	hypervisor_id 	string	true	hypervisor_id"
// @Failure 403
// @router /showUptime [post]
func (h *HypervisorController) ShowUptime() {
	claims, _ := h.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(h.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		h.Data["json"] = response
		h.ServeJSON()
	}()

	bodyString := []string{
		"hypervisor_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	hypervisorList, err := compute.GetCompute(claims["username"].(string)).ShowHypervisorUptime(bodyResult["hypervisor_id"].(string))

	if err != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = hypervisorList
	return
}

// TenantUsage ...
// @Title TenantUsage
// @Description hint TenantUsage
// @Param   startTime   time    true "startTime"
// @Param   endTime     time    true "endTime"
// @Failure 403
// @router /tenantUsage [post]
func (h *HypervisorController) TenantUsage() {
	claims, _ := h.CheckToken()
	OsTenantID := h.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(h.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		h.Data["json"] = response
		h.ServeJSON()
	}()

	bodyString := []string{
		"hypervisor_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	startTime, err := time.Parse(time.RFC3339, bodyResult["startTime"].(string))
	endTime, err := time.Parse(time.RFC3339, bodyResult["endTime"].(string))

	usageSingle, err := compute.GetCompute(claims["username"].(string)).UsageSingleTenant(OsTenantID, startTime, endTime)

	if err != nil {
		h.RWMutex.Lock()
		defer h.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = usageSingle
	return
}
