package ostack

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/images"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/integration/imageservice"
	"gitlab.com/fibo-stack/backend/models"
)

// ImageServiceController struct
type ImageServiceController struct {
	shared.BaseController
}

// URLMapping ...
func (i *ImageServiceController) URLMapping() {
	i.Mapping("ListImages", i.ListImages)
	i.Mapping("CreateImage", i.CreateImage)
	i.Mapping("UpdateImage", i.UpdateImage)
	i.Mapping("DeleteImage", i.DeleteImage)
	i.Mapping("ListMembers", i.ListMembers)
	i.Mapping("AddMemberToImage", i.AddMemberToImage)
	i.Mapping("UpdateStatusOfMember", i.UpdateStatusOfMember)
	i.Mapping("DeleteMemberFromImage", i.DeleteMemberFromImage)
	i.Mapping("GetInfoAboutImportAPI", i.GetInfoAboutImportAPI)
	i.Mapping("CreateImageImport", i.CreateImageImport)
	i.Mapping("UploadImageData", i.UploadImageData)
	i.Mapping("StageImageData", i.StageImageData)
	i.Mapping("DownloadImageData", i.DownloadImageData)
	i.Mapping("ListTasks", i.ListTasks)
	i.Mapping("GetTask", i.GetTask)
	i.Mapping("CreateTask", i.CreateTask)
	i.Mapping("ListDiskFormat", i.ListDiskFormat)
}

// Prepare ...
func (i *ImageServiceController) Prepare() {
	_, tokenErr := i.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		i.Data["Data"] = jsons
		i.ServeJSON()
	}
}

// ListImages ...
// @Title ListImages
// @Description hint ListImages
// @Failure 403
// @router /listImages [get]
func (i *ImageServiceController) ListImages() {
	claims, _ := i.CheckToken()
	var response models.BaseResponse
	OsTenantID := i.GetHeaderValue("OS-Tenant-ID")
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	image, err := imageservice.GetImageService(claims["username"].(string)).ListImages(OsTenantID)
	var images []images.Image

	for _, img := range image {
		if !strings.HasPrefix(img.Name, "fibo_") {
			images = append(images, img)
		}
	}

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.Body = images
	return
}

// CreateImage ...
// @Title CreateImage
// @Description hint CreateImage
// @Param	name 	string	true		"name"
// @Param	minDisk 	string	true		"minDisk"
// @Param	minRam 	string	true		"minRam"
// @Param	disk_format 	string	true		"disk_format"
// @Param	container_format 	string	true		"container_format"
// @Param	visible 	string	true		"visible"
// @Failure 403
// @router /createImage [post]
func (i *ImageServiceController) CreateImage() {
	claims, _ := i.CheckToken()

	file, handler, errFile := i.Ctx.Input.Context.Request.FormFile("image_file")
	name := i.Ctx.Input.Context.Request.FormValue("name")
	minRAMStr := i.Ctx.Input.Context.Request.FormValue("min_ram")
	minDiskStr := i.Ctx.Input.Context.Request.FormValue("min_disk")
	diskFormat := i.Ctx.Input.Context.Request.FormValue("disk_format")
	containerFormat := i.Ctx.Input.Context.Request.FormValue("container_format")
	visible := i.Ctx.Input.Context.Request.FormValue("visible")

	minRAM, err := strconv.Atoi(minRAMStr)
	minDisk, err := strconv.Atoi(minDiskStr)

	var visibility images.ImageVisibility

	if visible == "public" {
		visibility = images.ImageVisibilityPrivate
	} else if visible == "private" {
		visibility = images.ImageVisibilityPrivate
	}

	fmt.Println(name)

	if errFile != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(errFile.Error())
		return
	}

	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	tempFile, err := ioutil.TempFile("temp-images", "*."+handler.Filename)
	if err != nil {
		fmt.Println("err::", err)
	}

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)

	var response models.BaseResponse
	defer func() {
		os.Remove(tempFile.Name())
		i.Data["json"] = response
		i.ServeJSON()
	}()

	fmt.Println("tempfile", tempFile)
	fmt.Println("name", handler.Filename)
	fmt.Println("diskFormat", diskFormat)
	fmt.Println("containerFormat", containerFormat)
	fmt.Println("visibility", visibility)
	fmt.Println("minDisk", minDisk)
	fmt.Println("minRAM", minRAM)

	image, err := imageservice.GetImageService(claims["username"].(string)).CreateImage(name, diskFormat, containerFormat, visibility, minDisk, minRAM)
	errUpload := imageservice.GetImageService(claims["username"].(string)).UploadImageData(image.ID, tempFile.Name())

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	if errUpload != nil {
		response.StatusCode = 1
		response.ErrorMsg = errUpload.Error()
		return
	}
	response.StatusCode = 0
	response.Body = image
	return
}

// UpdateImage ...
// @Title UpdateImage
// @Description hint UpdateImage
// @Param	imageID 	string	true		"imageID"
// @Param	newName 	string	true		"newName"
// @Failure 403
// @router /updateImage [put]
func (i *ImageServiceController) UpdateImage() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
		"newName",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	image, err := imageservice.GetImageService(claims["username"].(string)).UpdateImage(bodyResult["imageID"].(string), bodyResult["newName"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = image
	return
}

// DeleteImage ...
// @Title DeleteImage
// @Description hint DeleteImage
// @Param	imageID 	string	true		"imageID"
// @Failure 403
// @router /deleteImage [delete]
func (i *ImageServiceController) DeleteImage() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := imageservice.GetImageService(claims["username"].(string)).DeleteImage(bodyResult["imageID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	return
}

// ListMembers ...
// @Title ListMembers
// @Description hint ListMembers
// @Param	imageID 	string	true		"imageID"
// @Failure 403
// @router /listMembers [post]
func (i *ImageServiceController) ListMembers() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	members, err := imageservice.GetImageService(claims["username"].(string)).ListMembers(bodyResult["imageID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = members
	return
}

// AddMemberToImage ...
// @Title AddMemberToImage
// @Description hint AddMemberToImage
// @Param	projectID 	string	true		"projectID"
// @Param	imageID 	string	true		"imageID"
// @Failure 403
// @router /addMemberToImage [post]
func (i *ImageServiceController) AddMemberToImage() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
		"projectID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	member, err := imageservice.GetImageService(claims["username"].(string)).AddMemberToImage(bodyResult["projectID"].(string), bodyResult["imageID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = member
	return
}

// UpdateStatusOfMember ...
// @Title UpdateStatusOfMember
// @Description hint UpdateStatusOfMember
// @Param	projectID 	string	true		"projectID"
// @Param	imageID 	string	true		"imageID"
// @Param	status 	string	true		"status"
// @Failure 403
// @router /updateStatusOfMember [post]
func (i *ImageServiceController) UpdateStatusOfMember() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
		"projectID",
		"status",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	member, err := imageservice.GetImageService(claims["username"].(string)).UpdateStatusOfMember(bodyResult["projectID"].(string), bodyResult["imageID"].(string), bodyResult["status"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = member
	return
}

// DeleteMemberFromImage ...
// @Title DeleteMemberFromImage
// @Description hint DeleteMemberFromImage
// @Param	projectID 	string	true		"projectID"
// @Param	imageID 	string	true		"imageID"
// @Failure 403
// @router /deleteMemberFromImage [delete]
func (i *ImageServiceController) DeleteMemberFromImage() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
		"projectID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := imageservice.GetImageService(claims["username"].(string)).DeleteMemberFromImage(bodyResult["projectID"].(string), bodyResult["imageID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	return
}

// GetInfoAboutImportAPI ...
// @Title GetInfoAboutImportAPI
// @Description hint GetInfoAboutImportAPI
// @Failure 403
// @router /downloadImageData [get]
func (i *ImageServiceController) GetInfoAboutImportAPI() {
	claims, _ := i.CheckToken()
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	importInfo, err := imageservice.GetImageService(claims["username"].(string)).GetInfoAboutImportAPI()

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = importInfo
	return
}

// CreateImageImport ...
// @Title CreateImageImport
// @Description hint CreateImageImport
// @Param	imageID 	string	true		"imageID"
// @Param	uri 	string	true		"uri"
// @Failure 403
// @router /createImageImport [post]
func (i *ImageServiceController) CreateImageImport() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
		"uri",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := imageservice.GetImageService(claims["username"].(string)).CreateImageImport(bodyResult["imageID"].(string), bodyResult["uri"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	return
}

// UploadImageData ...
// @Title UploadImageData
// @Description hint UploadImageData
// @Param	imageID 	string	true		"imageID"
// @Param	pathFile 	string	true		"pathFile"
// @Failure 403
// @router /uploadImageData [post]
func (i *ImageServiceController) UploadImageData() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
		"uri",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := imageservice.GetImageService(claims["username"].(string)).UploadImageData(bodyResult["imageID"].(string), bodyResult["pathFile"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	return
}

// StageImageData ...
// @Title StageImageData
// @Description hint StageImageData
// @Param	imageID 	string	true		"imageID"
// @Param	pathFile 	string	true		"pathFile"
// @Failure 403
// @router /stageImageData [post]
func (i *ImageServiceController) StageImageData() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
		"pathFile",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := imageservice.GetImageService(claims["username"].(string)).StageImageData(bodyResult["imageID"].(string), bodyResult["pathFile"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	return
}

// DownloadImageData ...
// @Title DownloadImageData
// @Description hint DownloadImageData
// @Param	imageID 	string	true		"imageID"
// @Failure 403
// @router /downloadImageData [post]
func (i *ImageServiceController) DownloadImageData() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"imageID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	imageData, err := imageservice.GetImageService(claims["username"].(string)).DownloadImageData(bodyResult["imageID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = imageData
	return
}

// ListTasks ...
// @Title ListTasks
// @Description hint ListTasks
// @Param	projectID 	string	true		"projectID"
// @Failure 403
// @router /listTasks [post]
func (i *ImageServiceController) ListTasks() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"projectID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	tasks, err := imageservice.GetImageService(claims["username"].(string)).ListTasks(bodyResult["projectID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = tasks
	return
}

// GetTask ...
// @Title GetTask
// @Description hint GetTask
// @Param	taskID 	string	true		"taskID"
// @Failure 403
// @router /getTask [post]
func (i *ImageServiceController) GetTask() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"taskID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	task, err := imageservice.GetImageService(claims["username"].(string)).GetTask(bodyResult["taskID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = task
	return
}

// CreateTask ...
// @Title CreateTask
// @Description hint CreateTask
// @Param 	importFrom	string	true	"importFrom"
// @Param	importFromFormat string	true	"importFromFormat"
// @Param	containerFormat	string	true	"containerFormat"
// @Param	diskFormat	string	true	"diskFormat"
// @Failure 403
// @router /createTask [post]
func (i *ImageServiceController) CreateTask() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"importFrom",
		"importFromFormat",
		"containerFormat",
		"diskFormat",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	tasks, err := imageservice.GetImageService(claims["username"].(string)).CreateTask(bodyResult["importFrom"].(string), bodyResult["importFromFormat"].(string), bodyResult["containerFormat"].(string), bodyResult["diskFormat"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = tasks
	return
}

// ListDiskFormat ...
// @Title ListDiskFormat
// @Description hint ListDiskFormat
// @Failure 403
// @router /listImageData [get]
func (i *ImageServiceController) ListDiskFormat() {
	var main []shared.ChildKeyValue
	var response models.BaseResponse
	var retVal = make(map[string]interface{})
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	var childAmi shared.ChildKeyValue
	childAmi.Key = "ami"
	childAmi.Value = "Amazon Machine Image"

	var childAri shared.ChildKeyValue
	childAri.Key = "ari"
	childAri.Value = "Amazon Ramdisk Image"

	var childAki shared.ChildKeyValue
	childAki.Key = "aki"
	childAki.Value = "Amazon Kernel Image"

	var childVhd shared.ChildKeyValue
	childVhd.Key = "vhd"
	childVhd.Value = "Virtual Hard Disk"

	var childVmdk shared.ChildKeyValue
	childVmdk.Key = "vmdk"
	childVmdk.Value = "Virtual Machine Disk"

	var childRaw shared.ChildKeyValue
	childRaw.Key = "raw"
	childRaw.Value = ""

	var childQcow2 shared.ChildKeyValue
	childQcow2.Key = "qcow2"
	childQcow2.Value = "QEMU Emulator"

	var childVdi shared.ChildKeyValue
	childVdi.Key = "vdi"
	childVdi.Value = "Virtual Disk Image"

	var childIso shared.ChildKeyValue
	childIso.Key = "iso"
	childIso.Value = "Optical Disk Image"

	main = append(main, childIso)
	main = append(main, childVdi)
	main = append(main, childQcow2)
	main = append(main, childRaw)
	main = append(main, childVmdk)
	main = append(main, childVhd)
	main = append(main, childAki)
	main = append(main, childAmi)
	main = append(main, childAri)

	// Valid values are ami, ari, aki, bare, and ovf.
	containerFormats := [5]string{"ami", "ari", "aki", "bare", "ovf"}
	// Valid values are ami, ari, aki, bare, and ovf.
	visible := [2]images.ImageVisibility{images.ImageVisibilityPrivate, images.ImageVisibilityPublic}

	retVal["disk_formats"] = main
	retVal["container_formats"] = containerFormats
	retVal["visible"] = visible
	response.StatusCode = 0
	response.Body = retVal
	return
}
