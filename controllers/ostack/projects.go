package ostack

import (
	"fmt"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"

	blockquota "github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/quotasets"
	computequota "github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/quotasets"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/identity"
)

// MembersStruct ...
type MembersStruct struct {
	OsUserID string  `json:"os_user_id"`
	Email    string  `json:"email"`
	RoleID   float64 `json:"role_id"`
	IsRemove bool    `json:"is_remove"`
}

// ProjectsController struct
type ProjectsController struct {
	shared.BaseController
}

// URLMapping ...
func (p *ProjectsController) URLMapping() {
	p.Mapping("List", p.List)
	p.Mapping("Create", p.Create)
	p.Mapping("Update", p.Update)
	p.Mapping("Get", p.Get)
	p.Mapping("Delete", p.Delete)
	p.Mapping("ManageMembersCreate", p.ManageMembersCreate)
	p.Mapping("ManageMembersDelete", p.ManageMembersDelete)
	p.Mapping("ManageMembersUpdate", p.ManageMembersUpdate)
}

// Prepare ...
func (p *ProjectsController) Prepare() {
	_, tokenErr := p.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		p.Data["Data"] = jsons
		p.ServeJSON()
	}
}

// List ...
// @Title ListProject
// @Description hint ListProject
// @Failure 403
// @router /list [get]
func (p *ProjectsController) List() {
	claims, _ := p.CheckToken()
	logger := utils.GetLogger()
	utils.SetLumlog(claims["email"].(string))

	var response models.BaseResponse

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	adminProvider, err := shared.AuthOSAdmin()
	projects, err := identity.GetKeystone(claims["username"].(string)).ListProjects(adminProvider)

	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = projects
	return
}

// Create ...
// @Title Create
// @Description hint Create
// @Param	name body	string	true	projectName"
// @Param	description body	string	true	description"
// @Param instance body int true "instance"
// @Param cpu body int true "cpu"
// @Param ram body int true "ram"
// @Param keypair body int true "keypair"
// @Param volume body int true "volume"
// @Param snapshot body int true "snapshot"
// @Param volume_size body int true "volume_size"
// @Param external_ip body int true "external_ip"
// @Param security_group body int true "security_group"
// @Failure 403
// @router /create [post]
func (p *ProjectsController) Create() {
	claims, _ := p.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"name",
		"description",
		"instance",
		"cpu",
		"ram",
		"keypair",
		"volume",
		"snapshot",
		"volume_size",
		"external_ip",
		"security_group",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	instances := int(bodyResult["instance"].(float64))
	cpu := int(bodyResult["cpu"].(float64))
	ram := int(bodyResult["ram"].(float64))
	keypair := int(bodyResult["keypair"].(float64))
	volume := int(bodyResult["volume"].(float64))
	snapshot := int(bodyResult["snapshot"].(float64))
	volumeSize := int(bodyResult["volume_size"].(float64))
	floatingIps := int(bodyResult["external_ip"].(float64))
	securityGroup := int(bodyResult["security_group"].(float64))

	adminProvider, err := shared.AuthOSAdmin()
	projectName := bodyResult["name"].(string)
	description := bodyResult["description"].(string)

	project, err := identity.GetKeystone(claims["username"].(string)).CreateProject(projectName, description, adminProvider)
	p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.Create, err)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	_, computeErr := compute.GetCompute("admin").SetQuotaset(adminProvider, project.ID, floatingIps, cpu, ram, keypair, instances, securityGroup)
	if computeErr != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(computeErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = computeErr.Error()
		return
	}

	_, blockErr := blockstorage.GetBlockstorage("admin").SetQuotaset(adminProvider, project.ID, volume, snapshot, volumeSize)
	if blockErr != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(blockErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = blockErr.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = project
	return
}

// Update ...
// @Title Update
// @Description hint Update
// @Param	project_id 	string	true	"project_id"
// @Param	name 	string	true	"name"
// @Param	description 	string	true	"description"
// @Param	enabled 	bool	true	"enabled"
// @Param   instance body int true "instance"
// @Param   cpu body int true "cpu"
// @Param   ram body int true "ram"
// @Param   keypair body int true "keypair"
// @Param   volume body int true "volume"
// @Param   snapshot body int true "snapshot"
// @Param   volume_size body int true "volume_size"
// @Param   external_ip body int true "external_ip"
// @Param   security_group body int true "security_group"
// @Failure 403
// @router /update [put]
func (p *ProjectsController) Update() {
	claims, _ := p.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"project_id",
		"name",
		"description",
		"enabled",
		"instance",
		"cpu",
		"ram",
		"keypair",
		"volume",
		"snapshot",
		"volume_size",
		"external_ip",
		"security_group",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	adminProvider, err := shared.AuthOSAdmin()
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	projectID := bodyResult["project_id"].(string)
	name := bodyResult["name"].(string)
	description := bodyResult["description"].(string)
	enabled := bodyResult["enabled"].(bool)

	instances := int(bodyResult["instance"].(float64))
	cpu := int(bodyResult["cpu"].(float64))
	ram := int(bodyResult["ram"].(float64))
	keypair := int(bodyResult["keypair"].(float64))
	volume := int(bodyResult["volume"].(float64))
	snapshot := int(bodyResult["snapshot"].(float64))
	volumeSize := int(bodyResult["volume_size"].(float64))
	floatingIps := int(bodyResult["external_ip"].(float64))
	securityGroup := int(bodyResult["security_group"].(float64))

	project, err := identity.GetKeystone(claims["username"].(string)).UpdateProject(projectID, name, description, enabled, adminProvider)
	p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.Update, err)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	_, computeErr := compute.GetCompute("admin").SetQuotaset(adminProvider, projectID, floatingIps, cpu, ram, keypair, instances, securityGroup)
	p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.ProjectSetComputeQuota, computeErr)
	if computeErr != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(computeErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = computeErr.Error()
		return
	}

	_, blockErr := blockstorage.GetBlockstorage("admin").SetQuotaset(adminProvider, projectID, volume, snapshot, volumeSize)
	p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.ProjectSetBlockQuota, blockErr)
	if blockErr != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(blockErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = blockErr.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = project
	return
}

// Get ...
// @Title Get
// @Description Get Project detail
// @Param	project_id 	string	true	"project_id"
// @Failure 403
// @router /get [post]
func (p *ProjectsController) Get() {

	type ResponseMain struct {
		IsDomain      bool                        `json:"is_domain"`
		Description   string                      `json:"description"`
		DomainID      string                      `json:"domain_id"`
		Enabled       bool                        `json:"enabled"`
		ID            string                      `json:"id"`
		Name          string                      `json:"name"`
		ParentID      string                      `json:"parent_id"`
		Tags          []string                    `json:"tags"`
		ComputeQuota  computequota.QuotaDetailSet `json:"compute_quota"`
		VolumeQuota   blockquota.QuotaUsageSet    `json:"volume_quota"`
		ReservedQuota map[string]int              `json:"reserved_quota"`
	}

	var res ResponseMain

	claims, _ := p.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"project_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	projectID := bodyResult["project_id"].(string)

	project, err := identity.GetKeystone(claims["username"].(string)).GetProject(projectID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	res.IsDomain = project.IsDomain
	res.Description = project.Description
	res.DomainID = project.DomainID
	res.Enabled = project.Enabled
	res.ID = project.ID
	res.Name = project.Name
	res.ParentID = project.ParentID
	res.Tags = project.Tags

	computelimits, err := compute.GetCompute(claims["username"].(string)).GetDetailQuotaset(projectID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	res.ComputeQuota = computelimits

	storagelimits, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetQuotasetUsage(projectID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	res.VolumeQuota = storagelimits

	projects, errProjects := models.GetProjectRole(projectID)
	if errProjects != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errProjects.Error())
		response.StatusCode = 100
		response.ErrorMsg = errProjects.Error()
		return
	}

	reserve := make(map[string]int)
	reservedInstance := 0
	reservedCPU := 0
	reservedRAM := 0
	reservedKeypair := 0
	reservedVolume := 0
	reservedSnapshot := 0
	reservedVolumeSize := 0
	reservedExternalIP := 0
	reservedSecurityGroup := 0

	for _, project := range projects {
		userQuota, errQuota := models.GetQuota(project.QuotaID)
		if errQuota == nil {
			reservedInstance = reservedInstance + userQuota.Instances
			reservedCPU = reservedCPU + userQuota.CPU
			reservedRAM = reservedRAM + userQuota.RAM
			reservedKeypair = reservedKeypair + userQuota.Keypair
			reservedVolume = reservedVolume + userQuota.Volume
			reservedSnapshot = reservedSnapshot + userQuota.Snapshot
			reservedVolumeSize = reservedVolumeSize + userQuota.VolumeSize
			reservedExternalIP = reservedExternalIP + userQuota.ExternalIP
			reservedSecurityGroup = reservedSecurityGroup + userQuota.SecurityGroup
		}
	}

	reserve["instance"] = reservedInstance
	reserve["cpu"] = reservedCPU
	reserve["ram"] = reservedRAM
	reserve["keypair"] = reservedKeypair
	reserve["volume"] = reservedVolume
	reserve["snapshot"] = reservedSnapshot
	reserve["volume_size"] = reservedVolumeSize
	reserve["external_ip"] = reservedExternalIP
	reserve["security_group"] = reservedSecurityGroup

	res.ReservedQuota = reserve

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = res
	return
}

// Delete ...
// @Title Delete
// @Description hint Delete
// @Param	project_ids 	[]string	true	project_ids"
// @Failure 403
// @router /delete [delete]
func (p *ProjectsController) Delete() {
	claims, _ := p.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	var err error

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"project_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	adminProvider, err := shared.AuthOSAdmin()
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	for _, v := range bodyResult["project_ids"].([]interface{}) {
		project, getErr := identity.GetKeystone(claims["username"].(string)).GetProject(v.(string))
		if getErr != nil {
			response.StatusCode = 100
			response.ErrorMsg = getErr.Error()
			return
		}
		err = identity.GetKeystone(claims["username"].(string)).DeleteProject(v.(string), adminProvider)
		p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.Delete, err)
	}

	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ManageMembersCreate ...
// @Title Manage project members
// @Description hint Manage project members
// @Param	os_user_id body	string	true os_user_id
// @Param	email body	string	true email
// @Param	role_id body	int	true role_id
// @Param	project_id  body	string	true	project_id
// @Param	project_name  body	string	true	project_name
// @Param   instance body int true "instance"
// @Param   cpu body int true "cpu"
// @Param   ram body int true "ram"
// @Param   keypair body int true "keypair"
// @Param   volume body int true "volume"
// @Param   snapshot body int true "snapshot"
// @Param   volume_size body int true "volume_size"
// @Param   external_ip body int true "external_ip"
// @Param   security_group body int true "security_group"
// @Failure 403
// @router /member/create [post]
func (p *ProjectsController) ManageMembersCreate() {
	claims, _ := p.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"os_user_id",
		"email",
		"role_id",
		"project_id",
		"project_name",
		"instance",
		"cpu",
		"ram",
		"keypair",
		"volume",
		"snapshot",
		"volume_size",
		"external_ip",
		"security_group",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	adminProvider, adminErr := shared.AuthOSAdmin()
	if adminErr != nil {
		response.Body = false
		response.StatusCode = utils.StatusBadRequest
		return
	}

	memberRoleID := beego.AppConfig.String("member.role.id")

	projectID := bodyResult["project_id"].(string)
	projectName := bodyResult["project_name"].(string)
	email := bodyResult["email"].(string)
	roleID := bodyResult["role_id"].(float64)
	osUserID := bodyResult["os_user_id"].(string)

	instances := int(bodyResult["instance"].(float64))
	cpu := int(bodyResult["cpu"].(float64))
	ram := int(bodyResult["ram"].(float64))
	keypair := int(bodyResult["keypair"].(float64))
	volume := int(bodyResult["volume"].(float64))
	snapshot := int(bodyResult["snapshot"].(float64))
	volumeSize := int(bodyResult["volume_size"].(float64))
	floatingIps := int(bodyResult["external_ip"].(float64))
	securityGroup := int(bodyResult["security_group"].(float64))

	computelimits, err := compute.GetCompute(claims["username"].(string)).GetDetailQuotaset(projectID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	storagelimits, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetQuotasetUsage(projectID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	fmt.Println("computelimits.Instances.Limit", computelimits.Instances.Limit)
	fmt.Println("computelimits.Cores.Limit", computelimits.Cores.Limit)
	fmt.Println("computelimits.RAM.Limit", computelimits.RAM.Limit)
	fmt.Println("computelimits.KeyPairs.Limit", computelimits.KeyPairs.Limit)
	fmt.Println("computelimits.FloatingIPs.Limit", computelimits.FloatingIPs.Limit)
	fmt.Println("computelimits.SecurityGroupRules.Limit", computelimits.SecurityGroupRules.Limit)

	fmt.Println("storagelimits.Volumes.Limit", storagelimits.Volumes.Limit)
	fmt.Println("storagelimits.Snapshots.Limit", storagelimits.Snapshots.Limit)
	fmt.Println("storagelimits.Gigabytes.Limit", storagelimits.Gigabytes.Limit)

	if computelimits.Instances.Limit < instances ||
		computelimits.Cores.Limit < cpu ||
		computelimits.RAM.Limit < ram ||
		computelimits.KeyPairs.Limit < keypair ||
		computelimits.FloatingIPs.Limit < floatingIps ||
		storagelimits.Volumes.Limit < volume ||
		storagelimits.Snapshots.Limit < snapshot ||
		storagelimits.Gigabytes.Limit < volumeSize {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error("Your instance count quota has been exceeded")
		response.StatusCode = 100
		response.ErrorMsg = "Your instance count quota has been exceeded"
		response.Body = true
		return
	}

	var quota database.SysUserQuota
	quota.Instances = instances
	quota.CPU = cpu
	quota.RAM = ram
	quota.Keypair = keypair
	quota.Volume = volume
	quota.Snapshot = snapshot
	quota.VolumeSize = volumeSize
	quota.ExternalIP = floatingIps
	quota.SecurityGroup = securityGroup

	newQuota, quotaErr := models.CreateQuotas(quota)
	if quotaErr != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quotaErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = quotaErr.Error()
		return
	}

	project, getErr := identity.GetKeystone(claims["username"].(string)).GetProject(projectID)
	if getErr != nil {
		response.StatusCode = 100
		response.ErrorMsg = getErr.Error()
		return
	}

	errRole := identity.GetKeystone("admin").AssignRoleToUserInProjectAdmin(adminProvider, projectID, osUserID, memberRoleID)
	p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.ProjectAddMember, errRole)
	if errRole != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errRole.Error())
		response.StatusCode = 100
		response.ErrorMsg = errRole.Error()
		return
	}

	var projectRole database.UserProjectRole
	projectRole.Email = email
	projectRole.OsTenantID = projectID
	projectRole.OsUserID = osUserID
	projectRole.RoleID = uint32(roleID)
	projectRole.OsTenantName = projectName
	projectRole.QuotaID = newQuota.ID
	_, errProjectRole := models.CreateProjectRole(projectRole)
	if errProjectRole != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errProjectRole.Error())
		response.StatusCode = 100
		response.ErrorMsg = errProjectRole.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ManageMembersDelete ...
// @Title Manage project members
// @Description hint Manage project members
// @Param	os_user_id body string true "os_user_id"
// @Param	project_id  body	string	true	"project_id"
// @Failure 403
// @router /member/delete [delete]
func (p *ProjectsController) ManageMembersDelete() {
	claims, _ := p.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	o := orm.NewOrm()
	o.Begin()

	defer func() {
		if response.StatusCode == 0 {
			o.Commit()
		} else {
			o.Rollback()
		}
		p.Data["json"] = response
		p.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"project_id",
		"os_user_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	projectID := bodyResult["project_id"].(string)
	osUserIDs := bodyResult["os_user_ids"].([]interface{})

	project, getErr := identity.GetKeystone(claims["username"].(string)).GetProject(projectID)
	if getErr != nil {
		response.StatusCode = 100
		response.ErrorMsg = getErr.Error()
		return
	}

	var errors []error

	for _, osUserID := range osUserIDs {
		projectRole, projectRoleErr := models.GetProjectWithTenantAndUser(projectID, osUserID.(string))
		if projectRoleErr != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = projectRoleErr.Error()
			return
		}

		roles, err := identity.GetKeystone(claims["username"].(string)).ListAssignmentsOnResource(osUserID.(string), projectID)
		if err != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = err.Error()
			return
		}

		for _, role := range roles {
			errRole := identity.GetKeystone(claims["username"].(string)).UnAssignRoleToUserInProject(projectID, osUserID.(string), role.ID)
			p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.ProjectRemoveMember, errRole)
			if errRole != nil {
				errors = append(errors, errRole)
				logger.Error(errRole.Error())
				return
			}
		}

		quotaError := models.DeleteQuota(o, projectRole.QuotaID)
		if quotaError != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = err.Error()
			errors = append(errors, quotaError)
			logger.Error(quotaError.Error())
			return
		}
		roleErrpr := models.DeleteCreateProjectRoleTenant(o, projectID, osUserID.(string))
		if roleErrpr != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = err.Error()
			errors = append(errors, roleErrpr)
			logger.Error(roleErrpr.Error())
			return
		}
	}

	if len(errors) != 0 {
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = "Error occurred"
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ManageMembersUpdate ...
// @Title Manage project members
// @Description hint Manage project members
// @Param	role_id body	int	true role_id
// @Param	os_user_id body	string	true os_user_id
// @Param	project_id  body	string	true	project_id"
// @Param	quota_id  body	string	true	quota_id"
// @Param   instance body int true "instance"
// @Param   cpu body int true "cpu"
// @Param   ram body int true "ram"
// @Param   keypair body int true "keypair"
// @Param   volume body int true "volume"
// @Param   snapshot body int true "snapshot"
// @Param   volume_size body int true "volume_size"
// @Param   external_ip body int true "external_ip"
// @Param   security_group body int true "security_group"
// @Failure 403
// @router /member/update [put]
func (p *ProjectsController) ManageMembersUpdate() {
	logger := utils.GetLogger()
	claims, _ := p.CheckToken()
	utils.SetLumlog(claims["email"].(string))
	var response models.BaseResponse

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"os_user_id",
		"quota_id",
		"role_id",
		"project_id",
		"instance",
		"cpu",
		"ram",
		"keypair",
		"volume",
		"snapshot",
		"volume_size",
		"external_ip",
		"security_group",
	}

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	projectID := bodyResult["project_id"].(string)
	roleID := bodyResult["role_id"].(float64)
	osUserID := bodyResult["os_user_id"].(string)
	quotaID := uint32(bodyResult["quota_id"].(float64))

	instances := bodyResult["instance"].(float64)
	cpu := bodyResult["cpu"].(float64)
	ram := bodyResult["ram"].(float64)
	keypair := bodyResult["keypair"].(float64)
	volume := bodyResult["volume"].(float64)
	snapshot := bodyResult["snapshot"].(float64)
	volumeSize := bodyResult["volume_size"].(float64)
	floatingIps := bodyResult["external_ip"].(float64)
	securityGroup := bodyResult["security_group"].(float64)

	computelimits, err := compute.GetCompute(claims["username"].(string)).GetDetailQuotaset(projectID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	storagelimits, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetQuotasetUsage(projectID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	if computelimits.Instances.Limit < int(instances) ||
		computelimits.Cores.Limit < int(cpu) ||
		computelimits.RAM.Limit < int(ram) ||
		computelimits.KeyPairs.Limit < int(keypair) ||
		computelimits.FloatingIPs.Limit < int(floatingIps) ||
		computelimits.SecurityGroupRules.Limit < int(securityGroup) ||
		storagelimits.Volumes.Limit < int(volume) ||
		storagelimits.Snapshots.Limit < int(snapshot) ||
		storagelimits.Gigabytes.Limit < int(volumeSize) {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error("Your instance count quota has been exceeded")
		response.StatusCode = 100
		response.ErrorMsg = "Your instance count quota has been exceeded"
		response.Body = true
		return
	}

	project, getErr := identity.GetKeystone(claims["username"].(string)).GetProject(projectID)
	if getErr != nil {
		response.StatusCode = 100
		response.ErrorMsg = getErr.Error()
		return
	}

	updateErr := models.UpdateProjectRole(projectID, osUserID, uint32(roleID))
	p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.ProjectUpdateUserRole, updateErr)
	if updateErr != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		logger.Error(updateErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = updateErr.Error()
		return
	}

	quotaErr := models.UpdateQuota(quotaID, instances, cpu, ram, keypair, volume, snapshot, volumeSize, floatingIps, securityGroup)
	p.CreateLog(helper.OPENSTACK, helper.Project, project.Name, project.ID, helper.ProjectUpdateUserQuota, updateErr)
	if quotaErr != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		logger.Error(quotaErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = quotaErr.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ProjectUserQuota ...
// @Title ProjectUserQuota
// @Description Project user quota
// @Param	project_id body string true project_id
// @Param	os_user_id  body	string	true	os_user_id
// @Failure 403
// @router /user/quota [post]
func (p *ProjectsController) ProjectUserQuota() {
	logger := utils.GetLogger()
	claims, _ := p.CheckToken()
	utils.SetLumlog(claims["email"].(string))
	var response models.BaseResponse

	bodyResult := shared.RetrieveDataFromBody(p.Ctx.Request.Body)
	bodyString := []string{
		"project_id",
		"os_user_id",
	}

	defer func() {
		p.Data["json"] = response
		p.ServeJSON()
	}()

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	projectID := bodyResult["project_id"].(string)
	osUserID := bodyResult["os_user_id"].(string)

	quota, err := models.UserQuota(projectID, osUserID)
	if err != nil {
		p.RWMutex.Lock()
		defer p.RWMutex.Unlock()
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = quota
	return
}
