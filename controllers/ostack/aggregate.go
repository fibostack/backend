package ostack

import (
	"strconv"

	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/utils"

	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/models"
)

// AggregateController struct
type AggregateController struct {
	shared.BaseController
}

// URLMapping ...
func (a *AggregateController) URLMapping() {
	a.Mapping("CreateAggregate", a.CreateAggregate)
	a.Mapping("ShowAggregate", a.ShowAggregate)
	a.Mapping("DeleteAggregate", a.DeleteAggregate)
	a.Mapping("UpdateAggregate", a.UpdateAggregate)
	a.Mapping("ListAggregates", a.ListAggregates)
	a.Mapping("AddHost", a.AddHost)
	a.Mapping("RemoveAggregate", a.RemoveAggregate)
	a.Mapping("CreateOrUpdateMetadata", a.CreateOrUpdateMetadata)
}

// Prepare ...
func (a *AggregateController) Prepare() {
	_, tokenErr := a.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		a.Data["Data"] = jsons
		a.ServeJSON()
	}
}

// CreateAggregate ...
// @Title CreateAggregate
// @Description hint CreateAggregate
// @Param	name 	string	true		"name"
// @Param	az 	string	true		"az"
// @Failure 403
// @router /createAggregate [post]
func (a *AggregateController) CreateAggregate() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyString := []string{
		"ID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	aggregate, err := compute.GetCompute(claims["username"].(string)).CreateAggregate(bodyResult["name"].(string), bodyResult["az"].(string))

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = aggregate
	return
}

// ShowAggregate ...
// @Title ShowAggregate
// @Description hint ShowAggregate
// @Param	aggregateID 	string	true		"aggregateID"
// @Failure 403
// @router /showAggregate [post]
func (a *AggregateController) ShowAggregate() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyString := []string{
		"aggregateID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	id, err := strconv.Atoi(bodyResult["aggregateID"].(string))
	aggregate, err := compute.GetCompute(claims["username"].(string)).ShowAggregate(id)

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = aggregate
	return
}

// DeleteAggregate ...
// @Title DeleteAggregate
// @Description hint DeleteAggregate
// @Param	aggregateID 	string	true		"aggregateID"
// @Failure 403
// @router /deleteAggregate [delete]
func (a *AggregateController) DeleteAggregate() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyString := []string{
		"aggregateID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	id, _ := strconv.Atoi(bodyResult["aggregateID"].(string))
	err := compute.GetCompute(claims["username"].(string)).DeleteAggregate(id)

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// UpdateAggregate ...
// @Title UpdateAggregate
// @Description hint UpdateAggregate
// @Param	aggregateID 	string	true		"aggregateID"
// @Param	newName 	string	true		"newName"
// @Param	availabilityZone 	string	true		"availabilityZone"
// @Failure 403
// @router /updateAggregate [put]
func (a *AggregateController) UpdateAggregate() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyString := []string{
		"aggregateID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	id, err := strconv.Atoi(bodyResult["aggregateID"].(string))

	aggregate, err := compute.GetCompute(claims["username"].(string)).UpdateAggregate(id, bodyResult["newName"].(string), bodyResult["availabilityZone"].(string))

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = aggregate
	return
}

// ListAggregates ...
// @Title ListAggregates
// @Description hint ListAggregates
// @Failure 403
// @router /listAggregate [get]
func (a *AggregateController) ListAggregates() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	aggregate, err := compute.GetCompute(claims["username"].(string)).ListAggregates()

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = aggregate
	return
}

// AddHost ...
// @Title AddHost
// @Description hint AddHost
// @Param	aggregateID 	string	true		"aggregateID"
// @Param	hostName 	string	true		"hostName"
// @Failure 403
// @router /addHost [post]
func (a *AggregateController) AddHost() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyString := []string{
		"aggregateID",
		"hostName",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	id, err := strconv.Atoi(bodyResult["aggregateID"].(string))
	aggregate, err := compute.GetCompute(claims["username"].(string)).AddHost(id, bodyResult["hostName"].(string))

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = aggregate
	return
}

// RemoveAggregate ...
// @Title RemoveAggregate
// @Description hint RemoveAggregate
// @Param	aggregateID 	string	true		"aggregateID"
// @Param	hostName 	string	true		"hostName"
// @Failure 403
// @router /removeAggregate [delete]
func (a *AggregateController) RemoveAggregate() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyString := []string{
		"aggregateID",
		"hostName",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	id, err := strconv.Atoi(bodyResult["aggregateID"].(string))
	aggregate, err := compute.GetCompute(claims["username"].(string)).RemoveAggregate(id, bodyResult["hostName"].(string))

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = aggregate
	return
}

// CreateOrUpdateMetadata ...
// @Title CreateOrUpdateMetadata
// @Description hint CreateOrUpdateMetadata
// @Param	aggregateID 	string	true		"aggregateID"
// @Param	metadata 	string	true		"metadata"
// @Failure 403
// @router /createOrUpdateMetadata [post]
func (a *AggregateController) CreateOrUpdateMetadata() {
	claims, _ := a.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyString := []string{
		"aggregateID",
		"metadata",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	test := make(map[string]interface{})

	id, err := strconv.Atoi(bodyResult["aggregateID"].(string))
	test["metadata"] = bodyResult["metadata"].(string)
	aggregate, err := compute.GetCompute(claims["username"].(string)).CreateOrUpdateMetadata(id, test)

	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = aggregate
	return
}
