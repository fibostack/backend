package ostack

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/identity"
	"gitlab.com/fibo-stack/backend/utils"

	"gitlab.com/fibo-stack/backend/integration/monasca"
	"gitlab.com/fibo-stack/backend/utils/dao"

	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/fibo-stack/backend/models"
)

// ProjectService ...
type ProjectService struct {
	Username string
}

//VolumeInfo ...
type VolumeInfo struct {
	Size   int    `json:"size"`
	Device string `json:"device"`
}

// ExtInstanceInfo ...
type ExtInstanceInfo struct {
	models.InstanceInfo
	VolumeInfos []VolumeInfo `json:"volumes"`
}

// GetProjectService ...
func GetProjectService(username string) *ProjectService {
	return &ProjectService{
		Username: username,
	}
}

// GetInstanceCPUUsageList ...
//CPU 사용률
func (n ProjectService) GetInstanceCPUUsageList(request models.DetailReq) (result []map[string]interface{}, _ models.ErrMessage) {

	CPUUsageResp, err := dao.GetInstanceCPUUsageList(request)
	if err != nil {
		models.MonitLogger.Error(err)
		return result, err
	}
	CPUUsage, _ := utils.GetResponseConverter().InfluxConverterList(CPUUsageResp, models.MetricNameCPUUsage)

	datamap := CPUUsage[models.ResultDataName].([]map[string]interface{})
	for _, data := range datamap {

		swapFree := data["usage"].(json.Number)
		convertData, _ := strconv.ParseFloat(swapFree.String(), 64)
		//swap 사용률을 구한다. ( 100 -  freeUsage)
		data["usage"] = utils.RoundFloatDigit2(convertData)
	}
	result = append(result, CPUUsage)
	return result, nil

}

// GetInstanceMemoryUsageList ...
//Instance Memory Usage
func (n ProjectService) GetInstanceMemoryUsageList(request models.DetailReq) (result []map[string]interface{}, _ models.ErrMessage) {

	memoryResp, err := dao.GetInstanceMemoryUsageList(request)
	if err != nil {
		models.MonitLogger.Error(err)
		return result, err
	}
	memoryUsage, _ := utils.GetResponseConverter().InfluxConverter4Usage(memoryResp, models.MetricNameMemoryUsage)

	datamap := memoryUsage[models.ResultDataName].([]map[string]interface{})
	for _, data := range datamap {

		swapFree := data["usage"].(float64)
		data["usage"] = utils.RoundFloatDigit2(swapFree)
	}

	result = append(result, memoryUsage)
	return result, nil

}

// GetInstanceDiskIoKbyteList ...
//Disk IO Read Kbyte
func (n ProjectService) GetInstanceDiskIoKbyteList(request models.DetailReq, gubun string) (result []map[string]interface{}, _ models.ErrMessage) {

	memoryResp, err := dao.GetInstanceDiskIoKbyte(request, gubun)
	if err != nil {
		models.MonitLogger.Error(err)
		return result, err
	}
	var resultName string

	if gubun == "read" {
		resultName = models.MetricNameDiskReadKbyte
	} else {
		resultName = models.MetricNameDiskWriteKbyte
	}
	byte, _ := utils.GetResponseConverter().InfluxConverterList(memoryResp, resultName)

	datamap := byte[models.ResultDataName].([]map[string]interface{})
	fmt.Println(datamap)
	for _, data := range datamap {
		usage := utils.TypeCheckerFloat64(data["usage"]).(float64)
		data["usage"] = utils.RoundFloatDigit2(usage)
	}

	result = append(result, byte)
	return result, nil

}

// GetInstanceNetworkIoKbyteList ...
//Network IO Kbyte
func (n ProjectService) GetInstanceNetworkIoKbyteList(request models.DetailReq) (result []map[string]interface{}, _ models.ErrMessage) {

	networkInResp, err := dao.GetInstanceNetworkKbyte(request, "in")
	networkOutResp, err := dao.GetInstanceNetworkKbyte(request, "out")

	if err != nil {
		models.MonitLogger.Error(err)
		return result, err
	}

	inData, _ := utils.GetResponseConverter().InfluxConverterList(networkInResp, models.MetricNameNetworkIn)
	outData, _ := utils.GetResponseConverter().InfluxConverterList(networkOutResp, models.MetricNameNetworkOut)

	inDatamap := inData[models.ResultDataName].([]map[string]interface{})
	for _, data := range inDatamap {
		usage := utils.TypeCheckerFloat64(data["usage"]).(float64)
		data["usage"] = utils.RoundFloatDigit2(usage)
	}

	outDatamap := outData[models.ResultDataName].([]map[string]interface{})
	for _, data := range outDatamap {
		usage := utils.TypeCheckerFloat64(data["usage"]).(float64)
		data["usage"] = utils.RoundFloatDigit2(usage)
	}

	result = append(result, inData)
	result = append(result, outData)
	return result, nil

}

// GetInstanceNetworkPacketsList ...
//Network Packets
func (n ProjectService) GetInstanceNetworkPacketsList(request models.DetailReq) (result []map[string]interface{}, _ models.ErrMessage) {

	networkInResp, err := dao.GetInstanceNetworkPackets(request, "in")
	networkOutResp, err := dao.GetInstanceNetworkPackets(request, "out")

	if err != nil {
		models.MonitLogger.Error(err)
		return result, err
	}

	inData, _ := utils.GetResponseConverter().InfluxConverterList(networkInResp, models.MetricNameNetworkIn)
	outData, _ := utils.GetResponseConverter().InfluxConverterList(networkOutResp, models.MetricNameNetworkOut)

	inDatamap := inData[models.ResultDataName].([]map[string]interface{})

	for _, data := range inDatamap {
		usage := utils.TypeCheckerFloat64(data["usage"]).(float64)
		data["usage"] = utils.RoundFloatDigit2(usage)
	}

	outDatamap := outData[models.ResultDataName].([]map[string]interface{})

	for _, data := range outDatamap {
		usage := utils.TypeCheckerFloat64(data["usage"]).(float64)
		data["usage"] = utils.RoundFloatDigit2(usage)
	}

	result = append(result, inData)
	result = append(result, outData)
	return result, nil

}

// GetProjectSummary ...
func (n ProjectService) GetProjectSummary(apiRequest models.ProjectReq) (result []models.ProjectSummaryInfo, err error) {
	userID, err := monasca.GetKeystone(n.Username).GetuserIDByName()
	//Get Project List by User Own
	projectLists, err := identity.GetKeystone(n.Username).GetUserProjectList(userID)

	var searchProjectList []models.ProjectInfo

	for _, project := range projectLists {
		if apiRequest.ProjectName != "" && strings.Contains(project.ID, apiRequest.ProjectName) {
			searchProjectList = append(searchProjectList, project)
		} else if apiRequest.ProjectName == "" {
			searchProjectList = append(searchProjectList, project)
		}
	}

	if err != nil {
		fmt.Println("Get nodes resources error :", err)
	}

	var projectSummaryInfos []models.ProjectSummaryInfo

	for _, project := range searchProjectList {

		var projectSummaryInfo models.ProjectSummaryInfo

		projectInstances, projectResourceLimit, projectNetworkLimit, projectFloatingIps, projectSecurityGroups, projectStorageResources, _ := GetProjectSummarySub(n.Username, project.ID, project.Name)

		var totalVcpus, totalMemory float64
		for _, instance := range projectInstances {

			totalVcpus = totalVcpus + instance.Vcpus
			totalMemory = totalMemory + instance.MemoryMb
			//total_disk = total_disk + instance.Disk_gb
		}

		projectSummaryInfo.Name = project.Name
		projectSummaryInfo.ID = project.ID
		projectSummaryInfo.Enabled = project.Enabled
		projectSummaryInfo.InstancesUsed = len(projectInstances)
		projectSummaryInfo.MemoryMbUsed = totalMemory
		projectSummaryInfo.VcpusUsed = totalVcpus

		projectSummaryInfo.MemoryMbLimit = projectResourceLimit.MemoryMbLimit
		projectSummaryInfo.InstancesLimit = projectResourceLimit.InstancesLimit
		projectSummaryInfo.VcpusLimit = projectResourceLimit.CoresLimit

		projectSummaryInfo.SecurityGroupsLimit = projectNetworkLimit.SecurityGroupLimit
		projectSummaryInfo.FloatingIpsLimit = projectNetworkLimit.FloatingIpsLimit

		projectSummaryInfo.FloatingIpsUsed = len(projectFloatingIps)
		projectSummaryInfo.SecurityGroupsUsed = projectSecurityGroups

		projectSummaryInfo.VolumeStorageLimit = projectStorageResources.VolumesLimit
		projectSummaryInfo.VolumeStorageUsed = projectStorageResources.Volumes
		projectSummaryInfo.VolumeStorageLimitGb = projectStorageResources.VolumeLimitGb
		projectSummaryInfo.VolumeStorageUsedGb = projectStorageResources.VolumeGb

		projectSummaryInfos = append(projectSummaryInfos, projectSummaryInfo)
	}

	return projectSummaryInfos, nil

}

// GetProjectSummarySub ...
func GetProjectSummarySub(username, ProjectID, projectName string) (projectInstances []models.InstanceInfo,
	projectResourcesLimit models.ProjectResourcesLimit, projectNetworkLimit models.ProjectNetworkLimit,
	projectFloatingIps []models.FloatingIPInfo, projectSecurityGroups int, projectStorageResources models.ProjectStorageResources,
	_ models.ErrMessage) {
	defaultTenantID := models.GetUserDefTenantID(username)
	var errs []models.ErrMessage
	var err error
	var wg sync.WaitGroup
	wg.Add(6)
	for i := 0; i < 6; i++ {
		go func(wg *sync.WaitGroup, index int) {
			switch index {
			case 0:
				projectInstances, err = monasca.GetNova(username).GetProjectInstances(defaultTenantID, ProjectID)
				if err != nil {
					//errs = append(errs, err)

				}
			case 1:
				projectResourcesLimit, err = monasca.GetNova(username).GetProjectResourcesLimit(defaultTenantID, ProjectID)
				if err != nil {
					//errs = append(errs, err)
				}
			case 2:
				projectNetworkLimit, err = monasca.GetNeutron(username).GetProjectNetworkLimit(ProjectID)
				if err != nil {
					//errs = append(errs, err)
				}
			case 3:
				projectFloatingIps, err = monasca.GetNeutron(username).GetProjectFloatingIPs(ProjectID)
				if err != nil {
					//errs = append(errs, err)
				}
			case 4:
				projectSecurityGroups, err = monasca.GetNeutron(username).GetProjectSecurityGroups(ProjectID)
				if err != nil {
					//errs = append(errs, err)
				}
			case 5:
				projectStorageResources, err = monasca.GetCinder(username).GetProjectStorageResources(ProjectID)
				if err != nil {
					//errs = append(errs, err)
				}
			}

			wg.Done()
		}(&wg, i)
	}
	wg.Wait()

	//==========================================================================
	// Error가 여러건일 경우 대해 고려해야함.
	if len(errs) > 0 {
		var returnErrMessage string
		for _, err := range errs {
			returnErrMessage = returnErrMessage + " " + err["Message"].(string)
		}
		errMessage := models.ErrMessage{
			"Message": returnErrMessage,
		}
		return projectInstances, projectResourcesLimit, projectNetworkLimit, projectFloatingIps, projectSecurityGroups, projectStorageResources, errMessage
	}
	//==========================================================================
	models.MonitLogger.Debug("projectStorageResources::", projectStorageResources)
	return projectInstances, projectResourcesLimit, projectNetworkLimit, projectFloatingIps, projectSecurityGroups, projectStorageResources, nil
}

// GetProjectInstanceList ...
func (n ProjectService) GetProjectInstanceList(apiRequest models.ProjectReq) (resultArr map[string]interface{}, err error) {
	var result []ExtInstanceInfo
	// defaultTenantID := models.GetUserDefTenantID(n.Username)
	instanceMainList, _ := monasca.GetNova(n.Username).GetProjectInstancesList(apiRequest.ProjectID, apiRequest)
	instanceSubInfo, _ := monasca.GetNova(n.Username).GetProjectInstances(apiRequest.ProjectID, apiRequest.ProjectID)

	totInstance := 0
	for _, instance := range instanceSubInfo {
		if apiRequest.HostName != "" && strings.Contains(instance.Name, apiRequest.HostName) {
			totInstance++
		} else if apiRequest.HostName == "" {
			totInstance++
		}
	}

	var errs []models.ErrMessage
	for _, mainInstance := range instanceMainList {
		for _, subInstance := range instanceSubInfo {
			if mainInstance.InstanceID == subInstance.InstanceID {
				mainInstance.Flavor = subInstance.Flavor
				mainInstance.TenantID = subInstance.TenantID
				mainInstance.Vcpus = subInstance.Vcpus
				mainInstance.MemoryMb = subInstance.MemoryMb
				mainInstance.DiskGb = subInstance.DiskGb
				mainInstance.StartedAt = subInstance.StartedAt
				mainInstance.Uptime = subInstance.Uptime
			}
		}
		var req models.InstanceReq
		req.InstanceID = mainInstance.InstanceID
		cpuData, memTotData, memFreeData, err := GetProjectInstanceStatusSub(req, dao.GetInfluxConn())

		if err != nil {
			errs = append(errs, err)
		}

		CPUUsage := utils.GetDataFloatFromInterfaceSingle(cpuData)
		memTot := utils.GetDataFloatFromInterfaceSingle(memTotData)
		memFree := utils.GetDataFloatFromInterfaceSingle(memFreeData)

		mainInstance.CPUUsage = CPUUsage
		if float64(memTot) > 0 && float64(memFree) > 0 {
			mainInstance.MemoryUsage = utils.RoundFloatDigit2(100 - ((memFree / memTot) * 100))
		}
		if int(memTot) > 0 && int(memFree) > 0 {
			mainInstance.MemoryUsage = utils.RoundFloatDigit2(100 - ((memFree / memTot) * 100))
		}
		tmp := ExtInstanceInfo{}
		tmp.InstanceInfo = mainInstance
		for _, v := range mainInstance.Volumes {
			volume := v.(map[string]interface{})
			getVolume, _ := blockstorage.GetBlockstorage(apiRequest.Username).GetVolume(volume["id"].(string))
			t := VolumeInfo{}
			t.Size = getVolume.Size
			for _, k := range getVolume.Attachments {
				if k.ServerID == mainInstance.InstanceID {
					t.Device = k.Device
				}
			}
			tmp.VolumeInfos = append(tmp.VolumeInfos, t)
		}
		result = append(result, tmp)
	}

	resultArr = map[string]interface{}{
		models.ResultCnt:       totInstance,
		models.ResultProjectID: apiRequest.ProjectID,
		models.ResultDataName:  result,
	}

	return resultArr, nil

}

// GetProjectInstanceStatusSub ...
func GetProjectInstanceStatusSub(request models.InstanceReq, f client.Client) (map[string]interface{}, map[string]interface{}, map[string]interface{}, models.ErrMessage) {

	var errs []models.ErrMessage
	var err models.ErrMessage
	var wg sync.WaitGroup
	var cpuResp, memTotalResp, memFreeResp client.Response

	wg.Add(3)
	for i := 0; i < 3; i++ {
		go func(wg *sync.WaitGroup, index int) {
			switch index {
			case 0:
				cpuResp, err = dao.GetInstanceCPUUsage(request)
				if err != nil {
					errs = append(errs, err)
				}
			case 1:
				memTotalResp, err = dao.GetInstanceTotalMemoryUsage(request)
				if err != nil {
					errs = append(errs, err)
				}
			case 2:
				memFreeResp, err = dao.GetInstanceFreeMemoryUsage(request)
				if err != nil {
					errs = append(errs, err)
				}
			}

			wg.Done()
		}(&wg, i)
	}
	wg.Wait()

	//==========================================================================
	// Error가 여러건일 경우 대해 고려해야함.
	if len(errs) > 0 {
		var returnErrMessage string
		for _, err := range errs {
			returnErrMessage = returnErrMessage + " " + err["Message"].(string)
		}
		errMessage := models.ErrMessage{
			"Message": returnErrMessage,
		}
		return nil, nil, nil, errMessage
	}
	//==========================================================================

	CPUUsage, _ := utils.GetResponseConverter().InfluxConverter(cpuResp)
	memTotal, _ := utils.GetResponseConverter().InfluxConverter(memTotalResp)
	memFree, _ := utils.GetResponseConverter().InfluxConverter(memFreeResp)

	return CPUUsage, memTotal, memFree, nil
}

// GetProjectInstanceListWithoutStats ...
func (n ProjectService) GetProjectInstanceListWithoutStats(apiRequest models.ProjectReq) (result []ExtInstanceInfo, err error) {

	// defaultTenantID := models.GetUserDefTenantID(n.Username)
	instanceMainList, _ := monasca.GetNova(n.Username).GetProjectInstancesList(apiRequest.ProjectID, apiRequest)
	instanceSubInfo, _ := monasca.GetNova(n.Username).GetProjectInstances(apiRequest.ProjectID, apiRequest.ProjectID)

	totInstance := 0
	for _, instance := range instanceSubInfo {
		if apiRequest.HostName != "" && strings.Contains(instance.Name, apiRequest.HostName) {
			totInstance++
		} else if apiRequest.HostName == "" {
			totInstance++
		}
	}

	for _, mainInstance := range instanceMainList {
		for _, subInstance := range instanceSubInfo {
			if mainInstance.InstanceID == subInstance.InstanceID {
				mainInstance.Flavor = subInstance.Flavor
				mainInstance.TenantID = subInstance.TenantID
				mainInstance.Vcpus = subInstance.Vcpus
				mainInstance.MemoryMb = subInstance.MemoryMb
				mainInstance.DiskGb = subInstance.DiskGb
				mainInstance.StartedAt = subInstance.StartedAt
				mainInstance.Uptime = subInstance.Uptime
			}
		}

		tmp := ExtInstanceInfo{}

		tmp.InstanceInfo = mainInstance
		for _, v := range mainInstance.Volumes {
			volume := v.(map[string]interface{})
			getVolume, _ := blockstorage.GetBlockstorage(apiRequest.Username).GetVolume(volume["id"].(string))
			t := VolumeInfo{}
			t.Size = getVolume.Size
			for _, k := range getVolume.Attachments {
				if k.ServerID == mainInstance.InstanceID {
					t.Device = k.Device
				}
			}
			tmp.VolumeInfos = append(tmp.VolumeInfos, t)
		}
		result = append(result, tmp)
	}

	return result, nil

}
