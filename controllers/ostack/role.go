package ostack

import (
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/integration/identity"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// RolesController struct
type RolesController struct {
	shared.BaseController
}

// URLMapping URL mapping
func (u *RolesController) URLMapping() {
}

// Prepare ...
func (u *RolesController) Prepare() {
	_, tokenErr := u.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		u.Data["Data"] = jsons
		u.ServeJSON()
	}
}

// List ...
// @Title List
// @Description hint List
// @Failure 403
// @router /list [get]
func (u *RolesController) List() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	roles, err := identity.GetKeystone(claims["username"].(string)).ListRoles()
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = roles
	return
}

// Create ...
// @Title Create
// @Description hint Create
// @Param	name body	string	true	"name"
// @Failure 403
// @router /create [post]
func (u *RolesController) Create() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"name",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	name := bodyResult["name"].(string)

	roles, err := identity.GetKeystone(claims["username"].(string)).CreateRole(name)
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = roles
	return
}

// Update ...
// @Title update
// @Description hint update
// @Param	role_id body	string	true	"role_id"
// @Param	name body	string	true	"name"
// @Failure 403
// @router /update [put]
func (u *RolesController) Update() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"role_id",
		"name",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	roleID := bodyResult["role_id"].(string)
	name := bodyResult["name"].(string)

	roles, err := identity.GetKeystone(claims["username"].(string)).UpdateRole(roleID, name)
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = roles
	return
}

// Delete ...
// @Title Delete
// @Description hint Delete
// @Param	role_id body	string	true	"role_id"
// @Failure 403
// @router /delete [delete]
func (u *RolesController) Delete() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"role_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	roleID := bodyResult["role_id"].(string)
	errRole := identity.GetKeystone(claims["username"].(string)).DeleteRole(roleID)
	if errRole != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}
