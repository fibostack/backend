package ostack

import (
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/models"
)

// MainController struct
type MainController struct {
	shared.BaseController
}

// Get ...
func (c *MainController) Get() {
	models.UserListDatabase()

	c.TplName = "status.html"
}
