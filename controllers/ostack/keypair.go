package ostack

import (
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// KeyPairController struct
type KeyPairController struct {
	shared.BaseController
}

// URLMapping ...
func (i *KeyPairController) URLMapping() {
	i.Mapping("Get", i.Get)
	i.Mapping("List", i.List)
	i.Mapping("Create", i.Create)
	i.Mapping("Delete", i.Delete)
	i.Mapping("Import", i.Import)
}

// Prepare ...
func (i *KeyPairController) Prepare() {
	_, tokenErr := i.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		i.Data["Data"] = jsons
		i.ServeJSON()
	}
}

// Create ...
// @Title Create
// @Description get Create
// @Param    keyPairName    string    false    "keyPairName"
// @Failure 403
// @router /create [post]
func (i *KeyPairController) Create() {
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"keyPairName",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	name := bodyResult["keyPairName"].(string)

	quota, quataErr := models.UserQuota(osTenantID, OsUserID)
	if quataErr != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quataErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = quataErr.Error()
		response.Body = true
		return
	}

	if quota.Keypair < quota.UsedKeypair+1 {
		response.StatusCode = 1
		response.ErrorMsg = "Your keypair quota has been exceeded"
		response.Body = true
		return
	}

	keypair, err := compute.GetCompute(claims["username"].(string)).CreateKeypair(name)
	i.CreateLog(helper.OPENSTACK, helper.KEYPAIR, name, name, helper.KeyPairCreate, err)

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	models.AddUsageQuotaKPAndSG(osTenantID, OsUserID, true, false)
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = keypair
	return
}

// Get ...
// @Title Get
// @Description get Get
// @Param    keyPairName    string    false    "keyPairName"
// @Failure 403
// @router /get [post]
func (i *KeyPairController) Get() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"keyPairName",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	keyPair, err := compute.GetCompute(claims["username"].(string)).GetKeyPair(bodyResult["keyPairName"].(string))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = keyPair
	return
}

// List ...
// @Title List
// @Description get List
// @Failure 403
// @router /list [get]
func (i *KeyPairController) List() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	listKeyPairs, err := compute.GetCompute(claims["username"].(string)).ListKeypairs()

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = listKeyPairs
	return
}

// Delete ...
// @Title Delete
// @Description get Delete
// @Param    keyPairName    []string    false    "keyPairName"
// @Failure 403
// @router /delete [delete]
func (i *KeyPairController) Delete() {
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"keyPairName",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	errs := []error{}
	OsUserID := claims["userID"].(string)

	for _, v := range bodyResult["keyPairName"].([]interface{}) {
		err := compute.GetCompute(claims["username"].(string)).DeleteKeypair(v.(string))
		i.CreateLog(helper.OPENSTACK, helper.KEYPAIR, v.(string), v.(string), helper.KeyPairDelete, err)
		if err != nil {
			errs = append(errs, err)
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
		} else {
			models.MinusUsageQuotaKPAndSG(osTenantID, OsUserID, true, false)
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}
	response.StatusCode = 0
	response.Body = true
	return

}

// Import ...
// @Title Import
// @Description get Import
// @Param    keyPairName    string    false    "keyPairName"
// @Param    publicKey    string    false    "publicKey"
// @Failure 403
// @router /import [post]
func (i *KeyPairController) Import() {
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"keyPairName",
		"publicKey",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	name := bodyResult["keyPairName"].(string)
	publicKey := bodyResult["publicKey"].(string)

	quota, quataErr := models.UserQuota(osTenantID, OsUserID)
	if quataErr != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quataErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = quataErr.Error()
		response.Body = true
		return
	}

	if quota.Keypair < quota.UsedKeypair+1 {
		response.StatusCode = 1
		response.ErrorMsg = "Your keypair quota has been exceeded"
		response.Body = true
		return
	}

	importKeyPair, err := compute.GetCompute(claims["username"].(string)).ImportKeypair(name, publicKey)
	i.CreateLog(helper.OPENSTACK, helper.KEYPAIR, name, name, helper.KeyPairDelete, err)

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	models.AddUsageQuotaKPAndSG(osTenantID, OsUserID, true, false)

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = importKeyPair
	return
}
