package ostack

import (
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/models"
)

// MonascaController ...
type MonascaController struct {
	shared.BaseController
	// bodyLock sync.RWMutex
}

// Prepare ...
func (s *MonascaController) Prepare() {
	_, tokenErr := s.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		s.Data["Data"] = jsons
		s.ServeJSON()
	}
}

// ProjectSummary ...
// @Title ProjectSummary
// @Description post ProjectSummary
// @Param	projectName	string	false	"projectName"
// @Failure 403
// @router /projectSummary [post]
func (s *MonascaController) ProjectSummary() {
	claims, _ := s.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"projectName",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	// projectName은 조회조건 (Optional)
	var apiRequest models.ProjectReq
	apiRequest.ProjectName = bodyResult["projectName"].(string)

	projectSummary, err := GetProjectService(claims["username"].(string)).GetProjectSummary(apiRequest)

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = projectSummary
	return
}

// GetProjectInstanceList ...
// @Title GetProjectInstanceList
// @Description post GetProjectInstanceList
// @Param	hostName	string	false	"hostName (Optional)"
// @Param	limit	string	false	"limit (Optional)"
// @Param	marker	string	false	"marker (Optional)"
// @Failure 403
// @router /getProjectInstanceList [post]
func (s *MonascaController) GetProjectInstanceList() {
	claims, _ := s.CheckToken()
	OsTenantID := s.GetHeaderValue("OS-Tenant-ID")
	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"hostName",
		"limit",
		"marker",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var apiRequest models.ProjectReq
	apiRequest.ProjectID = OsTenantID
	//hostname은 조회조건 (Optional)
	apiRequest.HostName = bodyResult["hostName"].(string)
	//Paging Size (Optional)
	apiRequest.Limit = bodyResult["limit"].(string)
	//Paging 처리시 현재 Page Limit의 마지막 Instance Id를 요청 받으면 다음 Page를 조회 할 수 있다.
	//Limit과 같이 사용되어야 함 (Optional)
	apiRequest.Marker = bodyResult["marker"].(string)
	apiRequest.Username = claims["username"].(string)

	validation := apiRequest.ProjectInstanceRequestValidate(apiRequest)
	if validation != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	projectSummary, err := GetProjectService(claims["username"].(string)).GetProjectInstanceList(apiRequest)

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	response.StatusCode = 0
	response.Body = projectSummary
	return

}

// GetInstanceCPUUsageList ...
// @Title GetInstanceCpuUsageList
// @Description post GetInstanceCpuUsageList
// @Param	instanceId	string	false	"instanceId"
// @Param	defaultTimeRange	string	false	"defaultTimeRange"
// @Param	timeRangeFrom	string	false	"timeRangeFrom"
// @Param	timeRangeTo	string	false	"timeRangeTo"
// @Param	groupBy	string	false	"groupBy"
// @Failure 403
// @router /getInstanceCpuUsageList [post]
func (s *MonascaController) GetInstanceCPUUsageList() {
	claims, _ := s.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"instanceId",
		"defaultTimeRange",
		"timeRangeFrom",
		"timeRangeTo",
		"groupBy",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var apiRequest models.DetailReq
	apiRequest.InstanceID = bodyResult["instanceId"].(string)
	apiRequest.DefaultTimeRange = bodyResult["defaultTimeRange"].(string)
	apiRequest.TimeRangeFrom = bodyResult["timeRangeFrom"].(string)
	apiRequest.TimeRangeTo = bodyResult["timeRangeTo"].(string)
	apiRequest.GroupBy = bodyResult["groupBy"].(string)

	validation := apiRequest.InstanceMetricRequestValidate(apiRequest)
	if validation != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	cpuUsageList, _ := GetProjectService(claims["username"].(string)).GetInstanceCPUUsageList(apiRequest)
	memUsageList, _ := GetProjectService(claims["username"].(string)).GetInstanceMemoryUsageList(apiRequest)
	diskUsageReadList, _ := GetProjectService(claims["username"].(string)).GetInstanceDiskIoKbyteList(apiRequest, "read")
	diskUsageWriteList, _ := GetProjectService(claims["username"].(string)).GetInstanceDiskIoKbyteList(apiRequest, "write")
	netUsageKBList, _ := GetProjectService(claims["username"].(string)).GetInstanceNetworkIoKbyteList(apiRequest)
	netUsagePList, _ := GetProjectService(claims["username"].(string)).GetInstanceNetworkPacketsList(apiRequest)

	ret := make(map[string]interface{})
	ret["cpu"] = cpuUsageList
	ret["mem"] = memUsageList
	ret["diskR"] = diskUsageReadList
	ret["diskW"] = diskUsageWriteList
	ret["netKB"] = netUsageKBList
	ret["netP"] = netUsagePList
	response.StatusCode = 0
	response.Body = ret
	return
}

// GetInstanceMemoryUsageList ...
// @Title GetInstanceMemoryUsageList
// @Description post GetInstanceMemoryUsageList
// @Param	InstanceID	string	false	"InstanceID"
// @Param	defaultTimeRange	string	false	"defaultTimeRange"
// @Param	timeRangeFrom	string	false	"timeRangeFrom"
// @Param	timeRangeTo	string	false	"timeRangeTo"
// @Param	groupBy	string	false	"groupBy"
// @Failure 403
// @router /getInstanceMemoryUsageList [post]
func (s *MonascaController) GetInstanceMemoryUsageList() {
	claims, _ := s.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"InstanceID",
		"defaultTimeRange",
		"timeRangeFrom",
		"timeRangeTo",
		"groupBy",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var apiRequest models.DetailReq
	apiRequest.InstanceID = bodyResult["InstanceID"].(string)
	apiRequest.DefaultTimeRange = bodyResult["defaultTimeRange"].(string)
	apiRequest.TimeRangeFrom = bodyResult["timeRangeFrom"].(string)
	apiRequest.TimeRangeTo = bodyResult["timeRangeTo"].(string)
	apiRequest.GroupBy = bodyResult["groupBy"].(string)

	validation := apiRequest.InstanceMetricRequestValidate(apiRequest)
	if validation != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	memUsageList, err := GetProjectService(claims["username"].(string)).GetInstanceMemoryUsageList(apiRequest)
	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = "hud"
		return
	}
	response.StatusCode = 0
	response.Body = memUsageList
	return
}

// GetInstanceDiskReadList ...
// @Title GetInstanceDiskReadList
// @Description post GetInstanceDiskReadList
// @Param	InstanceID	string	false	"InstanceID"
// @Param	defaultTimeRange	string	false	"defaultTimeRange"
// @Param	timeRangeFrom	string	false	"timeRangeFrom"
// @Param	timeRangeTo	string	false	"timeRangeTo"
// @Param	groupBy	string	false	"groupBy"
// @Failure 403
// @router /getInstanceDiskReadList [post]
func (s *MonascaController) GetInstanceDiskReadList() {
	claims, _ := s.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"InstanceID",
		"defaultTimeRange",
		"timeRangeFrom",
		"timeRangeTo",
		"groupBy",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var apiRequest models.DetailReq
	apiRequest.InstanceID = bodyResult["InstanceID"].(string)
	apiRequest.DefaultTimeRange = bodyResult["defaultTimeRange"].(string)
	apiRequest.TimeRangeFrom = bodyResult["timeRangeFrom"].(string)
	apiRequest.TimeRangeTo = bodyResult["timeRangeTo"].(string)
	apiRequest.GroupBy = bodyResult["groupBy"].(string)

	validation := apiRequest.InstanceMetricRequestValidate(apiRequest)
	if validation != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	diskUsageList, err := GetProjectService(claims["username"].(string)).GetInstanceDiskIoKbyteList(apiRequest, "read")
	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = "hud"
		return
	}
	response.StatusCode = 0
	response.Body = diskUsageList
	return
}

// GetInstanceDiskWriteList ...
// @Title GetInstanceDiskWriteList
// @Description post GetInstanceDiskWriteList
// @Param	InstanceID	string	false	"InstanceID"
// @Param	defaultTimeRange	string	false	"defaultTimeRange"
// @Param	timeRangeFrom	string	false	"timeRangeFrom"
// @Param	timeRangeTo	string	false	"timeRangeTo"
// @Param	groupBy	string	false	"groupBy"
// @Failure 403
// @router /getInstanceDiskWriteList [post]
func (s *MonascaController) GetInstanceDiskWriteList() {
	claims, _ := s.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"InstanceID",
		"defaultTimeRange",
		"timeRangeFrom",
		"timeRangeTo",
		"groupBy",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var apiRequest models.DetailReq
	apiRequest.InstanceID = bodyResult["InstanceID"].(string)
	apiRequest.DefaultTimeRange = bodyResult["defaultTimeRange"].(string)
	apiRequest.TimeRangeFrom = bodyResult["timeRangeFrom"].(string)
	apiRequest.TimeRangeTo = bodyResult["timeRangeTo"].(string)
	apiRequest.GroupBy = bodyResult["groupBy"].(string)

	validation := apiRequest.InstanceMetricRequestValidate(apiRequest)
	if validation != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	diskUsageList, err := GetProjectService(claims["username"].(string)).GetInstanceDiskIoKbyteList(apiRequest, "write")
	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = "hud"
		return
	}
	response.StatusCode = 0
	response.Body = diskUsageList
	return

}

// GetInstanceNetworkIoList ...
// @Title GetInstanceNetworkIoList
// @Description post GetInstanceNetworkIoList
// @Param	InstanceID	string	false	"InstanceID"
// @Param	defaultTimeRange	string	false	"defaultTimeRange"
// @Param	timeRangeFrom	string	false	"timeRangeFrom"
// @Param	timeRangeTo	string	false	"timeRangeTo"
// @Param	groupBy	string	false	"groupBy"
// @Failure 403
// @router /getInstanceNetworkIoList [post]
func (s *MonascaController) GetInstanceNetworkIoList() {
	claims, _ := s.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"InstanceID",
		"defaultTimeRange",
		"timeRangeFrom",
		"timeRangeTo",
		"groupBy",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var apiRequest models.DetailReq
	apiRequest.InstanceID = bodyResult["InstanceID"].(string)
	apiRequest.DefaultTimeRange = bodyResult["defaultTimeRange"].(string)
	apiRequest.TimeRangeFrom = bodyResult["timeRangeFrom"].(string)
	apiRequest.TimeRangeTo = bodyResult["timeRangeTo"].(string)
	apiRequest.GroupBy = bodyResult["groupBy"].(string)

	validation := apiRequest.InstanceMetricRequestValidate(apiRequest)
	if validation != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	netUsageList, err := GetProjectService(claims["username"].(string)).GetInstanceNetworkIoKbyteList(apiRequest)

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = "hud"
		return
	}
	response.StatusCode = 0
	response.Body = netUsageList
	return
}

// GetInstanceNetworkPacketsList ...
// @Title GetInstanceNetworkPacketsList
// @Description post GetInstanceNetworkPacketsList
// @Param	InstanceID	string	false	"InstanceID"
// @Param	defaultTimeRange	string	false	"defaultTimeRange"
// @Param	timeRangeFrom	string	false	"timeRangeFrom"
// @Param	timeRangeTo	string	false	"timeRangeTo"
// @Param	groupBy	string	false	"groupBy"
// @Failure 403
// @router /GetInstanceNetworkPacketsList [post]
func (s *MonascaController) GetInstanceNetworkPacketsList() {
	claims, _ := s.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()
	bodyString := []string{
		"InstanceID",
		"defaultTimeRange",
		"timeRangeFrom",
		"timeRangeTo",
		"groupBy",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var apiRequest models.DetailReq
	apiRequest.InstanceID = bodyResult["InstanceID"].(string)
	apiRequest.DefaultTimeRange = bodyResult["defaultTimeRange"].(string)
	apiRequest.TimeRangeFrom = bodyResult["timeRangeFrom"].(string)
	apiRequest.TimeRangeTo = bodyResult["timeRangeTo"].(string)
	apiRequest.GroupBy = bodyResult["groupBy"].(string)

	validation := apiRequest.InstanceMetricRequestValidate(apiRequest)
	if validation != nil {
		response.StatusCode = 1
		response.ErrorMsg = validation.Error()
		return
	}
	netUsageList, err := GetProjectService(claims["username"].(string)).GetInstanceNetworkPacketsList(apiRequest)
	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = "hud"
		return
	}
	response.StatusCode = 0
	response.Body = netUsageList
	return
}
