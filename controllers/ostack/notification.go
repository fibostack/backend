package ostack

import (
	"fmt"
	"os"
	"time"

	"github.com/sacOO7/gowebsocket"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// NotificationController struct
type NotificationController struct {
	shared.BaseController
}

// URLMapping ...
func (i *NotificationController) URLMapping() {
	i.Mapping("GetNotification", i.GetNotification)
	i.Mapping("ChangeNotificationStatus", i.ChangeNotificationStatus)
	i.Mapping("ListNotification", i.ListNotification)
}

var connstring = ""

var socket gowebsocket.Socket

var su SocketUser

/****************************
	Connect to Notification Websocket
*****************************/
type (
	// NotifyMessage ...
	NotifyMessage struct {
		Action string     `json:"action"`
		UserID string     `json:"userId"`
		Data   NotifyData `json:"data"`
	}
	// NotifyData ...
	NotifyData struct {
		ID        int       `json:"ID"`
		UserID    string    `json:"UserID"`
		TopicName string    `json:"TopicName"`
		NotifName string    `json:"NotifName"`
		IsError   int       `json:"IsError"`
		Date      time.Time `json:"Date"`
		TenantID  string    `json:"TenantID"`
		IsNew     int       `json:"IsNew"`
		URL       string    `json:"URL"`
	}
	// SocketUser ...
	SocketUser struct {
		Broadcast chan NotifyMessage
		Interrupt chan os.Signal
	}
)

// Prepare ...
func (i *NotificationController) Prepare() {
	_, tokenErr := i.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		i.Data["Data"] = jsons
		i.ServeJSON()
	}
}

// GetNotification ...
// @Title GetNotification
// @Description get GetNotification
// @Param    notificationID    int    false    "notificationID"
// @Failure 403
// @router /getNotification [post]
func (i *NotificationController) GetNotification() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	bodyString := []string{
		"notificationID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	notification, err := models.GetNotification(bodyResult["notificationID"].(int))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = notification
	return
}

// ListNotification ...
// @Title ListNotification
// @Description get ListNotification
// @Failure 403
// @router /listNotification [get]
func (i *NotificationController) ListNotification() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	notifications, err := models.ListNotification(claims["userID"].(string))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = notifications
	return
}

// ChangeNotificationStatus ...
// @Title ChangeNotificationStatus
// @Description get ChangeNotificationStatus
// @Param    notificationID    float64    false    "notificationID"
// @Param    status    int    false    "status"
// @Failure 403
// @router /changeNotificationStatus [post]
func (i *NotificationController) ChangeNotificationStatus() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	bodyString := []string{
		"status",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	notif, err := models.ChangeNotificationStatus(int(bodyResult["notificationID"].(float64)), int(bodyResult["status"].(float64)))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = notif
	return
}

// SendPushNotificationToUser ...
func SendPushNotificationToUser(userid, topicname, notifname, tenantid, url string, iserror error) error {
	now := time.Now()

	if iserror == nil {
		ok := shared.PushNotification(userid, topicname, notifname)
		_, err := models.CreateNotification(userid, topicname, notifname, tenantid, url, "", 1, now)
		fmt.Println(ok)
		return err
	}

	return nil
}
