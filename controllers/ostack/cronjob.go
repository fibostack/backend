package ostack

import (
	"time"

	"gitlab.com/fibo-stack/backend/controllers/shared"
)

// CronjobController struct
type CronjobController struct {
	shared.BaseController
}

// inTimeSpan ...
func inTimeSpan(start, end, check time.Time) bool {
	if check.After(start) && check.Before(end) {
		return true
	}
	if start == check || end == check {
		return true
	}
	return false
}

// TimeRange struct
type TimeRange struct {
	Start time.Time
	End   time.Time
	Value float64
}
