package ostack

import (
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// InterfaceController struct
type InterfaceController struct {
	shared.BaseController
}

// URLMapping ...
func (i *InterfaceController) URLMapping() {
	i.Mapping("GetServerInterface", i.GetServerInterface)
	i.Mapping("CreateServerInterface", i.CreateServerInterface)
	i.Mapping("CreateServerInterface", i.CreateServerInterface)
	i.Mapping("DeleteServerInterface", i.DeleteServerInterface)
}

// Prepare ...
func (i *InterfaceController) Prepare() {
	_, tokenErr := i.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		i.Data["Data"] = jsons
		i.ServeJSON()
	}
}

// ListServerInterface ...
// @Title ListServerInterface
// @Description hint ListServerInterface
// @Param	serverID 	string	true		"serverID"
// @Failure 403
// @router /listServerInterface [post]
func (i *InterfaceController) ListServerInterface() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	interfaceList, err := compute.GetCompute(claims["username"].(string)).ListServerInterface(bodyResult["serverID"].(string))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = interfaceList
	return
}

// GetServerInterface ...
// @Title GetServerInterface
// @Description hint GetServerInterface
// @Param	portID 	string	true		"portID"
// @Param	serverID 	string	true		"serverID"
// @Failure 403
// @router /getServerInterface [post]
func (i *InterfaceController) GetServerInterface() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"serverID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	serverInterface, err := compute.GetCompute(claims["username"].(string)).GetServerInterface(bodyResult["portID"].(string), bodyResult["serverID"].(string))
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = serverInterface
	return
}

// CreateServerInterface ...
// @Title CreateServerInterface
// @Description hint CreateServerInterface
// @Param	serverID 	string	true		"serverID"
// @Param	networkID 	string	true		"networkID"
// @Failure 403
// @router /createServerInterface [post]
func (i *InterfaceController) CreateServerInterface() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
		"networkID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	serverInterface, err := compute.GetCompute(claims["username"].(string)).CreateServerInterface(bodyResult["networkID"].(string), bodyResult["serverID"].(string))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = serverInterface
	return
}

// DeleteServerInterface ...
// @Title DeleteServerInterface
// @Description hint DeleteServerInterface
// @Param	portID 	string	true		"portID"
// @Param	serverID 	string	true		"serverID"
// @Failure 403
// @router /deleteServerInterface [delete]
func (i *InterfaceController) DeleteServerInterface() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := compute.GetCompute(claims["username"].(string)).DeleteServerInterface(bodyResult["portID"].(string), bodyResult["serverID"].(string))
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}
