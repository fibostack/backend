package ostack

import (
	"fmt"
	math "math/rand"
	"strconv"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/roles"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/users"
	"gitlab.com/fibo-stack/backend/api"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/identity"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
	"golang.org/x/crypto/bcrypt"
)

// UsersController struct
type UsersController struct {
	shared.BaseController
}

// URLMapping URL mapping
func (u *UsersController) URLMapping() {
	u.Mapping("Create", u.Create)
	u.Mapping("EnableUser", u.EnableUser)
	u.Mapping("DeleteUser", u.DeleteUser)
	u.Mapping("ChangePasswordUser", u.ChangePasswordUser)
	u.Mapping("GetUserDetail", u.GetUserDetail)
	u.Mapping("List", u.List)
}

// Prepare ...
func (u *UsersController) Prepare() {
	_, tokenErr := u.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		u.Data["Data"] = jsons
		u.ServeJSON()
	}
}

// GetUserDetail ...
// @Title GetUserDetail
// @Description Get User Detail
// @Failure 403
// @router /GetUserDetail [get]
func (u *UsersController) GetUserDetail() {
	claims, _ := u.CheckToken()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	user, err := models.GetUserFromDatabase(claims["userID"].(string))
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = user
	return
}

// UpdateUserFromDatabase ...
// @Title UpdateUserFromDatabase
// @Description hint UpdateUserFromDatabase
// @Param	user_id body	string	true	"user_id"
// @Param	firstname body	string	true	"firstname"
// @Param	lastname body	string	true	"lastname"
// @Param	email body	string	true	"email"
// @Param	phone body	 string	true	"phone"
// @Failure 403
// @router /updateUserFromDatabase [put]
func (u *UsersController) UpdateUserFromDatabase() {
	// claims, _ := u.CheckToken()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"userID",
		"firstname",
		"lastname",
		"email",
		"phone",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	id, _ := strconv.ParseUint(bodyResult["userID"].(string), 10, 32)

	user := database.SysUser{
		Base:      database.Base{ID: uint32(id)},
		Firstname: bodyResult["firstname"].(string),
		Lastname:  bodyResult["lastname"].(string),
		Email:     bodyResult["email"].(string),
		MobileNum: bodyResult["phone"].(string),
	}

	updatedUser := models.UpdateUserFromDatabase(&user)

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = updatedUser
	return
}

// DeleteUserFromDatabase ...
// @Title DeleteUserFromDatabase
// @Description hint DeleteUserFromDatabase
// @Param	user_id body	string	true	"user_id"
// @Failure 403
// @router /deleteUserFromDatabase [delete]
func (u *UsersController) DeleteUserFromDatabase() {
	// claims, _ := u.CheckToken()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"user_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	user := models.DeleteUserFromDatabase(bodyResult["user_id"].(string))

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = user
	return
}

// Create ...
// @Description New user register openstack
// @Param   password  body  string  true        "The password for register"
// @Param   email body   string  true        "The email for register"
// @Param   firstname body  string  true        "The firstname for register"
// @Param   lastname  body  string  true        "The lastname for register"
// @Success 200 {string} id
// @router /create [post]
func (u *UsersController) Create() {
	var response models.BaseResponse
	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyString := []string{
		"email",
		"password",
		"firstname",
		"lastname",
		"project_id",
	}

	email := bodyResult["email"].(string)
	projectID := bodyResult["project_id"].(string)
	firstname := bodyResult["firstname"].(string)
	lastname := bodyResult["lastname"].(string)
	password := bodyResult["password"].(string)

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	checkEmail, _ := models.CheckEmail(bodyResult["email"].(string))
	if checkEmail {
		response.StatusCode = 100
		response.ErrorMsg = "Registered email address"
		response.Body = true
		return
	}

	math.Seed(time.Now().UnixNano())
	passHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	bbb := make([]rune, 10)
	for i := range bbb {
		bbb[i] = letters[math.Intn(len(letters))]
	}

	OsPwd := string(bbb)

	adminProvider, err := shared.AuthOSAdmin()
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	//create client to identity service
	client, err := openstack.NewIdentityV3(adminProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})

	opts1 := users.CreateOpts{
		Name:             email,
		DomainID:         "default",
		DefaultProjectID: projectID,
		Enabled:          gophercloud.Enabled,
		Password:         OsPwd,
		Extra: map[string]interface{}{
			"email": email,
		},
	}
	user, err := users.Create(client, opts1).Extract()

	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	//Get roles from service
	var roleID string
	listOpts := roles.ListOpts{
		DomainID: "",
	}
	allPages, err := roles.List(client, listOpts).AllPages()
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	allRoles, err := roles.ExtractRoles(allPages)
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	//Set user role member
	for _, role := range allRoles {
		fmt.Printf("%+v\a", role.Name)
		if role.Name == "member" {
			roleID = role.ID
		}
	}

	err1 := roles.Assign(client, roleID, roles.AssignOpts{
		UserID:    user.ID,
		ProjectID: projectID,
	}).ExtractErr()

	if err1 != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	if err != nil {
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		return
	}

	o := orm.NewOrm()
	newUser := database.SysUser{
		Username:      email,
		Password:      string(passHash),
		Email:         email,
		Firstname:     firstname,
		Lastname:      lastname,
		Role:          "member",
		OsPwd:         OsPwd,
		LastLoginDate: time.Time{},
		OsTenantID:    projectID,
		OsUserID:      user.ID,
		QuotaPlan:     helper.QuotaPlanSandbox,
	}
	_, inserError := o.Insert(&newUser)

	if inserError != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// EnableUser ...
// @Title EnableUser
// @Description hint EnableUser
// @Param	userID	string	false	"userID"
// @Param	changeStatus	bool	false	"changeStatus"
// @Failure 403
// @router /enableUser [put]
func (u *UsersController) EnableUser() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"userID",
		"changeStatus",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	user, err := identity.GetKeystone(claims["username"].(string)).EnableDisableUser(bodyResult["userID"].(string), bodyResult["changeStatus"].(bool))
	u.CreateLog(helper.OPENSTACK, helper.User, user.Name, user.ID, helper.UpdateStatus, err)

	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = user
	return
}

// ChangePasswordUser ...
// @Title ChangePasswordUser
// @Description hint ChangePasswordUser
// @Param	userID body	string	true	"userID"
// @Param	originalPassword body	string	true	"originalPassword"
// @Param	password body	string	true	"password"
// @Failure 403
// @router /changePasswordUser [put]
func (u *UsersController) ChangePasswordUser() {
	claims, _ := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"userID",
		"originalPassword",
		"password",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	osUserID := bodyResult["userID"].(string)
	originalPassword := bodyResult["originalPassword"].(string)
	password := bodyResult["password"].(string)

	user, userErr := identity.GetKeystone(claims["username"].(string)).GetUser(osUserID)
	if userErr != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(userErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = userErr.Error()
		return
	}

	updateErr := identity.GetKeystone(claims["username"].(string)).ChangePasswordUser(osUserID, originalPassword, password)
	u.CreateLog(helper.OPENSTACK, helper.User, user.Name, user.ID, helper.UserChangePassword, updateErr)

	if updateErr != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(updateErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = updateErr.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// DeleteUser ...
// @Title DeleteUser
// @Description hint DeleteUser
// @Param	userID 	body	string	true	"userID"
// @Failure 403
// @router /deleteUser [delete]
func (u *UsersController) DeleteUser() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"userID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	osUserID := bodyResult["userID"].(string)

	user, userErr := identity.GetKeystone(claims["username"].(string)).GetUser(osUserID)
	if userErr != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(userErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = userErr.Error()
		return
	}

	err = identity.GetKeystone(claims["username"].(string)).DeleteUser(bodyResult["userID"].(string))
	u.CreateLog(helper.OPENSTACK, helper.User, user.Name, user.ID, helper.Delete, userErr)

	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// UpdatePassword ...
// @Title DeleteUser
// @Description hint DeleteUser
// @Param 	password body	string	true	"password"
// @Param   original_password  body string true	"original_password"
// @Failure 403
// @router /updatePassword [put]
func (u *UsersController) UpdatePassword() {
	claims, _ := u.CheckToken()
	var response models.BaseResponse
	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()
	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"password",
		"original_password",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	o := orm.NewOrm()
	var user database.SysUser
	o.QueryTable("sys_user").Filter("username", claims["username"].(string)).One(&user)
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(bodyResult["original_password"].(string))); err != nil {
		response.StatusCode = 100
		response.ErrorMsg = "Old password is wrong."
		return
	}
	passHash, _ := bcrypt.GenerateFromPassword([]byte(bodyResult["password"].(string)), bcrypt.DefaultCost)
	user.Password = string(passHash)
	_, err := o.Update(&user)
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	return
}

// GetPersonalInfo ...
// @Title DeleteUser
// @Description hint DeleteUser
// @Failure 403
// @router /getPersonalInfo [get]
func (u *UsersController) GetPersonalInfo() {
	claims, _ := u.CheckToken()
	var response models.BaseResponse
	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()
	o := orm.NewOrm()
	var user database.SysUser
	o.QueryTable("sys_user").Filter("username", claims["username"].(string)).One(&user)
	response.StatusCode = 0
	response.Body = user
	return
}

// VerifyMobile ...
// @Title VerifyMobile
// @Description hint VerifyMobile
// @Param	mobile body	string	true	"mobile number"
// @Param	code body	string	true	"verify code"
// @Failure 403
// createrBy darkhaa
// updatedDate 20200317
// @router /verifyMobile [post]
func (u *UsersController) VerifyMobile() {
	claims, _ := u.CheckToken()
	logger := utils.GetLogger()
	o := orm.NewOrm()
	o.Begin()

	var response models.BaseResponse
	defer func() {
		if response.StatusCode != 0 {
			o.Rollback()
		} else {
			o.Commit()
		}
		u.Data["json"] = response
		u.ServeJSON()
	}()
	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"code",
		"mobile_num",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	user, userError := models.VerifyUserMobile(o, claims["userID"].(string), bodyResult["code"].(string), bodyResult["mobile_num"].(string))
	if userError != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(userError.Error())
		response.StatusCode = 100
		response.ErrorMsg = userError.Error()
		return
	}

	adminProvider, adminProviderErr := shared.AuthOSAdmin()

	if adminProviderErr == nil {
		_, ComQuatoErr := compute.GetCompute(user.Email).NormalQuotaset(adminProvider, user.OsTenantID)
		fmt.Print(ComQuatoErr)
		if ComQuatoErr != nil {
			fmt.Print(ComQuatoErr.Error())
		}

		_, BlockQuatoErr := blockstorage.GetBlockstorage(user.Email).NormalQuotaset(adminProvider, user.OsTenantID)
		fmt.Print(BlockQuatoErr)
		if BlockQuatoErr != nil {
			fmt.Print(BlockQuatoErr.Error())
		}
	}

	o.Commit()

	response.StatusCode = 0
	response.Body = user
	return
}

// SendCodeMobile ...
/// creator: Darkhaa 2020-03-18
// FOR TEMPORARY USE - Daagii duu
// @Description Hereglegiin dugaariig avch batalgaajuulah codeiig utsruu ilgeeh
// @Param	mobile_num 	 body	string	true		"utasnii dugaar"
// @Success 200
// @Failure 403 user not exists
// @router /sendCodeMobile [post]
func (u *UsersController) SendCodeMobile() {
	claims, _ := u.CheckToken()
	var response models.BaseResponse
	logger := utils.GetLogger()
	conn := orm.NewOrm()
	conn.Begin()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
		logger.Sync()
	}()

	bodyString := []string{
		"mobile_num",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	randomNum := strconv.Itoa(shared.RandomNumber(10000, 99999))
	mobileNum := bodyResult["mobile_num"].(string)
	osUsesrID := claims["userID"].(string)

	_, err := models.InsertPhoneVerifyCode(conn, randomNum, osUsesrID, mobileNum)
	if err != nil {
		conn.Rollback()
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}

	if updateErr := models.UpdateUserMobile(conn, osUsesrID, mobileNum); updateErr != nil {
		conn.Rollback()
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(updateErr.Error())
		response.StatusCode = 2
		response.ErrorMsg = updateErr.Error()
		return
	}

	conn.Commit()

	smsTxt := "FIBO verification code: " + randomNum
	smsResponse, smsError := api.SendSMS(mobileNum, smsTxt)
	fmt.Print(smsResponse, smsError)
	if smsError != nil {
		smsInsertError := models.InsertSysMsg(conn, smsTxt, helper.MsgVerifiyMobile, mobileNum, osUsesrID, smsError.Error())
		if smsInsertError != nil {
			u.RWMutex.Lock()
			defer u.RWMutex.Unlock()
			utils.SetLumlog(osUsesrID)
			logger.Error(smsInsertError.Error())
		}

		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(smsError.Error())
		response.StatusCode = 3
		response.ErrorMsg = smsError.Error()
		return
	}

	if smsInsertError := models.InsertSysMsg(conn, smsTxt, helper.MsgVerifiyMobile, mobileNum, osUsesrID, ""); smsInsertError != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(smsInsertError.Error())
		response.StatusCode = 4
		response.ErrorMsg = smsInsertError.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	return
}

// List ...
// @Title List
// @Description Get User Detail
// @Failure 403
// @router /list [get]
func (u *UsersController) List() {
	claims, _ := u.CheckToken()
	var response models.BaseResponse
	logger := utils.GetLogger()

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	users, err := models.UserListDatabase()
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 4
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = users
	return
}
