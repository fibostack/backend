package ostack

import (
	"strings"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/flavors"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// FlavorController struct
type FlavorController struct {
	shared.BaseController
}

// URLMapping ...
func (f *FlavorController) URLMapping() {
	f.Mapping("List", f.List)
	f.Mapping("Create", f.Create)
	f.Mapping("ListFlavorAccess", f.ListFlavorAccess)
	f.Mapping("Delete", f.Delete)
	f.Mapping("Get", f.Get)
	f.Mapping("GrantAccess", f.GrantAccess)
	f.Mapping("RemoveAccess", f.RemoveAccess)
}

// Prepare ...
func (f *FlavorController) Prepare() {
	_, tokenErr := f.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		f.Data["Data"] = jsons
		f.ServeJSON()
	}
}

// List ...
// @Title List
// @Description hint List
// @Failure 403
// @router /list [get]
func (f *FlavorController) List() {
	claims, _ := f.CheckToken()
	logger := utils.GetLogger()

	var response models.BaseResponse

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	listFlavors, err := compute.GetCompute(claims["username"].(string)).ListFlavors()
	if err != nil {
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	var flavors []flavors.Flavor

	for _, flvrs := range listFlavors {
		if !strings.HasPrefix(flvrs.Name, "fibo_") {
			flavors = append(flavors, flvrs)
		}
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = flavors
	return
}

// ListFlavorExtra ...
// @Title ListFlavorExtra
// @Description hint ListFlavorExtra
// @Failure 403
// @router /listFlavorExtra [get]
func (f *FlavorController) ListFlavorExtra() {
	claims, _ := f.CheckToken()
	logger := utils.GetLogger()

	var response models.BaseResponse
	retVal := make(map[string]interface{})
	// pAccess := flavors.PublicAccess
	// pAccess = flavors.PrivateAccess

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	listFlavorsPub, e1 := compute.GetCompute(claims["username"].(string)).ListFlavors()

	if e1 != nil {
		retVal["public"] = ""
	} else {
		retVal["public"] = listFlavorsPub
	}

	if e1 != nil {
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(e1.Error())
		response.StatusCode = 100
		response.ErrorMsg = e1.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = retVal

	listFlavorPriv, e2 := compute.GetCompute(claims["username"].(string)).ListFlavors()

	if e2 != nil {
		retVal["private"] = ""
	} else {
		retVal["private"] = listFlavorPriv
	}

	if e2 != nil {
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(e2.Error())
		response.StatusCode = 100
		response.ErrorMsg = e2.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = retVal
}

// Delete ...
// @Title Delete remote console url
// @Param	flavor_ids  body	[]string	true		"flavor_ids"
// @Description hint CreatRemoteConsole
// @Failure 403
// @router /delete [delete]
func (f *FlavorController) Delete() {
	claims, _ := f.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(f.Ctx.Request.Body)
	errs := []error{}
	var response models.BaseResponse

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	bodyString := []string{
		"flavor_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	for _, v := range bodyResult["flavor_ids"].([]interface{}) {
		flavor, err := compute.GetCompute(claims["username"].(string)).GetFlavor(v.(string))
		if err != nil {
			response.StatusCode = 1
			response.ErrorMsg = err.Error()
			return
		}
		errDelete := compute.GetCompute(claims["username"].(string)).DeleteFlavor(v.(string))
		f.CreateLog(helper.OPENSTACK, helper.Flavor, flavor.Name, flavor.ID, helper.Delete, errDelete)
		if errDelete != nil {
			errs = append(errs, errDelete)
			f.RWMutex.Lock()
			defer f.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errDelete.Error())
		}
	}

	if len(errs) != 0 {
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\f"
		}
		response.StatusCode = 1
		response.ErrorMsg = "Error"
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Create flavor
// @Title Create flavor
// @Param name body string true "name"
// @Param ram body int true "ram"
// @Param vcpu body int true "vcpu"
// @Param is_public body int bool "is_public"
// @Description hint CreatRemoteConsole
// @Failure 403
// @router /create [post]
func (f *FlavorController) Create() {
	claims, _ := f.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(f.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	bodyString := []string{
		"name",
		"ram",
		"vcpu",
		"is_public",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	name := bodyResult["name"].(string)
	ram := int(bodyResult["ram"].(float64)) * 1024
	vcpu := int(bodyResult["vcpu"].(float64))
	isPublic := bodyResult["is_public"].(bool)

	flavor, err := compute.GetCompute(claims["username"].(string)).CreateFlavor(name, ram, vcpu, isPublic)
	if err != nil {
		f.CreateLog(helper.OPENSTACK, helper.Flavor, name, "", helper.Create, err)
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	f.CreateLog(helper.OPENSTACK, helper.Flavor, name, flavor.ID, helper.Create, err)

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = flavor
	return
}

// Get ...
// @Title GetFlavor
// @Param	Get 	string	true		"flavorID"
// @Description hint GetFlavor
// @Failure 403
// @router /get [post]
func (f *FlavorController) Get() {
	claims, _ := f.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(f.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	bodyString := []string{
		"flavorID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	flavor, err := compute.GetCompute(claims["username"].(string)).GetFlavor(bodyResult["flavorID"].(string))
	if err != nil {
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = flavor
	return
}

// ListFlavorAccess ...
// @Title ListFlavorAccess
// @Param	flavorID 	string	true		"flavorID"
// @Description hint GetFlavor
// @Failure 403
// @router /listFlavorAccess [post]
func (f *FlavorController) ListFlavorAccess() {
	claims, _ := f.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(f.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	bodyString := []string{
		"flavorID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	flavor, err := compute.GetCompute(claims["username"].(string)).ListFlavorAccess(bodyResult["flavorID"].(string))

	if err != nil {
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = flavor
	return
}

// GrantAccess ...
// @Title GrantAccess
// @Param	flavorID 	string	true		"flavorID"
// @Description hint CreatRemoteConsole
// @Failure 403
// @router /grantAccess [post]
func (f *FlavorController) GrantAccess() {
	claims, _ := f.CheckToken()
	OsTenantID := f.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(f.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	bodyString := []string{
		"flavorID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	accessList, err := compute.GetCompute(claims["username"].(string)).GrantAccessToFlavor(bodyResult["flavorID"].(string), OsTenantID)

	if err != nil {
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = accessList
	return
}

// RemoveAccess ...
// @Title RemoveAccess
// @Param	flavorID 	string	true		"flavorID"
// @Description hint CreatRemoteConsole
// @Failure 403
// @router /removeAccess [post]
func (f *FlavorController) RemoveAccess() {
	claims, _ := f.CheckToken()
	OsTenantID := f.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(f.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		f.Data["json"] = response
		f.ServeJSON()
	}()

	bodyString := []string{
		"flavorID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	accessList, err := compute.GetCompute(claims["username"].(string)).RemoveAccessToFlavor(bodyResult["flavorID"].(string), OsTenantID)

	if err != nil {
		f.RWMutex.Lock()
		defer f.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = accessList
	return
}
