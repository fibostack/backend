package ostack

import (
	"strings"

	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/images"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/imageservice"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// ImageController struct
type ImageController struct {
	shared.BaseController
}

// URLMapping ...
func (i *ImageController) URLMapping() {
	i.Mapping("ImageList", i.ImageList)
	i.Mapping("GetImage", i.GetImage)
	i.Mapping("DeleteImage", i.DeleteImage)
	i.Mapping("CreateServerFromImage", i.CreateServerFromImage)
}

// Prepare ...
func (i *ImageController) Prepare() {
	_, tokenErr := i.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		i.Data["Data"] = jsons
		i.ServeJSON()
	}
}

// ImageList ...
// @Title ImageList
// @Description hint ImageList
// @Failure 403
// @router /listImage [get]
func (i *ImageController) ImageList() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	OsTenantID := i.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse
	data := make(map[string]interface{})

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	publicImage, err := imageservice.GetImageService(claims["username"].(string)).ListImages("")
	privateImage, err := imageservice.GetImageService(claims["username"].(string)).ListImages(OsTenantID)
	var images []images.Image

	for _, img := range publicImage {
		if !strings.HasPrefix(img.Name, "fibo_") {
			images = append(images, img)
		}
	}

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	data["publicImages"] = images
	data["privateImages"] = privateImage

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = data
}

// GetImage ...
// @Title GetImage
// @Description hint GetImage
// @Param	imageID 	string	true		"imageID"
// @Failure 403
// @router /getImage [post]
func (i *ImageController) GetImage() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"imageID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	image, err := compute.GetCompute(claims["username"].(string)).GetImage(bodyResult["imageID"].(string))
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = image
	return
}

// DeleteImage ...
// @Title DeleteImage
// @Description hint DeleteImage
// @Param	imageID 	[]string	true		"imageID"
// @Failure 403
// @router /deleteImage [delete]
func (i *ImageController) DeleteImage() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	errs := []error{}

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"imageID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	for _, v := range bodyResult["imageID"].([]interface{}) {
		image, err := compute.GetCompute(claims["username"].(string)).GetImage(bodyResult["imageID"].(string))

		deleteErr := compute.GetCompute(claims["username"].(string)).DeleteImage(v.(string))
		i.CreateLog(helper.OPENSTACK, helper.Image, image.Name, v.(string), helper.Delete, deleteErr)

		if deleteErr != nil {
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			errs = append(errs, err)
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// CreateServerFromImage ...
// @Title CreateServerFromImage
// @Description hint CreateServerFromImage
// @Param	serverName 	string	true		"serverName"
// @Param	flavorID 	string	true		"flavorID"
// @Param	imageID 	string	true		"imageID"
// @Failure 403
// @router /createServerFromImage [post]
func (i *ImageController) CreateServerFromImage() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"imageID",
		"serverName",
		"flavorID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	server, err := compute.GetCompute(claims["username"].(string)).CreateServerFromImage(bodyResult["serverName"].(string), bodyResult["flavorID"].(string), bodyResult["imageID"].(string))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = server
	return
}
