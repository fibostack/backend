package ostack

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/routers"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/subnets"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/networking"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// NetworkController struct
type NetworkController struct {
	MainController
}

// URLMapping ...
func (n *NetworkController) URLMapping() {
	// n.Mapping("GetClientNetwork", n.GetClientNetwork)
	n.Mapping("ListNetworks", n.ListNetworks)
	n.Mapping("CreateNetwork", n.CreateNetwork)
	n.Mapping("UpdateNetwork", n.UpdateNetwork)
	n.Mapping("DeleteNetwork", n.DeleteNetwork)
	n.Mapping("GetNetwork", n.GetNetwork)

	n.Mapping("ListPorts", n.ListPorts)
	n.Mapping("CreatePort", n.CreatePort)
	n.Mapping("UpdatePort", n.UpdatePort)
	n.Mapping("GetPort", n.GetPort)
	n.Mapping("DeletePort", n.DeletePorts)

	n.Mapping("ListFloatingIPs", n.ListFloatingIPs)
	n.Mapping("CreateFloatingIP", n.CreateFloatingIP)
	n.Mapping("AssociateFIP", n.AssociateFIP)
	n.Mapping("DisassociateFIP", n.DisassociateFIP)
	n.Mapping("DeleteFLoatingIP", n.DeleteFLoatingIP)

	n.Mapping("ListSubnets", n.ListSubnets)
	n.Mapping("CreateSubnet", n.CreateSubnet)
	n.Mapping("DeleteSubnet", n.DeleteSubnet)
	n.Mapping("UpdateSubnet", n.UpdateSubnet)
	n.Mapping("GetSubnet", n.GetSubnet)

	n.Mapping("RemoveGatewayFromSubnet", n.RemoveGatewayFromSubnet)

	n.Mapping("ListRouters", n.ListRouters)
	n.Mapping("CreateRouter", n.CreateRouter)
	n.Mapping("UpdateRouter", n.UpdateRouter)
	n.Mapping("GetRouter", n.GetRouter)

	n.Mapping("AddRoutesToRouter", n.AddRoutesToRouter)
	n.Mapping("RemoveRoutesFromRouter", n.RemoveRoutesFromRouter)
	n.Mapping("RemoveAllRoutesFromRouter", n.RemoveAllRoutesFromRouter)
	n.Mapping("DeleteRouter", n.DeleteRouter)

	n.Mapping("AddInterfaceSubnetToRouter", n.AddInterfaceSubnetToRouter)
	n.Mapping("AddInterfacePortToRouter", n.AddInterfacePortToRouter)
	n.Mapping("RemoveInterfaceSubnetFromRouter", n.RemoveInterfaceSubnetFromRouter)
	n.Mapping("RemoveInterfacePortFromRouter", n.RemoveInterfacePortFromRouter)
}

// Prepare ...
func (n *NetworkController) Prepare() {
	_, tokenErr := n.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		n.Data["Data"] = jsons
		n.ServeJSON()
	}
}

// ListNetworks ...
// @Title ListNetworks
// @Description post ListNetworks
// @Failure 403
// @router /list [get]
func (n *NetworkController) ListNetworks() {
	OsTenantID := n.GetHeaderValue("OS-Tenant-ID")
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	allNetworks, err := networking.GetNetworking(claims["username"].(string)).ListNetworksAll(OsTenantID)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = allNetworks
	return
}

// CreateNetwork ...
// @Title CreateNetwork
// @Description post CreateNetwork
// @Param    name  body   string    false    "name"
// @Param    desc  body   string    false    "desc"
// @Param    is_shared  body   bool    false    "is_shared"
// @Failure 403
// @router /create [post]
func (n *NetworkController) CreateNetwork() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"name",
		"desc",
		"is_shared",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	name := bodyResult["name"].(string)
	desc := bodyResult["desc"].(string)
	isShared := bodyResult["is_shared"].(bool)

	network, err := networking.GetNetworking(claims["username"].(string)).CreateNetwork(name, desc, isShared)
	n.CreateLog(helper.OPENSTACK, helper.Network, network.Name, network.ID, helper.Create, err)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = network
	return
}

// UpdateNetwork ...
// @Title UpdateNetwork
// @Description post UpdateNetwork
// @Param    network_id body    string    true    "network_id"
// @Param    name  body  string    true    "name"
// @Param    desc  body  string    true    "desc"
// @Param    state  body  bool    true    "state"
// @Param    is_shared  body  bool    true    "is_shared"
// @Failure 403
// @router /update [put]
func (n *NetworkController) UpdateNetwork() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"network_id",
		"name",
		"state",
		"is_shared",
		"desc",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	networkID := bodyResult["network_id"].(string)
	name := bodyResult["name"].(string)
	state := bodyResult["state"].(bool)
	isShared := bodyResult["is_shared"].(bool)
	desc := bodyResult["desc"].(string)

	network, err := networking.GetNetworking(claims["username"].(string)).UpdateNetwork(networkID, name, desc, isShared, state)
	n.CreateLog(helper.OPENSTACK, helper.Network, network.Name, network.ID, helper.Update, err)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = network
	return
}

// DeleteNetwork ...
// @Title DeleteNetwork
// @Description post DeleteNetwork
// @Param    network_id body   string    false    "network_id"
// @Failure 403
// @router /delete [delete]
func (n *NetworkController) DeleteNetwork() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"network_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	networkID := bodyResult["network_id"].(string)

	errDelete := networking.GetNetworking(claims["username"].(string)).DeleteNetwork(networkID)
	network, _ := networking.GetNetworking(claims["username"].(string)).GetNetwork(networkID)
	n.CreateLog(helper.OPENSTACK, helper.Network, network.Name, network.ID, helper.Delete, errDelete)
	if errDelete != nil {
		logger.Error(errDelete.Error())
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = errDelete.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// GetNetwork ...
// @Title GetNetwork
// @Description post GetNetwork
// @Param    network_id   body  string    false    "network_id"
// @Failure 403
// @router /get [post]
func (n *NetworkController) GetNetwork() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"network_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	networkID := bodyResult["network_id"].(string)

	network, err := networking.GetNetworking(claims["username"].(string)).GetNetwork(networkID)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = network
	return
}

// ListFloatingIPs ...
// @Title ListFloatingIPs
// @Description post ListFloatingIPs
// @Failure 403
// @router /listFloatingIPs [get]
func (n *NetworkController) ListFloatingIPs() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	floatingIPs, err := networking.GetNetworking(claims["username"].(string)).ListFLoatingIPs("")

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = floatingIPs
	return
}

// CreateFloatingIP ...
// @Title CreateFloatingIP
// @Description post CreateFloatingIP
// @Param	description string	false "description"
// @Failure 403
// @router /createFloatingIP [post]
func (n *NetworkController) CreateFloatingIP() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	osTenantID := n.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"description",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	networkID := beego.AppConfig.String("public.network.id")
	floatingIP, createErr := networking.GetNetworking(claims["username"].(string)).CreateFloatingIP(networkID, bodyResult["description"].(string))
	logID := n.CreateLog(helper.OPENSTACK, helper.Network, floatingIP.FloatingIP, floatingIP.ID, helper.Create, createErr)
	models.AddUsageQuotaFIP(osTenantID, OsUserID)

	models.CreateUsageAction(uint32(SysUserID), OsUserID, "IP", floatingIP.FloatingIP, floatingIP.ID, "", "", floatingIP.Status, floatingIP.FloatingIP, logID, time.Now(), time.Time{}, 0, 0, 0)
	if createErr != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(createErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = createErr.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = floatingIP
	return
}

// AssociateFIP ...
// @Title UpdateFloatingIP
// @Description post UpdateFloatingIP
// @Param    fipID    string    false    "fipID"
// @Param	portID	string	false	"portID"
// @Failure 403
// @router /associateFIP [post]
func (n *NetworkController) AssociateFIP() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"fipID",
		"portID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	fIP, err := networking.GetNetworking(claims["username"].(string)).AssociatFloatingIP(bodyResult["fipID"].(string), bodyResult["portID"].(string))
	n.CreateLog(helper.OPENSTACK, helper.Network, fIP.FloatingIP, fIP.ID, helper.NetworkAssociateFIP, err)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = fIP
	return
}

// DisassociateFIP ...
// @Title DisassociateFIP
// @Description post DisassociateFIP
// @Param    floating_ip    string    false    "floating_ip"
// @Failure 403
// @router /disassociatedFIP [post]
func (n *NetworkController) DisassociateFIP() {
	claims, _ := n.CheckToken()
	OsTenantID := n.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"floating_ip",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	var tmp string
	fips, err := networking.GetNetworking(claims["username"].(string)).ListFLoatingIPs(OsTenantID)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	for _, f := range fips {
		if f.FloatingIP == bodyResult["floating_ip"] {
			tmp = f.ID
			break
		}
	}
	if tmp == "" {
		err := errors.New("IP address not found")
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	fIP, err := networking.GetNetworking(claims["username"].(string)).DisassociateFloatingIP(tmp)
	n.CreateLog(helper.OPENSTACK, helper.Network, fIP.FloatingIP, fIP.ID, helper.NetworkDisassociateFIP, err)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = fIP
	return
}

// DeleteFLoatingIP ...
// @Title DeleteFLoatingIP
// @Description post DeleteFloatingIP
// @Param    fipID    string    false    "fipID"
// @Failure 403
// @router /deleteFloatingIP [delete]
func (n *NetworkController) DeleteFLoatingIP() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	osTenantID := n.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"fipID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	fIP, getErr := networking.GetNetworking(claims["username"].(string)).GetFloatingIP(bodyResult["fipID"].(string))
	if getErr != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(getErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = getErr.Error()
		response.Body = true
		return
	}
	deleteErr := networking.GetNetworking(claims["username"].(string)).DeleteFloatingIP(bodyResult["fipID"].(string))
	logID := n.CreateLog(helper.OPENSTACK, helper.Network, fIP.FloatingIP, fIP.ID, helper.NetworkDeleteFIP, deleteErr)
	models.MinusUsageQuotaFIP(osTenantID, OsUserID)

	if deleteErr != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(deleteErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = deleteErr.Error()
		response.Body = true
		return
	}

	go func(fipID, username string, logID uint32) {
		for {
			time.Sleep(1 * time.Second)
			fipObj, errGet := networking.GetNetworking(username).GetFloatingIP(fipID)
			fmt.Print(fipObj)
			if errGet != nil {
				fmt.Print(errGet.Error())
				if errGet.Error() == "Resource not found" {
					o := orm.NewOrm()
					_, errTable := o.QueryTable("usg_history").Filter("os_resource_id", fipID).Filter("end_date", "").Update(orm.Params{
						"sys_user_id":        SysUserID,
						"os_user_id":         OsUserID,
						"status":             "DELETED",
						"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
						"ip":                 "",
						"last_log_action_id": logID,
					})
					if errTable != nil {
						n.RWMutex.Lock()
						defer n.RWMutex.Unlock()
						utils.SetLumlog(claims["email"].(string))
						logger.Error(errTable.Error())
					}
					return
				}
				n.RWMutex.Lock()
				defer n.RWMutex.Unlock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errGet.Error())
				return
			}
			n.RWMutex.Lock()
			defer n.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errGet.Error())
		}
	}(bodyResult["fipID"].(string), claims["username"].(string), uint32(logID))
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ListPorts ...
// @Title ListPorts
// @Description post ListPorts
// @Param	network_id body	string	true	"network_id"
// @Failure 403
// @router /port/list [post]
func (n *NetworkController) ListPorts() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"network_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	networkID := bodyResult["network_id"].(string)

	ports, err := networking.GetNetworking(claims["username"].(string)).ListPorts(networkID)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = ports
	return
}

// CreatePort ...
// @Title CreatePort
// @Description post CreatePort
// @Param	network_id body	string	true	"network_id"
// @Param	name body	string	true	"name"
// @Param	desc body	string	true	"desc"
// @Param	sec_group_ids body	[]string	true	"sec_group_ids"
// @Failure 403
// @router /port/create [post]
func (n *NetworkController) CreatePort() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"network_id",
		"name",
		"sec_group_ids",
		"desc",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	networkID := bodyResult["network_id"].(string)
	name := bodyResult["name"].(string)
	desc := bodyResult["desc"].(string)

	secGIDs := []string{}
	for _, s := range bodyResult["sec_group_ids"].([]interface{}) {
		secGIDs = append(secGIDs, s.(string))
	}

	port, err := networking.GetNetworking(claims["username"].(string)).CreatePort(networkID, name, desc, secGIDs)
	n.CreateLog(helper.OPENSTACK, helper.Network, port.Name, port.ID, helper.NetworkCreatePort, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = port
	return
}

// UpdatePort ...
// @Title UpdatePort
// @Description post UpdatePort
// @Param	port_id body	string	true	"port_id"
// @Param	name body	string	true	"name"
// @Param	sec_group_ids body	[]string	true	"sec_group_ids"
// @Param	state body	bool	true	"state"
// @Param	desc body	string	true	"desc"
// @Failure 403
// @router /port/update [put]
func (n *NetworkController) UpdatePort() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"port_id",
		"name",
		"sec_group_ids",
		"desc",
		"state",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	secGIDs := []string{}
	for _, s := range bodyResult["sec_group_ids"].([]interface{}) {
		secGIDs = append(secGIDs, s.(string))
	}

	portID := bodyResult["port_id"].(string)
	name := bodyResult["name"].(string)
	desc := bodyResult["desc"].(string)
	state := bodyResult["state"].(bool)

	port, err := networking.GetNetworking(claims["username"].(string)).UpdatePort(portID, name, desc, secGIDs, state)
	n.CreateLog(helper.OPENSTACK, helper.Network, port.Name, port.ID, helper.NetworkUpdatePort, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = port
	return
}

// GetPort ...
// @Title GetPort
// @Description post GetPort
// @Param	portID	string	true	"portID"
// @Failure 403
// @router /getPort [post]
func (n *NetworkController) GetPort() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"portID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	port, err := networking.GetNetworking(claims["username"].(string)).GetPort(bodyResult["portID"].(string))

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = port
	return
}

// DeletePorts ...
// @Title DeletePorts
// @Description delete DeletePorts
// @Param    port_ids    []string    true    "port_ids"
// @Failure 403
// @router /port/delete [delete]
func (n *NetworkController) DeletePorts() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"port_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	errs := []error{}
	for _, s := range bodyResult["port_ids"].([]interface{}) {
		port, getError := networking.GetNetworking(claims["username"].(string)).GetPort(s.(string))
		if getError != nil {
			errs = append(errs, getError)
			n.RWMutex.Lock()
			defer n.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			response.StatusCode = 100
			response.ErrorMsg = getError.Error()
			response.Body = false
			logger.Error(getError.Error())
			return
		}
		err := networking.GetNetworking(claims["username"].(string)).DeletePort(s.(string))
		n.CreateLog(helper.OPENSTACK, helper.Network, port.Name, port.ID, helper.NetworkDeletePort, err)
		if err != nil {
			errs = append(errs, err)
			n.RWMutex.Lock()
			defer n.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
		}
	}
	if len(errs) != 0 {
		response.StatusCode = 100
		response.ErrorMsg = "Error Occurred"
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ListSubnets ...
// @Title ListSubnets
// @Description post ListSubnets
// @Param	network_id body	string	true	"network_id"
// @Failure	403
// @router	/subnet/list [post]
func (n *NetworkController) ListSubnets() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"network_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	networkID := bodyResult["network_id"].(string)
	subnets, err := networking.GetNetworking(claims["username"].(string)).ListSubnets(networkID)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = subnets
	return
}

// ListSubnetFromPrivate ...
// @Title ListSubnetFromPrivate
// @Description post ListSubnetFromPrivate
// @Failure	403
// @router	/listSubnetFromPrivate [get]
func (n *NetworkController) ListSubnetFromPrivate() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	subnets := []subnets.Subnet{}
	networks, err := networking.GetNetworking(claims["username"].(string)).ListNetworks()
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	errs := []error{}
	for _, n := range networks {
		subnet, err := networking.GetNetworking(claims["username"].(string)).ListSubnets(n.ID)
		if err == nil {
			for _, s := range subnet {
				subnets = append(subnets, s)
			}
		} else {
			errs = append(errs, err)
		}
	}

	if len(errs) != 0 {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		for _, e := range errs {
			utils.SetLumlog(claims["email"].(string))
			logger.Error(e.Error())
		}
		response.StatusCode = 100
		response.ErrorMsg = "Subnets not fully retrieved"
		return
	}
	response.StatusCode = 0
	response.Body = subnets
	return
}

// CreateSubnet ...
// @Title CreateSubnet
// @Description post CreateSubnet
// @Param	name body	string	true	"name"
// @Param	gateway_ip body	string	true	"gateway_ip"
// @Param	network_id body	string	true	"network_id"
// @Param	cidr body	string	true	"cidr"
// @Param	allocation_pools body []string	true	"allocation_pools"
// @Param	dns_name_servers body	[]string	true	"dns_name_servers"
// @Param	host_routes body	[]string	true	"host_routes"
// @Param	desc body	string	true	"desc"
// @Failure	403
// @router	/subnet/create [post]
func (n *NetworkController) CreateSubnet() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"name",
		"gateway_ip",
		"network_id",
		"cidr",
		"allocation_pools",
		"dns_name_servers",
		"host_routes",
		"desc",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	dnsNSs := []string{}
	networkID := bodyResult["network_id"].(string)
	name := bodyResult["name"].(string)
	cidr := bodyResult["cidr"].(string)
	gatewayIP := bodyResult["gateway_ip"].(string)
	desc := bodyResult["desc"].(string)

	for _, d := range bodyResult["dns_name_servers"].([]interface{}) {
		dnsNSs = append(dnsNSs, d.(string))
	}

	allocationPools := []subnets.AllocationPool{}
	for _, a := range bodyResult["allocation_pools"].([]interface{}) {
		var tmp subnets.AllocationPool
		t := a.(map[string]interface{})
		tmp.Start = t["start"].(string)
		tmp.End = t["end"].(string)
		allocationPools = append(allocationPools, tmp)
	}

	hostRoutes := []subnets.HostRoute{}
	for _, h := range bodyResult["host_routes"].([]interface{}) {
		var tmp subnets.HostRoute
		t := h.(map[string]interface{})
		tmp.DestinationCIDR = t["destination"].(string)
		tmp.NextHop = t["nexthop"].(string)
		hostRoutes = append(hostRoutes, tmp)
	}

	subnet, err := networking.GetNetworking(claims["username"].(string)).CreateSubnet(name, gatewayIP, networkID, cidr, desc, allocationPools, dnsNSs, hostRoutes)
	n.CreateLog(helper.OPENSTACK, helper.Network, subnet.Name, subnet.ID, helper.NetworkCreateSubnet, err)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = subnet
	return
}

// UpdateSubnet ...
// @Title UpdateSubnet
// @Description post UpdateSubnet
// @Param	subnet_id body string	true	"subnet_id"
// @Param 	name	body string	false	"name"
// @Param	gateway_ip body	string	true	"gateway_ip"
// @Param	cidr	string	true	"cidr"
// @Param	allocation_pools body []string	true	"allocation_pools"
// @Param	dns_name_servers body	[]string	true	"dns_name_servers"
// @Param	host_routes body	[]string	true	"host_routes"
// @Param	enable_dhcp body	bool	true	"enable_dhcp"
// @Param	desc body	string	true	"desc"
// @Failure	403
// @router	/subnet/update [put]
func (n *NetworkController) UpdateSubnet() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"subnet_id",
		"name",
		"gateway_ip",
		"cidr",
		"host_routes",
		"allocation_pools",
		"dns_name_servers",
		"enable_dhcp",
		"desc",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	allocP := []subnets.AllocationPool{}
	for _, a := range bodyResult["allocation_pools"].([]interface{}) {
		var tmp subnets.AllocationPool
		t := a.(map[string]interface{})
		tmp.Start = t["start"].(string)
		tmp.End = t["end"].(string)
		allocP = append(allocP, tmp)
	}

	hostR := []subnets.HostRoute{}
	for _, h := range bodyResult["host_routes"].([]interface{}) {
		var tmp subnets.HostRoute
		t := h.(map[string]interface{})
		tmp.DestinationCIDR = t["destination"].(string)
		tmp.NextHop = t["nexthop"].(string)
		hostR = append(hostR, tmp)
	}

	dnsNS := []string{}
	for _, d := range bodyResult["dns_name_servers"].([]interface{}) {
		dnsNS = append(dnsNS, d.(string))
	}

	subnetID := bodyResult["subnet_id"].(string)
	name := bodyResult["name"].(string)
	desc := bodyResult["desc"].(string)
	gatewayIP := bodyResult["gateway_ip"].(string)
	enableDHCP := bodyResult["enable_dhcp"].(bool)

	subnet, err := networking.GetNetworking(claims["username"].(string)).UpdateSubnet(subnetID, name, desc, gatewayIP, allocP, hostR, dnsNS, enableDHCP)
	n.CreateLog(helper.OPENSTACK, helper.Network, subnet.Name, subnet.ID, helper.NetworkUpdateSubnet, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = subnet
	return
}

// RemoveGatewayFromSubnet ...
// @Title RemoveGatewayFromSubnet
// @Description delete RemoveGatewayFromSubnet
// @Param	subnetID 	string	true	"subnetID"
// @Failure	403
// @router	/subnet/removeGateway [delete]
func (n *NetworkController) RemoveGatewayFromSubnet() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"subnetID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	subnet, err := networking.GetNetworking(claims["username"].(string)).RemoveGatewayFromSubnet(bodyResult["subnetID"].(string))
	n.CreateLog(helper.OPENSTACK, helper.Network, subnet.Name, subnet.ID, helper.NetworkRemoveGTWSubnet, err)

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = subnet
	return
}

// GetSubnet ...
// @Title GetSubnet
// @Description post GetSubnet
// @Param	subnet_id 	string	true	"subnet_id"
// @Failure	403
// @router	/subnet/get [post]
func (n *NetworkController) GetSubnet() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"subnet_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	subnet, err := networking.GetNetworking(claims["username"].(string)).GetSubnet(bodyResult["subnet_id"].(string))
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = subnet
	return
}

// DeleteSubnet ...
// @Title DeleteSubnet
// @Description delete DeleteSubnet
// @Param	subnet_ids string	true	"subnet_ids"
// @Failure	403
// @router	/subnet/delete [delete]
func (n *NetworkController) DeleteSubnet() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"subnet_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	var errors []error

	for _, id := range bodyResult["subnet_ids"].([]interface{}) {
		subnet, getErr := networking.GetNetworking(claims["username"].(string)).GetSubnet(id.(string))

		if getErr != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = getErr.Error()
			response.Body = false
			return
		}

		err := networking.GetNetworking(claims["username"].(string)).DeleteSubnet(id.(string))
		n.CreateLog(helper.OPENSTACK, helper.Network, subnet.Name, subnet.ID, helper.NetworkDeleteSubnet, err)
		if err != nil {
			errors = append(errors, err)
			logger.Error(err.Error())
		}
	}

	if len(errors) > 0 {
		response.StatusCode = utils.StatusBadRequest
		response.ErrorMsg = "Error Occurred"
		response.Body = false
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ListRouters ...
// @Title ListRouters
// @Description get ListRouters
// @Failure	403
// @router	/router/list [get]
func (n *NetworkController) ListRouters() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	listRouters, err := networking.GetNetworking(claims["username"].(string)).ListRouters()

	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = listRouters
	return
}

// CreateRouter ...
// @Title CreateRouter
// @Description post CreateRouter
// @Param	ext_network_id body	string	true	"ext_network_id"
// @Param	state body	bool	true	"state"
// @Param	name body	string	true	"name"
// @Param	desc body	string	false	"desc"
// @Failure	403
// @router	/router/create [post]
func (n *NetworkController) CreateRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"ext_network_id",
		"name",
		"desc",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	extNetworkID := bodyResult["ext_network_id"].(string)
	name := bodyResult["name"].(string)
	desc := bodyResult["desc"].(string)
	router, err := networking.GetNetworking(claims["username"].(string)).CreateRouter(extNetworkID, name, desc)
	// n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkCreateRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = router
	return
}

// UpdateRouter ...
// @Title UpdateRouter
// @Description post UpdateRouter
// @Param	router_id body	string	true	"router_id"
// @Param	ext_network_id body	bool	true	"ext_network_id"
// @Param	state	body bool	true	"state"
// @Param	distributed	body bool	true	"distributed"
// @Param	name	body string	true	"name"
// @Param	desc body	string	false	"desc"
// @Failure	403
// @router	/router/update [put]
func (n *NetworkController) UpdateRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"name",
		"router_id",
		"ext_network_id",
		"state",
		"desc",
		"distributed",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	extNetwork := bodyResult["ext_network_id"].(string)
	routerID := bodyResult["router_id"].(string)
	name := bodyResult["name"].(string)
	state := bodyResult["state"].(bool)
	distributed := bodyResult["distributed"].(bool)
	desc := bodyResult["desc"].(string)

	router, err := networking.GetNetworking(claims["username"].(string)).UpdateRouter(name, routerID, extNetwork, desc, state, distributed)
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkUpdateRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = router
	return
}

// GetRouter ...
// @Title GetRouter
// @Description post GetRouter
// @Param	router_id   body	string	true	"router_id"
// @Failure	403
// @router	/router/get [post]
func (n *NetworkController) GetRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	routerID := bodyResult["router_id"].(string)

	router, err := networking.GetNetworking(claims["username"].(string)).GetRouter(routerID)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = router
	return
}

// AddRoutesToRouter ...
// @TItle AddRoutesToRouter
// @Description post AddRoutesToRouter
// @Param	router_id  body	string	true	"router_id"
// @Param	routes	 body	[]interface{}	true	"routes"
// @Failure 403
// @router	/router/addRoute [post]
func (n *NetworkController) AddRoutesToRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
		"routes",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	routerID := bodyResult["router_id"].(string)

	aRoutes := []routers.Route{}
	for _, r := range bodyResult["routes"].([]interface{}) {
		aRoutes = append(aRoutes, r.(routers.Route))
	}

	router, err := networking.GetNetworking(claims["username"].(string)).AddRoutesToRouter(routerID, aRoutes)
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkAddRoutesToRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = router
	return
}

// RemoveRoutesFromRouter ...
// @TItle RemoveRoutesFromRouter
// @Description delete RemoveRoutesFromRouter
// @Param	router_id body 	string	true	"router_id"
// @Param	routes	body	[]interface{}	true	"routes"
// @Failure 403
// @router	/router/removeRoute [delete]
func (n *NetworkController) RemoveRoutesFromRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
		"routes",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	dRoutes := []routers.Route{}
	for _, r := range bodyResult["routes"].([]interface{}) {
		dRoutes = append(dRoutes, r.(routers.Route))
	}
	routerID := bodyResult["router_id"].(string)

	router, err := networking.GetNetworking(claims["username"].(string)).RemoveRoutesFromRouter(routerID, dRoutes)
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkRemoveRoutesToRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = router
	return
}

// RemoveAllRoutesFromRouter ...
// @TItle RemoveAllRoutesFromRouter
// @Description delete RemoveAllRoutesFromRouter
// @Param	router_id 	string	true	"router_id"
// @Failure 403
// @router	/router/removeAllRoutes [delete]
func (n *NetworkController) RemoveAllRoutesFromRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	router, err := networking.GetNetworking(claims["username"].(string)).RemoveAllRoutesFromRouter(bodyResult["router_id"].(string))
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkRemoveAllRoutesToRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = router
	return
}

// DeleteRouter ...
// @TItle DeleteRouter
// @Description delete DeleteRouter
// @Param	router_id body	string	true	"router_id"
// @Failure 403
// @router	/router/delete [delete]
func (n *NetworkController) DeleteRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	router, getError := networking.GetNetworking(claims["username"].(string)).GetRouter(bodyResult["router_id"].(string))
	if getError != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(getError.Error())
		response.StatusCode = 100
		response.ErrorMsg = getError.Error()
		response.Body = true
		return
	}

	err := networking.GetNetworking(claims["username"].(string)).DeleteRouter(bodyResult["router_id"].(string))
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkDeleteRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// AddInterfaceSubnetToRouter ...
// @TItle AddInterfaceSubnetToRouter
// @Description delete AddInterfaceSubnetToRouter
// @Param	router_id body	string	true	"router_id"
// @Param	subnet_id body	string	true	"subnet_id"
// @Failure 403
// @router	/router/addInterfaceSubnet [post]
func (n *NetworkController) AddInterfaceSubnetToRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
		"subnet_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	routerID := bodyResult["router_id"].(string)
	subnetID := bodyResult["subnet_id"].(string)
	// errs := []error{}

	router, getError := networking.GetNetworking(claims["username"].(string)).GetRouter(routerID)
	if getError != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(getError.Error())
		response.StatusCode = 100
		response.ErrorMsg = getError.Error()
		response.Body = true
		return
	}

	interfaceInfo, err := networking.GetNetworking(claims["username"].(string)).AddInterfaceSubnetToRouter(routerID, subnetID)
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkAddSubnetToRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = interfaceInfo
	return
}

// AddInterfacePortToRouter ...
// @TItle AddInterfacePortToRouter
// @Description delete AddInterfacePortToRouter
// @Param	router_id body	string	true	"router_id"
// @Param	port_id   body	string	true	"port_id"
// @Failure 403
// @router	/router/addInterfacePort [post]
func (n *NetworkController) AddInterfacePortToRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
		"port_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	routerID := bodyResult["router_id"].(string)
	portID := bodyResult["port_id"].(string)

	router, getError := networking.GetNetworking(claims["username"].(string)).GetRouter(routerID)
	if getError != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(getError.Error())
		response.StatusCode = 100
		response.ErrorMsg = getError.Error()
		response.Body = true
		return
	}

	interfaceInfo, err := networking.GetNetworking(claims["username"].(string)).AddInterfacePortToRouter(routerID, portID)
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkAddPortToRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = interfaceInfo
	return
}

// RemoveInterfacePortFromRouter ...
// @Title    RemoveInterfacePortFromRouter
// @Description delete RemoveInterfacePortFromRouter
// @Param    interface_id body    string    true    "interface_id"
// @Param    port_id   body  string    true    "port_id"
// @Failure    403
// @router    /router/removeInterfacePort [delete]
func (n *NetworkController) RemoveInterfacePortFromRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
		"interface_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	portID := bodyResult["interface_id"].(string)
	routerID := bodyResult["router_id"].(string)

	router, getError := networking.GetNetworking(claims["username"].(string)).GetRouter(routerID)
	if getError != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(getError.Error())
		response.StatusCode = 100
		response.ErrorMsg = getError.Error()
		response.Body = true
		return
	}

	_, err := networking.GetNetworking(claims["username"].(string)).RemoveInterfacePortFromRouter(routerID, portID)
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkRemovePortFromRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		if strings.Contains(err.Error(), "cannot be deleted, as it is required by one or more floating IPs") {
			response.ErrorMsg = "Cannot delete this interface. Before doing it, please disassociate floating IPs from instances."
		} else if strings.Contains(err.Error(), "Resource not found") {
			response.ErrorMsg = "Cannot delete this interface. Please delete distributed interface."
		} else {
			response.ErrorMsg = err.Error()
		}

		return
	}

	response.StatusCode = 0
	response.Body = true
	return
}

// RemoveInterfaceSubnetFromRouter ...
// @Title    RemoveInterfaceSubnetFromRouter
// @Description delete RemoveInterfaceSubnetFromRouter
// @Param    router_id   body string    true    "router_id"
// @Param    subnet_id   body string    true    "subnet_id"
// @Failure    403
// @router    /router/removeInterfaceSubnet [delete]
func (n *NetworkController) RemoveInterfaceSubnetFromRouter() {
	claims, _ := n.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Request.Body)
	bodyString := []string{
		"router_id",
		"subnet_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	subnetID := bodyResult["subnet_id"].(string)
	routerID := bodyResult["router_id"].(string)

	router, getError := networking.GetNetworking(claims["username"].(string)).GetRouter(routerID)
	if getError != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(getError.Error())
		response.StatusCode = 100
		response.ErrorMsg = getError.Error()
		response.Body = true
		return
	}

	_, err := networking.GetNetworking(claims["username"].(string)).RemoveInterfaceSubnetFromRouter(routerID, subnetID)
	n.CreateLog(helper.OPENSTACK, helper.Network, router.Name, router.ID, helper.NetworkRemoveSubnetFromRouter, err)
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	return
}

// InterfaceList ...
// @Title	InterfaceList
// @Description	get InterfaceList
// @Param	router_id	string	true	"router_id"
// @Failure	403
// @router	/router/interface/list [post]
func (n *NetworkController) InterfaceList() {
	claims, _ := n.CheckToken()
	var response models.BaseResponse
	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(n.Ctx.Input.Context.Request.Body)
	bodyString := []string{
		"router_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	routerID := bodyResult["router_id"].(string)
	ports, err := networking.GetNetworking(claims["username"].(string)).ListPortsDevice(routerID)
	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = ports
	return
}

// GetNetWorkFloatingIps ...
// @Title	GetNetWorkFloatingIps
// @Description	get hm
// @Failure	403
// @router	/GetNetWorkFloatingIps [post]
func (n *NetworkController) GetNetWorkFloatingIps() {
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		n.Data["json"] = response
		n.ServeJSON()
	}()

	//authenticate to openstack
	adminProvider, err := shared.AuthOSAdmin()
	if err != nil {
		n.RWMutex.Lock()
		defer n.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	floatingIps, err := networking.GetNetworking(beego.AppConfig.String("username")).ListNetworkIPAvailabilitiesTotal(adminProvider, beego.AppConfig.String("public.network.id"))

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = floatingIps
	return
}
