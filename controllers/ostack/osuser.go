package ostack

import (
	"fmt"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"gitlab.com/fibo-stack/backend/api"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/identity"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
	"golang.org/x/crypto/bcrypt"
)

// MainResponse ...
type MainResponse struct {
	ID            uint32          `orm:"auto;column(id)" json:"id"`
	Username      string          `orm:"column(username)" json:"username"`
	Firstname     string          `orm:"column(firstname)" json:"firstname"`
	Lastname      string          `orm:"column(lastname)" json:"lastname"`
	Email         string          `orm:"column(email)" json:"email"`
	OsUserID      string          `orm:"column(os_user_id);unique;size(100)" json:"os_user_id"`
	Role          string          `orm:"column(role)" json:"role"`
	LastLoginDate time.Time       `orm:"column(last_login_date);null" json:"last_login_date"`
	MobileNum     string          `orm:"column(mobile_num)" json:"phone"`
	IsActive      bool            `orm:"column(is_active)" json:"is_active"`
	Projects      []ProjectStruct `json:"projects"`
}

// ProjectStruct ...
type ProjectStruct struct {
	ProjectID   string `json:"project_id"`
	ProjectName string `json:"project_name"`
	RoleID      int    `json:"role_id"`
	RoleName    string `json:"role_name"`
}

// OsUsersController struct
type OsUsersController struct {
	shared.BaseController
}

// URLMapping URL mapping
func (u *OsUsersController) URLMapping() {
	u.Mapping("Create", u.Create)
	u.Mapping("List", u.List)
	u.Mapping("Get", u.Get)
	u.Mapping("Delete", u.Delete)
	u.Mapping("UserRoleList", u.UserRoleList)
	u.Mapping("UpdateUser", u.UpdateUser)
	u.Mapping("ChangePasswordUser", u.ChangePasswordUser)
}

// Create ...
// @Title CreateUser
// @Description hint CreateUser
// @Param	password	string	false	"password"
// @Param	email	string	false	"email"
// @Param	firstname	string	false	"firstname"
// @Param	mobile_num	string	false	"mobile_num"
// @Param	lastname	string	false	"lastname"
// @Failure 403
// @router /create [post]
func (u *OsUsersController) Create() {
	claims, _ := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"password",
		"email",
		"firstname",
		"lastname",
		"mobile_num",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	isExist, err := models.CheckEmail(bodyResult["email"].(string))
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}

	if isExist {
		response.StatusCode = 1
		response.ErrorMsg = "Already registered."
		return
	}

	email := bodyResult["email"].(string)
	password := bodyResult["password"].(string)
	mobileNum := bodyResult["mobile_num"].(string)
	lastname := bodyResult["lastname"].(string)
	firstname := bodyResult["firstname"].(string)
	OsPwd := shared.GeneratePassword()

	adminProvider, errAdmin := shared.AuthOSAdmin()
	if errAdmin != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errAdmin.Error())
		response.StatusCode = 2
		response.ErrorMsg = errAdmin.Error()
		return
	}

	projects, errProject := identity.GetKeystone(claims["username"].(string)).ListProjects(adminProvider)
	if errProject != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errProject.Error())
		response.StatusCode = 2
		response.ErrorMsg = errProject.Error()
		return
	}

	projectID := ""
	projectName := ""

	for _, i := range projects {
		if i.Name == "userdefault" {
			projectID = i.ID
			projectName = i.Name
			break
		}
	}

	if projectID == "" && projectName == "" {
		response.StatusCode = 2
		response.ErrorMsg = "Default project not found"
		return
	}

	user, userErr := identity.GetKeystone(claims["username"].(string)).CreateUser(projectID, email, OsPwd)
	if userErr != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(userErr.Error())
		response.StatusCode = 2
		response.ErrorMsg = userErr.Error()
		return
	}

	u.CreateLog(helper.OPENSTACK, helper.User, email, user.ID, helper.Create, userErr)
	memberRoleID := beego.AppConfig.String("member.role.id")

	errRole := identity.GetKeystone(claims["username"].(string)).AssignRoleToUserInProject(projectID, user.ID, memberRoleID)

	if errRole != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errRole.Error())
		response.StatusCode = 3
		response.ErrorMsg = errRole.Error()
		return
	}

	passHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	userData := database.SysUser{
		OsUserID:   user.ID,
		OsPwd:      OsPwd,
		OsTenantID: projectID,
		Username:   email,
		Email:      email,
		MobileNum:  mobileNum,
		Firstname:  firstname,
		Lastname:   lastname,
		Password:   string(passHash),
		QuotaPlan:  helper.QuotaPlanBasic,
		IsActive:   true,
	}

	o := orm.NewOrm()
	_, errUser := o.Insert(&userData)
	if errUser != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errUser.Error())
		response.StatusCode = 3
		response.ErrorMsg = errUser.Error()
		return
	}

	role, errRole := models.GetRoleName("Admin")
	if errRole != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errRole.Error())
		response.StatusCode = 3
		response.ErrorMsg = errRole.Error()
		return
	}

	var porjectRole database.UserProjectRole
	porjectRole.Email = email
	porjectRole.OsTenantID = projectID
	porjectRole.OsTenantName = projectName
	porjectRole.RoleID = role.ID
	porjectRole.OsUserID = user.ID
	_, errProjectRole := models.CreateProjectRole(porjectRole)
	if errProjectRole != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errProjectRole.Error())
		response.StatusCode = 3
		response.ErrorMsg = errProjectRole.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = user
	return
}

// List UserListOpenstack ...
// @Title UserListOpenstack
// @Description hint UserListOpenstack
// @Failure 403
// @router /list [get]
func (u *OsUsersController) List() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	response.StatusCode = 1
	response.ErrorMsg = "Error occured"
	response.Body = false

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	var mainRes []MainResponse

	mainIndexs := make(map[string]interface{})

	users, err := models.UsersAllProject()
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	var index = 0
	for _, i := range users {
		fmt.Println(i.Email, i.OsUserID)
		// fmt.Println("mainIndexs[i.OsUserID]", mainIndexs[i.OsUserID])
		if mainIndexs[i.OsUserID] == nil {
			mainIndexs[i.OsUserID] = index
			var each MainResponse
			each.ID = uint32(i.ID)
			each.Username = i.Username
			each.Firstname = i.Firstname
			each.Lastname = i.Lastname
			each.Email = i.Email
			each.OsUserID = i.OsUserID
			each.LastLoginDate = i.LastLoginDate
			each.MobileNum = i.MobileNum
			each.IsActive = i.IsActive

			var eachproject ProjectStruct
			eachproject.ProjectID = i.OsTenantID
			eachproject.ProjectName = i.OsTenantName
			eachproject.RoleID = i.RoleID
			eachproject.RoleName = i.RoleName
			each.Projects = append(each.Projects, eachproject)
			mainRes = append(mainRes, each)
			index++
		} else {
			var eachproject ProjectStruct
			eachproject.ProjectID = i.OsTenantID
			eachproject.ProjectName = i.OsTenantName
			eachproject.RoleID = i.RoleID
			eachproject.RoleName = i.RoleName
			mainRes[mainIndexs[i.OsUserID].(int)].Projects = append(mainRes[mainIndexs[i.OsUserID].(int)].Projects, eachproject)
		}
		fmt.Println(len(mainIndexs))
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = mainRes
	return
}

// UserRoleList ...
// @Title RoleList
// @Description hint RoleList
// @Failure 403
// @router /rolelist [get]
func (u *OsUsersController) UserRoleList() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	roles, err := identity.GetKeystone(claims["username"].(string)).ListRoles()
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = roles
	return
}

// Get UserDetail ...
// @Title GetUserDetail
// @Description Get User Detail
// @Param	os_user_id	string	false	"os_user_id"
// @Failure 403
// @router /get [post]
func (u *OsUsersController) Get() {
	claims, _ := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"os_user_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := bodyResult["os_user_id"].(string)
	user, err := models.GetUserFromDatabase(OsUserID)
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = user
	return
}

// UpdateUserFromDatabase ...
// @Title UpdateUserFromDatabase
// @Description hint UpdateUserFromDatabase
// @Param	user_id	string	false	"user_id"
// @Param	firstname	string	false	"firstname"
// @Param	lastname	string	false	"lastname"
// @Param	email	string	false	"email"
// @Param	phone	string	false	"phone"
// @Failure 403
// @router /updateUserFromDatabase [put]
func (u *OsUsersController) UpdateUserFromDatabase() {
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"userID",
		"firstname",
		"lastname",
		"email",
		"phone",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	id, _ := strconv.ParseUint(bodyResult["userID"].(string), 10, 32)

	user := database.SysUser{
		Base:      database.Base{ID: uint32(id)},
		Firstname: bodyResult["firstname"].(string),
		Lastname:  bodyResult["lastname"].(string),
		Email:     bodyResult["email"].(string),
		MobileNum: bodyResult["phone"].(string),
	}

	updatedUser := models.UpdateUserFromDatabase(&user)
	u.CreateLog(helper.OPENSTACK, helper.User, user.Email, bodyResult["userID"].(string), helper.Update, updatedUser)

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = updatedUser
	return
}

// Delete user ...
// @Title Delete user
// @Description hint DeleteUserFromDatabase
// @Param	os_user_ids	[]string	false	"os_user_ids"
// @Failure 403
// @router /delete [delete]
func (u *OsUsersController) Delete() {
	claims, _ := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"os_user_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	for _, osUserID := range bodyResult["os_user_ids"].([]interface{}) {
		user, errUser := identity.GetKeystone(claims["username"].(string)).GetUser(osUserID.(string))
		if errUser != nil {
			u.RWMutex.Lock()
			defer u.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errUser.Error())
			response.StatusCode = 100
			response.ErrorMsg = errUser.Error()
			return
		}

		errOS := identity.GetKeystone(claims["username"].(string)).DeleteUser(osUserID.(string))
		u.CreateLog(helper.OPENSTACK, helper.User, user.Name, user.ID, helper.Delete, errOS)

		if errOS != nil {
			u.RWMutex.Lock()
			defer u.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errOS.Error())
			response.StatusCode = 100
			response.ErrorMsg = errOS.Error()
			return
		}

		errDB := models.DeleteUserFromDatabase(osUserID.(string))
		if errDB != nil {
			u.RWMutex.Lock()
			defer u.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errDB.Error())
			response.StatusCode = 100
			response.ErrorMsg = errDB.Error()
			return
		}
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// UpdateUser status ...
// @Title UpdateUser
// @Description hint UpdateUserstatus
// @Param	os_user_id body	string	false	"os_user_id"
// @Param	project_id body	string	false	"change default project"
// @Param	status	body bool	false	"change user status enabled"
// @Failure 403
// @router /update [put]
func (u *OsUsersController) UpdateUser() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	o := orm.NewOrm()

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"os_user_id",
		"status",
		"project_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	osUserID := bodyResult["os_user_id"].(string)
	enabled := bodyResult["status"].(bool)
	projectID := bodyResult["project_id"].(string)

	var userData database.SysUser
	userGetError := o.QueryTable("sys_user").Filter("os_user_id", osUserID).One(&userData)
	if userGetError != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(userGetError.Error())
		response.StatusCode = 100
		response.ErrorMsg = userGetError.Error()
		return
	}

	adminProvider, adminerr := shared.AuthOSAdmin()
	if adminerr != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(adminerr.Error())
		response.StatusCode = 100
		response.ErrorMsg = adminerr.Error()
		return
	}
	updatedUser, err := identity.GetKeystone(claims["username"].(string)).UpdateUser(adminProvider, osUserID, projectID, enabled)
	u.CreateLog(helper.OPENSTACK, helper.User, updatedUser.Name, updatedUser.ID, helper.UpdateStatus, err)
	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	if userData.OsTenantID != projectID {
		models.DeleteProvider(userData.Username)

		_, errProvider := models.InitOpenstackProvider(userData.Username, userData.OsPwd, projectID)
		if errProvider != nil {
			u.RWMutex.Lock()
			defer u.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errProvider.Error())
			response.StatusCode = 100
			response.ErrorMsg = errProvider.Error()
			return
		}
	}

	_, updateErr1 := o.QueryTable("sys_user").Filter("os_user_id", osUserID).Update(orm.Params{
		"os_tenant_id": projectID,
	})

	if updateErr1 != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(updateErr1.Error())
		response.StatusCode = 100
		response.ErrorMsg = updateErr1.Error()
		return
	}

	errDB := models.UpdateUserActived(osUserID, enabled)
	if errDB != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errDB.Error())
		response.StatusCode = 100
		response.ErrorMsg = errDB.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ChangePasswordUser ...
// @Title ChangePasswordUser
// @Description hint ChangePasswordUser
// @Param	userID	string	false	"userID"
// @Param	originalPassword	string	false	"originalPassword"
// @Param	password	string	false	"password"
// @Failure 403
// @router /changePasswordUser [put]
func (u *OsUsersController) ChangePasswordUser() {
	claims, err := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"userID",
		"originalPassword",
		"password",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	osUserID := bodyResult["userID"].(string)

	user, _ := identity.GetKeystone(claims["username"].(string)).GetUser(osUserID)

	err = identity.GetKeystone(claims["username"].(string)).ChangePasswordUser(bodyResult["userID"].(string), bodyResult["originalPassword"].(string), bodyResult["password"].(string))
	u.CreateLog(helper.OPENSTACK, helper.User, user.Name, osUserID, helper.UserChangePassword, err)

	if err != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// UpdatePassword ...
// @Title DeleteUser
// @Description hint DeleteUser
// @Param	password	string	false	"password"
// @Param   original_password string false	"original_password"
// @Failure 403
// @router /updatePassword [put]
func (u *OsUsersController) UpdatePassword() {
	claims, _ := u.CheckToken()
	var response models.BaseResponse
	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()
	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"password",
		"original_password",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	o := orm.NewOrm()
	var user database.SysUser
	o.QueryTable("sys_user").Filter("username", claims["username"].(string)).One(&user)
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(bodyResult["original_password"].(string))); err != nil {
		response.StatusCode = 100
		response.ErrorMsg = "Old password is wrong."
		return
	}
	passHash, _ := bcrypt.GenerateFromPassword([]byte(bodyResult["password"].(string)), bcrypt.DefaultCost)
	user.Password = string(passHash)
	_, err := o.Update(&user)
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	return
}

// GetPersonalInfo ...
// @Title DeleteUser
// @Description hint DeleteUser
// @Failure 403
// @router /getPersonalInfo [get]
func (u *OsUsersController) GetPersonalInfo() {
	type UserResponse struct {
		ID            uint32    `orm:"auto;column(id)" json:"id"`
		CreatedAt     time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
		Username      string    `orm:"column(username)" json:"username"`
		Firstname     string    `orm:"column(firstname)" json:"firstname"`
		Lastname      string    `orm:"column(lastname)" json:"lastname"`
		MobileNum     string    `orm:"column(mobile_num)" json:"phone"`
		Email         string    `orm:"column(email)" json:"email"`
		OsUserID      string    `orm:"column(os_user_id);unique;size(100)" json:"os_user_id"`
		OsTenantID    string    `orm:"column(os_tenant_id)" json:"os_tenant_id"`
		Role          string    `orm:"column(role)" json:"role"`
		LastLoginDate time.Time `orm:"column(last_login_date);null" json:"last_login_date"`
		QuotaPlan     string    `orm:"column(quota_plan)" json:"quota_plan"`
		IsActive      bool      `orm:"column(is_active)" json:"is_active"`
		IsAdmin       bool      `orm:"column(is_admin);null" json:"is_admin"`
	}

	claims, _ := u.CheckToken()
	var response models.BaseResponse
	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()
	o := orm.NewOrm()
	var user database.SysUser
	var userResponse UserResponse

	userErr := o.QueryTable("sys_user").Filter("email", claims["email"].(string)).One(&user)
	if userErr != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		response.StatusCode = 100
		response.ErrorMsg = userErr.Error()
		return
	}

	userResponse.ID = user.ID
	userResponse.CreatedAt = user.CreatedAt
	userResponse.Username = user.Username
	userResponse.Firstname = user.Firstname
	userResponse.Lastname = user.Lastname
	userResponse.MobileNum = user.MobileNum
	userResponse.Email = user.Email
	userResponse.OsUserID = user.OsUserID
	userResponse.OsTenantID = user.OsTenantID
	userResponse.Role = user.Role
	userResponse.LastLoginDate = user.LastLoginDate
	userResponse.QuotaPlan = user.QuotaPlan
	userResponse.IsActive = user.IsActive
	if userResponse.OsTenantID == beego.AppConfig.String("tenantID") {
		userResponse.IsAdmin = true
	} else {
		userResponse.IsAdmin = false
	}

	response.StatusCode = 0
	response.Body = userResponse
	return
}

// VerifyMobile ...
// @Title VerifyMobile
// @Description hint VerifyMobile
// @Param	mobile	string	false	"mobile number"
// @Param	code	string	false	"verify code"
// @Failure 403
// createrBy darkhaa
// updatedDate 20200317
// @router /verifyMobile [post]
func (u *OsUsersController) VerifyMobile() {
	claims, _ := u.CheckToken()
	logger := utils.GetLogger()
	o := orm.NewOrm()
	o.Begin()

	var response models.BaseResponse
	defer func() {
		if response.StatusCode != 0 {
			o.Rollback()
		} else {
			o.Commit()
		}
		u.Data["json"] = response
		u.ServeJSON()
	}()
	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	bodyString := []string{
		"code",
		"mobile_num",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	user, userError := models.VerifyUserMobile(o, claims["userID"].(string), bodyResult["code"].(string), bodyResult["mobile_num"].(string))
	if userError != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(userError.Error())
		response.StatusCode = 100
		response.ErrorMsg = userError.Error()
		return
	}

	adminProvider, adminProviderErr := shared.AuthOSAdmin()

	if adminProviderErr == nil {
		_, ComQuatoErr := compute.GetCompute(user.Email).NormalQuotaset(adminProvider, user.OsTenantID)
		fmt.Print(ComQuatoErr)
		if ComQuatoErr != nil {
			fmt.Print(ComQuatoErr.Error())
		}

		_, BlockQuatoErr := blockstorage.GetBlockstorage(user.Email).NormalQuotaset(adminProvider, user.OsTenantID)
		fmt.Print(BlockQuatoErr)
		if BlockQuatoErr != nil {
			fmt.Print(BlockQuatoErr.Error())
		}
	}

	o.Commit()

	response.StatusCode = 0
	response.Body = user
	return
}

// SendCodeMobile ...
/// creator: Darkhaa 2020-03-18
// FOR TEMPORARY USE - Daagii duu
// @Description Hereglegiin dugaariig avch batalgaajuulah codeiig utsruu ilgeeh
// @Param	mobile_num 		string	true		"utasnii dugaar"
// @Success 200
// @Failure 403 user not exists
// @router /sendCodeMobile [post]
func (u *OsUsersController) SendCodeMobile() {
	claims, _ := u.CheckToken()
	var response models.BaseResponse
	logger := utils.GetLogger()
	conn := orm.NewOrm()
	conn.Begin()

	bodyResult := shared.RetrieveDataFromBody(u.Ctx.Request.Body)
	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
		logger.Sync()
	}()

	bodyString := []string{
		"mobile_num",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	randomNum := strconv.Itoa(shared.RandomNumber(10000, 99999))
	mobileNum := bodyResult["mobile_num"].(string)
	osUsesrID := claims["userID"].(string)

	_, err := models.InsertPhoneVerifyCode(conn, randomNum, osUsesrID, mobileNum)
	if err != nil {
		conn.Rollback()
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}

	if updateErr := models.UpdateUserMobile(conn, osUsesrID, mobileNum); updateErr != nil {
		conn.Rollback()
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(updateErr.Error())
		response.StatusCode = 2
		response.ErrorMsg = updateErr.Error()
		return
	}

	conn.Commit()

	smsTxt := "FIBO verification code: " + randomNum
	smsResponse, smsError := api.SendSMS(mobileNum, smsTxt)
	fmt.Print(smsResponse, smsError)
	if smsError != nil {
		smsInsertError := models.InsertSysMsg(conn, smsTxt, helper.MsgVerifiyMobile, mobileNum, osUsesrID, smsError.Error())
		if smsInsertError != nil {
			u.RWMutex.Lock()
			defer u.RWMutex.Unlock()
			utils.SetLumlog(osUsesrID)
			logger.Error(smsInsertError.Error())
		}

		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(smsError.Error())
		response.StatusCode = 3
		response.ErrorMsg = smsError.Error()
		return
	}

	if smsInsertError := models.InsertSysMsg(conn, smsTxt, helper.MsgVerifiyMobile, mobileNum, osUsesrID, ""); smsInsertError != nil {
		u.RWMutex.Lock()
		defer u.RWMutex.Unlock()
		utils.SetLumlog(osUsesrID)
		logger.Error(smsInsertError.Error())
		response.StatusCode = 4
		response.ErrorMsg = smsInsertError.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	return
}

// UserProjects ...
// @Title get user projects
// @Description hint UserProjects
// @Failure 403
// @router /projects [get]
func (u *OsUsersController) UserProjects() {
	claims, _ := u.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		u.Data["json"] = response
		u.ServeJSON()
	}()

	projects, err := identity.GetKeystone(claims["username"].(string)).ListUsersProjects(claims["userID"].(string))
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		logger.Error(err.Error())
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = projects
	return
}
