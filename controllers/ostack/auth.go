package ostack

import (
	"fmt"
	math "math/rand"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/roles"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/users"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/identity"
	"gitlab.com/fibo-stack/backend/utils"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/models"
)

// AuthController operations for Auth
type AuthController struct {
	shared.BaseController
}

// Prepare ...
func (a *AuthController) Prepare() {
	// a.GetLang()
}

// URLMapping ...
func (a *AuthController) URLMapping() {
	a.Mapping("Login", a.Login)
	a.Mapping("Logout", a.Logout)
	a.Mapping("ForgotPassword", a.ForgotPassword)
	a.Mapping("ResetPassword", a.ResetPassword)
	a.Mapping("InitAdminUser", a.InitAdminUser)
}

// Login ...
// @Description Logs user into the system
// @Param	email		body 	string	true		"The email for login"
// @Param	password		body 	string	true		"The password for login"
// @Success 200 {string} login success
// @Failure 403 user not exist
// @router /login [post]
func (a *AuthController) Login() {
	var response models.BaseResponse
	logger := utils.GetLogger()
	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
		logger.Sync()
	}()
	if bodyResult["email"] == nil || bodyResult["password"] == nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		logger.Error("Request_Body")
		response.StatusCode = 100
		response.ErrorMsg = "Email and password is required."
		response.Body = true
		return
	}
	var userData database.SysUser
	userData, msg, err := models.SignInUser(bodyResult["email"].(string), bodyResult["password"].(string), "user")

	if err != nil {
		fmt.Println(err.Error())
		if strings.Contains(err.Error(), "no row found") {
			a.RWMutex.Lock()
			defer a.RWMutex.Unlock()
			logger.Error(err.Error())
			response.StatusCode = 100
			response.ErrorMsg = "Wrong email or Password"
			response.Body = true
			return
		}
	}
	if msg == "email" {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error("Email wrong")
		response.StatusCode = 100
		response.ErrorMsg = "Wrong email or Password"
		response.Body = true
		return
	}
	if msg == "password" {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = "Wrong email or Password"
		response.Body = true
		return
	}
	if msg == "user" {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error("Member type is wrong")
		response.StatusCode = 100
		response.ErrorMsg = "Wrong role"
		response.Body = true
		return
	}
	if msg == "activate" {
		//authenticate to openstack and add provider in array map
		_, err1 := models.InitOpenstackProvider(userData.Username, userData.OsPwd, userData.OsTenantID)
		if err1 != nil {
			a.RWMutex.Lock()
			defer a.RWMutex.Unlock()
			utils.SetLumlog("")
			logger.Error(err.Error())
			response.StatusCode = 100
			response.ErrorMsg = "Error to connect Openstack"
			response.Body = true
			return
		}
		tokenString := shared.GenerateJWTString(userData)
		claim := shared.ExtractJWTString(tokenString)
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog(claim["userID"].(string))
		logger.Error("Not activated")
		response.StatusCode = 6
		response.ErrorMsg = "Email is not activated"
		response.Body = tokenString
		return
	}
	if models.GetProvider(userData.Username) != nil {
		models.DeleteProvider(userData.Username)
	}

	//authenticate to openstack and add provider in array map
	_, err1 := models.InitOpenstackProvider(userData.Username, userData.OsPwd, userData.OsTenantID)
	if err1 != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		utils.SetLumlog("")
		logger.Error(err1.Error())
		response.StatusCode = 100
		response.ErrorMsg = err1.Error()
		response.Body = true
		return
	}

	models.UpdateLoginDate(userData)

	tokenString := shared.GenerateJWTString(userData)
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = tokenString
	return
}

// Logout ...
// @Description Logs out current logged in user session
// @Success 200 {string} logout success
// @router /logout [get]
func (a *AuthController) Logout() {
	claims, _ := a.CheckToken()
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	if claims == nil {
		response.StatusCode = 0
		response.Body = true
		response.ErrorMsg = ""
		return
	}

	models.DeleteProvider(claims["username"].(string))
	response.StatusCode = 0
	response.Body = true
	response.ErrorMsg = ""
	return
}

// ForgotPassword ...
// @Description Logs out current logged in user session
// @Param	email body	string	true		"email"
// @Success 200 {string} logout success
// @router /forgotPassword [post]
func (a *AuthController) ForgotPassword() {
	var response models.BaseResponse
	logger := utils.GetLogger()
	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)

	userData, token, err := models.ForgotPassword(bodyResult["email"].(string))
	fmt.Print(userData, token)
	if err == nil {
		// ok := shared.EmailForgotPassword(userData.Firstname, userData.Lastname, userData.Email, token)
		response.StatusCode = 0
		response.ErrorMsg = ""
		response.Body = "Implement this feature by connecting your email with fibostack."
		return
	}
	if strings.Contains(err.Error(), "no row found") {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = "User not found"
		response.Body = true
		return
	}
	a.RWMutex.Lock()
	defer a.RWMutex.Unlock()
	logger.Error(err.Error())
	response.StatusCode = 1
	response.Body = true
	response.ErrorMsg = err.Error()
	return
}

// ResetPassword ...
// @Description Logs out current logged in user session
// @Param	token  body	string	true		"token"
// @Param	newPassword  body	string	true		"newPassword"
// @Success 200 {string} logout success
// @router /resetPassword [post]
func (a *AuthController) ResetPassword() {
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(a.Ctx.Request.Body)

	result, err := models.ResetPassword(bodyResult["token"].(string), bodyResult["newPassword"].(string))

	if err == nil {
		response.StatusCode = 0
		response.ErrorMsg = ""
		response.Body = result
		return
	}

	response.StatusCode = 1
	response.Body = result
	response.ErrorMsg = err.Error()
	return
}

// InitAdminUser ...
// @Description New user register openstack
// @Success 200 {string} id
// @router /initUser [get]
func (a *AuthController) InitAdminUser() {
	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	email := "demo@fibo.cloud"
	firstname := "demo"
	lastname := "demo"
	password := "fibo123"

	checkEmail, _ := models.CheckEmail(email)
	if checkEmail {
		response.StatusCode = 100
		response.ErrorMsg = "Registered email address"
		response.Body = true
		return
	}

	math.Seed(time.Now().UnixNano())
	passHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	bbb := make([]rune, 10)
	for i := range bbb {
		bbb[i] = letters[math.Intn(len(letters))]
	}

	OsPwd := string(bbb)

	// create tenant (project)
	//authenticate to openstack
	adminProvider, err := shared.AuthOSAdmin()
	//create client to identity service
	client, err := openstack.NewIdentityV3(adminProvider, gophercloud.EndpointOpts{
		Region: "RegionOne",
	})

	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	opts1 := users.CreateOpts{
		Name:             email,
		DomainID:         "default",
		DefaultProjectID: beego.AppConfig.String("tenantID"),
		Enabled:          gophercloud.Enabled,
		Password:         OsPwd,
		Extra: map[string]interface{}{
			"email": email,
		},
	}

	user, err := users.Create(client, opts1).Extract()
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	//Get roles from service
	var roleID string
	listOpts := roles.ListOpts{
		DomainID: "",
	}
	allPages, err := roles.List(client, listOpts).AllPages()
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	allRoles, err := roles.ExtractRoles(allPages)
	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	//Set user role member
	for _, role := range allRoles {
		fmt.Printf("%+v\a", role.Name)
		if role.Name == "admin" {
			roleID = role.ID
		}
	}

	err1 := roles.Assign(client, roleID, roles.AssignOpts{
		UserID:    user.ID,
		ProjectID: beego.AppConfig.String("tenantID"),
	}).ExtractErr()

	if err1 != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	if err != nil {
		response.ErrorMsg = err.Error()
		response.StatusCode = 1
		return
	}

	o := orm.NewOrm()
	newUser := database.SysUser{
		Username:      email,
		Password:      string(passHash),
		Email:         email,
		Firstname:     firstname,
		Lastname:      lastname,
		Role:          "admin",
		OsPwd:         OsPwd,
		LastLoginDate: time.Time{},
		OsTenantID:    beego.AppConfig.String("tenantID"),
		OsUserID:      user.ID,
		QuotaPlan:     helper.QuotaPlanSandbox,
		IsActive:      *gophercloud.Enabled,
	}
	_, inserError := o.Insert(&newUser)
	if inserError != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	projectRole := database.UserProjectRole{
		Email:        email,
		OsUserID:     user.ID,
		OsTenantID:   beego.AppConfig.String("tenantID"),
		OsTenantName: "admin",
		RoleID:       uint32(helper.AdminRole),
	}

	_, errRole := models.CreateProjectRole(projectRole)
	if errRole != nil {
		response.StatusCode = 100
		response.ErrorMsg = errRole.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// InitProject ...
// @Title InitProject
// @Description InitProject
// @Failure 403
// @router /initProject [get]
func (a *AuthController) InitProject() {
	logger := utils.GetLogger()

	var response models.BaseResponse

	defer func() {
		a.Data["json"] = response
		a.ServeJSON()
	}()

	adminProvider, err := shared.AuthOSAdmin()
	project, err := identity.GetKeystone("admin").CreateProject("userdefault", "default users project", adminProvider)
	if err != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	_, quotaErr := blockstorage.GetBlockstorage("admin").InitialQuotaset(adminProvider, project.ID)
	if quotaErr != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		response.StatusCode = 100
		response.ErrorMsg = quotaErr.Error()
		return
	}

	_, quotaErrCompute := compute.GetCompute("admin").InitialQuotaset(adminProvider, project.ID)
	if quotaErrCompute != nil {
		a.RWMutex.Lock()
		defer a.RWMutex.Unlock()
		logger.Error(quotaErrCompute.Error())
		response.StatusCode = 100
		response.ErrorMsg = quotaErrCompute.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = project
	return
}
