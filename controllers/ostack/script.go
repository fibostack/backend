package ostack

import (
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// ScriptController struct
type ScriptController struct {
	shared.BaseController
}

// URLMapping ...
func (s *ScriptController) URLMapping() {
	s.Mapping("ListScript", s.ListScript)
	s.Mapping("CreateScript", s.CreateScript)
}

// Prepare ...
func (s *ScriptController) Prepare() {
	_, tokenErr := s.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		s.Data["Data"] = jsons
		s.ServeJSON()
	}

}

// CreateScript ...
// @Title CreateScript
// @Description post CreateScript
// @Param	name	string	false	"name"
// @Param	path	string	false	"path"
// @Param	imagePath	string	false	"imagePath"
// @Failure 403
// @router /createScript [post]
func (s *ScriptController) CreateScript() {
	logger := utils.GetLogger()
	claims, _ := s.CheckToken()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"name",
		"path",
		"imagePath",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := models.CreateLapp(bodyResult["name"].(string), bodyResult["path"].(string)) // TODO script butsaadag bolgoh

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ListScript ...
// @Title ListScript
// @Description post ListScript
// @Failure 403
// @router /listScript [get]
func (s *ScriptController) ListScript() {
	// claims, _ := s.CheckToken()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	scripts := models.ListLapp()

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = scripts
	return
}
