package ostack

import (
	"strings"

	"github.com/astaxie/beego"
	"github.com/gophercloud/gophercloud"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/identity"
	"gitlab.com/fibo-stack/backend/integration/networking"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// SystemController struct
type SystemController struct {
	shared.BaseController
}

// RetEndpoint ...
type RetEndpoint struct {
	ID        string   `json:"id"`
	Name      string   `json:"name"`
	Region    string   `json:"region"`
	Type      string   `json:"type"`
	EndPoints []RetURL `json:"endpoints"`
}

// RetURL ...
type RetURL struct {
	Interface gophercloud.Availability `json:"interface"`
	URL       string                   `json:"url"`
}

// URLMapping ...
func (s *SystemController) URLMapping() {
	s.Mapping("ListAgents", s.ListAgents)
	s.Mapping("ListCompute", s.ListCompute)
	s.Mapping("ListBlockstorage", s.ListBlockstorage)
	s.Mapping("ProjectLimit", s.ProjectLimit)
}

// Prepare ...
func (s *SystemController) Prepare() {
	_, tokenErr := s.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		s.Data["Data"] = jsons
		s.ServeJSON()
	}
}

// ProjectLimit ...
// @Title ProjectLimit
// @Description list ProjectLimit
// @Success 201 {server} server
// @Failure 403
// @router /projectLimit  [get]
func (s *SystemController) ProjectLimit() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	OsTenantID := s.GetHeaderValue("OS-Tenant-ID")

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	computelimits, err := compute.GetCompute(claims["username"].(string)).GetDetailQuotaset(OsTenantID)

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	storagelimits, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetQuotasetDetails(OsTenantID)

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.Body = map[string]interface{}{
		"compute": computelimits,
		"storage": storagelimits,
	}
	return
}

// ActionLog ...
// @Title ActionLog
// @Description list ActionLog
// @Param	current body	int	true	current
// @Param	pageSize body	int	true	pageSize
// @Param	sortField body	string	true	"sort hiih column"
// @Param	sortOrder body	string	true	"sort iin torol"
// @Param	fieldName body	string	true	"fieldName"
// @Param	keyword body	string	true	"keyword"
// @Success 201 {server} server
// @Failure 403
// @router /actionLog [post]
func (s *SystemController) ActionLog() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)

	retVal := make(map[string]interface{})

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyString := []string{
		"current",
		"pageSize",
		"fieldName",
		"keyword",
		"sortField",
		"sortOrder",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	osUserID := claims["userID"].(string)
	osTenantID := s.GetHeaderValue("OS-Tenant-ID")

	eachPageSize := int(bodyResult["current"].(float64))
	pageNum := int(bodyResult["pageSize"].(float64))
	sortField := bodyResult["sortField"].(string)
	sortOrder := bodyResult["sortOrder"].(string)
	fieldName := bodyResult["fieldName"].(string)
	keyword := bodyResult["keyword"].(string)

	// userID := claims["userID"].(string)

	actionLogs, totalLogs, err := models.ActionLogList(osUserID, osTenantID, eachPageSize, pageNum, sortField, sortOrder, fieldName, keyword)
	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	retVal["logs"] = actionLogs
	retVal["total_logs"] = totalLogs

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = retVal
	return
}

// ListBlockstorage ...
// @Title ListBlockstorage
// @Description list ActionLog
// @Success 201 {server} server
// @Failure 403
// @router /blockstorage/service [get]
func (s *SystemController) ListBlockstorage() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	lists, err := blockstorage.GetBlockstorage(claims["username"].(string)).ListServices()

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = lists
	return
}

// ListAgents ...
// @Title ListAgents
// @Description list ListAgents
// @Success 201 {server} server
// @Failure 403
// @router /network/agent [get]
func (s *SystemController) ListAgents() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	lists, err := networking.GetNetworking(claims["username"].(string)).ListAgents()

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = lists
	return
}

// ListCompute ...
// @Title ListAgents
// @Description list ListAgents
// @Success 201 {server} server
// @Failure 403
// @router /compute/service [get]
func (s *SystemController) ListCompute() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	lists, err := compute.GetCompute(claims["username"].(string)).ListService()

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = lists
	return
}

// ListServices ...
// @Title ListServices
// @Description list ListServices
// @Success 201 {server} server
// @Failure 403
// @router /services [get]
func (s *SystemController) ListServices() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	var retMain []RetEndpoint

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	services, err := identity.GetKeystone(claims["username"].(string)).ListServices()
	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	mainIndexs := make(map[string]int)
	for ind, service := range services {
		eachService := RetEndpoint{
			ID:   service.ID,
			Name: service.Extra["name"].(string),
			Type: service.Type,
		}
		mainIndexs[service.ID] = ind
		retMain = append(retMain, eachService)
	}

	endpoints, err := identity.GetKeystone(claims["username"].(string)).ListEndpoints()
	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	for _, endpoint := range endpoints {
		main := retMain[mainIndexs[endpoint.ServiceID]]
		if main.ID == endpoint.ServiceID {
			var url RetURL
			url.Interface = endpoint.Availability
			url.URL = strings.Replace(endpoint.URL, "%(tenant_id)s", beego.AppConfig.String("tenantID"), -1)
			main.Region = endpoint.Region
			main.EndPoints = append(main.EndPoints, url)
			retMain[mainIndexs[endpoint.ServiceID]] = main
		}
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = retMain
	return
}

// ListComputeQuotas ...
// @Title ListComputeQuotas
// @Description list ListComputeQuotas
// @Success 201 {server} server
// @Failure 403
// @router /compute/quotas [get]
func (s *SystemController) ListComputeQuotas() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	lists, err := compute.GetCompute(claims["username"].(string)).GetDetailQuotaset("")

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = lists
	return
}

// ListBlockQuotas ...
// @Title ListBlockQuotas
// @Description list ListBlockQuotas
// @Success 201 {server} server
// @Failure 403
// @router /blockstorage/quotas [get]
func (s *SystemController) ListBlockQuotas() {
	claims, err := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	lists, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetQuotaset("")

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = lists
	return
}
