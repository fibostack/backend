package ostack

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/secgroups"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/security/rules"
	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/integration/networking"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"

	"gitlab.com/fibo-stack/backend/integration/compute"
)

// SecurityGroupController struct
type SecurityGroupController struct {
	shared.BaseController
}

// URLMapping ...
func (s *SecurityGroupController) URLMapping() {
	s.Mapping("List", s.List)
	s.Mapping("Create", s.Create)
	s.Mapping("Get", s.Get)
	s.Mapping("Delete", s.Delete)
	s.Mapping("Update", s.Update)
	s.Mapping("RuleCreate", s.RuleCreate)
	s.Mapping("RuleDelete", s.RuleDelete)
}

// MainRuleResponse ...
type MainRuleResponse struct {
	rules.SecGroupRule
	secgroups.IPRange
}

// SecWithRule ...
type SecWithRule struct {
	ID string
	// The human-readable name of the group, which needs to be unique.
	Name string `json:"name"`

	// The human-readable description of the group.
	Description string `json:"description"`

	// The rules which determine how this security group operates.
	Rules []MainRuleResponse `json:"rules"`

	// The ID of the tenant to which this security group belongs.
	TenantID string `json:"tenant_id"`
}

// Prepare ...
func (s *SecurityGroupController) Prepare() {
	_, tokenErr := s.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		s.Data["Data"] = jsons
		s.ServeJSON()
	}
}

// List ...
// @Title List
// @Description hint SecurityGroupList
// @Failure 403
// @router /list [get]
func (s *SecurityGroupController) List() {

	var mainResponse []MainRuleResponse

	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var datas []SecWithRule
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	securityGroupList, err := compute.GetCompute(claims["username"].(string)).ListSecGroups()

	for _, secGroup := range securityGroupList {
		rules, _ := networking.GetNetworking(claims["username"].(string)).ListSecGroupRules(secGroup.ID)

		for _, rule := range rules {
			if secGroup.ID == rule.SecGroupID {
				var eachResponseRule MainRuleResponse
				eachResponseRule.Description = rule.Description
				eachResponseRule.Direction = rule.Direction
				eachResponseRule.EtherType = rule.EtherType
				eachResponseRule.SecGroupID = rule.SecGroupID
				eachResponseRule.PortRangeMin = rule.PortRangeMin
				eachResponseRule.PortRangeMax = rule.PortRangeMax
				eachResponseRule.Protocol = rule.Protocol
				eachResponseRule.RemoteGroupID = rule.RemoteGroupID
				eachResponseRule.RemoteIPPrefix = rule.RemoteIPPrefix
				eachResponseRule.TenantID = rule.TenantID
				eachResponseRule.ProjectID = rule.ProjectID
				eachResponseRule.ID = rule.ID

				for _, sgrule := range secGroup.Rules {
					if sgrule.ID == eachResponseRule.ID {
						eachResponseRule.IPRange.CIDR = sgrule.IPRange.CIDR
					}
				}

				mainResponse = append(mainResponse, eachResponseRule)
			}
		}

		secwtihrules := SecWithRule{
			ID:          secGroup.ID,
			Name:        secGroup.Name,
			Description: secGroup.Description,
			Rules:       mainResponse,
			TenantID:    secGroup.TenantID,
		}
		datas = append(datas, secwtihrules)
		mainResponse = nil
	}

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = datas
	return
}

// Get ...
// @Title Get
// @Description hint Get
// @Param	sg_id	string	false	"sg_id"
// @Failure 403
// @router /get [post]
func (s *SecurityGroupController) Get() {
	var mainResponse []MainRuleResponse
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"sg_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	sg, err := compute.GetCompute(claims["username"].(string)).GetSecGroup(bodyResult["sg_id"].(string))

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	rules, _ := networking.GetNetworking(claims["username"].(string)).ListSecGroupRules(sg.ID)

	for _, rule := range rules {
		if sg.ID == rule.SecGroupID {
			var eachResponseRule MainRuleResponse
			eachResponseRule.Description = rule.Description
			eachResponseRule.Direction = rule.Direction
			eachResponseRule.EtherType = rule.EtherType
			eachResponseRule.SecGroupID = rule.SecGroupID
			eachResponseRule.PortRangeMin = rule.PortRangeMin
			eachResponseRule.PortRangeMax = rule.PortRangeMax
			eachResponseRule.Protocol = rule.Protocol
			eachResponseRule.RemoteGroupID = rule.RemoteGroupID
			eachResponseRule.RemoteIPPrefix = rule.RemoteIPPrefix
			eachResponseRule.TenantID = rule.TenantID
			eachResponseRule.ProjectID = rule.ProjectID
			eachResponseRule.ID = rule.ID

			for _, sgrule := range sg.Rules {
				if sgrule.ID == eachResponseRule.ID {
					eachResponseRule.IPRange.CIDR = sgrule.IPRange.CIDR
				}
			}

			mainResponse = append(mainResponse, eachResponseRule)
		}
	}

	secwtihrules := SecWithRule{
		ID:          sg.ID,
		Name:        sg.Name,
		Description: sg.Description,
		Rules:       mainResponse,
		TenantID:    sg.TenantID,
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = secwtihrules
	return
}

// ListSecurityGroupServer ...
// @Title ListSecurityGroupServer
// @Description hint ListSecurityGroupServer
// @Param	serverID	string	false	"serverID"
// @Failure 403
// @router /listSecurityGroupServer [post]
func (s *SecurityGroupController) ListSecurityGroupServer() {
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	ListSecurityGroupServer, err := compute.GetCompute(claims["username"].(string)).ListSecGroupServer(bodyResult["serverID"].(string))

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = ListSecurityGroupServer
	return
}

// Create ...
// @Title Create
// @Description hint Create
// @Param	groupName	string	false	"groupName"
// @Param	description	string	false	"description"
// @Failure 403
// @router /create [post]
func (s *SecurityGroupController) Create() {
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	osTenantID := s.GetHeaderValue("OS-Tenant-ID")
	osUserID := claims["userID"].(string)

	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"groupName",
		"description",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	groupName := bodyResult["groupName"].(string)
	description := bodyResult["description"].(string)

	quota, quataErr := models.UserQuota(osTenantID, osUserID)
	if quataErr != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quataErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = quataErr.Error()
		response.Body = true
		return
	}

	if quota.SecurityGroup < quota.UsedSecurityGroup+1 {
		response.StatusCode = 1
		response.ErrorMsg = "Your security group quota has been exceeded"
		response.Body = true
		return
	}

	createSecGroup, err := compute.GetCompute(claims["username"].(string)).CreateSecGroup(groupName, description)
	s.CreateLog(helper.OPENSTACK, helper.SecurityGroup, groupName, createSecGroup.ID, helper.Create, err)

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}

	models.AddUsageQuotaKPAndSG(osTenantID, osUserID, false, true)
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = createSecGroup
	return
}

// RuleCreate ...
// @Title RuleCreate
// @Description hint RuleCreate
// @Param	direction	string	false	"direction"
// @Param	remoteGroupID	string	false	"remoteGroupID"
// @Param	secGroupID	string	false	"secGroupID"
// @Param	description	string	false	"description"
// @Param	portRangeMin	int	false	"portRangeMin"
// @Param	portRangeMax	int	false	"portRangeMax"
// @Param	protocol	string	false	"protocol"
// @Failure 403
// @router /rule/create [post]
func (s *SecurityGroupController) RuleCreate() {
	claims, _ := s.CheckToken()

	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"direction",
		"remoteGroupID",
		"secGroupID",
		"description",
		"portRangeMin",
		"portRangeMax",
		"protocol",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	fPort := int(bodyResult["portRangeMin"].(float64))
	tPort := int(bodyResult["portRangeMax"].(float64))

	direction := bodyResult["direction"].(string)
	remoteGroupID := bodyResult["remoteGroupID"].(string)
	secGroupID := bodyResult["secGroupID"].(string)
	description := bodyResult["description"].(string)
	protocol := bodyResult["protocol"].(string)

	CreateSecurityGroupRuleInstance, err := networking.GetNetworking(claims["username"].(string)).CreateSecGroupRule(direction, remoteGroupID, secGroupID, description, fPort, tPort, protocol)
	s.CreateLog(helper.OPENSTACK, helper.SecurityGroup, description, CreateSecurityGroupRuleInstance.ID, helper.Create, err)

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = CreateSecurityGroupRuleInstance
	return
}

// AddSecurityGroupto ...
// @Title AddSecurityGroupto
// @Description hint AddSecurityGroupto
// @Param	serverID	string	false	"serverID"
// @Param	sgID	string	false	"sgID"
// @Failure 403
// @router /addSecGrouptoServer [post]
func (s *SecurityGroupController) AddSecurityGroupto() {
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"serverID",
		"sgID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := compute.GetCompute(claims["username"].(string)).AddSecGroupToServer(bodyResult["serverID"].(string), bodyResult["sgID"].(string))

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Remove ...
// @Title RemoveSecGroup
// @Description hint Remove security group from server
// @Param	serverID	string	false	"serverID"
// @Param	sgID	string	false	"sgID"
// @Failure 403
// @router /remove [post]
func (s *SecurityGroupController) Remove() {
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"serverID",
		"sgID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := compute.GetCompute(claims["username"].(string)).RemoveSecGroupFromServer(bodyResult["serverID"].(string), bodyResult["sgID"].(string))

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Delete ...
// @Title Delete
// @Description hint Delete
// @Param	sgID	[]string	false	"sgID"
// @Failure 403
// @router /delete [delete]
func (s *SecurityGroupController) Delete() {
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	osTenantID := s.GetHeaderValue("OS-Tenant-ID")
	osUserID := claims["userID"].(string)

	var err error

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"sgID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	for _, v := range bodyResult["sgID"].([]interface{}) {
		sg, sgerr := compute.GetCompute(claims["username"].(string)).GetSecGroup(v.(string))

		if sgerr != nil {
			response.StatusCode = 100
			return
		}

		deleteErr := compute.GetCompute(claims["username"].(string)).DeleteSecGroup(v.(string))
		s.CreateLog(helper.OPENSTACK, helper.SecurityGroup, sg.Name, sg.ID, helper.Delete, deleteErr)

		if deleteErr != nil {
			response.StatusCode = 100
		} else {
			response.StatusCode = 0
			models.MinusUsageQuotaKPAndSG(osTenantID, osUserID, false, true)
		}
	}

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// RuleDelete ...
// @Title RuleDelete
// @Description hint RuleDelete
// @Param	rule_ids	[]string	false	"rule_ids"
// @Failure 403
// @router /rule/delete [delete]
func (s *SecurityGroupController) RuleDelete() {
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	var err error

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"rule_ids",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	for _, v := range bodyResult["rule_ids"].([]interface{}) {
		err = compute.GetCompute(claims["username"].(string)).DeleteSecGroupRule(v.(string))
		// s.CreateLog(helper.OPENSTACK, helper.SecurityGroup, strconv.Itoa(sgRule.ToPort), sgRule.ID, helper.SecurityGroupRuleDelete, err)

		if err != nil {
			s.RWMutex.Lock()
			defer s.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 100
		} else {
			response.StatusCode = 0
		}
	}

	if err != nil {
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Update ...
// @Title Update
// @Description hint Update
// @Param	sgID	string	false	"sgID"
// @Param	name	string	false	"name"
// @Param	description	string	false	"description"
// @Failure 403
// @router /update [put]
func (s *SecurityGroupController) Update() {
	claims, _ := s.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse

	defer func() {
		s.Data["json"] = response
		s.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(s.Ctx.Request.Body)
	bodyString := []string{
		"sgID",
		"name",
		"description",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	sgID := bodyResult["sgID"].(string)
	name := bodyResult["name"].(string)
	description := bodyResult["description"].(string)

	secGroup, err := compute.GetCompute(claims["username"].(string)).UpdateSecGroup(sgID, name, description)
	s.CreateLog(helper.OPENSTACK, helper.SecurityGroup, name, sgID, helper.Update, err)

	if err != nil {
		s.RWMutex.Lock()
		defer s.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = secGroup
	return
}
