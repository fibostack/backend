package ostack

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"

	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/volumes"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"

	"gitlab.com/fibo-stack/backend/controllers/shared"
	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/helper"
	"gitlab.com/fibo-stack/backend/models"

	"gitlab.com/fibo-stack/backend/utils"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/instanceactions"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/integration/blockstorage"
	"gitlab.com/fibo-stack/backend/integration/compute"
	"gitlab.com/fibo-stack/backend/integration/heat"
	"gitlab.com/fibo-stack/backend/integration/networking"
)

// InstanceController struct ...
type InstanceController struct {
	shared.BaseController
}

// URLMapping ...
func (i *InstanceController) URLMapping() {
	i.Mapping("List", i.List)
	i.Mapping("Get", i.Get)
	i.Mapping("Start", i.Start)
	i.Mapping("Stop", i.Stop)
	i.Mapping("Resize", i.Resize)
	i.Mapping("Delete", i.Delete)
	i.Mapping("CreateSnapshot", i.CreateSnapshot)
	i.Mapping("Suspend", i.Suspend)
	i.Mapping("Console", i.Console)
	i.Mapping("Resume", i.Resume)
	i.Mapping("ConfirmResize", i.ConfirmResize)
	i.Mapping("RevertResize", i.RevertResize)
	i.Mapping("Create", i.Create)
	i.Mapping("ShowConsole", i.ShowConsole)
	i.Mapping("Clone", i.Clone)
	i.Mapping("Action", i.Action)
	i.Mapping("OnWAF", i.OnWAF)
	i.Mapping("OffWAF", i.OffWAF)
}

// ServerWithName ...
type ServerWithName struct {
	Instance   servers.Server
	TenantName string
	UserName   string
}

// Prepare ...
func (i *InstanceController) Prepare() {
	_, tokenErr := i.CheckToken()
	jsons := make(map[string]interface{})

	if tokenErr != nil {
		jsons["Error"] = tokenErr
		i.Data["Data"] = jsons
		i.ServeJSON()
	}
}

// List ...
// @Title GetAll
// @Description get GetAll Instance list авах
// @Failure 403
// @router /list [get]
func (i *InstanceController) List() {
	var response models.BaseResponse
	myCache := utils.GetUCache()

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	type MainResponse struct {
		servers.Server
		HasWaf      bool                   `json:"has_waf"`
		WafAddreses map[string]interface{} `json:"waf_addresses"`
		Volumes     []volumes.Volume       `json:"volumes"`
	}

	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	var mainResponse []MainResponse
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	servers, err := compute.GetCompute(claims["username"].(string)).ListServers(osTenantID)
	wafs, _ := models.ListInstanceWAFs()

	wafInstances := make(map[string]MainResponse)

	for _, server := range servers {
		if strings.HasSuffix(server.Name, "_waf") {
			wafInstances[server.Name] = MainResponse{Server: server}
		} else {
			flavor, err := compute.GetCompute(claims["username"].(string)).GetFlavor(server.Flavor["id"].(string))
			if err == nil {
				server.Flavor["cpu"] = flavor.VCPUs
				server.Flavor["ram"] = flavor.RAM
				server.Flavor["name"] = flavor.Name
			}
			var eachResponse MainResponse
			eachResponse.ID = server.ID
			eachResponse.TenantID = server.TenantID
			eachResponse.UserID = server.UserID
			eachResponse.Name = server.Name
			eachResponse.Updated = server.Updated
			eachResponse.Created = server.Created
			eachResponse.HostID = server.HostID
			eachResponse.Status = server.Status
			eachResponse.Progress = server.Progress
			eachResponse.AccessIPv4 = server.AccessIPv4
			eachResponse.AccessIPv6 = server.AccessIPv6
			eachResponse.Image = server.Image
			eachResponse.Flavor = server.Flavor
			eachResponse.Addresses = server.Addresses
			eachResponse.Metadata = server.Metadata
			eachResponse.Links = server.Links
			eachResponse.KeyName = server.KeyName
			eachResponse.AdminPass = server.AdminPass
			eachResponse.SecurityGroups = server.SecurityGroups
			eachResponse.AttachedVolumes = server.AttachedVolumes
			eachResponse.Fault = server.Fault
			eachResponse.Tags = server.Tags
			eachResponse.HasWaf = false

			if _, ok := wafs[server.ID]; ok {
				eachResponse.HasWaf = true
			}

			state := ""
			if myCache.Exist(server.ID) {
				state = myCache.Get(server.ID)
			}

			if state == helper.Starting && server.Status == "SHUTOFF" {
				eachResponse.Status = state
			} else if state == helper.Stoping && server.Status == "ACTIVE" {
				eachResponse.Status = state
			} else if state == helper.Suspending && server.Status == "SUSPENDED" {
				eachResponse.Status = state
			} else if state == helper.Resuming && server.Status == "SUSPENDED" {
				eachResponse.Status = state
			} else if state == helper.Deleting {
				eachResponse.Status = state
			} else {
				myCache.Delete(server.ID)
			}

			for _, volume := range server.AttachedVolumes {
				getVolume, err := blockstorage.GetBlockstorage(claims["username"].(string)).GetVolume(volume.ID)
				if err == nil {
					eachResponse.Volumes = append(eachResponse.Volumes, *getVolume)
				}
			}

			mainResponse = append(mainResponse, eachResponse)
		}
	}

	for ind, mres := range mainResponse {
		name := fmt.Sprintf("%s_waf", mres.Name)
		if _, ok := wafInstances[name]; ok {
			mainResponse[ind].WafAddreses = wafInstances[name].Addresses
		}
	}

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.Body = mainResponse
	return
}

// ListAZ ...
// @Title ListAZ
// @Description get GetAll AZs list авах
// @Failure 403
// @router /listAZ [get]
func (i *InstanceController) ListAZ() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	azs, err := compute.GetCompute(claims["username"].(string)).ListAZs()

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = azs
	return
}

// Get ...
// @Title Get
// @Description post Get
// @Param	serverID	string	true	"serverID"
// @Failure 403
// @router /get [post]
func (i *InstanceController) Get() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	data := make(map[string]interface{})
	secGroups := make(map[string]interface{})

	server, errServer := compute.GetCompute(claims["username"].(string)).GetServer(bodyResult["serverID"].(string))

	if errServer != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errServer.Error())
		response.StatusCode = 1
		response.ErrorMsg = errServer.Error()
		response.Body = true
		return
	}

	volumeList, errVolume := blockstorage.GetBlockstorage(claims["username"].(string)).ListVolumes(claims["userID"].(string))
	if errVolume != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errVolume.Error())
		response.StatusCode = 1
		response.ErrorMsg = errVolume.Error()
		response.Body = true
		return
	}

	secGroupList, errSecgroup := compute.GetCompute(claims["username"].(string)).ListSecGroups()
	if errSecgroup != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errSecgroup.Error())
		response.StatusCode = 1
		response.ErrorMsg = errSecgroup.Error()
		response.Body = true
		return
	}

	var mainResponse []MainRuleResponse

	for _, s := range secGroupList {
		rules, _ := networking.GetNetworking(claims["username"].(string)).ListSecGroupRules(s.ID)

		for _, rule := range rules {
			if s.ID == rule.SecGroupID {
				var eachResponseRule MainRuleResponse
				eachResponseRule.Description = rule.Description
				eachResponseRule.Direction = rule.Direction
				eachResponseRule.EtherType = rule.EtherType
				eachResponseRule.SecGroupID = rule.SecGroupID
				eachResponseRule.PortRangeMin = rule.PortRangeMin
				eachResponseRule.PortRangeMax = rule.PortRangeMax
				eachResponseRule.Protocol = rule.Protocol
				eachResponseRule.RemoteGroupID = rule.RemoteGroupID
				eachResponseRule.RemoteIPPrefix = rule.RemoteIPPrefix
				eachResponseRule.TenantID = rule.TenantID
				eachResponseRule.ProjectID = rule.ProjectID
				eachResponseRule.ID = rule.ID

				for _, sgrule := range s.Rules {
					if sgrule.ID == eachResponseRule.ID {
						eachResponseRule.IPRange.CIDR = sgrule.IPRange.CIDR
					}
				}

				mainResponse = append(mainResponse, eachResponseRule)
			}
		}

		secwtihrules := SecWithRule{
			ID:          s.ID,
			Name:        s.Name,
			Description: s.Description,
			Rules:       mainResponse,
			TenantID:    s.TenantID,
		}
		secGroups[s.ID] = secwtihrules
		mainResponse = nil
	}
	flavorID := server.Flavor
	flavor, errGetFlavor := compute.GetCompute(claims["username"].(string)).GetFlavor(flavorID["id"].(string))
	if errGetFlavor != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errGetFlavor.Error())
		response.StatusCode = 1
		response.ErrorMsg = errGetFlavor.Error()
		response.Body = true
		return
	}

	data["flavor"] = flavor
	data["server"] = server
	data["volumeList"] = volumeList
	data["secGroupList"] = secGroups
	response.StatusCode = 0
	response.Body = data
	response.ErrorMsg = ""
	return

}

// Start ...
// @Title Start
// @Description post Start
// @Param	serverID	[]string	false	"serverID"
// @Failure 403
// @router /start [post]
func (i *InstanceController) Start() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	myCache := utils.GetUCache()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	errs := []error{}
	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	for _, instanceID := range bodyResult["serverID"].([]interface{}) {
		server, getError := compute.GetCompute(claims["username"].(string)).GetServer(instanceID.(string))
		errStart := compute.GetCompute(claims["username"].(string)).StartServer(instanceID.(string))
		logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, instanceID.(string), helper.InstanceStart, errStart)
		flavor, err := compute.GetCompute(claims["username"].(string)).GetFlavor(server.Flavor["id"].(string))

		if errStart != nil {
			errs = append(errs, errStart)
			i.RWMutex.Lock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			i.RWMutex.Unlock()
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = errStart.Error()
			response.Body = false
			return
		}

		if errStart == nil {
			o := orm.NewOrm()
			if getError == nil {
				var usageObj database.UsgHistory
				usageObj = database.UsgHistory{
					SysUserID:       uint32(SysUserID),
					OsUserID:        OsUserID,
					Type:            "Instance",
					Hostname:        server.Name,
					OsResourceID:    server.ID,
					OsInstanceID:    server.ID,
					Flavor:          flavor.Name,
					DiskSize:        0,
					CPU:             flavor.VCPUs,
					RAM:             flavor.RAM,
					Status:          "ACTIVE",
					StartDate:       time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
					EndDate:         "",
					IP:              "",
					LastLogActionID: uint32(logID),
				}
				_, errTable := o.Insert(&usageObj)
				if errTable != nil {
					errs = append(errs, errTable)
					i.RWMutex.Lock()
					defer i.RWMutex.Unlock()
					utils.SetLumlog(claims["email"].(string))
					logger.Error(errTable.Error())
				}
				myCache.Set(server.ID, helper.Starting)
			}
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Reboot ...
// @Title Reboot
// @Description post Reboot
// @Param	server_ids	[]string	false	"server_ids"
// @Param	type	string	false	"type"
// @Failure 403
// @router /reboot [post]
func (i *InstanceController) Reboot() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"server_ids",
		"type",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	rebootType := bodyResult["type"].(string)
	errs := []error{}

	for _, v := range bodyResult["server_ids"].([]interface{}) {
		server, getErr := compute.GetCompute(claims["username"].(string)).GetServer(v.(string))
		if getErr != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = getErr.Error()
			response.Body = false
			return
		}

		errReboot := compute.GetCompute(claims["username"].(string)).RebootServer(v.(string), rebootType)
		if rebootType == "hard" {
			i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, v.(string), helper.InstanceHardreboot, errReboot)
		} else {
			i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, v.(string), helper.InstanceSoftreboot, errReboot)
		}

		if errReboot != nil {
			errs = append(errs, errReboot)
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errReboot.Error())
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Stop ...
/// Created: Darhaa  2020-03-04
/// Updated: Darhaa  2020-03-09
// @Title Stop
// @Description post Stop
// @Param	serverID	[]string	false	"serverID"
// @Failure 403
// @router /stop [post]
func (i *InstanceController) Stop() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	myCache := utils.GetUCache()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	errs := []error{}
	for _, intanceID := range bodyResult["serverID"].([]interface{}) {
		server, getError := compute.GetCompute(claims["username"].(string)).GetServer(intanceID.(string))
		if getError != nil {
			response.StatusCode = utils.StatusBadRequest
			response.ErrorMsg = getError.Error()
			response.Body = false
			return
		}
		errStop := compute.GetCompute(claims["username"].(string)).StopServer(intanceID.(string))
		logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, intanceID.(string), helper.InstanceStop, errStop)
		if errStop != nil {
			errs = append(errs, errStop)
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errStop.Error())
		} else {
			o := orm.NewOrm()
			_, errTable := o.QueryTable("usg_history").Filter("os_resource_id", intanceID.(string)).Filter("end_date", "").Update(orm.Params{
				"sys_user_id":        SysUserID,
				"os_user_id":         OsUserID,
				"status":             "SHUTOFF",
				"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
				"last_log_action_id": logID,
			})

			if errTable != nil {
				errs = append(errs, errTable)
				i.RWMutex.Lock()
				defer i.RWMutex.Unlock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errTable.Error())
			}
			myCache.Set(server.ID, helper.Stoping)
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Delete ...
// @Title Delete
// @Description delete Delete
// @Param	serverID	[]string	false	"serverID"
// @Failure 403
// @router /delete [delete]
func (i *InstanceController) Delete() {
	claims, _ := i.CheckToken()
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()
	myCache := utils.GetUCache()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	volumes, volumesErr := blockstorage.GetBlockstorage(claims["username"].(string)).ListVolumes(osTenantID)
	if volumesErr != nil {
		response.StatusCode = utils.StatusBadRequest
		response.Body = false
		response.ErrorMsg = volumesErr.Error()
		return
	}

	quota, quotaErr := models.UserQuota(osTenantID, OsUserID)
	if quotaErr != nil {
		response.StatusCode = utils.StatusBadRequest
		response.Body = false
		response.ErrorMsg = quotaErr.Error()
		return
	}

	// errs := make(map[string](map[string]interface{}))
	errs := []error{}
	for _, instanceID := range bodyResult["serverID"].([]interface{}) {
		volumeSize := 0

		for _, vol := range volumes {
			if vol.Bootable == "true" {
				for _, i := range vol.Attachments {
					if i.ServerID == instanceID {
						volumeSize = volumeSize + vol.Size
					}
				}
			}
		}

		server, err := compute.GetCompute(claims["username"].(string)).GetServer(instanceID.(string))
		if err != nil {
			errs = append(errs, err)
			i.RWMutex.Lock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			i.RWMutex.Unlock()
		}
		flavor, _ := compute.GetCompute(claims["username"].(string)).GetFlavor(server.Flavor["id"].(string))

		errDelete := compute.GetCompute(claims["username"].(string)).DeleteServer(server.ID)
		logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, instanceID.(string), helper.InstanceDelete, errDelete)
		if errDelete != nil {
			errs = append(errs, err)
			i.RWMutex.Lock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			i.RWMutex.Unlock()
		} else {
			models.MinusUsageQuotaInstance(quota.ID, flavor.VCPUs, flavor.RAM, volumeSize)
			o := orm.NewOrm()
			_, errTable := o.QueryTable("usg_history").Filter("os_instance_id", instanceID.(string)).Filter("end_date", "").Update(orm.Params{
				"sys_user_id":        SysUserID,
				"os_user_id":         OsUserID,
				"status":             "DELETED",
				"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
				"last_log_action_id": logID,
			})
			if errTable != nil {
				i.RWMutex.Lock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errTable.Error())
				i.RWMutex.Unlock()
			}
			myCache.Set(server.ID, helper.Deleting)
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Resize ...
// @Title Resize
// @Description hint Resize
// @Param	serverID	string	false	"serverID"
// @Param	newflavorID	string	false	"newflavorID"
// @Failure 403
// @router /resize [put]
func (i *InstanceController) Resize() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
		"newflavorID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	serverID := bodyResult["serverID"].(string)

	server, _ := compute.GetCompute(claims["username"].(string)).GetServer(serverID)
	errResize := compute.GetCompute(claims["username"].(string)).ResizeServer(serverID, bodyResult["newflavorID"].(string))
	i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, serverID, helper.InstanceResize, errResize)

	if errResize != nil {
		response.StatusCode = 1
		response.ErrorMsg = errResize.Error()
		response.Body = true
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errResize.Error())
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// ConfirmResize ...
// @Title ConfirmResize
// @Description hint ConfirmResize
// @Param	serverID	string	false	"serverID"
// @Failure 403
// @router /confirmResize [post]
func (i *InstanceController) ConfirmResize() {
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	serverID := bodyResult["serverID"].(string)

	errConfirm := compute.GetCompute(claims["username"].(string)).ConfirmResizeServer(serverID)
	if errConfirm != nil {
		response.StatusCode = 1
		response.ErrorMsg = errConfirm.Error()
		response.Body = true
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errConfirm.Error())
		return
	}

	server, errServer := compute.GetCompute(claims["username"].(string)).GetServer(serverID)
	logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, serverID, helper.InstanceResizereject, errConfirm)

	quota, quotaErr := models.UserQuota(osTenantID, OsUserID)
	if quotaErr != nil {
		response.StatusCode = 1
		response.ErrorMsg = quotaErr.Error()
		response.Body = true
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quotaErr.Error())
		return
	}

	if errConfirm == nil && errServer == nil {
		flavor, errFlavor := compute.GetCompute(claims["username"].(string)).GetFlavor(server.Flavor["id"].(string))
		if errFlavor == nil {
			models.AddUsageQuotaInstance(quota.ID, flavor.VCPUs, flavor.RAM, 0)
			o := orm.NewOrm()
			_, errTable := o.QueryTable("usg_history").Filter("os_instance_id", server.ID).Filter("type", "Instance").Filter("end_date", "").Update(orm.Params{
				"sys_user_id":        SysUserID,
				"os_user_id":         OsUserID,
				"status":             "RESIZE",
				"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
				"ip":                 "",
				"last_log_action_id": logID,
			})
			if errTable != nil {
				i.RWMutex.Lock()
				defer i.RWMutex.Unlock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errTable.Error())
			} else {
				var usageObj database.UsgHistory
				usageObj = database.UsgHistory{
					SysUserID:       uint32(SysUserID),
					OsUserID:        OsUserID,
					Type:            "Instance",
					Hostname:        server.Name,
					OsResourceID:    server.ID,
					OsInstanceID:    server.ID,
					Flavor:          flavor.Name,
					DiskSize:        0,
					CPU:             flavor.VCPUs,
					RAM:             flavor.RAM,
					Status:          "ACTIVE",
					StartDate:       time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
					EndDate:         "",
					IP:              "",
					LastLogActionID: uint32(logID),
				}
				_, errTableInsert := o.Insert(&usageObj)
				if errTable != nil {
					i.RWMutex.Lock()
					defer i.RWMutex.Unlock()
					utils.SetLumlog(claims["email"].(string))
					logger.Error(errTableInsert.Error())
				}
			}
		} else {
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(errFlavor.Error())
		}
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// RevertResize ...
// @Title RevertResize
// @Description hint RevertResize
// @Param	serverID	string	false	"serverID"
// @Failure 403
// @router /revertResize [post]
func (i *InstanceController) RevertResize() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := compute.GetCompute(claims["username"].(string)).RevertResizeServer(bodyResult["serverID"].(string))
	server, _ := compute.GetCompute(claims["username"].(string)).GetServer(bodyResult["serverID"].(string))

	if err != nil {
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
	}

	i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, server.ID, helper.InstanceResizereject, err)
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// CreateSnapshot ...
// @Title CreateSnapshot
// @Description hint Create Snapshot from Instance
// @Param	serverID	string	false	"serverID"
// @Param	name	string	false	"name"
// @Failure 403
// @router /createSnapshot [post]
func (i *InstanceController) CreateSnapshot() {
	claims, _ := i.CheckToken()
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
		"name",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	osUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	quota, quataErr := models.UserQuota(osTenantID, osUserID)
	if quataErr != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quataErr.Error())
		response.StatusCode = 1
		response.ErrorMsg = quataErr.Error()
		response.Body = true
		return
	}

	if quota.Snapshot < quota.UsedSnapshot+1 {
		response.StatusCode = 1
		response.ErrorMsg = "Your snapshot quota has been exceeded"
		response.Body = true
		return
	}

	volumeList, err := blockstorage.GetBlockstorage(claims["username"].(string)).ListVolumes(osTenantID)
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}

	serverID := bodyResult["serverID"].(string)

	var volumeS volumes.Volume
	for _, v := range volumeList {
		if v.Attachments[0].ServerID == serverID {
			volumeS = v
			break
		}
	}

	server, _ := compute.GetCompute(claims["username"].(string)).GetServer(serverID)
	//Snapshot from volume (force)
	snapshot, errSnapshot := blockstorage.GetBlockstorage(claims["username"].(string)).CreateSnapshot(volumeS.ID, bodyResult["name"].(string), "")
	logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, serverID, helper.InstanceSnapshot, errSnapshot)
	models.AddUsageQuotaSnapshot(osTenantID, osUserID)

	go func(snapID, username string, logID uint32) {
		for {
			snapGet, errGet := blockstorage.GetBlockstorage(username).GetSnapshot(snapID)
			if errGet != nil {
				i.RWMutex.Lock()
				defer i.RWMutex.Unlock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errGet.Error())
				return
			}

			//  “available”, “error”, “creating”, “deleting”, and “error_deleting”.
			if snapGet.Status == "available" {
				o := orm.NewOrm()
				var usageObj database.UsgHistory
				usageObj = database.UsgHistory{
					SysUserID:       uint32(SysUserID),
					OsUserID:        osUserID,
					Type:            "SnapShot",
					Hostname:        snapGet.Name,
					OsResourceID:    snapGet.ID,
					OsInstanceID:    "",
					Flavor:          "",
					DiskSize:        snapGet.Size,
					CPU:             0,
					RAM:             0,
					Status:          "AVAILABLE",
					StartDate:       time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
					EndDate:         "",
					IP:              "",
					LastLogActionID: uint32(logID),
				}
				_, errTable := o.Insert(&usageObj)
				if errTable != nil {
					i.RWMutex.Lock()
					defer i.RWMutex.Unlock()
					utils.SetLumlog(claims["email"].(string))
					logger.Error(errTable.Error())
				}
				return
			}

			if snapGet.Status == "error" {
				return
			}

			time.Sleep(3 * time.Second)
		}

	}(snapshot.ID, claims["username"].(string), uint32(logID))

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = snapshot
	return
}

// Suspend ...
// @Title Suspend
// @Description hint Suspend
// @Param	serverID	[]string	false	"serverID"
// @Failure 403
// @router /suspend [post]
func (i *InstanceController) Suspend() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	myCache := utils.GetUCache()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	errs := []error{}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	for _, instanceID := range bodyResult["serverID"].([]interface{}) {
		err := compute.GetCompute(claims["username"].(string)).SuspendServer(instanceID.(string))
		server, _ := compute.GetCompute(claims["username"].(string)).GetServer(bodyResult["serverID"].(string))

		logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, server.ID, helper.InstanceSuspend, err)
		if err != nil {
			errs = append(errs, err)
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 1
			response.ErrorMsg = err.Error()
			response.Body = true
		} else {
			o := orm.NewOrm()
			_, errTable := o.QueryTable("usg_history").Filter("os_resource_id", instanceID.(string)).Filter("end_date", "").Update(orm.Params{
				"sys_user_id":        SysUserID,
				"os_user_id":         OsUserID,
				"status":             "SUSPEND",
				"end_date":           time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
				"last_log_action_id": logID,
			})

			if errTable != nil {
				errs = append(errs, errTable)
				i.RWMutex.Lock()
				defer i.RWMutex.Unlock()
				utils.SetLumlog(claims["email"].(string))
				logger.Error(errTable.Error())
			}
			myCache.Set(server.ID, helper.Suspending)
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Resume ...
// @Title Resume
// @Description hint Resume
// @Param	serverID	[]string	false	"serverID"
// @Failure 403
// @router /resume [post]
func (i *InstanceController) Resume() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	myCache := utils.GetUCache()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse
	errs := []error{}

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	for _, intsanceID := range bodyResult["serverID"].([]interface{}) {
		err := compute.GetCompute(claims["username"].(string)).ResumeServer(intsanceID.(string))
		server, _ := compute.GetCompute(claims["username"].(string)).GetServer(bodyResult["serverID"].(string))
		logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, server.ID, helper.InstanceResume, err)
		if err != nil {
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 1
			response.ErrorMsg = err.Error()
			response.Body = true
			errs = append(errs, err)
		} else {
			go func(intsanceID, username string, logID int64) {
				for {
					server, errServer := compute.GetCompute(claims["username"].(string)).GetServer(intsanceID)
					if errServer == nil {
						if server.Status == "ACTIVE" {
							flavor, errFlavor := compute.GetCompute(claims["username"].(string)).GetFlavor(server.Flavor["id"].(string))
							o := orm.NewOrm()
							if errFlavor == nil {
								var usageObj database.UsgHistory
								usageObj = database.UsgHistory{
									SysUserID:       uint32(SysUserID),
									OsUserID:        OsUserID,
									Type:            "Instance",
									Hostname:        server.Name,
									OsResourceID:    server.ID,
									OsInstanceID:    server.ID,
									Flavor:          flavor.Name,
									DiskSize:        0,
									CPU:             flavor.VCPUs,
									RAM:             flavor.RAM,
									Status:          "ACTIVE",
									StartDate:       time.Now().Format(helper.TimeFormatYYYYMMDDHHMMSS),
									EndDate:         "",
									IP:              "",
									LastLogActionID: uint32(logID),
								}
								_, errTable := o.Insert(&usageObj)
								if errTable != nil {
									errs = append(errs, errTable)
									i.RWMutex.Lock()
									defer i.RWMutex.Unlock()
									utils.SetLumlog(claims["email"].(string))
									logger.Error(errTable.Error())
								}
								myCache.Set(server.ID, helper.Resuming)
							}
							return
						}
						if server.Status == "ERROR" {
							return
						}
					} else {
						return
					}

					time.Sleep(3 * time.Second)
				}
			}(intsanceID.(string), claims["username"].(string), logID)
		}
	}

	if len(errs) != 0 {
		response.StatusCode = 1
		for _, e := range errs {
			response.ErrorMsg += e.Error() + "\n"
		}
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Console ...
// @Title Creating remote console url
// @Param	serverID 	string	true		"serverID"
// @Description hint Console
// @Failure 403
// @router /console [post]
func (i *InstanceController) Console() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	vnc, err := compute.GetCompute(claims["username"].(string)).CreateRemoteConsole(bodyResult["serverID"].(string))
	server, serverErr := compute.GetCompute(claims["username"].(string)).GetServer(bodyResult["serverID"].(string))
	if serverErr == nil {
		i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, server.ID, helper.InstanceVNC, err)
	}

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = vnc
	return
}

// AddSecGroup ...
// @Title	AddSecGroup
// @Description add secgroup to instance
// @Param	secgroup_id	string	true	"secgroup_id"
// @Param	server_id	string	true	"server_id"
// @Success	200
// @Failure	403
// @router	/addSecGroup [post]
func (i *InstanceController) AddSecGroup() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"secgroup_id",
		"server_id",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	err := compute.GetCompute(claims["username"].(string)).AddSecGroupToServer(bodyResult["server_id"].(string), bodyResult["secgroup_id"].(string))
	server, _ := compute.GetCompute(claims["username"].(string)).GetServer(bodyResult["server_id"].(string))
	i.CreateLog(helper.OPENSTACK, helper.INSTANCE, server.Name, server.ID, helper.InstanceAddSecGroup, err)
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = true
	return
}

// Create ...
// @Title Create
// @Description post Create Instance үүсгэх
// @Param   flavor_id   string  true    "flavor_id"
// @Param   image_id    string  true    "image_id"
// @Param   name    string  true    "name"
// @Param   keypair_name    string  true    "keypair_name"
// @Param   secgroup_ids    []string    true    "secgroup_ids"
// @Param   custom_flavor   map[string]interface{}  true    "custom_flavor"
// @Param   disk_size int true "disk_size"
// @Param   script_id string true "script_id"
// @Param   availability_zone string true "availability_zone"
// @Param   password   string  true    "password"
// @Param   username   string  true    "username"
// @Param   network_id  string  true    "network_id"
// @Failure 403
// @router /create [post]
func (i *InstanceController) Create() {
	claims, _ := i.CheckToken()
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()
	var response models.BaseResponse
	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{
		"flavor_id",
		"image_id",
		"name",
		"keypair_name",
		"secgroup_ids",
		"custom_flavor",
		"disk_size",
		"script_id",
		"availability_zone",
		"password",
		"username",
		"network_id",
	}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}
	// Get paramaters
	flavorID := bodyResult["flavor_id"].(string)
	imageID := bodyResult["image_id"].(string)
	name := bodyResult["name"].(string)
	keypairName := bodyResult["keypair_name"].(string)
	secgroupIDs := bodyResult["secgroup_ids"].([]interface{})
	customFlavor := bodyResult["custom_flavor"].(map[string]interface{})
	diskSize := int(bodyResult["disk_size"].(float64))
	availabilityZone := bodyResult["availability_zone"].(string)
	instancePassword := bodyResult["password"].(string)
	instanceUsername := bodyResult["username"].(string)
	networkID := bodyResult["network_id"].(string)
	OsUserID := claims["userID"].(string)
	SysUserID := claims["sysUserID"].(float64)

	if imageID == "" || name == "" {
		response.StatusCode = 1
		response.ErrorMsg = "Missing params"
		return
	}

	isUbuntu := false
	isCentos := false

	switch imageID {
	// Ubuntu 18.04 LTS
	case "545579a8-ab88-4333-ab2b-6b56bd57cb08":
		isUbuntu = true
		// Ubuntu 20.04 LTS
	case "507181c5-fe98-48ca-a532-e9339f4c860a":
		isUbuntu = true
		// CentOS 7
	case "c5e25cd8-2e31-4982-a190-57fd5907fa26":
		isCentos = true
		// CentOS 8
	case "647bd041-6d1b-485a-86a1-8ed3e4eefce5":
		isCentos = true
	}

	if networkID == "" {
		networkID = beego.AppConfig.String("public.network.id")
	}
	//get scipt file
	scriptID, _ := strconv.Atoi(bodyResult["script_id"].(string))

	lapp := models.GetLappByID(uint32(scriptID)) //lighting app
	// get image

	image, _ := compute.GetCompute(claims["username"].(string)).GetImage(imageID)
	core, _ := ioutil.ReadFile("files/root.txt")
	rootName, _ := ioutil.ReadFile("files/rootName.txt")
	rootPass, _ := ioutil.ReadFile("files/rootPass.txt")
	ubuntuRepo, _ := ioutil.ReadFile("files/ubuntuRepo.yml")
	centosRepo, _ := ioutil.ReadFile("files/centosRepo.yml")

	var scripted []byte

	if len(lapp.PathYml) > 0 {
		instanceApp, _ := ioutil.ReadFile("files/" + image.ID + lapp.PathYml)
		// []byte, []byte append
		scripted = make([]byte, 0, len(rootName)+len(instanceApp))
		scripted = append(scripted, core...)
		if instanceUsername != "" && instancePassword != "" {
			scripted = append(scripted, []byte(rootName)...)
			scripted = append(scripted, []byte(instanceUsername)...)
			scripted = append(scripted, rootPass...)
			scripted = append(scripted, []byte(instancePassword)...)
			scripted = append(scripted, []byte("\n")...)
			scripted = append(scripted, []byte("ssh_pwauth: True")...)
		}
		scripted = append(scripted, instanceApp...)
	}

	if scriptID == 0 {
		scripted = make([]byte, 0)
		scripted = append(scripted, core...)
		if instanceUsername != "" && instancePassword != "" {
			scripted = append(scripted, []byte(rootName)...)
			scripted = append(scripted, []byte(instanceUsername)...)
			scripted = append(scripted, rootPass...)
			scripted = append(scripted, []byte(instancePassword)...)
			scripted = append(scripted, []byte("\n")...)
			scripted = append(scripted, []byte("ssh_pwauth: True")...)
		}

		if isCentos {
			scripted = append(scripted, centosRepo...)
		}
		if isUbuntu {
			scripted = append(scripted, ubuntuRepo...)
		}
	}

	fmt.Println("--------------------")
	fmt.Println(string(scripted))
	fmt.Println("--------------------")

	isCustomFlavor := len(customFlavor)
	secGroup := make([]string, len(secgroupIDs))
	for i, v := range secgroupIDs {
		secGroup[i] = fmt.Sprint(v)
	}

	quota, quotaErr := models.CheckUserQuota(osTenantID, OsUserID)
	if quotaErr != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(quotaErr.Error())
		response.StatusCode = 100
		response.ErrorMsg = quotaErr.Error()
		response.Body = true
		return
	}

	if quota.VolumeSize < quota.UsedVolumeSize+diskSize && quota.Volume < quota.UsedVolume+1 {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error("Your quota has been exceeded")
		response.StatusCode = 100
		response.ErrorMsg = "Your disk quota has been exceeded"
		response.Body = true
		return
	}

	if quota.Instances < quota.UsedInstances+1 {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error("Your instance count quota has been exceeded")
		response.StatusCode = 100
		response.ErrorMsg = "Your instance count quota has been exceeded"
		response.Body = true
		return
	}

	if isCustomFlavor != 0 {

		customRAM := int(customFlavor["ram"].(float64)) * 1024
		customVCPU := int(customFlavor["vcpu"].(float64))

		if (quota.CPU < quota.UsedCPU+customVCPU) && (quota.RAM < quota.UsedRAM+customRAM) {
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error("Your quota has been exceeded")
			response.StatusCode = 100
			response.ErrorMsg = "Your cpu, ram quota has been exceeded"
			response.Body = true
			return
		}

		flavor, err := compute.GetCompute(claims["username"].(string)).CreateFlavor(name+"_flavor", customRAM, customVCPU, true)
		if err != nil {
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 100
			response.ErrorMsg = err.Error()
			response.Body = true
			return
		}

		server, err := compute.GetCompute(claims["username"].(string)).CreateServerForVolume(imageID, name, flavor.ID, keypairName, diskSize, secGroup, scripted, availabilityZone, true, networkID)
		if err != nil {
			i.RWMutex.Lock()
			defer i.RWMutex.Unlock()
			utils.SetLumlog(claims["email"].(string))
			logger.Error(err.Error())
			response.StatusCode = 100
			response.ErrorMsg = err.Error()
			response.Body = true
			return
		}

		models.AddUsageQuotaInstance(quota.ID, customVCPU, customRAM, diskSize)

		logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, name, server.ID, helper.InstanceCreate, err)

		if err == nil {
			go func(serverid string, username string, logID int64, customVCPU, customRAM, diskSize int) {
				for {
					server, errServer := compute.GetCompute(claims["username"].(string)).GetServer(serverid)
					if errServer != nil {
						return
					}

					if server.Status == "ACTIVE" {
						models.CreateUsageAction(uint32(SysUserID), OsUserID, "Instance", name, server.ID, server.ID, flavor.Name, "ACTIVE", "", logID, time.Now(), time.Time{}, 0, flavor.VCPUs, flavor.RAM)

						if bodyResult["network_id"].(string) == "" {
							for _, l := range server.Addresses[beego.AppConfig.String("public.network.name")].([]interface{}) {
								tmp := l.(map[string]interface{})
								models.CreateUsageAction(uint32(SysUserID), OsUserID, "IP", tmp["addr"].(string), "", server.ID, "", "ACTIVE", tmp["addr"].(string), logID, time.Now(), time.Time{}, 0, 0, 0)
							}
						}

						for _, volume := range server.AttachedVolumes {
							models.CreateUsageAction(uint32(SysUserID), OsUserID, "Volume", volume.ID, volume.ID, server.ID, "", "ACTIVE", "", logID, time.Now(), time.Time{}, diskSize, 0, 0)
						}
						fmt.Println(server.AttachedVolumes)
						return
					}
					if server.Status == "ERROR" {
						return
					}

					time.Sleep(3 * time.Second)
				}
			}(server.ID, claims["username"].(string), logID, customVCPU, customRAM, diskSize)

		}

		response.StatusCode = 0
		response.ErrorMsg = ""
		response.Body = server
		return
	}

	server, err := compute.GetCompute(claims["username"].(string)).CreateServerForVolume(imageID, name, flavorID, keypairName, diskSize, secGroup, scripted, availabilityZone, true, networkID)
	logID := i.CreateLog(helper.OPENSTACK, helper.INSTANCE, name, server.ID, helper.InstanceCreate, err)
	flavorRes, errGetFlavor := compute.GetCompute(claims["username"].(string)).GetFlavor(flavorID)
	if errGetFlavor != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
	}
	models.AddUsageQuotaInstance(quota.ID, flavorRes.VCPUs, flavorRes.RAM, diskSize)

	if err == nil {
		go func(serverid string, username string, logID int64) {
			for {
				server, errServer := compute.GetCompute(claims["username"].(string)).GetServer(serverid)
				if errServer != nil {
					return
				}

				if server.Status == "ACTIVE" {
					models.CreateUsageAction(uint32(SysUserID), OsUserID, "Instance", name, server.ID, server.ID, flavorRes.Name, "ACTIVE", "", logID, time.Now(), time.Time{}, 0, flavorRes.VCPUs, flavorRes.RAM)

					if bodyResult["network_id"].(string) == "" {
						for _, l := range server.Addresses[beego.AppConfig.String("public.network.name")].([]interface{}) {
							tmp := l.(map[string]interface{})
							models.CreateUsageAction(uint32(SysUserID), OsUserID, "IP", tmp["addr"].(string), "", server.ID, "", "ACTIVE", tmp["addr"].(string), logID, time.Now(), time.Time{}, 0, 0, 0)
						}
					}
					for _, volume := range server.AttachedVolumes {
						models.CreateUsageAction(uint32(SysUserID), OsUserID, "Volume", volume.ID, volume.ID, server.ID, "", "ACTIVE", "", logID, time.Now(), time.Time{}, diskSize, 0, 0)
					}
					fmt.Println(server.AttachedVolumes)
					return
				}
				if server.Status == "ERROR" {
					return
				}

				time.Sleep(3 * time.Second)
			}
		}(server.ID, claims["username"].(string), logID)
	}

	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 100
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = server
	return
}

// Clone ...
// @Title clone instance
// @Description hint Create Snapshot from Instance
// @Param	serverID	string	false	"serverID"
// @Param	name	string	false	"name"
// @Failure 403
// @router /clone [post]
func (i *InstanceController) Clone() {
	claims, _ := i.CheckToken()
	osTenantID := i.GetHeaderValue("OS-Tenant-ID")
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
		"serverName",
		"keypairName",
		"instancePassword",
		"instanceUsername",
	}

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	// osUserID := claims["userID"].(string)
	// sysUserID := claims["sysUserID"].(float64)
	network := beego.AppConfig.String("public.network.id")

	serverID := bodyResult["serverID"].(string)
	serverName := bodyResult["serverName"].(string)
	keypairName := bodyResult["keypairName"].(string)
	instancePassword := bodyResult["instancePassword"].(string)
	instanceUsername := bodyResult["instanceUsername"].(string)

	volumeList, err := blockstorage.GetBlockstorage(claims["username"].(string)).ListVolumes(osTenantID)
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		return
	}
	var volumeS volumes.Volume
	for _, v := range volumeList {
		if len(v.Attachments) > 0 {
			if v.Attachments[0].ServerID == serverID {
				volumeS = v
				break
			}
		}
	}

	server, errServer := compute.GetCompute(claims["username"].(string)).GetServer(serverID)
	if errServer != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errServer.Error())
		response.StatusCode = 1
		response.ErrorMsg = errServer.Error()
		response.Body = true
		return
	}

	// Request ----------------------------------
	core, _ := ioutil.ReadFile("files/root.txt")
	rootName, _ := ioutil.ReadFile("files/rootName.txt")
	rootPass, _ := ioutil.ReadFile("files/rootPass.txt")

	var userData []byte

	if instancePassword != "" && instanceUsername != "" {
		userData = make([]byte, 0)
		userData = append(userData, core...)
		if instanceUsername != "" && instancePassword != "" {
			userData = append(userData, []byte(rootName)...)
			userData = append(userData, []byte(instanceUsername)...)
			userData = append(userData, rootPass...)
			userData = append(userData, []byte(instancePassword)...)
		}
	}

	fmt.Println("--------------------")
	fmt.Println("--------------------")
	fmt.Println(string(userData))
	fmt.Println("--------------------")
	fmt.Println("--------------------")
	// Request ----------------------------------

	volumeSnap, errVolumeSnap := blockstorage.GetBlockstorage(claims["username"].(string)).CreateVolume(volumeS.Size, serverName+" volume", "", volumeS.ID)
	if errVolumeSnap != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errVolumeSnap.Error())
		response.StatusCode = 1
		response.ErrorMsg = errVolumeSnap.Error()
		return
	}

	fmt.Println("--------------------")
	fmt.Println("--------------------")
	fmt.Println(server.Flavor)
	fmt.Println("--------------------")
	fmt.Println("--------------------")

	newServer, errCreateServer := compute.GetCompute(claims["username"].(string)).CreateServerFromExistVolume(volumeSnap.ID, serverName, server.Flavor["id"].(string), network, keypairName, userData)
	if errCreateServer != nil {
		blockstorage.GetBlockstorage(claims["username"].(string)).DeleteVolume(volumeSnap.ID)
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errCreateServer.Error())
		response.StatusCode = 1
		response.ErrorMsg = errCreateServer.Error()
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = newServer
	return
}

// Action ...
// @Title Action
// @Description hint RevertResizeInstance
// @Param	serverID	string	false	"serverID"
// @Failure 403
// @router /action [post]
func (i *InstanceController) Action() {
	type InstanceAction struct {
		// Action is the name of the action.
		Action string `json:"action"`

		// InstanceUUID is the UUID of the instance.
		InstanceUUID string `json:"instance_uuid"`

		// Message is the related error message for when an action fails.
		Message string `json:"message"`

		// Project ID is the ID of the project which initiated the action.
		ProjectID string `json:"project_id"`

		// RequestID is the ID generated when performing the action.
		RequestID string `json:"request_id"`

		// StartTime is the time the action started.
		StartTime time.Time `json:"start_time"`

		// UserID is the ID of the user which initiated the action.
		UserID string `json:"user_id"`
	}

	var main []InstanceAction

	claims, _ := i.CheckToken()
	logger := utils.GetLogger()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
	}

	serverID := bodyResult["serverID"].(string)

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	provider := models.GetProvider(claims["email"].(string))
	opts := instanceactions.ListOpts{}

	allPages, _ := instanceactions.List(models.GetClientCompute(provider), serverID, opts).AllPages()
	actions, errActions := instanceactions.ExtractInstanceActions(allPages)

	for _, i := range actions {
		var each InstanceAction
		each.Action = i.Action
		each.InstanceUUID = i.InstanceUUID
		each.Message = i.Message
		each.ProjectID = i.ProjectID
		each.RequestID = i.RequestID
		each.UserID = i.UserID
		each.StartTime = i.StartTime

		main = append(main, each)
	}

	if errActions != nil {
		response.StatusCode = 1
		response.ErrorMsg = errActions.Error()
		response.Body = nil
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errActions.Error())
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = main
	return
}

// ShowConsole ...
// @Title ShowConsole
// @Description hint ShowConsole
// @Param	serverID	string	false	"serverID"
// @Param	length	string	false	"length"
// @Failure 403
// @router /showConsole [post]
func (i *InstanceController) ShowConsole() {
	claims, _ := i.CheckToken()

	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	var response models.BaseResponse

	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()

	bodyString := []string{
		"serverID",
		"length",
	}

	serverID := bodyResult["serverID"].(string)
	length := bodyResult["length"].(float64)

	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	console := compute.GetCompute(claims["username"].(string)).ShowConsole(serverID, int(length))
	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = console.Body
	return
}

// OnWAF ...
// @Title OnWAF
// @Description post OnWAF Instance үүсгэх
// @Param   serverID  string  true    "serverID"
// @Failure 403
// @router /on_waf [post]
func (i *InstanceController) OnWAF() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	OsTenantID := i.GetHeaderValue("OS-Tenant-ID")
	var response models.BaseResponse
	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{"serverID"}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	// Get paramaters
	instanceID := bodyResult["serverID"].(string)
	server, errServer := compute.GetCompute(claims["username"].(string)).GetServer(instanceID)
	if errServer != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(errServer.Error())
		response.StatusCode = 1
		response.ErrorMsg = errServer.Error()
		response.Body = true
		return
	}

	networkName := ""
	ipAddress := ""

	for k, v := range server.Addresses {
		networkName = k
		inters, _ := v.([]interface{})
		for _, ipAddr := range inters {
			tmp := make(map[string]interface{})
			tmp = ipAddr.(map[string]interface{})
			ipAddress = tmp["addr"].(string)
		}
		break
	}

	pubNetworkName := beego.AppConfig.String("public.network.name")

	result, err := heat.CreateStack(claims["username"].(string), server.Name, networkName, pubNetworkName, ipAddress)
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	resultBody := result.Body.(map[string]interface{})
	bodyStack := resultBody["stack"].(map[string]interface{})
	stackID := bodyStack["id"].(string)
	waf, err := models.CreateWAF(database.WAFs{
		OsTenantID: OsTenantID,
		OsUserID:   server.UserID,
		Name:       fmt.Sprintf("%s_waf", server.Name),
		WafID:      stackID,
		InstanceID: server.ID,
	})
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = waf
	return
}

// OffWAF ...
// @Title OnWAF
// @Description post OnWAF Instance үүсгэх
// @Param   serverID  string  true    "serverID"
// @Failure 403
// @router /off_waf [delete]
func (i *InstanceController) OffWAF() {
	claims, _ := i.CheckToken()
	logger := utils.GetLogger()
	var response models.BaseResponse
	bodyResult := shared.RetrieveDataFromBody(i.Ctx.Request.Body)
	defer func() {
		i.Data["json"] = response
		i.ServeJSON()
	}()
	bodyString := []string{"serverID"}
	if sCod, eMsg := shared.CheckBodyResult(bodyResult, bodyString); sCod == 100 {
		response.StatusCode = sCod
		response.ErrorMsg = eMsg
		return
	}

	// Get paramaters
	instanceID := bodyResult["serverID"].(string)

	waf, err := models.GetInstanceWAFs(instanceID)
	if err != nil {
		i.RWMutex.Lock()
		defer i.RWMutex.Unlock()
		utils.SetLumlog(claims["email"].(string))
		logger.Error(err.Error())
		response.StatusCode = 1
		response.ErrorMsg = err.Error()
		response.Body = true
		return
	}

	result := heat.DeleteStack(claims["username"].(string), waf)
	models.DeleteWAF(waf.ID)

	response.StatusCode = 0
	response.ErrorMsg = ""
	response.Body = result
	return
}
