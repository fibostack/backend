package main

import (
	"crypto/tls"
	"net/http"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
	"gitlab.com/fibo-stack/backend/middleware"
	"gitlab.com/fibo-stack/backend/models"
	_ "gitlab.com/fibo-stack/backend/routers"
	"gitlab.com/fibo-stack/backend/utils"
)

func main() {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}

	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "GET", "POST", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Access-Control-Allow-Origin", "Content-Type", "X-CSRF-Token", "Accept-Encoding", "Access-Control-Allow-Headers", "Content-Length", "Authorization", "Multi-Language", "OS-Tenant-ID", "SourceIP"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type", "X-CSRF-Token", "Accept-Encoding", "Authorization", "Multi-Language", "OS-Tenant-ID", "SourceIP"},
		AllowCredentials: true,
	}))

	utils.SetEndpoints()
	utils.UCacheInit()
	models.InitDB()
	beego.InsertFilter("*", beego.BeforeRouter, middleware.ProviderClient)

	beego.Run()
}
