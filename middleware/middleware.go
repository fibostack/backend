package middleware

import (
	"strings"

	"github.com/astaxie/beego/context"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/fibo-stack/backend/models"
)

var jwtKey = []byte("zzkAdVzUv2cMWxExdH9cHWecGgYe39pdsJEE6eXhVqMThFVGJAb5CB83SaGtspJezrVxxEpc7JLLDfsyZhDCCux6JqfLveuPaBZD3YtPKPWksbFrXCnDjaCVFhdhef8P")

// ProviderClient ...
var ProviderClient = func(ctx *context.Context) {
	apiURL := ctx.Input.URL()
	urlArray := strings.Split(apiURL, "/")
	if len(urlArray) > 2 && urlArray[3] != "auth" &&
		urlArray[3] != "roles" &&
		urlArray[3] != "initall" &&
		urlArray[3] != "permissions" &&
		urlArray[3] != "quotas" &&
		urlArray[3] != "initAll" &&
		urlArray[4] != "getPersonalInfo" {
		osTenantID := ctx.Request.Header["Os-Tenant-Id"]
		jwtToken := ctx.Request.Header["Authorization"]

		if len(osTenantID) == 0 {
			ctx.ResponseWriter.WriteHeader(422)
			return
		}

		if len(jwtToken) == 0 {
			ctx.ResponseWriter.WriteHeader(401)
			return
		}

		retClaim := jwt.MapClaims{}
		JwtToken, err := jwt.ParseWithClaims(jwtToken[0], retClaim, func(t *jwt.Token) (interface{}, error) {
			return []byte(jwtKey), nil
		})

		if err == nil {
			if !JwtToken.Valid {
				ctx.ResponseWriter.WriteHeader(401)
				return
			}
		} else {
			ctx.ResponseWriter.WriteHeader(401)
			return
		}

		email := retClaim["email"].(string)
		providerClient := models.GetProvider(email)

		if providerClient == nil {
			userData, _ := models.UserEmailSearch(email)
			provider, err := models.InitOpenstackProvider(userData.Email, userData.OsPwd, userData.OsTenantID)
			if err != nil {
				ctx.ResponseWriter.WriteHeader(401)
				return
			}
			models.SetProvider(provider, userData.Email)
		}

		return
	}

	return
}
