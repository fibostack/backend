
sudo cat > /etc/systemd/system/fibo.service <<EOL
[Unit]
Description=Fibo backend service            

[Service]
User=stack           
WorkingDirectory=/opt/stack/fibostack/backend    
ExecStart=/opt/stack/fibostack/backend/backend                          
Restart=always

[Install]
WantedBy=multi-user.target
EOL
sudo systemctl daemon-reload
sudo systemctl start fibo.service
sudo systemctl enable fibo.service

echo "Backend started at your_ip:8081"
echo "You can check by 'sudo systemctl status fibo.service'"

sudo rm /etc/apache2/ports.conf
sudo cat > /etc/apache2/ports.conf <<EOL
Listen 80
Listen 8888

<IfModule ssl_module>
	Listen 443
</IfModule>

<IfModule mod_gnutls.c>
	Listen 443
</IfModule>
EOL

sudo rm /etc/apache2/sites-available/horizon.conf
sudo cat > /etc/apache2/sites-available/horizon.conf <<EOL
<VirtualHost *:8888>
    WSGIScriptAlias /dashboard /opt/stack/horizon/openstack_dashboard/wsgi.py
    WSGIDaemonProcess horizon user=stack group=stack processes=3 threads=10 home=/opt/stack/horizon display-name=%{GROUP}
    WSGIApplicationGroup %{GLOBAL}

    SetEnv APACHE_RUN_USER stack
    SetEnv APACHE_RUN_GROUP stack
    WSGIProcessGroup horizon

    DocumentRoot /opt/stack/horizon/.blackhole/
    Alias /dashboard/media /opt/stack/horizon/openstack_dashboard/static
    Alias /dashboard/static /opt/stack/horizon/static

    RedirectMatch "^/$" "/dashboard/"

    <Directory />
        Options FollowSymLinks
        AllowOverride None
    </Directory>

    <Directory /opt/stack/horizon/>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride None
        # Apache 2.4 uses mod_authz_host for access control now (instead of
        #  "Allow")
        <IfVersion < 2.4>
            Order allow,deny
            Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
            Require all granted
        </IfVersion>
    </Directory>
    <IfVersion >= 2.4>
      ErrorLogFormat "%{cu}t %M"
    </IfVersion>
    ErrorLog /var/log/apache2/horizon_error.log
    LogLevel warn
    CustomLog /var/log/apache2/horizon_access.log combined
</VirtualHost>

WSGISocketPrefix /var/run/apache2
EOL

sudo a2dissite horizon.conf
sudo a2ensite horizon.conf

sudo cat > /etc/apache2/sites-available/frontend.conf <<EOL
<VirtualHost *:80>
    ServerName localhost
    ProxyPreserveHost on
    ProxyPass / http://localhost:3000/
</VirtualHost>
EOL

sudo a2ensite frontend.conf
sudo /etc/init.d/apache2 restart