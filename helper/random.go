package helper

import (
	"crypto/rand"
	"fmt"
)

// GenerateInvoiceNumber ...
func GenerateInvoiceNumber() string {
	b := make([]byte, 5)
	_, errRand := rand.Read(b)

	if errRand != nil {
		return ""
	}
	uniqueID := fmt.Sprintf("%X", b)

	return uniqueID
}
