package helper

// constants
const (
	Create = "Create"
	Update = "Update"
	Delete = "Delete"

	OPENSTACK   = "OPENSTACK"
	INSTANCE    = "Instance"
	EC2         = "EC2"
	HostedZone  = "Hosted Zone"
	HealthCheck = "Health Check"

	InstanceCreate             = "Create"
	InstanceStart              = "Start"
	InstanceStop               = "Stop"
	InstanceSoftreboot         = "SoftReboot"
	InstanceHardreboot         = "HardReboot"
	InstanceReboot             = "Reboot"
	InstanceResume             = "Resume"
	InstanceSuspend            = "Suspend"
	InstanceResize             = "Resize"
	InstanceDelete             = "Delete"
	InstanceResizeconfirm      = "ResizeConfirm"
	InstanceResizereject       = "ResizeReject"
	InstanceAssociatefloatip   = "AssociateFloatIP"
	InstanceDiassociatefloatip = "DiAssociateFloatIP"
	InstanceAttachinterface    = "AttachInterface"
	InstanceDetachinterface    = "DetachInterface"
	InstanceAttachvolume       = "InstanceAttachVolume"
	InstanceDetachvolume       = "DetachVolume"
	InstanceSnapshot           = "Snapshot"
	InstanceVNC                = "VNC"
	InstanceAddSecGroup        = "AddSecurityGroup"

	KEYPAIR = "KeyPair"

	KeyPairCreate = "Create"
	KeyPairImport = "Import"
	KeyPairDelete = "Delete"

	Image = "Image"

	Volume           = "Volume"
	VolumeAttach     = "Attach on instance"
	VolumeDetach     = "Detach on instance"
	VolumeToSnapshot = "Volume to snapshot"
	VolumeExtend     = "Extend"
	VolumeSnapshot   = "Create snapshot"

	Snapshot       = "Snapshot"
	SnapshotLaunch = "Launch instance"

	LoadBalancer = "LoadBalancer"

	SecurityGroup           = "SecurityGroup"
	SecurityGroupRuleCreate = "Rule create"
	SecurityGroupRuleDelete = "Rule Delete"

	Project                = "Project"
	ProjectSetComputeQuota = "Set compute quota"
	ProjectSetBlockQuota   = "Set block quota"
	ProjectAddMember       = "Add member"
	ProjectRemoveMember    = "Remove member"
	ProjectUpdateUserRole  = "Update user role"
	ProjectUpdateUserQuota = "Update user quota"

	User               = "User"
	UserChangePassword = "Change password"
	UpdateStatus       = "Update status"

	Credentials  = "Credentials"
	ChangeRegion = "Change region"

	Network           = "Network"
	NetworkCreatePort = "Create port"
	NetworkUpdatePort = "Update port"
	NetworkDeletePort = "Delete port"

	NetworkCreateFIP       = "Create floating ip"
	NetworkAssociateFIP    = "Associate floating ip"
	NetworkDisassociateFIP = "Disassociate floating ip"
	NetworkDeleteFIP       = "Delete floating ip"

	NetworkCreateSubnet    = "Create subnet"
	NetworkDeleteSubnet    = "Delete subnet"
	NetworkUpdateSubnet    = "Update subnet"
	NetworkRemoveGTWSubnet = "Remove gateway subnet"

	NetworkCreateRouter            = "Create router"
	NetworkUpdateRouter            = "Update router"
	NetworkDeleteRouter            = "Delete router"
	NetworkAddRoutesToRouter       = "Add router from router"
	NetworkRemoveRoutesToRouter    = "Remove router from router"
	NetworkRemoveAllRoutesToRouter = "Remove all routes from router"
	NetworkAddSubnetToRouter       = "Add subnet to router"
	NetworkRemoveSubnetFromRouter  = "Remove subnet to router"
	NetworkAddPortToRouter         = "Add port to router"
	NetworkRemovePortFromRouter    = "Remove port to router"

	Flavor               = "Flavor"
	Permission           = "Permission"
	Role                 = "Role"
	RoleUpdatePermission = "Update permission"

	BILLINGALARM = "BILLING ALARM"
	AlarmCreate  = "Create alarm"
	DeleteCreate = "Delete alarm"
)
