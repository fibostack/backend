package helper

// constants
const (
	ResourceTypeInstance = "Instance"
	ResourceTypeIP       = "IP"
	ResourceTypeVolume   = "Volume"
	ResourceTypeSnapshot = "Snapshot"

	AdminRole  = 1
	SeniorRole = 2
	JuniorRole = 3
	NoviceRole = 4
	ReaderRole = 5

	UsgDailyResourceTypeCPU      = "cpu"
	UsgDailyResourceTypeRAM      = "ram"
	UsgDailyResourceTypeIP       = "ip"
	UsgDailyResourceTypeVolume   = "volume"
	UsgDailyResourceTypeSnapshot = "snapshot"

	UsgDailyMeasurementCPU      = "cpu/hour"
	UsgDailyMeasurementRAM      = "ram/hour"
	UsgDailyMeasurementIP       = "ip/hour"
	UsgDailyMeasurementVolume   = "volume/hour"
	UsgDailyMeasurementSnapshot = "snapshot/hour"

	InvoiceStatusUnpaid          = "4"
	InvoiceStatusNotComplete     = "3"
	InvoiceStatusAwaitingConfirm = "2"
	InvoiceStatusPaid            = "1"

	KhanEcommerce = uint32(2)
	KhanAccount   = uint32(5)
	QPay          = uint32(8)

	QuotaPlanSandbox  = "sandbox"
	QuotaPlanBasic    = "basic"
	QuotaPlanAdvanced = "advanced"

	MsgVerifiyMobile = "1"
	MsgPayment       = "2"

	GolomtBillPayStatusIncome  = "INCOME"
	GolomtBillPayStatusOUTCOME = "OUTCOME"

	TimeFormatYYYYMMDDHHMMSS = "20060102150405"
	TimeFormatYYYYMMDD       = "20060102"

	Admin  = "admin"
	Member = "member"

	Starting   = "STARTING"
	Stoping    = "STOPING"
	Deleting   = "DELETING"
	Suspending = "SUSPENDING"
	Resuming   = "RESUMING"
)
