package blockstorage

import (
	"strings"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/objectstorage/v1/objects"
)

// GetObjects ...
func (c *Blockstorage) GetObjects(client *gophercloud.ServiceClient, containerName, objectName string) (*objects.GetHeader, error) {
	opts := objects.GetOpts{}
	result, err := objects.Get(client, containerName, objectName, opts).Extract()
	return result, err
}

// ListObjects ...
func (c *Blockstorage) ListObjects(client *gophercloud.ServiceClient, containerName string) ([]objects.Object, error) {
	listOpts := objects.ListOpts{
		Full: true,
	}

	allPages, err := objects.List(client, containerName, listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allObjects, err := objects.ExtractInfo(allPages)
	return allObjects, err
}

// CreateObject ...
func (c *Blockstorage) CreateObject(client *gophercloud.ServiceClient, containerName, folderName, objectName, MIME, content string) (*objects.CreateHeader, error) {
	objectPath := objectName
	if folderName != "/" {
		objectPath = folderName + objectName
	}

	createOpts := objects.CreateOpts{
		ContentType: MIME,
		Content:     strings.NewReader(content),
	}

	object, err := objects.Create(client, containerName, objectPath, createOpts).Extract()
	return object, err
}

// DeleteObject ...
func (c *Blockstorage) DeleteObject(client *gophercloud.ServiceClient, containerName, objectName string) (*objects.DeleteHeader, error) {
	deleteOps := objects.DeleteOpts{}
	object, err := objects.Delete(client, containerName, objectName, deleteOps).Extract()
	if err != nil {
		return nil, err
	}
	return object, err
}

// DownloadObject ...
func (c *Blockstorage) DownloadObject(client *gophercloud.ServiceClient, containerName, objectName string) ([]byte, error) {
	object := objects.Download(client, containerName, objectName, nil)
	if object.Err != nil {
		return nil, object.Err
	}
	// if "ExtractContent" method is not called, the HTTP connection will remain consumed
	content, err := object.ExtractContent()
	return content, err
}

// CreateTempURL ...
func (c *Blockstorage) CreateTempURL(client *gophercloud.ServiceClient, containerName, objectName string) (string, error) {
	opts := objects.CreateTempURLOpts{
		Method: objects.GET,
		TTL:    180,
	}

	url, err := objects.CreateTempURL(client, containerName, objectName, opts)

	return url, err
}

// CreateObjectFolder ...
func (c *Blockstorage) CreateObjectFolder(client *gophercloud.ServiceClient, containerName, objectName string) (*objects.CreateHeader, error) {
	object, err := objects.Create(client, containerName, objectName+"/", nil).Extract()
	return object, err
}

// ObjectCopy ...
func (c *Blockstorage) ObjectCopy(client *gophercloud.ServiceClient, containerName, objectName, destination string) (*objects.CopyHeader, error) {

	copyOpts := objects.CopyOpts{
		Destination: destination,
	}

	object, err := objects.Copy(client, containerName, objectName, copyOpts).Extract()
	return object, err
}
