package blockstorage

import (
	"time"

	"github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/services"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/services

// ServiceDetail ...
type ServiceDetail struct {
	Binary            string    `json:"binary"`
	DisabledReason    string    `json:"disabled_reason"`
	Host              string    `json:"host"`
	State             string    `json:"state"`
	Status            string    `json:"status"`
	UpdatedAt         time.Time `json:"updated_at"`
	Zone              string    `json:"zone"`
	Frozen            bool      `json:"frozen"`
	Cluster           string    `json:"cluster"`
	ReplicationStatus string    `json:"replication_status"`
	ActiveBackendID   string    `json:"active_backend_id"`
}

// ListServices ...
func (c *Blockstorage) ListServices() (retServices []ServiceDetail, err error) {
	provider := models.GetProvider(c.Username)
	listOpts := services.ListOpts{}

	allPages, err := services.List(models.GetClientBlockstorage(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allServices, err := services.ExtractServices(allPages)
	for _, i := range allServices {
		var each ServiceDetail
		each.Binary = i.Binary
		each.DisabledReason = i.DisabledReason
		each.Host = i.Host
		each.State = i.State
		each.Status = i.Status
		each.UpdatedAt = i.UpdatedAt
		each.Zone = i.Zone
		each.Frozen = i.Frozen
		each.Cluster = i.Cluster
		each.ReplicationStatus = i.ReplicationStatus
		each.ActiveBackendID = i.ActiveBackendID
		retServices = append(retServices, each)
	}

	return retServices, err
}
