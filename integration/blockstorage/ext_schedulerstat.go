package blockstorage

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/schedulerstats"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/schedulerstats

// ListStatBSPools ...
func (c *Blockstorage) ListStatBSPools() ([]schedulerstats.StoragePool, error) {
	provider := models.GetProvider(c.Username)
	listOpts := schedulerstats.ListOpts{
		Detail: true,
	}

	allPages, err := schedulerstats.List(models.GetClientBlockstorage(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allStats, err := schedulerstats.ExtractStoragePools(allPages)
	if err != nil {
		return nil, err
	}

	for _, stat := range allStats {
		fmt.Printf("%+v\n", stat)
	}
	return allStats, err
}
