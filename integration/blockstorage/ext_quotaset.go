package blockstorage

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/quotasets"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/quotasets

// GetQuotaset ...
func (c *Blockstorage) GetQuotaset(projectID string) (*quotasets.QuotaSet, error) {
	provider := models.GetProvider(c.Username)
	quotaset, err := quotasets.Get(models.GetClientBlockstorage(provider), projectID).Extract()
	return quotaset, err
}

// GetQuotasetDetails ...
func (c *Blockstorage) GetQuotasetDetails(projectID string) (quotasets.QuotaUsageSet, error) {
	provider := models.GetProvider(c.Username)
	quotaset, err := quotasets.GetUsage(models.GetClientBlockstorage(provider), projectID).Extract()
	return quotaset, err
}

// GetQuotasetUsage ...
func (c *Blockstorage) GetQuotasetUsage(projectID string) (quotasets.QuotaUsageSet, error) {
	provider := models.GetProvider(c.Username)
	quotaset, err := quotasets.GetUsage(models.GetClientBlockstorage(provider), projectID).Extract()
	return quotaset, err
}

// UpdateQuotaset ...
func (c *Blockstorage) UpdateQuotaset(projectID string, volumes int) (*quotasets.QuotaSet, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := quotasets.UpdateOpts{
		Volumes: gophercloud.IntToPointer(volumes),
	}

	quotaset, err := quotasets.Update(models.GetClientBlockstorage(provider), projectID, updateOpts).Extract()
	return quotaset, err
}

// DeleteQuotaset ...
func (c *Blockstorage) DeleteQuotaset(projectID string) error {
	provider := models.GetProvider(c.Username)
	err := quotasets.Delete(models.GetClientBlockstorage(provider), projectID).ExtractErr()
	return err
}

// InitialQuotaset ...
func (c *Blockstorage) InitialQuotaset(provider *gophercloud.ProviderClient, projectID string) (*quotasets.QuotaSet, error) {
	updateOpts := quotasets.UpdateOpts{
		Volumes:   gophercloud.IntToPointer(0),
		Snapshots: gophercloud.IntToPointer(0),
		Gigabytes: gophercloud.IntToPointer(0),
	}

	quotaset, err := quotasets.Update(models.GetClientBlockstorage(provider), projectID, updateOpts).Extract()
	return quotaset, err
}

// NormalQuotaset ...
func (c *Blockstorage) NormalQuotaset(provider *gophercloud.ProviderClient, projectID string) (*quotasets.QuotaSet, error) {
	updateOpts := quotasets.UpdateOpts{
		Volumes:   gophercloud.IntToPointer(10),
		Snapshots: gophercloud.IntToPointer(10),
		Gigabytes: gophercloud.IntToPointer(1000),
	}

	quotaset, err := quotasets.Update(models.GetClientBlockstorage(provider), projectID, updateOpts).Extract()
	return quotaset, err
}

// SetQuotaset ...
func (c *Blockstorage) SetQuotaset(provider *gophercloud.ProviderClient, projectID string, volumes, snapshots, gigabytes int) (*quotasets.QuotaSet, error) {
	updateOpts := quotasets.UpdateOpts{
		Volumes:   gophercloud.IntToPointer(volumes),
		Snapshots: gophercloud.IntToPointer(snapshots),
		Gigabytes: gophercloud.IntToPointer(gigabytes),
	}

	quotaset, err := quotasets.Update(models.GetClientBlockstorage(provider), projectID, updateOpts).Extract()
	return quotaset, err
}
