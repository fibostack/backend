package blockstorage

import (
	"github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/volumetenants"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/volumes"
	"gitlab.com/fibo-stack/backend/models"
)

// use admin
// https://godoc.org/github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/volumetenants

// VolumeWithTenant ...
type VolumeWithTenant struct {
	volumes.Volume
	volumetenants.VolumeTenantExt
}

// ListVolumeTenants ...
func (c *Blockstorage) ListVolumeTenants() ([]VolumeWithTenant, error) {
	provider := models.GetProvider(c.Username)
	var allVolumes []VolumeWithTenant

	allPages, err := volumes.List(models.GetClientBlockstorage(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	err = volumes.ExtractVolumesInto(allPages, &allVolumes)
	if err != nil {
		return nil, err
	}

	return allVolumes, err
}
