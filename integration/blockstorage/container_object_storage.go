package blockstorage

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/objectstorage/v1/containers"
)

// ListContainer ...
func (c *Blockstorage) ListContainer(client *gophercloud.ServiceClient) ([]containers.Container, error) {
	listOpts := containers.ListOpts{
		Full: true,
	}

	allPages, err := containers.List(client, listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allContainers, err := containers.ExtractInfo(allPages)
	return allContainers, err
}

// CreateContainer ...
func (c *Blockstorage) CreateContainer(client *gophercloud.ServiceClient, containerName string) (*containers.CreateHeader, error) {
	createOpts := containers.CreateOpts{
		ContentType: "application/json",
		Metadata: map[string]string{
			"foo": "bar",
		},
		ContainerRead:  ".r:*",
		ContainerWrite: "*:*",
	}

	container, err := containers.Create(client, containerName, createOpts).Extract()
	return container, err
}

// DeleteContainer ...
func (c *Blockstorage) DeleteContainer(client *gophercloud.ServiceClient, containerName string) (*containers.DeleteHeader, error) {
	container, err := containers.Delete(client, containerName).Extract()
	return container, err
}
