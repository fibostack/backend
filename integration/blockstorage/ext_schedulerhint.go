package blockstorage

import (
	"github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/schedulerhints"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/volumes"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/schedulerhints

// PlaceVolumeDifferentHostThanVolumeA ...
func (c *Blockstorage) PlaceVolumeDifferentHostThanVolumeA(volumeIDa, volumeNameb string, size int) (*volumes.Volume, error) {
	provider := models.GetProvider(c.Username)
	schedulerHints := schedulerhints.SchedulerHints{
		DifferentHost: []string{
			volumeIDa,
		},
	}

	volumeCreateOpts := volumes.CreateOpts{
		Name: volumeNameb,
		Size: size,
	}

	createOpts := schedulerhints.CreateOptsExt{
		VolumeCreateOptsBuilder: volumeCreateOpts,
		SchedulerHints:          schedulerHints,
	}

	volume, err := volumes.Create(models.GetClientBlockstorage(provider), createOpts).Extract()
	return volume, err
}

// PlaceVolumeSameHostAsVolumeA ...
func (c *Blockstorage) PlaceVolumeSameHostAsVolumeA(volumeIDa, volumeNameb string, size int) (*volumes.Volume, error) {
	provider := models.GetProvider(c.Username)
	schedulerHints := schedulerhints.SchedulerHints{
		SameHost: []string{
			volumeIDa,
		},
	}

	volumeCreateOpts := volumes.CreateOpts{
		Name: volumeNameb,
		Size: size,
	}

	createOpts := schedulerhints.CreateOptsExt{
		VolumeCreateOptsBuilder: volumeCreateOpts,
		SchedulerHints:          schedulerHints,
	}

	volume, err := volumes.Create(models.GetClientBlockstorage(provider), createOpts).Extract()
	return volume, err
}
