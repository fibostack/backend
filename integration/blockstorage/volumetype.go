package blockstorage

import (
	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/volumetypes"
	"gitlab.com/fibo-stack/backend/models"
)

// ListVolumeTypes ...
func (c *Blockstorage) ListVolumeTypes() ([]volumetypes.VolumeType, error) {
	provider := models.GetProvider(c.Username)
	listOpts := volumetypes.ListOpts{
		// AllTenants: true,
	}

	allPages, err := volumetypes.List(models.GetClientBlockstorage(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allVolumetypes, err := volumetypes.ExtractVolumeTypes(allPages)
	if err != nil {
		return nil, err
	}

	return allVolumetypes, err
}

// CreateVolumeType ...
func (c *Blockstorage) CreateVolumeType(name, description string, ispublic bool) (*volumetypes.VolumeType, error) {
	provider := models.GetProvider(c.Username)
	opts := volumetypes.CreateOpts{
		Name:        name,
		Description: description,
		IsPublic:    &ispublic,
		ExtraSpecs:  map[string]string{},
	}
	volumetype, err := volumetypes.Create(models.GetClientBlockstorage(provider), opts).Extract()
	return volumetype, err
}

// DeleteVolumeType ...
func (c *Blockstorage) DeleteVolumeType(volumetypeID string) error {
	provider := models.GetProvider(c.Username)
	err := volumetypes.Delete(models.GetClientBlockstorage(provider), volumetypeID).ExtractErr()
	return err
}

// GetVolumeType ...
func (c *Blockstorage) GetVolumeType(volumetypeID string) (*volumetypes.VolumeType, error) {
	provider := models.GetProvider(c.Username)
	volumetype, err := volumetypes.Get(models.GetClientBlockstorage(provider), volumetypeID).Extract()
	return volumetype, err
}

// UpdateVolumeType ...
func (c *Blockstorage) UpdateVolumeType(volumetypeID, newName, description string, ispublic bool) (*volumetypes.VolumeType, error) {
	provider := models.GetProvider(c.Username)
	opts := volumetypes.UpdateOpts{
		Name:        &newName,
		Description: &description,
		IsPublic:    &ispublic,
	}
	volumetype, err := volumetypes.Update(models.GetClientBlockstorage(provider), volumetypeID, opts).Extract()
	return volumetype, err
}
