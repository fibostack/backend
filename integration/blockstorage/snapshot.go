package blockstorage

import (
	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/snapshots"
	"gitlab.com/fibo-stack/backend/models"
)

// ListSnapshots ...
func (c *Blockstorage) ListSnapshots(tenantID string) ([]snapshots.Snapshot, error) {
	provider := models.GetProvider(c.Username)
	listOpts := snapshots.ListOpts{
		TenantID: tenantID,
	}

	allPages, err := snapshots.List(models.GetClientBlockstorage(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allSnapshots, err := snapshots.ExtractSnapshots(allPages)
	if err != nil {
		return nil, err
	}

	return allSnapshots, err
}

// WaitSnapshotStat ...
func (c *Blockstorage) WaitSnapshotStat(snapshotID string) error {
	provider := models.GetProvider(c.Username)
	err := snapshots.WaitForStatus(models.GetClientBlockstorage(provider), snapshotID, "Available", 30)
	return err
}

// CreateSnapshot ...
func (c *Blockstorage) CreateSnapshot(volumeID, name, description string) (*snapshots.Snapshot, error) {
	provider := models.GetProvider(c.Username)
	opts := snapshots.CreateOpts{
		VolumeID:    volumeID,
		Name:        name,
		Description: description,
		Force:       true,
	}

	snapshot, err := snapshots.Create(models.GetClientBlockstorage(provider), opts).Extract()
	return snapshot, err
}

// DeleteSnapshot ...
func (c *Blockstorage) DeleteSnapshot(snapshotID string) error {
	provider := models.GetProvider(c.Username)
	err := snapshots.Delete(models.GetClientBlockstorage(provider), snapshotID).ExtractErr()
	return err
}

// GetSnapshot ...
func (c *Blockstorage) GetSnapshot(snapshotID string) (*snapshots.Snapshot, error) {
	provider := models.GetProvider(c.Username)
	snapshot, err := snapshots.Get(models.GetClientBlockstorage(provider), snapshotID).Extract()
	return snapshot, err
}

// UpdateMetadataSnapshot ...
func (c *Blockstorage) UpdateMetadataSnapshot(snapshotID string, metadata map[string]interface{}) (*snapshots.Snapshot, error) {
	provider := models.GetProvider(c.Username)
	opts := snapshots.UpdateMetadataOpts{
		Metadata: metadata,
	}
	snapshot, err := snapshots.UpdateMetadata(models.GetClientBlockstorage(provider), snapshotID, opts).Extract()
	return snapshot, err
}
