package blockstorage

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/volumes"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/blockstorage/v3/volumes

// Blockstorage ...
type Blockstorage struct {
	Username string
}

// GetBlockstorage ...
func GetBlockstorage(username string) *Blockstorage {
	return &Blockstorage{
		Username: username,
	}
}

// WaitVolumeStat ...
func (c *Blockstorage) WaitVolumeStat(volumeID, userName string) error {
	provider := models.GetProvider(c.Username)
	err := volumes.WaitForStatus(models.GetClientBlockstorage(provider), volumeID, "Available", 30)
	return err
}

// ListVolumes ...
func (c *Blockstorage) ListVolumes(tenantID string) ([]volumes.Volume, error) {
	provider := models.GetProvider(c.Username)
	listOpts := volumes.ListOpts{
		// TenantID: tenantID,
	}

	allPages, err := volumes.List(models.GetClientBlockstorage(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allVolumes, err := volumes.ExtractVolumes(allPages)
	if err != nil {
		return nil, err
	}

	return allVolumes, err
}

// AllListVolumes ...
func (c *Blockstorage) AllListVolumes(tenantID string, adminProvider *gophercloud.ProviderClient) (int, error) {
	totalSize := 0
	listOpts := volumes.ListOpts{
		TenantID:   tenantID,
		AllTenants: true,
	}

	allPages, err := volumes.List(models.GetClientBlockstorage(adminProvider), listOpts).AllPages()
	if err != nil {
		return totalSize, err
	}

	allVolumes, err := volumes.ExtractVolumes(allPages)
	if err != nil {
		return totalSize, err
	}

	for _, v := range allVolumes {
		totalSize += v.Size
	}

	return totalSize, err
}

// CreateVolume ...
func (c *Blockstorage) CreateVolume(size int, name, description, sourceVolID string) (*volumes.Volume, error) {
	provider := models.GetProvider(c.Username)
	opts := volumes.CreateOpts{
		Size: size,
		// VolumeType:  volumeType,
		SourceVolID: sourceVolID,
		Name:        name,
		Description: description,
	}
	volume, err := volumes.Create(models.GetClientBlockstorage(provider), opts).Extract()
	return volume, err
}

// CreateVolumeFromSnapshot ...
func (c *Blockstorage) CreateVolumeFromSnapshot(size int, snapshotID, name, description string) (*volumes.Volume, error) {
	provider := models.GetProvider(c.Username)
	opts := volumes.CreateOpts{
		Size:        size,
		Name:        name,
		Description: description,
		SnapshotID:  snapshotID,
	}
	volume, err := volumes.Create(models.GetClientBlockstorage(provider), opts).Extract()
	return volume, err
}

// DeleteVolume ...
func (c *Blockstorage) DeleteVolume(volumeID string) error {
	provider := models.GetProvider(c.Username)
	opts := volumes.DeleteOpts{}
	err := volumes.Delete(models.GetClientBlockstorage(provider), volumeID, opts).ExtractErr()
	return err
}

// GetVolume ...
func (c *Blockstorage) GetVolume(volumeID string) (*volumes.Volume, error) {
	provider := models.GetProvider(c.Username)
	volume, err := volumes.Get(models.GetClientBlockstorage(provider), volumeID).Extract()
	return volume, err
}

// UpdateVolume ...
func (c *Blockstorage) UpdateVolume(volumeID string) (*volumes.Volume, error) {
	provider := models.GetProvider(c.Username)
	opts := volumes.UpdateOpts{}
	volume, err := volumes.Update(models.GetClientBlockstorage(provider), volumeID, opts).Extract()
	return volume, err
}
