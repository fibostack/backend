package blockstorage

import (
	"github.com/astaxie/beego"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/noauth"
	"gitlab.com/fibo-stack/backend/models"
)

var provider *gophercloud.ProviderClient
var client *gophercloud.ServiceClient

// CreateNoauthProvider ...
func CreateNoauthProvider() error {
	var err error
	provider, err = noauth.NewClient(gophercloud.AuthOptions{
		Username:   beego.AppConfig.String("username"),
		TenantName: beego.AppConfig.String("tenantName"),
	})
	if err != nil {
		panic(err)
	}
	client, err = noauth.NewBlockStorageNoAuth(provider, noauth.EndpointOpts{
		CinderEndpoint: models.CinderURL,
	})
	if err != nil {
		return err
	}
	return nil
}

// GetProviderNoauth ...
func GetProviderNoauth() *gophercloud.ProviderClient {
	return provider
}

// GetClientNoauth ...
func GetClientNoauth() *gophercloud.ServiceClient {
	return client
}
