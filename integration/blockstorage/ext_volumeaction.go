package blockstorage

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/volumeactions"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/blockstorage/extensions/volumeactions

// AttachVolumeToServer ...
func (c *Blockstorage) AttachVolumeToServer(serverID, volumeID string) error {
	provider := models.GetProvider(c.Username)
	attachOpts := volumeactions.AttachOpts{
		MountPoint:   "/mnt",
		Mode:         "rw",
		InstanceUUID: serverID,
	}

	err := volumeactions.Attach(models.GetClientBlockstorage(provider), volumeID, attachOpts).ExtractErr()
	return err
}

// DetachVolumeFromServer ...
func (c *Blockstorage) DetachVolumeFromServer(volumeID, attachmentID string) error {
	provider := models.GetProvider(c.Username)
	detachOpts := volumeactions.DetachOpts{
		AttachmentID: attachmentID,
	}

	err := volumeactions.Detach(models.GetClientBlockstorage(provider), volumeID, detachOpts).ExtractErr()
	return err
}

// CreateImageFromVolume ...
func (c *Blockstorage) CreateImageFromVolume(name, volumeID string) (volumeactions.VolumeImage, error) {
	provider := models.GetProvider(c.Username)
	uploadImageOpts := volumeactions.UploadImageOpts{
		ImageName: name,
		Force:     true,
	}

	volumeImage, err := volumeactions.UploadImage(models.GetClientBlockstorage(provider), volumeID, uploadImageOpts).Extract()
	return volumeImage, err
}

// ExtendVolumeSize ...
func (c *Blockstorage) ExtendVolumeSize(newSize int, volumeID string) error {
	provider := models.GetProvider(c.Username)
	extendOpts := volumeactions.ExtendSizeOpts{
		NewSize: newSize,
	}
	client := models.GetClientBlockstorage(provider)
	client.Microversion = "3.55"
	err := volumeactions.ExtendSize(client, volumeID, extendOpts).ExtractErr()
	return err
}

// InitializeVolumeConnection ...
func (c *Blockstorage) InitializeVolumeConnection(volumeID, ip, hostName, initiator, platform, osType string, multiPath gophercloud.EnabledState) (map[string]interface{}, error) {
	provider := models.GetProvider(c.Username)
	connectOpts := &volumeactions.InitializeConnectionOpts{
		IP:        ip,
		Host:      hostName,
		Initiator: initiator,
		Multipath: multiPath,
		Platform:  platform,
		OSType:    osType,
	}

	connectionInfo, err := volumeactions.InitializeConnection(models.GetClientBlockstorage(provider), volumeID, connectOpts).Extract()
	return connectionInfo, err

}

// TerminateVolumeConnection ...
func (c *Blockstorage) TerminateVolumeConnection(volumeID, ip, hostName, initiator, platform, osType string, multiPath gophercloud.EnabledState) error {
	provider := models.GetProvider(c.Username)
	terminateOpts := &volumeactions.TerminateConnectionOpts{
		IP:        ip,
		Host:      hostName,
		Initiator: initiator,
		Multipath: multiPath,
		Platform:  platform,
		OSType:    osType,
	}

	err := volumeactions.TerminateConnection(models.GetClientBlockstorage(provider), volumeID, terminateOpts).ExtractErr()
	return err
}

// ForceDeleteVolume ...
func (c *Blockstorage) ForceDeleteVolume(volumeID string) error {
	provider := models.GetProvider(c.Username)
	err := volumeactions.ForceDelete(models.GetClientBlockstorage(provider), volumeID).ExtractErr()
	return err
}

// ReserveVolume ...
func (c *Blockstorage) ReserveVolume(volumeID string) error {
	provider := models.GetProvider(c.Username)
	err := volumeactions.Reserve(models.GetClientBlockstorage(provider), volumeID).ExtractErr()
	return err
}

// UnreserveVolume ...
func (c *Blockstorage) UnreserveVolume(volumeID string) error {
	provider := models.GetProvider(c.Username)
	err := volumeactions.Unreserve(models.GetClientBlockstorage(provider), volumeID).ExtractErr()
	return err
}
