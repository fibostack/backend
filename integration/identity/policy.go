package identity

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/identity/v3/policies"
	"gitlab.com/fibo-stack/backend/models"
)

// ListPolicies ...
func (c *Keystone) ListPolicies() ([]policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	listOpts := policies.ListOpts{
		Type: "application/json",
	}

	allPages, err := policies.List(models.GetClientIdentity(provider,  c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allPolicies, err := policies.ExtractPolicies(allPages)
	if err != nil {
		return nil, err
	}

	for _, policy := range allPolicies {
		fmt.Printf("%+v\n", policy)
	}
	return allPolicies, err
}

// CreatePolicy ...
func (c *Keystone) CreatePolicy(types, blob, description string) (*policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	createOpts := policies.CreateOpts{
		Type: types,
		Blob: []byte("{'foobar_user': 'blob'}"),
		Extra: map[string]interface{}{
			"description": description,
		},
	}

	policy, err := policies.Create(models.GetClientIdentity(provider,  c.Username), createOpts).Extract()
	return policy, err
}

// GetPolicy ...
func (c *Keystone) GetPolicy(policyID string) (*policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	policy, err := policies.Get(models.GetClientIdentity(provider,  c.Username), policyID).Extract()
	return policy, err
}

// UpdatePolicy ...
func (c *Keystone) UpdatePolicy(policyID, typ, blob, description string) (*policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := policies.UpdateOpts{
		Type: typ,
		Blob: []byte("{'foobar_user': 'role:compute-user'}"),
		Extra: map[string]interface{}{
			"description": "policy for foobar_user",
		},
	}

	policy, err := policies.Update(models.GetClientIdentity(provider,  c.Username), policyID, updateOpts).Extract()
	return policy, err
}

// DeletePolicy ...
func (c *Keystone) DeletePolicy(policyID string) error {
	provider := models.GetProvider(c.Username)
	err := policies.Delete(models.GetClientIdentity(provider,  c.Username), policyID).ExtractErr()
	return err
}
