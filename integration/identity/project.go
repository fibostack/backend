package identity

import (
	"fmt"

	"gitlab.com/fibo-stack/backend/utils"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/projects"
	"gitlab.com/fibo-stack/backend/models"
)

// ListProjects ...
func (c *Keystone) ListProjects(provider *gophercloud.ProviderClient) ([]projects.Project, error) {
	listOpts := projects.ListOpts{
		// Enabled: gophercloud.Enabled,
	}

	allPages, err := projects.List(models.GetClientIdentity(provider, c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allProjects, err := projects.ExtractProjects(allPages)
	if err != nil {
		return nil, err
	}

	for _, project := range allProjects {
		fmt.Printf("%+v\n", project)
	}
	return allProjects, err
}

// CreateProject ...
func (c *Keystone) CreateProject(projectName, description string, provider *gophercloud.ProviderClient) (*projects.Project, error) {
	createOpts := projects.CreateOpts{
		Name:        projectName,
		Description: description,
	}

	project, err := projects.Create(models.GetClientIdentity(provider, c.Username), createOpts).Extract()
	return project, err
}

// UpdateProject ...
func (c *Keystone) UpdateProject(projectID, name, description string, projectStatus bool, provider *gophercloud.ProviderClient) (*projects.Project, error) {
	updateOpts := projects.UpdateOpts{
		Name:        name,
		Description: &description,
		Enabled:     &projectStatus,
	}

	project, err := projects.Update(models.GetClientIdentity(provider, c.Username), projectID, updateOpts).Extract()
	if err != nil {
		panic(err)
	}
	return project, err
}

// DeleteProject ...
func (c *Keystone) DeleteProject(projectID string, provider *gophercloud.ProviderClient) error {
	err := projects.Delete(models.GetClientIdentity(provider, c.Username), projectID).ExtractErr()
	return err
}

// GetProject ...
func (c *Keystone) GetProject(projectID string) (*projects.Project, error) {
	provider := models.GetProvider(c.Username)
	project, err := projects.Get(models.GetClientIdentity(provider, c.Username), projectID).Extract()
	return project, err
}

// GetUserProjectList ...
func (c *Keystone) GetUserProjectList(userID string) (projectLists []models.ProjectInfo, err error) {

	provider := models.GetProvider(c.Username)
	response, err := models.GetClientIdentity(provider, c.Username).Get(fmt.Sprintf("%s/%s/users/%s/projects", models.KeystoneURL, models.KeystoneVersion, userID), nil, nil)
	msg, err := utils.ResponseUnmarshal(response, err)

	for _, v := range msg["projects"].([]interface{}) {
		var projectInfo models.ProjectInfo
		vDetail := v.(map[string]interface{})
		projectInfo.ID = utils.TypeCheckerString(vDetail["id"]).(string)
		projectInfo.Name = utils.TypeCheckerString(vDetail["name"]).(string)
		projectInfo.IsDomain = vDetail["is_domain"].(bool)
		projectInfo.Description = utils.TypeCheckerString(vDetail["description"]).(string)
		projectInfo.Enabled = vDetail["enabled"].(bool)
		projectInfo.ParentID = utils.TypeCheckerString(vDetail["parent_id"]).(string)
		projectInfo.Links = vDetail["links"].(map[string]interface{})

		projectLists = append(projectLists, projectInfo)
	}
	return projectLists, nil
}
