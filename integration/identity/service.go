package identity

import (
	"github.com/gophercloud/gophercloud/openstack/identity/v3/services"
	"gitlab.com/fibo-stack/backend/models"
)

// RetService ...
type RetService struct {
	ID      string                 `json:"id"`
	Type    string                 `json:"type"`
	Enabled bool                   `json:"enabled"`
	Links   map[string]interface{} `json:"links"`
	Extra   map[string]interface{} `json:"extra"`
}

// ListServices ...
func (c *Keystone) ListServices() ([]RetService, error) {
	var retService []RetService

	provider := models.GetProvider(c.Username)
	listOpts := services.ListOpts{
		// ServiceType: serviceType,
	}

	allPages, err := services.List(models.GetClientIdentity(provider, c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allServices, err := services.ExtractServices(allPages)
	if err != nil {
		return nil, err
	}

	for _, service := range allServices {
		var each RetService
		each.ID = service.ID
		each.Type = service.Type
		each.Enabled = service.Enabled
		each.Links = service.Links
		each.Extra = service.Extra

		retService = append(retService, each)
	}

	return retService, err
}

// CreateService ...
func (c *Keystone) CreateService(typ, name, description string) (*services.Service, error) {
	provider := models.GetProvider(c.Username)
	createOpts := services.CreateOpts{
		Type: typ,
		Extra: map[string]interface{}{
			"name":        name,
			"description": description,
		},
	}

	service, err := services.Create(models.GetClientIdentity(provider, c.Username), createOpts).Extract()
	return service, err
}

// UpdateService ...
func (c *Keystone) UpdateService(serviceID string, enabled bool) (*services.Service, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := services.UpdateOpts{
		Enabled: &enabled,
		Extra: map[string]interface{}{
			"description": "Disabled Compute Service",
		},
	}
	service, err := services.Update(models.GetClientIdentity(provider, c.Username), serviceID, updateOpts).Extract()
	return service, err
}

// DeleteService ...
func (c *Keystone) DeleteService(serviceID string) error {
	provider := models.GetProvider(c.Username)
	err := services.Delete(models.GetClientIdentity(provider, c.Username), serviceID).ExtractErr()
	return err
}

// GetService ...
func (c *Keystone) GetService(serviceID string) (*services.Service, error) {
	provider := models.GetProvider(c.Username)
	service, err := services.Get(models.GetClientIdentity(provider, c.Username), serviceID).Extract()
	return service, err

}
