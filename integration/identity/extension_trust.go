package identity

import (
	"github.com/gophercloud/gophercloud/openstack/identity/v3/extensions/trusts"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/tokens"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/identity/v3/extensions/trusts

// CreateTrust ...
func (c *Keystone) CreateTrust(username, password, trustID string) error {
	provider := models.GetProvider(c.Username)
	var trustToken struct {
		tokens.Token
		trusts.TokenExt
	}

	authOptions := tokens.AuthOptions{
		UserID:   username,
		Password: password,
	}

	createOpts := trusts.AuthOptsExt{
		AuthOptionsBuilder: &authOptions,
		TrustID:            trustID,
	}

	err := tokens.Create(models.GetClientIdentity(provider,  c.Username), createOpts).ExtractInto(&trustToken)
	return err
}
