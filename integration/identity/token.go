package identity

import (
	"github.com/gophercloud/gophercloud/openstack/identity/v3/tokens"
	"gitlab.com/fibo-stack/backend/models"
)

// CreateToken ...
func (c *Keystone) CreateToken(username, password string) (*tokens.Token, error) {
	provider := models.GetProvider(c.Username)
	authOptions := tokens.AuthOptions{
		Username: username,
		Password: password,
	}

	token, err := tokens.Create(models.GetClientIdentity(provider,  c.Username), tokens.AuthOptionsBuilder(&authOptions)).ExtractToken()
	return token, err
}

// CreateTokenFromDomain ...
func (c *Keystone) CreateTokenFromDomain(username, password, domainName string) (*tokens.Token, error) {
	provider := models.GetProvider(c.Username)
	authOptions := tokens.AuthOptions{
		Username:   username,
		Password:   password,
		DomainName: domainName,
	}

	token, err := tokens.Create(models.GetClientIdentity(provider,  c.Username), tokens.AuthOptionsBuilder(&authOptions)).ExtractToken()
	return token, err
}

// CreateTokenFromToken ...
func (c *Keystone) CreateTokenFromToken(tokenID string) (*tokens.Token, error) {
	provider := models.GetProvider(c.Username)
	authOptions := tokens.AuthOptions{
		TokenID: tokenID,
	}
	token, err := tokens.Create(models.GetClientIdentity(provider,  c.Username), tokens.AuthOptionsBuilder(&authOptions)).ExtractToken()
	return token, err
}

// CreateTokenFromProjectIDScope ...
func (c *Keystone) CreateTokenFromProjectIDScope(projectID, username, password string) (*tokens.Token, error) {
	provider := models.GetProvider(c.Username)
	scope := tokens.Scope{
		ProjectID: projectID,
	}

	authOptions := tokens.AuthOptions{
		Scope:    scope,
		UserID:   username,
		Password: password,
	}

	token, err := tokens.Create(models.GetClientIdentity(provider,  c.Username), tokens.AuthOptionsBuilder(&authOptions)).ExtractToken()
	return token, err
}

// CreateTokenFromDomainIDScope ...
func (c *Keystone) CreateTokenFromDomainIDScope(domainID, username, password string) (*tokens.Token, error) {
	provider := models.GetProvider(c.Username)
	scope := tokens.Scope{
		DomainID: domainID,
	}

	authOptions := tokens.AuthOptions{
		Scope:    scope,
		UserID:   username,
		Password: password,
	}

	token, err := tokens.Create(models.GetClientIdentity(provider,  c.Username), tokens.AuthOptionsBuilder(&authOptions)).ExtractToken()
	return token, err
}

// CreateTokenFromProectNamespace ...
func (c *Keystone) CreateTokenFromProectNamespace(projectName, domainID, username, password string) (*tokens.Token, error) {
	provider := models.GetProvider(c.Username)
	scope := tokens.Scope{
		ProjectName: projectName,
		DomainID:    domainID,
	}

	authOptions := tokens.AuthOptions{
		Scope:    scope,
		UserID:   username,
		Password: password,
	}

	token, err := tokens.Create(models.GetClientIdentity(provider,  c.Username), tokens.AuthOptionsBuilder(&authOptions)).ExtractToken()
	return token, err
}
