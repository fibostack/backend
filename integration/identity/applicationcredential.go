package identity

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/identity/v3/applicationcredentials"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/identity/v3/applicationcredentials

// ListAppCredentials ...
func (c *Keystone) ListAppCredentials(userID, name string) ([]applicationcredentials.ApplicationCredential, error) {
	provider := models.GetProvider(c.Username)
	// var iTrue bool = true
	listOpts := applicationcredentials.ListOpts{
		Name: name,
		// Enabled: &iTrue,
	}

	allPages, err := applicationcredentials.List(models.GetClientIdentity(provider,  c.Username), userID, listOpts).AllPages()
	if err != nil {
		return nil, err
	}
	allAppCredentials, err := applicationcredentials.ExtractApplicationCredentials(allPages)
	if err != nil {
		return nil, err
	}
	for _, credential := range allAppCredentials {
		fmt.Printf("%+v\n", credential)
	}
	return allAppCredentials, err
}

// CreateAppCredential ...
func (c *Keystone) CreateAppCredential(userID, name, description, secret string, unrestricted bool) (*applicationcredentials.ApplicationCredential, error) {
	provider := models.GetProvider(c.Username)
	opts := applicationcredentials.CreateOpts{
		Name:         name,
		Description:  description,
		Unrestricted: unrestricted,
		Secret:       secret,
	}

	appCredential, err := applicationcredentials.Create(models.GetClientIdentity(provider,  c.Username), userID, opts).Extract()
	return appCredential, err
}

// DeleteAppCredential ...
func (c *Keystone) DeleteAppCredential(userID, appCredentialID string) error {
	provider := models.GetProvider(c.Username)
	err := applicationcredentials.Delete(models.GetClientIdentity(provider,  c.Username), userID, appCredentialID).ExtractErr()
	return err
}

// GetAppCredential ...
func (c *Keystone) GetAppCredential(userID, appCredentialID string) (*applicationcredentials.ApplicationCredential, error) {
	provider := models.GetProvider(c.Username)
	appCredential, err := applicationcredentials.Get(models.GetClientIdentity(provider,  c.Username), userID, appCredentialID).Extract()
	return appCredential, err
}
