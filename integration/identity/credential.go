package identity

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/identity/v3/credentials"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/identity/v3/credentials

// ListCredentials ...
func (c *Keystone) ListCredentials(userID string) ([]credentials.Credential, error) {
	provider := models.GetProvider(c.Username)
	// var iTrue bool = true
	listOpts := credentials.ListOpts{
		UserID: userID,
		// Enabled: &iTrue,
	}

	allPages, err := credentials.List(models.GetClientIdentity(provider,  c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}
	allCredentials, err := credentials.ExtractCredentials(allPages)
	if err != nil {
		return nil, err
	}
	for _, credential := range allCredentials {
		fmt.Printf("%+v\n", credential)
	}
	return allCredentials, err
}

// CreateCredential ...
func (c *Keystone) CreateCredential(projectID, userID, typ, blob string) (*credentials.Credential, error) {
	provider := models.GetProvider(c.Username)
	opts := credentials.CreateOpts{
		ProjectID: projectID,
		UserID:    userID,
		Blob:      blob,
		Type:      typ,
	}
	credential, err := credentials.Create(models.GetClientIdentity(provider,  c.Username), opts).Extract()
	return credential, err
}

// DeleteCredential ...
func (c *Keystone) DeleteCredential(credentialID string) error {
	provider := models.GetProvider(c.Username)
	err := credentials.Delete(models.GetClientIdentity(provider,  c.Username), credentialID).ExtractErr()
	return err
}

// GetCredential ...
func (c *Keystone) GetCredential(credentialID string) (*credentials.Credential, error) {
	provider := models.GetProvider(c.Username)
	credential, err := credentials.Get(models.GetClientIdentity(provider,  c.Username), credentialID).Extract()
	return credential, err
}

// UpdateCredential ...
func (c *Keystone) UpdateCredential(projectID, userID, blob, typ, credentialID string) (*credentials.Credential, error) {
	provider := models.GetProvider(c.Username)
	opts := credentials.UpdateOpts{
		ProjectID: projectID,
		UserID:    userID,
		Blob:      blob,
		Type:      typ,
	}
	credential, err := credentials.Update(models.GetClientIdentity(provider,  c.Username), credentialID, opts).Extract()
	return credential, err
}
