package identity

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/projects"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/users"
	"gitlab.com/fibo-stack/backend/models"
)

// Keystone ...
type Keystone struct {
	Username string
}

// GetKeystone ...
func GetKeystone(username string) *Keystone {
	return &Keystone{
		Username: username,
	}
}

// ListUsers ...
func (c *Keystone) ListUsers() ([]users.User, error) {
	provider := models.GetProvider(c.Username)
	listOpts := users.ListOpts{
		DomainID: "default",
	}

	allPages, err := users.List(models.GetClientIdentity(provider, c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allUsers, err := users.ExtractUsers(allPages)
	return allUsers, err
}

// ListUsersProjects ...
func (c *Keystone) ListUsersProjects(osUserID string) ([]projects.Project, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := users.ListProjects(models.GetClientIdentity(provider, c.Username), osUserID).AllPages()
	if err != nil {
		return nil, err
	}
	allProjects, err := projects.ExtractProjects(allPages)
	return allProjects, err
}

// CreateUser ...
func (c *Keystone) CreateUser(projectID, username, password string) (*users.User, error) {
	provider := models.GetProvider(c.Username)
	createOpts := users.CreateOpts{
		Name:             username,
		DomainID:         "default",
		DefaultProjectID: projectID,
		Enabled:          gophercloud.Enabled,
		Password:         password,
		Extra: map[string]interface{}{
			"email": username,
		},
	}

	user, err := users.Create(models.GetClientIdentity(provider, c.Username), createOpts).Extract()
	return user, err
}

// EnableDisableUser ...
func (c *Keystone) EnableDisableUser(userID string, enable bool) (*users.User, error) {
	provider := models.GetProvider(c.Username)

	updateOpts := users.UpdateOpts{
		Enabled: &enable,
	}

	user, err := users.Update(models.GetClientIdentity(provider, c.Username), userID, updateOpts).Extract()

	return user, err
}

// UpdateUser ...
func (c *Keystone) UpdateUser(provider *gophercloud.ProviderClient, userID, projectID string, enabled bool) (*users.User, error) {
	updateOpts := users.UpdateOpts{
		Enabled:          &enabled,
		DefaultProjectID: projectID,
	}
	user, err := users.Update(models.GetClientIdentity(provider, c.Username), userID, updateOpts).Extract()
	return user, err
}

// ChangePasswordUser ...
func (c *Keystone) ChangePasswordUser(userID, originalPassword, password string) error {
	provider := models.GetProvider(c.Username)
	changePasswordOpts := users.ChangePasswordOpts{
		OriginalPassword: originalPassword,
		Password:         password,
	}

	err := users.ChangePassword(models.GetClientIdentity(provider, c.Username), userID, changePasswordOpts).ExtractErr()
	return err
}

// DeleteUser ...
func (c *Keystone) DeleteUser(userID string) error {
	provider := models.GetProvider(c.Username)
	err := users.Delete(models.GetClientIdentity(provider, c.Username), userID).ExtractErr()
	return err
}

// GetUser ...
func (c *Keystone) GetUser(userID string) (*users.User, error) {
	provider := models.GetProvider(c.Username)
	user, err := users.Get(models.GetClientIdentity(provider, c.Username), userID).Extract()
	return user, err
}
