package identity

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/identity/v3/domains"
	"gitlab.com/fibo-stack/backend/models"
)

// ListDomains ...
func (c *Keystone) ListDomains(enabled bool) ([]domains.Domain, error) {
	provider := models.GetProvider(c.Username)
	listOpts := domains.ListOpts{
		Enabled: &enabled,
	}

	allPages, err := domains.List(models.GetClientIdentity(provider,  c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}
	allDomains, err := domains.ExtractDomains(allPages)
	if err != nil {
		return nil, err
	}
	for _, domain := range allDomains {
		fmt.Printf("%+v\n", domain)
	}
	return allDomains, err
}

// CreateDomain ...
func (c *Keystone) CreateDomain(name, description string) (*domains.Domain, error) {
	provider := models.GetProvider(c.Username)
	createOpts := domains.CreateOpts{
		Name:        name,
		Description: description,
	}

	domain, err := domains.Create(models.GetClientIdentity(provider,  c.Username), createOpts).Extract()
	return domain, err
}

// UpdateDomain ...
func (c *Keystone) UpdateDomain(domainID string, enabled bool) (*domains.Domain, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := domains.UpdateOpts{
		Enabled: &enabled,
	}

	domain, err := domains.Update(models.GetClientIdentity(provider,  c.Username), domainID, updateOpts).Extract()
	return domain, err
}

// DeleteDomain ...
func (c *Keystone) DeleteDomain(domainID string) error {
	provider := models.GetProvider(c.Username)
	err := domains.Delete(models.GetClientIdentity(provider,  c.Username), domainID).ExtractErr()
	return err
}
