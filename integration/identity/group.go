package identity

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/identity/v3/groups"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/projects"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/users"
	"gitlab.com/fibo-stack/backend/models"
)

// ListGroupsUser ...
func (c *Keystone) ListGroupsUser(userID string) ([]groups.Group, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := users.ListGroups(models.GetClientIdentity(provider,  c.Username), userID).AllPages()
	if err != nil {
		return nil, err
	}

	allGroups, err := groups.ExtractGroups(allPages)
	if err != nil {
		return nil, err
	}

	for _, group := range allGroups {
		fmt.Printf("%+v\n", group)
	}
	return allGroups, err
}

// AddUserToGroup ...
func (c *Keystone) AddUserToGroup(groupID, userID string) error {
	provider := models.GetProvider(c.Username)
	err := users.AddToGroup(models.GetClientIdentity(provider,  c.Username), groupID, userID).ExtractErr()
	return err
}

// CheckUserInGroup ...
func (c *Keystone) CheckUserInGroup(groupID, userID string) (bool, error) {
	provider := models.GetProvider(c.Username)
	ok, err := users.IsMemberOfGroup(models.GetClientIdentity(provider,  c.Username), groupID, userID).Extract()
	return ok, err
}

// RemoveUserFromGroup ...
func (c *Keystone) RemoveUserFromGroup(groupID, userID string) error {
	provider := models.GetProvider(c.Username)
	err := users.RemoveFromGroup(models.GetClientIdentity(provider,  c.Username), groupID, userID).ExtractErr()
	return err
}

// ListProjectsUser ...
func (c *Keystone) ListProjectsUser(userID string) ([]projects.Project, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := users.ListProjects(models.GetClientIdentity(provider,  c.Username), userID).AllPages()
	if err != nil {
		return nil, err
	}

	allProjects, err := projects.ExtractProjects(allPages)
	if err != nil {
		return nil, err
	}

	for _, project := range allProjects {
		fmt.Printf("%+v\n", project)
	}
	return allProjects, err
}

// ListUsersGroup ...
func (c *Keystone) ListUsersGroup(groupID string) ([]users.User, error) {
	provider := models.GetProvider(c.Username)
	listOpts := users.ListOpts{
		DomainID: "default",
	}
	allPages, err := users.ListInGroup(models.GetClientIdentity(provider,  c.Username), groupID, listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allUsers, err := users.ExtractUsers(allPages)
	if err != nil {
		return nil, err
	}

	for _, user := range allUsers {
		fmt.Printf("%+v\n", user)
	}
	return allUsers, err
}
