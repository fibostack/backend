package identity

import (
	"fmt"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/roles"
	"gitlab.com/fibo-stack/backend/models"
)

// RetUser ...
type RetUser struct {
	DomainID string                 `json:"domain_id"`
	ID       string                 `json:"id"`
	Links    map[string]interface{} `json:"links"`
	Name     string                 `json:"name"`
	Extra    map[string]interface{} `json:"extra"`
}

// ListRoles ...
func (c *Keystone) ListRoles() ([]RetUser, error) {
	provider := models.GetProvider(c.Username)
	listOpts := roles.ListOpts{}

	allPages, err := roles.List(models.GetClientIdentity(provider, c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRoles, err := roles.ExtractRoles(allPages)
	var retRoles []RetUser

	for _, i := range allRoles {
		var each RetUser
		each.DomainID = i.DomainID
		each.ID = i.ID
		each.Links = i.Links
		each.Name = i.Name
		each.Extra = i.Extra

		retRoles = append(retRoles, each)
	}

	return retRoles, err
}

// ListAssignmentsOnResource ...
func (c *Keystone) ListAssignmentsOnResource(osUserID, osTenantID string) ([]roles.Role, error) {
	provider := models.GetProvider(c.Username)
	listAssignmentsOnResourceOpts := roles.ListAssignmentsOnResourceOpts{
		UserID:    osUserID,
		ProjectID: osTenantID,
	}

	allPages, err := roles.ListAssignmentsOnResource(models.GetClientIdentity(provider, c.Username), listAssignmentsOnResourceOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRoles, err := roles.ExtractRoles(allPages)
	return allRoles, err
}

// CreateRole ...
func (c *Keystone) CreateRole(roleName string) (*roles.Role, error) {
	provider := models.GetProvider(c.Username)
	createOpts := roles.CreateOpts{
		Name: roleName,
		Extra: map[string]interface{}{
			"description": "manual",
		},
	}

	role, err := roles.Create(models.GetClientIdentity(provider, c.Username), createOpts).Extract()
	return role, err
}

// UpdateRole ...
func (c *Keystone) UpdateRole(roleID, name string) (*roles.Role, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := roles.UpdateOpts{
		Name: name,
	}

	role, err := roles.Update(models.GetClientIdentity(provider, c.Username), roleID, updateOpts).Extract()
	return role, err
}

// DeleteRole ...
func (c *Keystone) DeleteRole(roleID string) error {
	provider := models.GetProvider(c.Username)
	err := roles.Delete(models.GetClientIdentity(provider, c.Username), roleID).ExtractErr()
	return err
}

// ListRoleAssignments ...
func (c *Keystone) ListRoleAssignments(userID, scopeProjectID string) ([]roles.RoleAssignment, error) {
	provider := models.GetProvider(c.Username)
	listOpts := roles.ListAssignmentsOpts{
		UserID:         userID,
		ScopeProjectID: scopeProjectID,
	}

	allPages, err := roles.ListAssignments(models.GetClientIdentity(provider, c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRoles, err := roles.ExtractRoleAssignments(allPages)
	if err != nil {
		return nil, err
	}

	for _, role := range allRoles {
		fmt.Printf("%+v\n", role)
	}
	return allRoles, err
}

// AssignRoleToUserInProject ...
func (c *Keystone) AssignRoleToUserInProject(projectID, userID, roleID string) error {
	provider := models.GetProvider(c.Username)
	opts := roles.AssignOpts{
		UserID:    userID,
		ProjectID: projectID,
	}
	err := roles.Assign(models.GetClientIdentity(provider, c.Username), roleID, opts).ExtractErr()
	return err
}

// AssignRoleToUserInProjectAdmin ...
func (c *Keystone) AssignRoleToUserInProjectAdmin(provider *gophercloud.ProviderClient, projectID, userID, roleID string) error {
	fmt.Println(projectID, userID, roleID)
	opts := roles.AssignOpts{
		UserID:    userID,
		ProjectID: projectID,
	}
	err := roles.Assign(models.GetClientIdentity(provider, c.Username), roleID, opts).ExtractErr()
	return err
}

// UnAssignRoleToUserInProject ...
func (c *Keystone) UnAssignRoleToUserInProject(projectID, userID, roleID string) error {
	provider := models.GetProvider(c.Username)
	opts := roles.UnassignOpts{
		UserID:    userID,
		ProjectID: projectID,
	}
	err := roles.Unassign(models.GetClientIdentity(provider, c.Username), roleID, opts).ExtractErr()
	return err
}
