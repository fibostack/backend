package identity

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/identity/v3/regions"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/identity/v3/regions

// ListRegions ...
func (c *Keystone) ListRegions(parentRegionID string) ([]regions.Region, error) {
	provider := models.GetProvider(c.Username)
	listOpts := regions.ListOpts{
		ParentRegionID: parentRegionID,
	}

	allPages, err := regions.List(models.GetClientIdentity(provider,  c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRegions, err := regions.ExtractRegions(allPages)
	if err != nil {
		return nil, err
	}

	for _, region := range allRegions {
		fmt.Printf("%+v\n", region)
	}
	return allRegions, err
}

// CreateRegion ...
func (c *Keystone) CreateRegion(regionID, description, email string) (*regions.Region, error) {
	provider := models.GetProvider(c.Username)
	createOpts := regions.CreateOpts{
		ID:          regionID,
		Description: description,
		Extra: map[string]interface{}{
			"email": email,
		},
	}

	region, err := regions.Create(models.GetClientIdentity(provider,  c.Username), createOpts).Extract()
	return region, err
}

// UpdateRegion ...
func (c *Keystone) UpdateRegion(regionID, description string) (*regions.Region, error) {
	provider := models.GetProvider(c.Username)
	// There is currently a bug in Keystone where updating the optional Extras
	// attributes set in regions.Create is not supported, see:
	// https://bugs.launchpad.net/keystone/+bug/1729933
	updateOpts := regions.UpdateOpts{
		Description: &description,
	}

	region, err := regions.Update(models.GetClientIdentity(provider,  c.Username), regionID, updateOpts).Extract()
	return region, err
}

// DeleteRegion ...
func (c *Keystone) DeleteRegion(regionID string) error {
	provider := models.GetProvider(c.Username)
	err := regions.Delete(models.GetClientIdentity(provider,  c.Username), regionID).ExtractErr()
	return err
}
