package identity

import (
	"github.com/gophercloud/gophercloud/openstack/identity/v3/extensions/ec2credentials"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/identity/v3/extensions/ec2credentials

// CreateEc2Credential ...
func (c *Keystone) CreateEc2Credential(projectID, userID string) (*ec2credentials.Credential, error) {
	provider := models.GetProvider(c.Username)
	createOpts := ec2credentials.CreateOpts{
		TenantID: projectID,
	}

	credential, err := ec2credentials.Create(models.GetClientIdentity(provider, ""), userID, createOpts).Extract()
	return credential, err
}
