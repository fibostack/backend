package identity

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/identity/v3/endpoints"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/identity/v3

// ListEndpoints ...
func (c *Keystone) ListEndpoints() ([]endpoints.Endpoint, error) {
	provider := models.GetProvider(c.Username)
	listOpts := endpoints.ListOpts{
		// ServiceID: "be2caa5315f143568ccf62815556a958",
	}

	allPages, err := endpoints.List(models.GetClientIdentity(provider, c.Username), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allEndpoints, err := endpoints.ExtractEndpoints(allPages)
	return allEndpoints, err
}

// CreateEndpoint ...
func (c *Keystone) CreateEndpoint(serviceID, serviceName, serviceURL string) (*endpoints.Endpoint, error) {
	provider := models.GetProvider(c.Username)
	createOpts := endpoints.CreateOpts{
		Availability: gophercloud.AvailabilityPublic,
		Name:         serviceName,
		Region:       "RegionOne",
		URL:          serviceURL,
		ServiceID:    serviceID,
	}
	endpoint, err := endpoints.Create(models.GetClientIdentity(provider, c.Username), createOpts).Extract()
	return endpoint, err
}

// UpdateEndpoint ...
func (c *Keystone) UpdateEndpoint(endpointID, serviceName, serviceURL string) (*endpoints.Endpoint, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := endpoints.UpdateOpts{
		Name: serviceName,
		URL:  serviceURL,
		// Region: "RegionTwo",
	}

	endpoint, err := endpoints.Update(models.GetClientIdentity(provider, c.Username), endpointID, updateOpts).Extract()
	return endpoint, err
}

// DeleteEndpoint ...
func (c *Keystone) DeleteEndpoint(endpointID string) error {
	provider := models.GetProvider(c.Username)
	err := endpoints.Delete(models.GetClientIdentity(provider, c.Username), endpointID).ExtractErr()
	return err
}
