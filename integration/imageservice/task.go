package imageservice

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/tasks"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/imageservice/v2/tasks

// ListTasks ...
func (c *ImageService) ListTasks(projectID string) ([]tasks.Task, error) {
	provider := models.GetProvider(c.Username)
	listOpts := tasks.ListOpts{
		ID: projectID,
	}

	allPages, err := tasks.List(models.GetClientImage(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allTasks, err := tasks.ExtractTasks(allPages)
	if err != nil {
		return nil, err
	}

	for _, task := range allTasks {
		fmt.Printf("%+v\n", task)
	}
	return allTasks, err
}

// GetTask ...
func (c *ImageService) GetTask(taskID string) (*tasks.Task, error) {
	provider := models.GetProvider(c.Username)
	task, err := tasks.Get(models.GetClientImage(provider), taskID).Extract()
	return task, err
}

// CreateTask ...
func (c *ImageService) CreateTask(importFrom, importFromFormat, containerFormat, diskFormat string) (*tasks.Task, error) {
	provider := models.GetProvider(c.Username)
	createOpts := tasks.CreateOpts{
		Type: "import",
		Input: map[string]interface{}{
			"image_properties": map[string]interface{}{
				"container_format": containerFormat,
				"disk_format":      diskFormat,
			},
			"import_from_format": importFromFormat,
			"import_from":        importFrom,
		},
	}

	task, err := tasks.Create(models.GetClientImage(provider), createOpts).Extract()
	return task, err
}
