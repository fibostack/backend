package imageservice

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/members"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/imageservice/v2/members

// ListMembers ...
func (c *ImageService) ListMembers(imageID string) ([]members.Member, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := members.List(models.GetClientImage(provider), imageID).AllPages()
	if err != nil {
		return nil, err
	}

	allMembers, err := members.ExtractMembers(allPages)
	if err != nil {
		return nil, err
	}

	for _, member := range allMembers {
		fmt.Printf("%+v\n", member)
	}
	return allMembers, err
}

// AddMemberToImage ...
func (c *ImageService) AddMemberToImage(projectID, imageID string) (*members.Member, error) {
	provider := models.GetProvider(c.Username)
	member, err := members.Create(models.GetClientImage(provider), imageID, projectID).Extract()
	return member, err
}

// UpdateStatusOfMember ...
func (c *ImageService) UpdateStatusOfMember(projectID, imageID, status string) (*members.Member, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := members.UpdateOpts{
		Status: status,
	}

	member, err := members.Update(models.GetClientImage(provider), imageID, projectID, updateOpts).Extract()
	return member, err
}

// DeleteMemberFromImage ...
func (c *ImageService) DeleteMemberFromImage(projectID, imageID string) error {
	provider := models.GetProvider(c.Username)
	err := members.Delete(models.GetClientImage(provider), imageID, projectID).ExtractErr()
	return err
}
