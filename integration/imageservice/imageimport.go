package imageservice

import (
	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/imageimport"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/imageservice/v2/imageimport

// GetInfoAboutImportAPI ...
func (c *ImageService) GetInfoAboutImportAPI() (*imageimport.ImportInfo, error) {
	provider := models.GetProvider(c.Username)
	importInfo, err := imageimport.Get(models.GetClientImage(provider)).Extract()
	return importInfo, err
}

// CreateImageImport ...
func (c *ImageService) CreateImageImport(imageID, url string) error {
	provider := models.GetProvider(c.Username)
	opts := imageimport.CreateOpts{
		Name: imageimport.WebDownloadMethod,
		// URI:  "http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img",
		URI: url,
	}

	err := imageimport.Create(models.GetClientImage(provider), imageID, opts).ExtractErr()
	return err
}
