package imageservice

import (
	"io/ioutil"
	"os"

	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/imagedata"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/imageservice/v2/imagedata

// UploadImageData ...
func (c *ImageService) UploadImageData(imageID, pathFile string) error {
	provider := models.GetProvider(c.Username)
	imageData, err := os.Open(pathFile)
	if err != nil {
		return err
	}
	defer imageData.Close()

	err = imagedata.Upload(models.GetClientImage(provider), imageID, imageData).ExtractErr()
	return err
}

// StageImageData ...
func (c *ImageService) StageImageData(imageID, pathFile string) error {
	provider := models.GetProvider(c.Username)
	imageData, err := os.Open(pathFile)
	if err != nil {
		return err
	}
	defer imageData.Close()

	err = imagedata.Stage(models.GetClientImage(provider), imageID, imageData).ExtractErr()
	return err
}

// DownloadImageData ...
func (c *ImageService) DownloadImageData(imageID string) ([]byte, error) {
	provider := models.GetProvider(c.Username)
	image, err := imagedata.Download(models.GetClientImage(provider), imageID).Extract()
	if err != nil {
		return nil, err
	}

	imageData, err := ioutil.ReadAll(image)
	return imageData, err
}
