package imageservice

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/images"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/imageservice/v2/images

// ImageService ...
type ImageService struct {
	Username string
}

// GetImageService ...
func GetImageService(username string) *ImageService {
	return &ImageService{
		Username: username,
	}
}

// ListImages ...
func (c *ImageService) ListImages(projectID string) ([]images.Image, error) {
	provider := models.GetProvider(c.Username)
	listOpts := images.ListOpts{
		Owner: projectID,
	}

	allPages, err := images.List(models.GetClientImage(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allImages, err := images.ExtractImages(allPages)
	if err != nil {
		return nil, err
	}

	for _, image := range allImages {
		fmt.Printf("%+v\n", image)
	}
	return allImages, err
}

// CreateImage ...
func (c *ImageService) CreateImage(name, diskFormat, containerFormat string, visiblity images.ImageVisibility, minDisk, minRAM int) (*images.Image, error) {
	provider := models.GetProvider(c.Username)

	createOpts := images.CreateOpts{
		Name:            name,
		Visibility:      &visiblity,
		MinDisk:         minDisk,
		MinRAM:          minRAM,
		DiskFormat:      diskFormat,
		ContainerFormat: containerFormat,
	}

	image, err := images.Create(models.GetClientImage(provider), createOpts).Extract()
	return image, err
}

// UpdateImage ...
func (c *ImageService) UpdateImage(imageID, newName string) (*images.Image, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := images.UpdateOpts{
		images.ReplaceImageName{
			NewName: newName,
		},
	}

	image, err := images.Update(models.GetClientImage(provider), imageID, updateOpts).Extract()
	return image, err
}

// DeleteImage ...
func (c *ImageService) DeleteImage(imageID string) error {
	provider := models.GetProvider(c.Username)
	err := images.Delete(models.GetClientImage(provider), imageID).ExtractErr()
	return err
}
