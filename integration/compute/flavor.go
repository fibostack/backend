package compute

import (
	"github.com/gophercloud/gophercloud"
	"gitlab.com/fibo-stack/backend/models"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/flavors"
)

// ListFlavors ...
func (c *Compute) ListFlavors() ([]flavors.Flavor, error) {
	provider := models.GetProvider(c.Username)
	listOpts := flavors.ListOpts{
		AccessType: flavors.AllAccess,
	}

	allPages, err := flavors.ListDetail(models.GetClientCompute(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allFlavors, err := flavors.ExtractFlavors(allPages)
	return allFlavors, err
}

// ListFlavorsAdmin ...
func (c *Compute) ListFlavorsAdmin(adminProvider *gophercloud.ProviderClient) ([]flavors.Flavor, error) {
	listOpts := flavors.ListOpts{
		AccessType: flavors.AllAccess,
	}

	allPages, err := flavors.ListDetail(models.GetClientCompute(adminProvider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allFlavors, err := flavors.ExtractFlavors(allPages)
	return allFlavors, err
}

// CreateFlavor ...
func (c *Compute) CreateFlavor(name string, ram, vcpu int, isPublic bool) (*flavors.Flavor, error) {
	provider := models.GetProvider(c.Username)
	rxtx := 1.0
	disk := 0

	createOpts := flavors.CreateOpts{
		Name:       name,
		RAM:        ram,
		VCPUs:      vcpu,
		RxTxFactor: rxtx,
		IsPublic:   &isPublic,
		Disk:       &disk,
	}

	flavor, err := flavors.Create(models.GetClientCompute(provider), createOpts).Extract()
	return flavor, err
}

// DeleteFlavor ...
func (c *Compute) DeleteFlavor(flavorID string) error {
	provider := models.GetProvider(c.Username)
	err := flavors.Delete(models.GetClientCompute(provider), flavorID).ExtractErr()
	return err
}

// GetFlavor ...
func (c *Compute) GetFlavor(flavorID string) (*flavors.Flavor, error) {
	provider := models.GetProvider(c.Username)
	flavor, err := flavors.Get(models.GetClientCompute(provider), flavorID).Extract()
	return flavor, err
}

// ListFlavorAccess ...
func (c *Compute) ListFlavorAccess(flavorID string) ([]flavors.FlavorAccess, error) {
	provider := models.GetProvider(c.Username)

	allPages, err := flavors.ListAccesses(models.GetClientCompute(provider), flavorID).AllPages()
	if err != nil {
		return nil, err
	}

	allAccesses, err := flavors.ExtractAccesses(allPages)
	return allAccesses, err
}

// GrantAccessToFlavor ...
func (c *Compute) GrantAccessToFlavor(flavorID, tenantID string) ([]flavors.FlavorAccess, error) {
	provider := models.GetProvider(c.Username)
	accessOpts := flavors.AddAccessOpts{
		Tenant: tenantID,
	}

	accessList, err := flavors.AddAccess(models.GetClientCompute(provider), flavorID, accessOpts).Extract()
	return accessList, err
}

// RemoveAccessToFlavor ...
func (c *Compute) RemoveAccessToFlavor(flavorID, tenantID string) ([]flavors.FlavorAccess, error) {
	provider := models.GetProvider(c.Username)
	accessOpts := flavors.RemoveAccessOpts{
		Tenant: tenantID,
	}

	accessList, err := flavors.RemoveAccess(models.GetClientCompute(provider), flavorID, accessOpts).Extract()
	return accessList, err
}

// CreateExtraSpecForFlavor ...
//Check later
func (c *Compute) CreateExtraSpecForFlavor(flavorID string) (map[string]string, error) {
	provider := models.GetProvider(c.Username)
	createOpts := flavors.ExtraSpecsOpts{
		"hw:cpu_policy":        "CPU-POLICY",
		"hw:cpu_thread_policy": "CPU-THREAD-POLICY",
	}
	createdExtraSpecs, err := flavors.CreateExtraSpecs(models.GetClientCompute(provider), flavorID, createOpts).Extract()
	return createdExtraSpecs, err
}

// GetExtraSpecForFlavor ...
//Check later
func (c *Compute) GetExtraSpecForFlavor(flavorID string) (map[string]string, error) {
	provider := models.GetProvider(c.Username)
	extraSpecs, err := flavors.ListExtraSpecs(models.GetClientCompute(provider), flavorID).Extract()
	return extraSpecs, err
}

// UpdateExtraSpecForFlavor ...
//Check later
func (c *Compute) UpdateExtraSpecForFlavor(flavorID string) (map[string]string, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := flavors.ExtraSpecsOpts{
		"hw:cpu_thread_policy": "CPU-THREAD-POLICY-UPDATED",
	}
	updatedExtraSpec, err := flavors.UpdateExtraSpec(models.GetClientCompute(provider), flavorID, updateOpts).Extract()
	return updatedExtraSpec, err
}

// DeleteExtraSpecFlavor ...
//Check later
func (c *Compute) DeleteExtraSpecFlavor(flavorID string) error {
	provider := models.GetProvider(c.Username)
	err := flavors.DeleteExtraSpec(models.GetClientCompute(provider), flavorID, "hw:cpu_thread_policy").ExtractErr()
	return err
}
