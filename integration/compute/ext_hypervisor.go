package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/hypervisors"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/hypervisors

// ShowHypervisor ...
func (c *Compute) ShowHypervisor(adminProvider *gophercloud.ProviderClient, hypervisorID string) (*hypervisors.Hypervisor, error) {
	// provider := models.GetProvider(c.Username)
	hypervisor, err := hypervisors.Get(models.GetClientCompute(adminProvider), hypervisorID).Extract()
	return hypervisor, err
}

// ListHypervisors ...
func (c *Compute) ListHypervisors(adminProvider *gophercloud.ProviderClient) ([]hypervisors.Hypervisor, error) {
	// provider := models.GetProvider(c.Username)
	allPages, err := hypervisors.List(models.GetClientCompute(adminProvider)).AllPages()
	if err != nil {
		return nil, err
	}

	allHypervisors, err := hypervisors.ExtractHypervisors(allPages)
	if err != nil {
		return nil, err
	}

	for _, hypervisor := range allHypervisors {
		fmt.Printf("%+v\n", hypervisor)
	}
	return allHypervisors, err
}

// ShowHypervisorStatistics ...
func (c *Compute) ShowHypervisorStatistics(provider *gophercloud.ProviderClient) (*hypervisors.Statistics, error) {
	hypervisorsStatistics, err := hypervisors.GetStatistics(models.GetClientCompute(provider)).Extract()
	return hypervisorsStatistics, err
}

// ShowHypervisorUptime ...
func (c *Compute) ShowHypervisorUptime(hypervisorID string) (*hypervisors.Uptime, error) {
	provider := models.GetProvider(c.Username)
	hypervisorUptime, err := hypervisors.GetUptime(models.GetClientCompute(provider), hypervisorID).Extract()
	return hypervisorUptime, err
}
