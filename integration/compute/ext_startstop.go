package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/startstop"
	"gitlab.com/fibo-stack/backend/models"
)

// StartServer ...
func (c *Compute) StartServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := startstop.Start(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// StopServer ...
func (c *Compute) StopServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := startstop.Stop(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}
