package compute

import (
	"time"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/services"
	"gitlab.com/fibo-stack/backend/models"
)

// ServiceDetail ...
type ServiceDetail struct {
	Binary         string    `json:"binary"`
	DisabledReason string    `json:"disabled_reason"`
	Host           string    `json:"host"`
	ID             string    `json:"-"`
	State          string    `json:"state"`
	Status         string    `json:"status"`
	UpdatedAt      time.Time `json:"updated_at"`
	Zone           string    `json:"zone"`
}

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/services

// ListService ...
func (c *Compute) ListService() ([]ServiceDetail, error) {
	provider := models.GetProvider(c.Username)
	var retServices []ServiceDetail

	opts := services.ListOpts{}

	allPages, err := services.List(models.GetClientCompute(provider), opts).AllPages()
	if err != nil {
		return nil, err
	}

	allServices, err := services.ExtractServices(allPages)
	for _, i := range allServices {
		var each ServiceDetail
		each.Binary = i.Binary
		each.DisabledReason = i.DisabledReason
		each.Host = i.Host
		each.ID = i.ID
		each.State = i.State
		each.Status = i.Status
		each.UpdatedAt = i.UpdatedAt
		each.Zone = i.Zone

		retServices = append(retServices, each)
	}
	return retServices, err
}
