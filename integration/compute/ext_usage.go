package compute

import (
	"fmt"
	"time"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/usage"
	"github.com/gophercloud/gophercloud/pagination"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/usage

// UsageSingleTenant ...
func (c *Compute) UsageSingleTenant(tenantID string, start, end time.Time) (*usage.TenantUsage, error) {
	provider := models.GetProvider(c.Username)
	// start := time.Date(2017, 01, 21, 10, 4, 20, 0, time.UTC)
	// end := time.Date(2017, 01, 21, 10, 4, 20, 0, time.UTC)

	singleTenantOpts := usage.SingleTenantOpts{
		Start: &start,
		End:   &end,
	}
	var tu *usage.TenantUsage
	err := usage.SingleTenant(models.GetClientCompute(provider), tenantID, singleTenantOpts).EachPage(func(page pagination.Page) (bool, error) {
		tenantUsage, err := usage.ExtractSingleTenant(page)
		if err != nil {
			return false, err
		}
		fmt.Printf("%+v\n", tenantUsage)
		tu = tenantUsage
		return true, nil
	})
	return tu, err
}

// UsageAllTenant ...
func (c *Compute) UsageAllTenant(start, end time.Time) ([]usage.TenantUsage, error) {
	provider := models.GetProvider(c.Username)
	allTenantsOpts := usage.AllTenantsOpts{
		Detailed: true,
		Start:    &start,
		End:      &end,
	}
	var tu []usage.TenantUsage
	err := usage.AllTenants(models.GetClientCompute(provider), allTenantsOpts).EachPage(func(page pagination.Page) (bool, error) {
		allTenantsUsage, err := usage.ExtractAllTenants(page)
		if err != nil {
			return false, err
		}
		tu = allTenantsUsage
		fmt.Printf("%+v\n", allTenantsUsage)

		return true, nil
	})
	return tu, err
}
