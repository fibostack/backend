package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/pauseunpause"
	"gitlab.com/fibo-stack/backend/models"
)

// PauseServer ...
func (c *Compute) PauseServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := pauseunpause.Pause(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// UnpauseServer ...
func (c *Compute) UnpauseServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := pauseunpause.Unpause(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}
