package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/secgroups"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/secgroups

// ListSecGroups []secgroups.SecurityGroup
func (c *Compute) ListSecGroups() ([]secgroups.SecurityGroup, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := secgroups.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allSecurityGroups, err := secgroups.ExtractSecurityGroups(allPages)
	return allSecurityGroups, err
}

// GetSecGroup ...
func (c *Compute) GetSecGroup(sgID string) (*secgroups.SecurityGroup, error) {
	provider := models.GetProvider(c.Username)
	sec, err := secgroups.Get(models.GetClientCompute(provider), sgID).Extract()
	return sec, err
}

// ListSecGroupServer security group
func (c *Compute) ListSecGroupServer(serverID string) ([]secgroups.SecurityGroup, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := secgroups.ListByServer(models.GetClientCompute(provider), serverID).AllPages()
	if err != nil {
		return nil, err
	}

	allSecurityGroups, err := secgroups.ExtractSecurityGroups(allPages)
	if err != nil {
		return nil, err
	}

	for _, sg := range allSecurityGroups {
		fmt.Printf("%+v\n", sg)
	}
	return allSecurityGroups, err
}

// CreateSecGroup groupName, Description
func (c *Compute) CreateSecGroup(groupName, description string) (*secgroups.SecurityGroup, error) {
	provider := models.GetProvider(c.Username)
	createOpts := secgroups.CreateOpts{
		Name:        groupName,
		Description: description,
	}

	sg, err := secgroups.Create(models.GetClientCompute(provider), createOpts).Extract()
	return sg, err
}

// CreateSecGroupRule groupName, Description
func (c *Compute) CreateSecGroupRule(sgID, ipProtocol, cidr, fromGroupID string, fromPort, toPort int) (*secgroups.Rule, error) {
	provider := models.GetProvider(c.Username)
	createOpts := secgroups.CreateRuleOpts{
		ParentGroupID: sgID,
		FromPort:      fromPort,
		ToPort:        toPort,
		FromGroupID:   fromGroupID,
		IPProtocol:    ipProtocol,
		CIDR:          cidr,
	}

	rule, err := secgroups.CreateRule(models.GetClientCompute(provider), createOpts).Extract()
	return rule, err
}

// AddSecGroupToServer serverID, sgID
func (c *Compute) AddSecGroupToServer(serverID, sgID string) error {
	provider := models.GetProvider(c.Username)
	err := secgroups.AddServer(models.GetClientCompute(provider), serverID, sgID).ExtractErr()
	return err
}

// RemoveSecGroupFromServer serverID, sgID
func (c *Compute) RemoveSecGroupFromServer(serverID, sgID string) error {
	provider := models.GetProvider(c.Username)
	err := secgroups.RemoveServer(models.GetClientCompute(provider), serverID, sgID).ExtractErr()
	return err
}

// DeleteSecGroup sgID
func (c *Compute) DeleteSecGroup(sgID string) error {
	provider := models.GetProvider(c.Username)
	err := secgroups.Delete(models.GetClientCompute(provider), sgID).ExtractErr()
	return err
}

// DeleteSecGroupRule sgID
func (c *Compute) DeleteSecGroupRule(ruleID string) error {
	provider := models.GetProvider(c.Username)
	err := secgroups.DeleteRule(models.GetClientCompute(provider), ruleID).ExtractErr()
	return err
}

// UpdateSecGroup sgID
func (c *Compute) UpdateSecGroup(sgID, groupName, description string) (*secgroups.SecurityGroup, error) {
	provider := models.GetProvider(c.Username)
	UpdateOptsBuilder := secgroups.UpdateOpts{
		Name:        groupName,
		Description: &description,
	}

	sg, err := secgroups.Update(models.GetClientCompute(provider), sgID, UpdateOptsBuilder).Extract()
	return sg, err
}
