package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/common/extensions"
	"gitlab.com/fibo-stack/backend/models"
	// "github.com/gophercloud/gophercloud/openstack/compute/v2/extensions"
)

// ListExtensions ...
func (c *Compute) ListExtensions() ([]extensions.Extension, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := extensions.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		panic(err)
	}
	allExtensions, err := extensions.ExtractExtensions(allPages)
	if err != nil {
		panic(err)
	}
	for _, exten := range allExtensions {
		fmt.Printf("%+v\n", exten)
	}
	return allExtensions, err
}

// GetExtension ...
func (c *Compute) GetExtension(alias string) (*extensions.Extension, error) {
	provider := models.GetProvider(c.Username)
	extension, err := extensions.Get(models.GetClientCompute(provider), alias).Extract()
	return extension, err
}
