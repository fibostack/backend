package compute

import (
	"strings"

	"github.com/astaxie/beego"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/flavors"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/bootfromvolume"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/keypairs"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

// Compute ...
type Compute struct {
	Username string
}

// GetCompute ...
func GetCompute(username string) *Compute {
	return &Compute{
		Username: username,
	}
}

// ListAllServers ...
func (c *Compute) ListAllServers(tenantID string, adminProvider *gophercloud.ProviderClient) ([]servers.Server, error) {
	listOpts := servers.ListOpts{
		AllTenants: true,
		TenantID:   tenantID,
	}

	allPages, err := servers.List(models.GetClientCompute(adminProvider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allServers, err := servers.ExtractServers(allPages)
	return allServers, err
}

// ListServers ...
func (c *Compute) ListServers(tenantID string) ([]servers.Server, error) {
	provider := models.GetProvider(c.Username)
	listOpts := servers.ListOpts{
		AllTenants: false,
		TenantID:   tenantID,
	}

	allPages, err := servers.List(models.GetClientCompute(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allServers, err := servers.ExtractServers(allPages)
	return allServers, err
}

// CreateServer ...
func (c *Compute) CreateServer(flavorID, imageID, name string, SecurityGroups []string, network []servers.Network) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	createOpts := servers.CreateOpts{
		Name:           name,
		ImageRef:       imageID,
		FlavorRef:      flavorID,
		SecurityGroups: SecurityGroups,
		Networks:       network,
	}

	server, err := servers.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// CreateServerWithAppWithKeypair ...
func (c *Compute) CreateServerWithAppWithKeypair(flavorID, imageID, name string, SecurityGroups []string, userData []byte, keypairName string, network []servers.Network) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	InstancecreateOpts := servers.CreateOpts{
		Name:           name,
		ImageRef:       imageID,
		FlavorRef:      flavorID,
		SecurityGroups: SecurityGroups,
		UserData:       userData,
		Networks:       network,
	}

	createOpts := keypairs.CreateOptsExt{
		CreateOptsBuilder: InstancecreateOpts,
		KeyName:           keypairName,
	}

	server, err := servers.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// CreateServerWithApp ...
func (c *Compute) CreateServerWithApp(flavorID, imageID, name string, SecurityGroups []string, userData []byte, network []servers.Network) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	InstancecreateOpts := servers.CreateOpts{
		Name:           name,
		ImageRef:       imageID,
		FlavorRef:      flavorID,
		SecurityGroups: SecurityGroups,
		UserData:       userData,
		Networks:       network,
	}

	server, err := servers.Create(models.GetClientCompute(provider), InstancecreateOpts).Extract()
	return server, err
}

// GetServer ...
// GetServer get all servers from list
func (c *Compute) GetServer(serverID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	// We need the UUID in string form
	server, err := servers.Get(models.GetClientCompute(provider), serverID).Extract()

	return server, err
}

// UpdateServer ...
func (c *Compute) UpdateServer(name string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	opts := servers.UpdateOpts{Name: name}
	server, err := servers.Update(models.GetClientCompute(provider), name, opts).Extract()
	return server, err
}

// DeleteServer ...
func (c *Compute) DeleteServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	server, errServer := servers.Get(models.GetClientCompute(provider), serverID).Extract()
	if errServer != nil {
		return errServer
	}
	fError := func() error {
		// proider1, fError := user.AuthOSAdmin()
		optsAdmin := gophercloud.AuthOptions{
			IdentityEndpoint: beego.AppConfig.String("identityEndpoint"),
			Username:         beego.AppConfig.String("username"),
			Password:         beego.AppConfig.String("password"),
			TenantID:         beego.AppConfig.String("tenantID"),
			DomainID:         "default",
		}
		provider1, err := openstack.AuthenticatedClient(optsAdmin)
		if err != nil {
			return err
		}
		clientAdmin, fError := openstack.NewComputeV2(provider1, gophercloud.EndpointOpts{
			Region: "RegionOne",
		})
		if fError != nil {
			return fError
		}
		flavor, fError := flavors.Get(models.GetClientCompute(provider), server.Flavor["id"].(string)).Extract()
		if fError != nil {
			return fError
		}
		//delete flavor
		if strings.Contains(flavor.Name, server.Name) {
			fError = flavors.Delete(clientAdmin, server.Flavor["id"].(string)).ExtractErr()
			if fError != nil {
				return fError
			}
		}
		return nil
	}()
	if fError != nil {
		return fError
	}

	//delete server
	err := servers.Delete(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// ForceDeleteServer ...
func (c *Compute) ForceDeleteServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := servers.ForceDelete(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// ChangeAdminPassword ...
func (c *Compute) ChangeAdminPassword(serverID string, newPassword string) error {
	provider := models.GetProvider(c.Username)
	err := servers.ChangeAdminPassword(models.GetClientCompute(provider), serverID, newPassword).ExtractErr()
	return err
}

// RebootServer ...
func (c *Compute) RebootServer(serverID, rebootType string) error {
	provider := models.GetProvider(c.Username)
	// You have a choice of two reboot methods: servers.SoftReboot or servers.HardReboot
	// result := servers.Reboot(models.GetClientCompute(provider), serverID, servers.SoftReboot)

	// return result
	rebootOpts := servers.RebootOpts{}
	if rebootType == "hard" {
		rebootOpts = servers.RebootOpts{
			Type: servers.HardReboot,
		}
	} else {
		rebootOpts = servers.RebootOpts{
			Type: servers.SoftReboot,
		}
	}

	err := servers.Reboot(models.GetClientCompute(provider), serverID, rebootOpts).ExtractErr()
	return err
}

// ResizeServer ...
func (c *Compute) ResizeServer(serverID string, newFlavorID string) error {
	provider := models.GetProvider(c.Username)
	opts := servers.ResizeOpts{
		FlavorRef: newFlavorID,
	}
	err := servers.Resize(models.GetClientCompute(provider), serverID, opts).ExtractErr()
	return err
}

// ConfirmResizeServer ...
func (c *Compute) ConfirmResizeServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := servers.ConfirmResize(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// RevertResizeServer ...
func (c *Compute) RevertResizeServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := servers.RevertResize(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// SnapshotServer ...
func (c *Compute) SnapshotServer(serverID, name string) (string, error) {
	provider := models.GetProvider(c.Username)
	snapshotOpts := servers.CreateImageOpts{
		Name: name,
	}
	imageID, err := servers.CreateImage(models.GetClientCompute(provider), serverID, snapshotOpts).ExtractImageID()
	return imageID, err
}

// WaitServerStat ...
//Wait until server status "ACTIVE"
func (c *Compute) WaitServerStat(serverID string) (err error) {
	provider := models.GetProvider(c.Username)
	err = servers.WaitForStatus(models.GetClientCompute(provider), serverID, "ACTIVE", 5)
	return
}

// CreateServerForVolume ...
func (c *Compute) CreateServerForVolume(bootImageID, serverName, flavorID, keypairName string, volumeSize int, securityGroups []string, userData []byte, aZ string, deleteOnTermintaion bool, networkID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	blockDevices := []bootfromvolume.BlockDevice{
		{
			DestinationType:     bootfromvolume.DestinationVolume,
			SourceType:          bootfromvolume.SourceImage,
			UUID:                bootImageID,
			VolumeSize:          volumeSize,
			DeleteOnTermination: deleteOnTermintaion,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:             serverName,
		FlavorRef:        flavorID,
		SecurityGroups:   securityGroups,
		UserData:         userData,
		AvailabilityZone: aZ,
		Networks: []servers.Network{
			{
				UUID: networkID,
			},
		},
	}

	serverWithKeyPair := keypairs.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		KeyName:           keypairName,
	}

	createOpts := bootfromvolume.CreateOptsExt{
		CreateOptsBuilder: serverWithKeyPair,
		BlockDevice:       blockDevices,
	}

	server, err := bootfromvolume.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// ShowConsole ...
func (c *Compute) ShowConsole(serverID string, length int) servers.ShowConsoleOutputResult {
	provider := models.GetProvider(c.Username)
	opts := servers.ShowConsoleOutputOpts{
		Length: length,
	}
	result := servers.ShowConsoleOutput(models.GetClientCompute(provider), serverID, opts)
	return result
}
