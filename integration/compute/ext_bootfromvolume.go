package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/bootfromvolume"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/keypairs"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/bootfromvolume

// CreateServerFromImage ...
func (c *Compute) CreateServerFromImage(serverName, flavorID, imageID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	blockDevices := []bootfromvolume.BlockDevice{
		bootfromvolume.BlockDevice{
			BootIndex:           0,
			DeleteOnTermination: true,
			DestinationType:     bootfromvolume.DestinationLocal,
			SourceType:          bootfromvolume.SourceImage,
			UUID:                imageID,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		FlavorRef: flavorID,
	}

	createOpts := bootfromvolume.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		BlockDevice:       blockDevices,
	}

	server, err := bootfromvolume.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// CreateServerFromSnapshot ...
func (c *Compute) CreateServerFromSnapshot(serverName, flavorID, imageID, bootImageID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	blockDevices := []bootfromvolume.BlockDevice{
		bootfromvolume.BlockDevice{
			BootIndex:           0,
			DeleteOnTermination: true,
			DestinationType:     bootfromvolume.DestinationLocal,
			SourceType:          bootfromvolume.SourceSnapshot,
			UUID:                bootImageID,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		FlavorRef: flavorID,
		ImageRef:  imageID,
	}

	createOpts := bootfromvolume.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		BlockDevice:       blockDevices,
	}

	server, err := bootfromvolume.Create(models.GetClientCompute(provider), createOpts).Extract()

	return server, err
}

// CreateServerFromNewVolume ...
func (c *Compute) CreateServerFromNewVolume(bootImageID, serverName, flavorID string, volumeSize int) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	blockDevices := []bootfromvolume.BlockDevice{
		bootfromvolume.BlockDevice{
			DeleteOnTermination: true,
			DestinationType:     bootfromvolume.DestinationVolume,
			SourceType:          bootfromvolume.SourceImage,
			UUID:                bootImageID,
			VolumeSize:          volumeSize,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		FlavorRef: flavorID,
	}

	createOpts := bootfromvolume.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		BlockDevice:       blockDevices,
	}

	server, err := bootfromvolume.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// CreateServerFromExistVolume ...
func (c *Compute) CreateServerFromExistVolume(volumeID, serverName, flavorID, networkID, keypairName string, userData []byte) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	blockDevices := []bootfromvolume.BlockDevice{
		bootfromvolume.BlockDevice{
			DeleteOnTermination: true,
			DestinationType:     bootfromvolume.DestinationVolume,
			SourceType:          bootfromvolume.SourceVolume,
			UUID:                volumeID,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		FlavorRef: flavorID,
		UserData:  userData,
		Networks: []servers.Network{
			{
				UUID: networkID,
			},
		},
	}

	serverWithKeyPair := keypairs.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		KeyName:           keypairName,
	}

	createOpts := bootfromvolume.CreateOptsExt{
		CreateOptsBuilder: serverWithKeyPair,
		BlockDevice:       blockDevices,
	}

	server, err := bootfromvolume.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// CreateServerFromMultiEphemeralDisk ...
func (c *Compute) CreateServerFromMultiEphemeralDisk(imageID, flavorID, name string, volumeSize int) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	blockDevices := []bootfromvolume.BlockDevice{
		bootfromvolume.BlockDevice{
			BootIndex:           0,
			DestinationType:     bootfromvolume.DestinationLocal,
			DeleteOnTermination: true,
			SourceType:          bootfromvolume.SourceImage,
			UUID:                imageID,
			VolumeSize:          volumeSize,
		},
		bootfromvolume.BlockDevice{
			BootIndex:           -1,
			DestinationType:     bootfromvolume.DestinationLocal,
			DeleteOnTermination: true,
			GuestFormat:         "ext4",
			SourceType:          bootfromvolume.SourceBlank,
			VolumeSize:          1,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      name,
		FlavorRef: flavorID,
		ImageRef:  imageID,
	}

	createOpts := bootfromvolume.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		BlockDevice:       blockDevices,
	}

	server, err := bootfromvolume.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}
