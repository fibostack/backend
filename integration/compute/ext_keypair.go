package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/keypairs"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/keypairs

// ListKeypairs ...
func (c *Compute) ListKeypairs() ([]keypairs.KeyPair, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := keypairs.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allKeyPairs, err := keypairs.ExtractKeyPairs(allPages)
	if err != nil {
		return nil, err
	}

	for _, kp := range allKeyPairs {
		fmt.Printf("%+v\n", kp)
	}
	return allKeyPairs, nil
}

// CreateKeypair ...
func (c *Compute) CreateKeypair(keypairName string) (*keypairs.KeyPair, error) {
	provider := models.GetProvider(c.Username)
	createOpts := keypairs.CreateOpts{
		Name: keypairName,
	}

	keypair, err := keypairs.Create(models.GetClientCompute(provider), createOpts).Extract()
	return keypair, err
}

// GetKeyPair ...
func (c *Compute) GetKeyPair(keypairName string) (*keypairs.KeyPair, error) {
	provider := models.GetProvider(c.Username)
	some, err := keypairs.Get(models.GetClientCompute(provider), keypairName).Extract()
	return some, err
}

// ImportKeypair ...
func (c *Compute) ImportKeypair(keypairName, publicKey string) (*keypairs.KeyPair, error) {
	provider := models.GetProvider(c.Username)
	createOpts := keypairs.CreateOpts{
		Name:      keypairName,
		PublicKey: publicKey,
	}

	keypair, err := keypairs.Create(models.GetClientCompute(provider), createOpts).Extract()
	return keypair, err
}

// DeleteKeypair ...
func (c *Compute) DeleteKeypair(keypairName string) error {
	provider := models.GetProvider(c.Username)
	err := keypairs.Delete(models.GetClientCompute(provider), keypairName).ExtractErr()
	return err
}

// CreateServerWithKeypair ...
func (c *Compute) CreateServerWithKeypair(serverName, imageID, flavorID, keypairName string, securityGroups []string, network []servers.Network) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	serverCreateOpts := servers.CreateOpts{
		Name:           serverName,
		ImageRef:       imageID,
		FlavorRef:      flavorID,
		SecurityGroups: securityGroups,
		Networks:       network,
	}

	createOpts := keypairs.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		KeyName:           keypairName,
	}

	server, err := servers.Create(models.GetClientCompute(provider), createOpts).Extract()

	return server, err
}
