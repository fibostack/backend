package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/tenantnetworks"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/tenantnetworks

// ListTenantNetworks ...
func (c *Compute) ListTenantNetworks() ([]tenantnetworks.Network, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := tenantnetworks.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allNetworks, err := tenantnetworks.ExtractNetworks(allPages)
	if err != nil {
		return nil, err
	}

	for _, network := range allNetworks {
		fmt.Printf("%+v\n", network)
	}
	return allNetworks, err
}

// GetTenantNetwork ...
func (c *Compute) GetTenantNetwork(networkID string) (*tenantnetworks.Network, error) {
	provider := models.GetProvider(c.Username)
	network, err := tenantnetworks.Get(models.GetClientCompute(provider), networkID).Extract()
	return network, err
}
