package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/availabilityzones"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/availabilityzones

// ServerWithAZ ...
type ServerWithAZ struct {
	servers.Server
	availabilityzones.ServerAvailabilityZoneExt
}

// ListServerAZ ...
func (c *Compute) ListServerAZ() ([]ServerWithAZ, error) {
	provider := models.GetProvider(c.Username)
	var allServers []ServerWithAZ
	allPages, err := servers.List(models.GetClientCompute(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	err = servers.ExtractServersInto(allPages, &allServers)
	if err != nil {
		return nil, err
	}

	return allServers, err
}

// ListAZs ...
func (c *Compute) ListAZs() ([]availabilityzones.AvailabilityZone, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := availabilityzones.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	availabilityZoneInfo, err := availabilityzones.ExtractAvailabilityZones(allPages)
	if err != nil {
		return nil, err
	}

	for _, zoneInfo := range availabilityZoneInfo {
		fmt.Printf("%+v\n", zoneInfo)
	}
	return availabilityZoneInfo, err
}

// ListAZsDetail ...
func (c *Compute) ListAZsDetail() ([]availabilityzones.AvailabilityZone, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := availabilityzones.ListDetail(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	availabilityZoneInfo, err := availabilityzones.ExtractAvailabilityZones(allPages)
	if err != nil {
		return nil, err
	}

	for _, zoneInfo := range availabilityZoneInfo {
		fmt.Printf("%+v\n", zoneInfo)
	}
	return availabilityZoneInfo, err
}
