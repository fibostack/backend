package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/servergroups"
	"gitlab.com/fibo-stack/backend/models"
)

// ListSerGroup ...
func (c *Compute) ListSerGroup() ([]servergroups.ServerGroup, error) {
	provider := models.GetProvider(c.Username)
	allpages, err := servergroups.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allServerGroups, err := servergroups.ExtractServerGroups(allpages)
	if err != nil {
		return nil, err
	}

	for _, sg := range allServerGroups {
		fmt.Printf("%#v\n", sg)
	}
	return allServerGroups, err
}

// CreateSerGroup sergroupName
func (c *Compute) CreateSerGroup(sergroupName string) (*servergroups.ServerGroup, error) {
	provider := models.GetProvider(c.Username)
	createOpts := servergroups.CreateOpts{
		Name:     sergroupName,
		Policies: []string{"anti-affinity"},
	}

	sg, err := servergroups.Create(models.GetClientCompute(provider), createOpts).Extract()
	return sg, err
}

// DeleteSerGroup sgID
func (c *Compute) DeleteSerGroup(sgID string) error {
	provider := models.GetProvider(c.Username)
	err := servergroups.Delete(models.GetClientCompute(provider), sgID).ExtractErr()
	return err
}

// GetSerGroup sgID
func (c *Compute) GetSerGroup(sgID string) (*servergroups.ServerGroup, error) {
	provider := models.GetProvider(c.Username)
	sg, err := servergroups.Get(models.GetClientCompute(provider), sgID).Extract()
	return sg, err
}
