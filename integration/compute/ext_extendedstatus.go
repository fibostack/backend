package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/extendedstatus"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

// ServerWithExt ...
type ServerWithExt struct {
	servers.Server
	extendedstatus.ServerExtendedStatusExt
}

// GetExtStatus ...
func (c *Compute) GetExtStatus() ([]ServerWithExt, error) {
	provider := models.GetProvider(c.Username)
	var allServers []ServerWithExt

	allPages, err := servers.List(models.GetClientCompute(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	err = servers.ExtractServersInto(allPages, &allServers)
	if err != nil {
		return nil, err
	}

	return allServers, err
}
