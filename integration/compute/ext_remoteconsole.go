package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/remoteconsoles"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/remoteconsoles

// CreateRemoteConsole ...
func (c *Compute) CreateRemoteConsole(serverID string) (*remoteconsoles.RemoteConsole, error) {
	provider := models.GetProvider(c.Username)
	//change compute service versioin
	client := models.GetClientCompute(provider)
	client.Microversion = "2.65"

	createOpts := remoteconsoles.CreateOpts{
		Protocol: remoteconsoles.ConsoleProtocolVNC,
		Type:     remoteconsoles.ConsoleTypeNoVNC,
	}

	remtoteConsole, err := remoteconsoles.Create(client, serverID, createOpts).Extract()

	return remtoteConsole, err
}
