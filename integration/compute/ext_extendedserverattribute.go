package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/extendedserverattributes"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

// ServerAttributesExt ...
type ServerAttributesExt struct {
	servers.Server
	extendedserverattributes.ServerAttributesExt
}

// GetExtSerAttribute ...
func (c *Compute) GetExtSerAttribute(serverID string) (ServerAttributesExt, error) {
	provider := models.GetProvider(c.Username)
	var serverWithAttributesExt ServerAttributesExt

	err := servers.Get(models.GetClientCompute(provider), serverID).ExtractInto(&serverWithAttributesExt)
	return serverWithAttributesExt, err
}
