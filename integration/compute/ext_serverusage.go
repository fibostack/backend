package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/serverusage"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/serverusage

// ServerUsageExt ...
type ServerUsageExt struct {
	servers.Server
	serverusage.UsageExt
}

// GetServerInfo serverID
func (c *Compute) GetServerInfo(serverID string) (ServerUsageExt, error) {
	provider := models.GetProvider(c.Username)
	var serverWithUsageExt ServerUsageExt

	err := servers.Get(models.GetClientCompute(provider), serverID).ExtractInto(&serverWithUsageExt)
	fmt.Printf("%+v\n", serverWithUsageExt)
	return serverWithUsageExt, err
}
