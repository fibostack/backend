package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/diskconfig"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/diskconfig

// ServerWithDiskConfig ...
type ServerWithDiskConfig struct {
	servers.Server
	diskconfig.ServerDiskConfigExt
}

// ListDiskConfigs ...
func (c *Compute) ListDiskConfigs() ([]ServerWithDiskConfig, error) {
	provider := models.GetProvider(c.Username)
	var allServers []ServerWithDiskConfig

	allPages, err := servers.List(models.GetClientCompute(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	err = servers.ExtractServersInto(allPages, &allServers)
	if err != nil {
		return nil, err
	}

	return allServers, err
}

// CreateDiskConfig ...
func (c *Compute) CreateDiskConfig(serverName, imageID, flavorID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		ImageRef:  imageID,
		FlavorRef: flavorID,
	}

	createOpts := diskconfig.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		DiskConfig:        diskconfig.Manual,
	}

	server, err := servers.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}
