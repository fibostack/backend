package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/volumeattach"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/volumeattach

// AttachVolume ...
func (c *Compute) AttachVolume(serverID, volumeID string) (*volumeattach.VolumeAttachment, error) {
	provider := models.GetProvider(c.Username)
	createOpts := volumeattach.CreateOpts{
		VolumeID: volumeID,
	}

	result, err := volumeattach.Create(models.GetClientCompute(provider), serverID, createOpts).Extract()
	return result, err
}

// DetachVolume ...
func (c *Compute) DetachVolume(serverID, attachmentID string) error {
	provider := models.GetProvider(c.Username)
	err := volumeattach.Delete(models.GetClientCompute(provider), serverID, attachmentID).ExtractErr()
	return err
}
