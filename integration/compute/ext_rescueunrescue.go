package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/rescueunrescue"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/rescueunrescue

// RescueServer ...
//if rescueimageref is left blank, the server will be rescued with the default image
//if adminpass is left blank, the server will generate a password.
func (c *Compute) RescueServer(serverID string) (string, error) {
	provider := models.GetProvider(c.Username)
	rescueOpts := rescueunrescue.RescueOpts{}

	adminPass, err := rescueunrescue.Rescue(models.GetClientCompute(provider), serverID, rescueOpts).Extract()
	return adminPass, err
}

// UnrescueServer ...
func (c *Compute) UnrescueServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := rescueunrescue.Unrescue(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}
