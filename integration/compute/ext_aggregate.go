package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/aggregates"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/aggregates

// CreateAggregate ...
func (c *Compute) CreateAggregate(name, az string) (*aggregates.Aggregate, error) {
	provider := models.GetProvider(c.Username)
	opts := aggregates.CreateOpts{
		Name:             name,
		AvailabilityZone: az,
	}

	aggregate, err := aggregates.Create(models.GetClientCompute(provider), opts).Extract()
	return aggregate, err
}

// ShowAggregate ...
func (c *Compute) ShowAggregate(aggregateID int) (*aggregates.Aggregate, error) {
	provider := models.GetProvider(c.Username)
	aggregate, err := aggregates.Get(models.GetClientCompute(provider), aggregateID).Extract()

	return aggregate, err
}

// DeleteAggregate ...
func (c *Compute) DeleteAggregate(aggregateID int) error {
	provider := models.GetProvider(c.Username)
	err := aggregates.Delete(models.GetClientCompute(provider), aggregateID).ExtractErr()
	return err
}

// UpdateAggregate ...
func (c *Compute) UpdateAggregate(aggregateID int, newName, availabilityZone string) (*aggregates.Aggregate, error) {
	provider := models.GetProvider(c.Username)
	opts := aggregates.UpdateOpts{
		Name:             newName,
		AvailabilityZone: availabilityZone,
	}

	aggregate, err := aggregates.Update(models.GetClientCompute(provider), aggregateID, opts).Extract()
	return aggregate, err
}

// ListAggregates ...
func (c *Compute) ListAggregates() ([]aggregates.Aggregate, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := aggregates.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allAggregates, err := aggregates.ExtractAggregates(allPages)
	if err != nil {
		return nil, err
	}

	for _, aggregate := range allAggregates {
		fmt.Printf("%+v\n", aggregate)
	}
	return allAggregates, err
}

// AddHost ...
func (c *Compute) AddHost(aggregateID int, hostName string) (*aggregates.Aggregate, error) {
	provider := models.GetProvider(c.Username)
	opts := aggregates.AddHostOpts{
		Host: hostName,
	}
	aggregate, err := aggregates.AddHost(models.GetClientCompute(provider), aggregateID, opts).Extract()
	return aggregate, err
}

// RemoveAggregate ...
func (c *Compute) RemoveAggregate(aggregateID int, hostName string) (*aggregates.Aggregate, error) {
	provider := models.GetProvider(c.Username)
	opts := aggregates.RemoveHostOpts{
		Host: hostName,
	}

	aggregate, err := aggregates.RemoveHost(models.GetClientCompute(provider), aggregateID, opts).Extract()
	return aggregate, err
}

// CreateOrUpdateMetadata ...
func (c *Compute) CreateOrUpdateMetadata(aggregateID int, metadata map[string]interface{}) (*aggregates.Aggregate, error) {
	provider := models.GetProvider(c.Username)
	opts := aggregates.SetMetadataOpts{
		Metadata: metadata,
	}

	aggregate, err := aggregates.SetMetadata(models.GetClientCompute(provider), aggregateID, opts).Extract()
	return aggregate, err
}
