package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/evacuate"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/evacuate

// EvacuateServerFromHost ...
func (c *Compute) EvacuateServerFromHost(serverID string) (string, error) {
	provider := models.GetProvider(c.Username)
	pass, err := evacuate.Evacuate(models.GetClientCompute(provider), serverID, evacuate.EvacuateOpts{}).ExtractAdminPass()
	return pass, err
}
