package compute

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/quotasets"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/quotasets

// GetQuotaSet ...
func (c *Compute) GetQuotaSet(projectID string) (*quotasets.QuotaSet, error) {
	provider := models.GetProvider(c.Username)
	quotaset, err := quotasets.Get(models.GetClientCompute(provider), projectID).Extract()
	return quotaset, err
}

// GetDetailQuotaset ...
func (c *Compute) GetDetailQuotaset(projectID string) (quotasets.QuotaDetailSet, error) {
	provider := models.GetProvider(c.Username)
	quotaset, err := quotasets.GetDetail(models.GetClientCompute(provider), projectID).Extract()
	return quotaset, err
}

// UpdateQuotaset ...
func (c *Compute) UpdateQuotaset(projectID string, fixedIP, core int) (*quotasets.QuotaSet, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := quotasets.UpdateOpts{
		FixedIPs: gophercloud.IntToPointer(fixedIP),
		Cores:    gophercloud.IntToPointer(core),
	}

	quotaset, err := quotasets.Update(models.GetClientCompute(provider), projectID, updateOpts).Extract()
	return quotaset, err
}

// InitialQuotaset ...
func (c *Compute) InitialQuotaset(provider *gophercloud.ProviderClient, projectID string) (*quotasets.QuotaSet, error) {
	updateOpts := quotasets.UpdateOpts{
		FloatingIPs: gophercloud.IntToPointer(0),
		Cores:       gophercloud.IntToPointer(0),
		RAM:         gophercloud.IntToPointer(0),
		KeyPairs:    gophercloud.IntToPointer(0),
		Instances:   gophercloud.IntToPointer(0),
	}

	quotaset, err := quotasets.Update(models.GetClientCompute(provider), projectID, updateOpts).Extract()
	return quotaset, err
}

// NormalQuotaset ...
func (c *Compute) NormalQuotaset(provider *gophercloud.ProviderClient, projectID string) (*quotasets.QuotaSet, error) {
	updateOpts := quotasets.UpdateOpts{
		FloatingIPs: gophercloud.IntToPointer(10),
		Cores:       gophercloud.IntToPointer(20),
		RAM:         gophercloud.IntToPointer(51200),
		KeyPairs:    gophercloud.IntToPointer(100),
		Instances:   gophercloud.IntToPointer(10),
	}

	quotaset, err := quotasets.Update(models.GetClientCompute(provider), projectID, updateOpts).Extract()
	return quotaset, err
}

// SetQuotaset ...
func (c *Compute) SetQuotaset(provider *gophercloud.ProviderClient, projectID string, floatingIPS, cors, ram, keypairs, instance, sgGroup int) (*quotasets.QuotaSet, error) {
	updateOpts := quotasets.UpdateOpts{
		FloatingIPs:    gophercloud.IntToPointer(floatingIPS),
		Cores:          gophercloud.IntToPointer(cors),
		RAM:            gophercloud.IntToPointer(ram),
		KeyPairs:       gophercloud.IntToPointer(keypairs),
		Instances:      gophercloud.IntToPointer(instance),
		SecurityGroups: gophercloud.IntToPointer(sgGroup),
	}

	quotaset, err := quotasets.Update(models.GetClientCompute(provider), projectID, updateOpts).Extract()
	return quotaset, err
}
