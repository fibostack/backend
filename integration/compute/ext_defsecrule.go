package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/defsecrules"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/defsecrules

// ListSecDefRules ...
func (c *Compute) ListSecDefRules() ([]defsecrules.DefaultRule, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := defsecrules.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allDefaultRules, err := defsecrules.ExtractDefaultRules(allPages)
	if err != nil {
		return nil, err
	}

	for _, df := range allDefaultRules {
		fmt.Printf("%+v\n", df)
	}
	return allDefaultRules, err
}

// GetSecDefRule ...
func (c *Compute) GetSecDefRule(ruleiD string) (*defsecrules.DefaultRule, error) {
	provider := models.GetProvider(c.Username)
	rule, err := defsecrules.Get(models.GetClientCompute(provider), ruleiD).Extract()
	return rule, err
}

// CreateSecDefRule ...
func (c *Compute) CreateSecDefRule(protocol, cidr string, fromProt, toPort int) (*defsecrules.DefaultRule, error) {
	provider := models.GetProvider(c.Username)
	createOpts := defsecrules.CreateOpts{
		IPProtocol: protocol,
		FromPort:   fromProt,
		ToPort:     toPort,
		CIDR:       cidr,
	}

	rule, err := defsecrules.Create(models.GetClientCompute(provider), createOpts).Extract()
	return rule, err
}

// DeleteSecDefRule ...
func (c *Compute) DeleteSecDefRule(ruleID string) error {
	provider := models.GetProvider(c.Username)
	err := defsecrules.Delete(models.GetClientCompute(provider), ruleID).ExtractErr()
	return err
}
