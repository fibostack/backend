package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/networks"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/networks

// ListNetworks ...
// All networks without TenantID
func (c *Compute) ListNetworks() ([]networks.Network, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := networks.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allNetworks, err := networks.ExtractNetworks(allPages)
	if err != nil {
		return nil, err
	}

	for _, network := range allNetworks {
		fmt.Printf("%+v\n", network)
	}
	return allNetworks, err
}

// GetNetwork ...
func (c *Compute) GetNetwork(networkID string) (*networks.Network, error) {
	provider := models.GetProvider(c.Username)
	network, err := networks.Get(models.GetClientCompute(provider), networkID).Extract()
	return network, err
}
