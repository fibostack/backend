package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/suspendresume"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/suspendresume

// SuspendServer serverID
func (c *Compute) SuspendServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := suspendresume.Suspend(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// ResumeServer serverID
func (c *Compute) ResumeServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := suspendresume.Resume(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}
