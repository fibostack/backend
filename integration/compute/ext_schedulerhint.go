package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/schedulerhints"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/schedulerhints

// CreateServerToServerGroup ...
func (c *Compute) CreateServerToServerGroup(serverGroupID, serverName, imageID, flavorID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	schedulerHints := schedulerhints.SchedulerHints{
		Group: serverGroupID,
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		ImageRef:  imageID,
		FlavorRef: flavorID,
	}

	createOpts := schedulerhints.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		SchedulerHints:    schedulerHints,
	}
	server, err := servers.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// CreateDifferentHostServer ...
// Create Server on a Different Host than Server A
func (c *Compute) CreateDifferentHostServer(serverIDa, serverName, imageID, flavorID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	schedulerHints := schedulerhints.SchedulerHints{
		DifferentHost: []string{
			serverIDa,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		ImageRef:  imageID,
		FlavorRef: flavorID,
	}

	createOpts := schedulerhints.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		SchedulerHints:    schedulerHints,
	}

	server, err := servers.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}

// CreateSameHostServer ...
//Create Server on the Same Host as Server A
func (c *Compute) CreateSameHostServer(serverIDa, serverName, imageID, flavorID string) (*servers.Server, error) {
	provider := models.GetProvider(c.Username)
	schedulerHints := schedulerhints.SchedulerHints{
		SameHost: []string{
			serverIDa,
		},
	}

	serverCreateOpts := servers.CreateOpts{
		Name:      serverName,
		ImageRef:  imageID,
		FlavorRef: flavorID,
	}

	createOpts := schedulerhints.CreateOptsExt{
		CreateOptsBuilder: serverCreateOpts,
		SchedulerHints:    schedulerHints,
	}

	server, err := servers.Create(models.GetClientCompute(provider), createOpts).Extract()
	return server, err
}
