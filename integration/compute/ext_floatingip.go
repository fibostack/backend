package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/floatingips"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/floatingips

// ListFloatingIPs ...
func (c *Compute) ListFloatingIPs() ([]floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := floatingips.List(models.GetClientCompute(provider)).AllPages()
	if err != nil {
		return nil, err
	}

	allFloatingIPs, err := floatingips.ExtractFloatingIPs(allPages)
	if err != nil {
		return nil, err
	}

	for _, fip := range allFloatingIPs {
		fmt.Printf("%+v\n", fip)
	}
	return allFloatingIPs, err
}

// CreateFloatingIP ...
func (c *Compute) CreateFloatingIP(poolName string) (*floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)
	createOpts := floatingips.CreateOpts{
		Pool: poolName,
	}

	fip, err := floatingips.Create(models.GetClientCompute(provider), createOpts).Extract()
	return fip, err
}

// DeleteFloatingIP ...
func (c *Compute) DeleteFloatingIP(floatingipID string) error {
	provider := models.GetProvider(c.Username)
	err := floatingips.Delete(models.GetClientCompute(provider), floatingipID).ExtractErr()
	return err
}

// AssociateFloatingIPWithServer ...
func (c *Compute) AssociateFloatingIPWithServer(serverID, floatingIP string) error {
	provider := models.GetProvider(c.Username)
	associateOpts := floatingips.AssociateOpts{
		FloatingIP: floatingIP,
	}

	err := floatingips.AssociateInstance(models.GetClientCompute(provider), serverID, associateOpts).ExtractErr()
	return err
}

// DisassociateFloatingIPFromServer ...
func (c *Compute) DisassociateFloatingIPFromServer(serverID, floatingIP string) error {
	provider := models.GetProvider(c.Username)
	disassociateOpts := floatingips.DisassociateOpts{
		FloatingIP: floatingIP,
	}

	err := floatingips.DisassociateInstance(models.GetClientCompute(provider), serverID, disassociateOpts).ExtractErr()
	return err
}

// GetFloatingIP ...
func (c *Compute) GetFloatingIP(floatingipID string) (*floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)
	fip, err := floatingips.Get(models.GetClientCompute(provider), floatingipID).Extract()
	return fip, err
}
