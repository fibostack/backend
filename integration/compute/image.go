package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/images"
	"gitlab.com/fibo-stack/backend/models"
)

// ListImage ...
func (c *Compute) ListImage() ([]images.Image, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := images.ListDetail(models.GetClientCompute(provider), images.ListOpts{}).AllPages()
	if err != nil {
		return nil, err
	}

	allImages, err := images.ExtractImages(allPages)
	if err != nil {
		return nil, err
	}
	return allImages, nil
}

// GetImage ...
func (c *Compute) GetImage(imageID string) (*images.Image, error) {
	provider := models.GetProvider(c.Username)
	image, err := images.Get(models.GetClientCompute(provider), imageID).Extract()
	return image, err
}

// DeleteImage ...
func (c *Compute) DeleteImage(imageID string) error {
	provider := models.GetProvider(c.Username)
	err := images.Delete(models.GetClientCompute(provider), imageID).ExtractErr()
	return err
}
