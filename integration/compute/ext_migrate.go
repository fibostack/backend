package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/migrate"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/migrate

// MigrateServer ...
func (c *Compute) MigrateServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := migrate.Migrate(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// LiveMigrateServer ...
func (c *Compute) LiveMigrateServer(serverID, host string, blockMig bool) error {
	provider := models.GetProvider(c.Username)
	migrationOpts := migrate.LiveMigrateOpts{
		Host:           &host,
		BlockMigration: &blockMig,
	}

	err := migrate.LiveMigrate(models.GetClientCompute(provider), serverID, migrationOpts).ExtractErr()
	return err
}
