package compute

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/limits"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/limits

// GetProjectLimit ...
func (c *Compute) GetProjectLimit(osTenantID string) (*limits.Limits, error) {
	provider := models.GetProvider(c.Username)
	opts := limits.GetOpts{
		TenantID: osTenantID,
	}

	limits, err := limits.Get(models.GetClientCompute(provider), opts).Extract()
	return limits, err
}

// GetProjectLimitByUsers ...
func (c *Compute) GetProjectLimitByUsers(tenantID string, adminProvider *gophercloud.ProviderClient) (*limits.Limits, error) {
	listOps := limits.GetOpts{
		TenantID: tenantID,
	}

	limits, err := limits.Get(models.GetClientCompute(adminProvider), listOps).Extract()
	return limits, err
}
