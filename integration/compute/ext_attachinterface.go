package compute

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/attachinterfaces"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/attachinterfaces

// ListServerInterface ...
func (c *Compute) ListServerInterface(serverID string) ([]attachinterfaces.Interface, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := attachinterfaces.List(models.GetClientCompute(provider), serverID).AllPages()
	if err != nil {
		return nil, err
	}

	allInterfaces, err := attachinterfaces.ExtractInterfaces(allPages)
	if err != nil {
		return nil, err
	}

	for _, interfac := range allInterfaces {
		fmt.Printf("%+v\n", interfac)
	}
	return allInterfaces, err
}

// GetServerInterface ...
func (c *Compute) GetServerInterface(portID, serverID string) (*attachinterfaces.Interface, error) {
	provider := models.GetProvider(c.Username)
	interfac, err := attachinterfaces.Get(models.GetClientCompute(provider), serverID, portID).Extract()
	return interfac, err
}

// CreateServerInterface ...
func (c *Compute) CreateServerInterface(networkID, serverID string) (*attachinterfaces.Interface, error) {
	provider := models.GetProvider(c.Username)
	attachOpts := attachinterfaces.CreateOpts{
		NetworkID: networkID,
	}
	interfac, err := attachinterfaces.Create(models.GetClientCompute(provider), serverID, attachOpts).Extract()
	return interfac, err
}

// DeleteServerInterface ...
func (c *Compute) DeleteServerInterface(portID, serverID string) error {
	provider := models.GetProvider(c.Username)
	err := attachinterfaces.Delete(models.GetClientCompute(provider), serverID, portID).ExtractErr()
	return err
}
