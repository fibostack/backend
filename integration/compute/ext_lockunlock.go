package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/lockunlock"
	"gitlab.com/fibo-stack/backend/models"
)

// LockServer ...
func (c *Compute) LockServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := lockunlock.Lock(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}

// UnlockServer ...
func (c *Compute) UnlockServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := lockunlock.Unlock(models.GetClientCompute(provider), serverID).ExtractErr()
	return err
}
