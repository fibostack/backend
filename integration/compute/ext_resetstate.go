package compute

import (
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/resetstate"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/resetstate

// ResetStateServer ...
func (c *Compute) ResetStateServer(serverID string) error {
	provider := models.GetProvider(c.Username)
	err := resetstate.ResetState(models.GetClientCompute(provider), serverID, resetstate.StateActive).ExtractErr()
	return err
}
