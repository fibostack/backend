package metrig

import (
	"github.com/gophercloud/utils/gnocchi/metric/v1/status"
	"gitlab.com/fibo-stack/backend/models"
)

// GetStatus Getting status
func (m *Metrig) GetStatus(details bool) (*status.Status, error) {
	// details := true

	getOpts := status.GetOpts{
		Details: &details,
	}

	gnocchiStatus, err := status.Get(models.GetGnocchi(models.GetProvider(m.Username)), getOpts).Extract()
	if err != nil {
		panic(err)
	}
	return gnocchiStatus, err
}
