package metrig

import (
	"github.com/gophercloud/utils/gnocchi/metric/v1/metrics"
	"gitlab.com/fibo-stack/backend/models"
)

// ListMetrics listing metrics
func (m *Metrig) ListMetrics(limit int) (result []metrics.Metric, err error) {
	provider := models.GetProvider(m.Username)
	listOpts := metrics.ListOpts{
		Limit: limit,
	}

	allPages, err := metrics.List(models.GetGnocchi(provider), listOpts).AllPages()
	if err != nil {
		return
	}

	result, err = metrics.ExtractMetrics(allPages)
	if err != nil {
		return
	}

	return
}

// GetMetric get metric details
func (m *Metrig) GetMetric(metricID string) (metric *metrics.Metric, err error) {
	provider := models.GetProvider(m.Username)
	metric, err = metrics.Get(models.GetGnocchi(provider), metricID).Extract()
	return
}

// CreateMetric Creating a metric and link it to an existing archive policy
// Creating a metric without an archive policy, assuming that Gnocchi has the needed archive policy rule and can assign the policy automatically
func (m *Metrig) CreateMetric(resourceID, archivePolicyName, name, unit string) (metric *metrics.Metric, err error) {
	provider := models.GetProvider(m.Username)
	createOpts := metrics.CreateOpts{
		// ArchivePolicyName: "low",
		// Name: "network.incoming.packets.rate",
		// Unit: "packet/s",
		ResourceID:        resourceID,
		ArchivePolicyName: archivePolicyName,
		Name:              name,
		Unit:              unit,
	}
	metric, err = metrics.Create(models.GetGnocchi(provider), createOpts).Extract()
	return
}

// DeleteMetric Deleting a Gnocchi metric
func (m *Metrig) DeleteMetric(metricID string) error {
	provider := models.GetProvider(m.Username)
	err := metrics.Delete(models.GetGnocchi(provider), metricID).ExtractErr()
	return err
}
