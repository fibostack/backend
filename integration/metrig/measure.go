package metrig

import (
	"time"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/pagination"
	"github.com/gophercloud/utils/gnocchi/metric/v1/measures"
	"gitlab.com/fibo-stack/backend/models"
)

// Metrig struct
type Metrig struct {
	Username string
}

// GetMetrig return Metrig
func GetMetrig(username string) *Metrig {
	return &Metrig{
		Username: username,
	}
}

// ListMeasuresOfMetric list measure of knowing metric
func (m *Metrig) ListMeasuresOfMetric(metricID string, startTime, stopTime time.Time) (result []measures.Measure, err error) {
	// 	startTime := time.Date(2018, 1, 4, 10, 0, 0, 0, time.UTC)
	// metricID := "9e5a6441-1044-4181-b66e-34e180753040"
	provider := models.GetProvider(m.Username)
	listOpts := measures.ListOpts{
		Resample:    "2h",
		Granularity: "1h",
		Start:       &startTime,
		Stop:        &stopTime,
	}
	var allPages pagination.Page
	allPages, err = measures.List(models.GetGnocchi(provider), metricID, listOpts).AllPages()
	if err != nil {
		return
	}

	result, err = measures.ExtractMeasures(allPages)
	if err != nil {
		return
	}
	return
}

// ListMeasuresOfMetric ...
func ListMeasuresOfMetric(metricID string, startTime, stopTime time.Time, gran, aggr string, client *gophercloud.ServiceClient) (result []measures.Measure, err error) {
	if aggr == "" {
		aggr = "mean"
	}
	listOpts := measures.ListOpts{}
	if startTime.IsZero() {
		listOpts = measures.ListOpts{
			// Resample:    "2h",
			Granularity: gran,
			Aggregation: aggr,
		}
	} else {
		listOpts = measures.ListOpts{
			// Resample:    "2h",
			Granularity: gran,
			Start:       &startTime,
			Stop:        &stopTime,
			Aggregation: aggr,
		}
	}

	var allPages pagination.Page
	allPages, err = measures.List(client, metricID, listOpts).AllPages()
	if err != nil {
		return
	}

	result, err = measures.ExtractMeasures(allPages)
	if err != nil {
		return
	}
	return
}

// ListMeasuresOfMetricForVolume ...
func ListMeasuresOfMetricForVolume(metricID string, client *gophercloud.ServiceClient) (result []measures.Measure, err error) {

	listOpts := measures.ListOpts{
		Granularity: "1m",
		Aggregation: "max",
	}

	var allPages pagination.Page
	allPages, err = measures.List(client, metricID, listOpts).AllPages()
	if err != nil {
		return
	}

	result, err = measures.ExtractMeasures(allPages)
	if err != nil {
		return
	}
	return
}

// CreateMeasureInsideSingleMetric create measure inside single metric
func (m *Metrig) CreateMeasureInsideSingleMetric(metricID string, timeStamp time.Time, value float64) error {
	provider := models.GetProvider(m.Username)
	createOpts := measures.CreateOpts{
		Measures: []measures.MeasureOpts{
			{
				Timestamp: &timeStamp,
				Value:     value,
			},
		},
	}
	// metricID := "9e5a6441-1044-4181-b66e-34e180753040"
	err := measures.Create(models.GetGnocchi(provider), metricID, createOpts).ExtractErr()
	return err
}

// CreateMeasuresMetricsViaID Creating measures inside different metrics via metric ID references in a single request (This is example)
func (m *Metrig) CreateMeasuresMetricsViaID() error {
	provider := models.GetProvider(m.Username)
	currentTimestamp := time.Now().UTC()
	pastHourTimestamp := currentTimestamp.Add(-1 * time.Hour)
	createOpts := measures.BatchCreateMetricsOpts{
		{
			ID: "777a01d6-4694-49cb-b86a-5ba9fd4e609e",
			Measures: []measures.MeasureOpts{
				{
					Timestamp: &currentTimestamp,
					Value:     200,
				},
				{
					Timestamp: &pastHourTimestamp,
					Value:     300,
				},
			},
		},
		{
			ID: "6dbc97c5-bfdf-47a2-b184-02e7fa348d21",
			Measures: []measures.MeasureOpts{
				{
					Timestamp: &currentTimestamp,
					Value:     111,
				},
				{
					Timestamp: &pastHourTimestamp,
					Value:     222,
				},
			},
		},
	}
	err := measures.BatchCreateMetrics(models.GetGnocchi(provider), createOpts).ExtractErr()
	return err
}
