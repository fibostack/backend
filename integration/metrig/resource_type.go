package metrig

import (
	"github.com/gophercloud/utils/gnocchi/metric/v1/resourcetypes"
	"gitlab.com/fibo-stack/backend/models"
)

// ListResourceTypes Listing resource types
func (m *Metrig) ListResourceTypes() (result []resourcetypes.ResourceType, err error) {
	allPages, err := resourcetypes.List(models.GetGnocchi(models.GetProvider(m.Username))).AllPages()
	if err != nil {
		return
	}
	result, err = resourcetypes.ExtractResourceTypes(allPages)
	if err != nil {
		return
	}
	return
}

// GetResourceType Getting a resource type
func (m *Metrig) GetResourceType(resourceTypeName string) (*resourcetypes.ResourceType, error) {
	// resourceTypeName := "compute_instance"
	resourceType, err := resourcetypes.Get(models.GetGnocchi(models.GetProvider(m.Username)), resourceTypeName).Extract()
	return resourceType, err
}

// CreateResourceType Creating a resource type
func (m *Metrig) CreateResourceType(name, nameType, portIDType string, maxLength int, requiredName bool, requiredPort bool) (*resourcetypes.ResourceType, error) {
	resourceTypeOpts := resourcetypes.CreateOpts{
		Name: name,
		Attributes: map[string]resourcetypes.AttributeOpts{
			"port_name": resourcetypes.AttributeOpts{
				Type: nameType,
				Details: map[string]interface{}{
					"max_length": maxLength,
					"required":   requiredName,
				},
			},
			"port_id": resourcetypes.AttributeOpts{
				Type: portIDType,
				Details: map[string]interface{}{
					"required": requiredPort,
				},
			},
		},
	}
	resourceType, err := resourcetypes.Create(models.GetGnocchi(models.GetProvider(m.Username)), resourceTypeOpts).Extract()
	return resourceType, err
}

// UpdateResourceType Updating a resource type
func (m *Metrig) UpdateResourceType(resourceTypeName string, enabledAttributeOptions, parendIDAttributeOptions resourcetypes.AttributeOpts) (*resourcetypes.ResourceType, error) {

	resourceTypeOpts := resourcetypes.UpdateOpts{
		Attributes: []resourcetypes.AttributeUpdateOpts{
			{
				Name:      "enabled",
				Operation: resourcetypes.AttributeAdd,
				Value:     &enabledAttributeOptions,
			},
			{
				Name:      "parent_id",
				Operation: resourcetypes.AttributeAdd,
				Value:     &parendIDAttributeOptions,
			},
			{
				Name:      "domain_id",
				Operation: resourcetypes.AttributeRemove,
			},
		},
	}
	resourceType, err := resourcetypes.Update(models.GetGnocchi(models.GetProvider(m.Username)), resourceTypeName, resourceTypeOpts).Extract()
	return resourceType, err
}

// DeleteResourceType Deleting a resource type
func (m *Metrig) DeleteResourceType(resoureTypeName string) error {
	err := resourcetypes.Delete(models.GetGnocchi(models.GetProvider(m.Username)), resoureTypeName).ExtractErr()
	return err
}
