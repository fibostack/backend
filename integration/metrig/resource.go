package metrig

import (
	"time"

	"github.com/gophercloud/gophercloud/pagination"
	"github.com/gophercloud/utils/gnocchi/metric/v1/resources"
	"gitlab.com/fibo-stack/backend/models"
)

// ListResources list resources by type
func (m *Metrig) ListResources(resourceType string) (result []resources.Resource, err error) {
	provider := models.GetProvider(m.Username)
	// resourceType: "instance",
	listOpts := resources.ListOpts{
		Details: true,
	}
	var allPages pagination.Page
	allPages, err = resources.List(models.GetGnocchi(provider), listOpts, resourceType).AllPages()
	if err != nil {
		return
	}

	result, err = resources.ExtractResources(allPages)
	if err != nil {
		return
	}
	return
}

// GetResource ...
func (m *Metrig) GetResource(resourceID, resourceType string) (resource *resources.Resource, err error) {
	// resourceID = "23d5d3f7-9dfa-4f73-b72b-8b0b0063ec55"
	provider := models.GetProvider(m.Username)
	// resourceType = "generic"
	resource, err = resources.Get(models.GetGnocchi(provider), resourceType, resourceID).Extract()
	return
}

// CreateResource Create resource without metric
func (m *Metrig) CreateResource(id, resourceType string) (resource *resources.Resource, err error) {
	provider := models.GetProvider(m.Username)
	createOpts := resources.CreateOpts{
		// ID: "23d5d3f7-9dfa-4f73-b72b-8b0b0063ec55",
		ID: id,
	}
	// resourceType = "generic"
	resource, err = resources.Create(models.GetGnocchi(provider), resourceType, createOpts).Extract()
	return
}

// CreateResourceWithExistingMetrics Creating a resource with links to some existing metrics with a starting timestamp of the resource
func (m *Metrig) CreateResourceWithExistingMetrics(id, projectID, resourceType string, metrics map[string]interface{}, startedAt time.Time) (*resources.Resource, error) {
	provider := models.GetProvider(m.Username)
	createOpts := resources.CreateOpts{
		ID:        id,
		ProjectID: projectID,
		StartedAt: &startedAt,
		Metrics:   metrics,
	}
	// resourceType = "compute_instance_disk"
	resource, err := resources.Create(models.GetGnocchi(provider), resourceType, createOpts).Extract()
	return resource, err
}

// CreataResourceWithMetrics Creating a resource and a metric a the same time
func (m *Metrig) CreataResourceWithMetrics(id, projectID, userID, resourceType string, metrics map[string]interface{}, startedAt time.Time) (*resources.Resource, error) {
	provider := models.GetProvider(m.Username)
	createOpts := resources.CreateOpts{
		ID:        id,
		ProjectID: projectID,
		UserID:    userID,
		Metrics:   metrics,
	}
	// resourceType = "compute_instance"
	resource, err := resources.Create(models.GetGnocchi(provider), resourceType, createOpts).Extract()
	return resource, err
}

// UpdateResource Updating a resource
func (m *Metrig) UpdateResource(resourceID, projectID, resourceType string) (*resources.Resource, error) {
	provider := models.GetProvider(m.Username)
	updateOpts := resources.UpdateOpts{
		// ProjectID: "4154f08883334e0494c41155c33c0fc9",
		ProjectID: projectID,
	}
	// resourceType = "generic"
	// resourceID = "23d5d3f7-9dfa-4f73-b72b-8b0b0063ec55"
	resource, err := resources.Update(models.GetGnocchi(provider), resourceType, resourceID, updateOpts).Extract()
	return resource, err
}

// UpdateResourceWithExistingMetric Updating a resource and associating an existing metric to it
func (m *Metrig) UpdateResourceWithExistingMetric(resourceID, resourceType string, metrics map[string]interface{}) (*resources.Resource, error) {
	updateOpts := resources.UpdateOpts{
		Metrics: &metrics,
	}
	resource, err := resources.Update(models.GetGnocchi(models.GetProvider(m.Username)), resourceType, resourceID, updateOpts).Extract()
	return resource, err
}

// UpdateResourceWithNewMetrics Updating a resource and creating an associated metric at the same time
func (m *Metrig) UpdateResourceWithNewMetrics(resourceID, resourceType string, metrics map[string]interface{}) (*resources.Resource, error) {
	updateOpts := resources.UpdateOpts{
		Metrics: &metrics,
	}
	resource, err := resources.Update(models.GetGnocchi(models.GetProvider(m.Username)), resourceType, resourceID, updateOpts).Extract()
	return resource, err
}

// DeleteResource Deleting a Gnocchi resource
func (m *Metrig) DeleteResource(resourceID, resourceType string) error {
	err := resources.Delete(models.GetGnocchi(models.GetProvider(m.Username)), resourceType, resourceID).ExtractErr()
	return err
}
