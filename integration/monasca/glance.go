package monasca

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/fibo-stack/backend/models"
)

// Glance ...
type Glance struct {
	Username string
}

// NewGlance ...
func NewGlance(username string) *Glance {
	return &Glance{
		Username: username,
	}
}

// GetImageInfo ...
/**
Description : Get Image information
*/
func (n *Glance) GetImageInfo(imageID, projectID string) (imageInfo models.ImageInfo, err error) {
	var data interface{}

	provider := models.GetProvider(n.Username)

	//client for Compute API operation
	client := models.GetClientImage(provider)

	response, err := client.Get(fmt.Sprintf("%s/%s/%s/images/%s", client.Endpoint, client.Microversion, projectID, imageID), nil, nil)
	if err != nil {
		return imageInfo, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return imageInfo, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})
	iInfo := msg["image"].(map[string]interface{})

	if len(iInfo) > 0 {
		resources := iInfo["metadata"].(map[string]interface{})
		if len(resources) > 0 {
			imageInfo.ID = iInfo["id"].(string)
			imageInfo.Name = iInfo["name"].(string)
			imageInfo.OsKind = resources["os_distro"].(string)
			imageInfo.OsType = resources["os_type"].(string)
			imageInfo.HypervisorType = resources["hypervisor_type"].(string)

			return imageInfo, err
		}
	}
	return imageInfo, err
}
