package monasca

import (
	"strings"
	"time"

	mod "github.com/monasca/golang-monascaclient/monascaclient/models"
	"gitlab.com/fibo-stack/backend/models"
)

// Monasca ...
type Monasca struct {
	Username string
}

// GetMonasca ...
func GetMonasca(username string) *Monasca {
	return &Monasca{
		Username: username,
	}
}

// GetAlarmNotificationList ...
//Alarm Notification List조회
func (m *Monasca) GetAlarmNotificationList(query mod.NotificationQuery) (result []models.AlarmNotification, err error) {
	client := models.GetClientMonasca(m.Username)
	notificationList, notiErr := client.GetNotificationMethods(&query)

	if notiErr != nil {
		return nil, notiErr
	}

	for _, data := range notificationList.Elements {
		var notification models.AlarmNotification
		notification.ID = data.ID
		notification.Name = data.Name
		notification.Email = data.Address

		result = append(result, notification)
	}
	return result, err
}

// GetAlarmNotification ...
func (m *Monasca) GetAlarmNotification(id string) (*mod.NotificationElement, error) {
	client := models.GetClientMonasca(m.Username)
	var notiQuery mod.NotificationQuery
	//query := new(mod.NotificationQuery)

	return client.GetNotificationMethod(id, &notiQuery)

}

// CreateAlarmNotification ...
func (m *Monasca) CreateAlarmNotification(query mod.NotificationRequestBody) (string, error) {
	client := models.GetClientMonasca(m.Username)
	res, err := client.CreateNotificationMethod(&query)
	return res.ResponseElement.ID, err
}

// UpdateAlarmNotification ...
func (m *Monasca) UpdateAlarmNotification(notificationID string, notificationRequestBody mod.NotificationRequestBody) error {
	client := models.GetClientMonasca(m.Username)
	_, err := client.UpdateNotificationMethod(notificationID, &notificationRequestBody)
	return err
}

// DeleteAlarmNotification ...
func (m *Monasca) DeleteAlarmNotification(notificationID string) error {
	client := models.GetClientMonasca(m.Username)
	err := client.DeleteNotificationMethod(notificationID)
	return err
}

// GetAlarmDefinitionList ...
func (m *Monasca) GetAlarmDefinitionList(query mod.AlarmDefinitionQuery) (result []models.AlarmDefinition, err error) {
	client := models.GetClientMonasca(m.Username)
	definitionList, defErr := client.GetAlarmDefinitions(&query)

	if defErr != nil {
		return nil, defErr
	}
	for _, data := range definitionList.Elements {

		var definition models.AlarmDefinition
		definition.ID = data.ID
		definition.Name = data.Name
		definition.AlarmAction = data.AlarmActions
		definition.OkAction = data.OkActions
		definition.Expression = data.Expression
		definition.MatchBy = data.MatchBy
		definition.Severity = data.Severity
		definition.Description = data.Description
		definition.UndeterminedActions = data.UndeterminedActions

		result = append(result, definition)
	}

	return result, err
}

// GetAlarmDefinition ...
func (m *Monasca) GetAlarmDefinition(id string) (result models.AlarmDefinition, err error) {
	client := models.GetClientMonasca(m.Username)
	definitionDetail, defErr := client.GetAlarmDefinition(id)

	if defErr != nil {
		return result, defErr
	}

	var definition models.AlarmDefinition
	definition.ID = definitionDetail.ID
	definition.Name = definitionDetail.Name
	definition.AlarmAction = definitionDetail.AlarmActions
	definition.OkAction = definitionDetail.OkActions
	definition.Expression = definitionDetail.Expression
	definition.MatchBy = definitionDetail.MatchBy
	definition.Severity = definitionDetail.Severity
	definition.Description = definitionDetail.Description
	definition.UndeterminedActions = definitionDetail.UndeterminedActions

	//definitionDetail.AlarmActions

	return definition, nil
}

// CreateAlarmDefinitionList ...
func (m *Monasca) CreateAlarmDefinitionList(alarmDefinitionRequestBody mod.AlarmDefinitionRequestBody) error {
	client := models.GetClientMonasca(m.Username)
	_, err := client.CreateAlarmDefinition(&alarmDefinitionRequestBody)
	return err
}

// UpdateAlarmDefinitionList ...
func (m *Monasca) UpdateAlarmDefinitionList(alarmDefinitionID string, alarmDefinitionRequestBody mod.AlarmDefinitionRequestBody) error {
	client := models.GetClientMonasca(m.Username)
	//_, err := client.UpdateAlarmDefinition(alarmDefinitionID, &alarmDefinitionRequestBody)
	_, err := client.PatchAlarmDefinition(alarmDefinitionID, &alarmDefinitionRequestBody)
	return err
}

// DeleteAlarmDefinitionList ...
func (m *Monasca) DeleteAlarmDefinitionList(alarmDefinitionID string) error {
	client := models.GetClientMonasca(m.Username)
	err := client.DeleteAlarmDefinition(alarmDefinitionID)
	return err
}

// GetAlarmCount ...
func (m *Monasca) GetAlarmCount(query mod.AlarmQuery) (result [][]int, alarmErr error) {

	// result, alarmErr := client.GetAlarmCount(&query)

	if alarmErr != nil {
		return result, alarmErr
	}
	return result, nil
}

// GetAlarmList ...
func (m *Monasca) GetAlarmList(query mod.AlarmQuery) (result []models.AlarmStatus, err error) {
	client := models.GetClientMonasca(m.Username)
	alarmStatusList, alarmErr := client.GetAlarms(&query)

	if alarmErr != nil {
		return result, alarmErr
	}

	for _, data := range alarmStatusList.Elements {

		var status models.AlarmStatus
		status.ID = data.ID
		status.State = data.State
		status.UpdateTime = data.StateUpdatedTimestamp.Add(time.Duration(models.GmtTimeGap) * time.Hour).Format("2006-01-02 15:04:05")

		var metricNameList []string
		for _, dimensionData := range data.Metrics {
			metricNameList = append(metricNameList, dimensionData.Name)

			if dimensionData.Dimensions["component"] != "" {
				status.Type = dimensionData.Dimensions["component"]
				status.Zone = dimensionData.Dimensions["zone"]
			} else {
				status.Type = "Node"
			}
			status.HostName = dimensionData.Dimensions["hostname"]
		}
		status.MetricName = strings.Join(metricNameList[:], ",")
		result = append(result, status)
	}

	if alarmErr != nil {
		err = alarmErr
	}
	return result, err
}

// GetAlarm ...
func (m *Monasca) GetAlarm(id string) (result models.AlarmStatus, err error) {
	client := models.GetClientMonasca(m.Username)
	alarmStatus, alarmErr := client.GetAlarm(id)

	if alarmErr != nil {
		return result, alarmErr
	}

	result.ID = alarmStatus.ID
	result.State = alarmStatus.State
	//fmt.Println( alarmStatus.StateUpdatedTimestamp.Add(time.Duration(models.GmtTimeGap) * time.Hour).Format("2006-01-02 15:04:05"))
	result.UpdateTime = alarmStatus.StateUpdatedTimestamp.Add(time.Duration(models.GmtTimeGap) * time.Hour).Format("2006-01-02 15:04:05")

	var metricNameList []string
	for _, dimensionData := range alarmStatus.Metrics {
		metricNameList = append(metricNameList, dimensionData.Name)

		if dimensionData.Dimensions["component"] != "" {
			result.Type = dimensionData.Dimensions["component"]
			result.Zone = dimensionData.Dimensions["zone"]
		} else {
			result.Type = "Node"
		}
		result.HostName = dimensionData.Dimensions["hostname"]
	}
	result.MetricName = strings.Join(metricNameList[:], ",")

	return result, err
}
