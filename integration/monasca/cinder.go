package monasca

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/astaxie/beego"

	"gitlab.com/fibo-stack/backend/database"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// Cinder ...
type Cinder struct {
	Username string
}

// GetCinder ...
func GetCinder(username string) *Cinder {
	return &Cinder{
		Username: username,
	}
}

// GetProjectStorageResources ...
/**
Description : Get project Storage Max & Used information
*/
func (n *Cinder) GetProjectStorageResources(projectID string) (result models.ProjectStorageResources, err error) {
	var data interface{}
	provider := models.GetProvider(n.Username)
	client := models.GetClientBlockstorage(provider)
	url := fmt.Sprintf("%s/v3/%s/limits", beego.AppConfig.String("cinder.target.url"), projectID)
	fmt.Println(url)
	response, err := client.Get(url, nil, nil)
	if err != nil {
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})

	subMsg := msg["limits"].(map[string]interface{})

	if len(subMsg) < 1 {
		return result, nil
	}

	resources := subMsg["absolute"].(map[string]interface{})

	if len(resources) > 0 {
		result.VolumeLimitGb = utils.TypeCheckerInt(resources["maxTotalVolumeGigabytes"]).(int)
		result.VolumeGb = utils.TypeCheckerInt(resources["totalGigabytesUsed"]).(int)
		result.VolumesLimit = utils.TypeCheckerInt(resources["maxTotalVolumes"]).(int)
		result.Volumes = utils.TypeCheckerInt(resources["totalVolumesUsed"]).(int)
		result.SnapshotsLimit = utils.TypeCheckerInt(resources["maxTotalSnapshots"]).(int)
		result.Snapshots = utils.TypeCheckerInt(resources["totalSnapshotsUsed"]).(int)
		result.BackupsLimit = utils.TypeCheckerInt(resources["maxTotalBackups"]).(int)
		result.Backups = utils.TypeCheckerInt(resources["totalBackupsUsed"]).(int)
	}
	return result, nil
}

// GetProjectStorageResourcesAdmin ...
func (n *Cinder) GetProjectStorageResourcesAdmin(user database.SysUser) (result models.ProjectStorageResources, err error) {
	var data interface{}
	_, errProvider := models.InitOpenstackProvider(user.Username, user.OsPwd, user.OsTenantID)
	if errProvider != nil {
		fmt.Println(errProvider.Error())
		// return result, errProvider
	}
	provider := models.GetProvider(user.Username)

	client := models.GetClientBlockstorage(provider)
	url := fmt.Sprintf("%s/v3/%s/limits", beego.AppConfig.String("cinder.target.url"), user.OsTenantID)
	response, err := client.Get(url, nil, nil)
	if err != nil {
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})

	subMsg := msg["limits"].(map[string]interface{})

	if len(subMsg) < 1 {
		return result, nil
	}

	resources := subMsg["absolute"].(map[string]interface{})

	if len(resources) > 0 {
		result.VolumeLimitGb = utils.TypeCheckerInt(resources["maxTotalVolumeGigabytes"]).(int)
		result.VolumeGb = utils.TypeCheckerInt(resources["totalGigabytesUsed"]).(int)
		result.VolumesLimit = utils.TypeCheckerInt(resources["maxTotalVolumes"]).(int)
		result.Volumes = utils.TypeCheckerInt(resources["totalVolumesUsed"]).(int)
		result.SnapshotsLimit = utils.TypeCheckerInt(resources["maxTotalSnapshots"]).(int)
		result.Snapshots = utils.TypeCheckerInt(resources["totalSnapshotsUsed"]).(int)
		result.BackupsLimit = utils.TypeCheckerInt(resources["maxTotalBackups"]).(int)
		result.Backups = utils.TypeCheckerInt(resources["totalBackupsUsed"]).(int)
	}
	return result, nil
}
