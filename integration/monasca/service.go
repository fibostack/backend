package monasca

import (
	"fmt"

	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// InstanceDao ...
type InstanceDao struct {
	influxClient client.Client
}

// GetInstanceDao ...
func GetInstanceDao(influxClient client.Client) *InstanceDao {
	return &InstanceDao{
		influxClient: influxClient,
	}
}

// GetInstanceCPUUsageList ...
//Query the current CPU usage list of the Instance.
func (d InstanceDao) GetInstanceCPUUsageList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {
			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuUsageSQL := "select mean(value) as usage from \"vm.cpu.utilization_norm_perc\"  where resource_id = '%s' "

	var q client.Query
	if request.DefaultTimeRange != "" {

		cpuUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		cpuUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetInstanceCpuUsageList Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceMemoryUsageList ...
//Query the current Memory usage list of the Instance.
func (d InstanceDao) GetInstanceMemoryUsageList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	memoryTotalSQL := "select mean(value) as usage from \"vm.mem.total_gb\"  where resource_id = '%s' "
	memoryFreeSQL := "select mean(value) as usage from \"vm.mem.free_gb\"  where resource_id = '%s' "

	models.MonitLogger.Debug("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		memoryTotalSQL += " and time > now() - %s  group by time(%s);"
		memoryFreeSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(memoryTotalSQL+memoryFreeSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		memoryTotalSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"
		memoryFreeSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(memoryTotalSQL+memoryFreeSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetInstanceMemoryUsageList Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceCPUUsage ...
//Query the current CPU utilization of the Instance.
func (d InstanceDao) GetInstanceCPUUsage(request models.InstanceReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuUsageSQL := "select value from \"vm.cpu.utilization_norm_perc\"  where time > now() - 2m and resource_id = '%s' order by time desc limit 1"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(cpuUsageSQL,
			request.InstanceID),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetInstanceCpuUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceTotalMemoryUsage ...
//Query the current total memory of Instance.
func (d InstanceDao) GetInstanceTotalMemoryUsage(request models.InstanceReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"vm.mem.total_gb\" where time > now() - 2m and resource_id = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.InstanceID),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debugf("GetInstanceTotalMemoryUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceFreeMemoryUsage ...
//Query the current free memory of Instance.
func (d InstanceDao) GetInstanceFreeMemoryUsage(request models.InstanceReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	freeMemSQL := "select value from \"vm.mem.free_gb\"  where time > now() - 2m and resource_id = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(freeMemSQL,
			request.InstanceID),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debugf("GetInstanceFreeMemoryUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceDiskIoKbyte ...
//Query disk io read Kbyte of Instance.
func (d InstanceDao) GetInstanceDiskIoKbyte(request models.DetailReq, gubun string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var diskUsageSQL string

	if gubun == "read" {
		diskUsageSQL = "select sum(value)/1024 as usage from \"vm.io.read_bytes_sec\"  where resource_id = '%s' "
	} else if gubun == "write" {
		diskUsageSQL = "select sum(value)/1024 as usage from \"vm.io.write_bytes_sec\"  where resource_id = '%s' "
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetInstanceDiskIoReadKbyte Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetInstanceNetworkKbyte ...
//Query the network io read Kbyte of Instance.
func (d InstanceDao) GetInstanceNetworkKbyte(request models.DetailReq, inOut string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var networkSQL string
	if inOut == "in" {
		networkSQL = "select sum(value)/1024 as usage from \"vm.net.in_bytes_sec\"  where resource_id = '%s' "
	} else if inOut == "out" {
		networkSQL = "select sum(value)/1024 as usage from \"vm.net.out_bytes_sec\"  where resource_id = '%s'"
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		networkSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		networkSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetInstanceNetworkKbyte Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetInstanceNetworkPackets ...
//Query the network io packets of Instance.
func (d InstanceDao) GetInstanceNetworkPackets(request models.DetailReq, inOut string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var networkSQL string
	if inOut == "in" {
		networkSQL = "select sum(value) as usage from \"vm.net.in_packets_sec\"  where resource_id = '%s' "
	} else if inOut == "out" {
		networkSQL = "select sum(value) as usage from \"vm.net.out_packets_sec\"  where resource_id = '%s'"
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		networkSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		networkSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetInstanceNetworkPackets Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}
