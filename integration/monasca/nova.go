package monasca

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/astaxie/beego"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// Nova ...
type Nova struct {
	Username string
}

// GetNova ...
func GetNova(username string) *Nova {
	return &Nova{
		Username: username,
	}
}

// GetOpenstackResources ...
/**
Description : Get openstack's total & used resources - to check why admin project id is needed.
*/
func (n *Nova) GetOpenstackResources() (result models.HypervisorResources, err error) {

	provider := models.GetProvider(n.Username)
	client := models.GetClientCompute(provider)
	response, err := client.Get(fmt.Sprintf("%s/%s/%s/os-hypervisors/statistics", client.Endpoint, client.Microversion, models.DefaultProjectID), nil, nil)
	msg, err := utils.ResponseUnmarshal(response, err)

	resources := msg["hypervisor_statistics"].(map[string]interface{})
	if len(resources) > 0 {
		result.VcpuTotal, _ = utils.TypeCheckerFloat64(resources["vcpus"]).(float64)
		result.VcpuUsed, _ = utils.TypeCheckerFloat64(resources["vcpus_used"]).(float64)
		result.DiskGbTotal, _ = utils.TypeCheckerFloat64(resources["local_gb"]).(float64)
		result.DiskGbUsed, _ = utils.TypeCheckerFloat64(resources["local_gb_used"]).(float64)
		result.DiskGbFree, _ = utils.TypeCheckerFloat64(resources["free_disk_gb"]).(float64)
		result.DiskGbLeastAvailable, _ = utils.TypeCheckerFloat64(resources["disk_available_least"]).(float64)
		result.MemoryMbTotal, _ = utils.TypeCheckerFloat64(resources["memory_mb"]).(float64)
		result.MemoryMbFree, _ = utils.TypeCheckerFloat64(resources["free_ram_mb"]).(float64)
		result.MemoryMbUsed, _ = utils.TypeCheckerFloat64(resources["memory_mb_used"]).(float64)
		//Api 에서 제공되는 running_vms 는 Total Vms 이다.
		result.VMTotal, _ = utils.TypeCheckerInt(resources["running_vms"]).(int)
	}
	return result, err
}

// GetComputeNodeResources ...
/**
Description : Compute Node Resource Summary
*/
func (n *Nova) GetComputeNodeResources(defaultProjectID string) (result []models.NodeResources, err error) {

	/*provider, err := utils.GetAdminToken(n.OpenstackProvider)
	if err != nil {
		return result, err
	}
	*/
	//client for Compute API operation
	provider := models.GetProvider(n.Username)
	client := models.GetClientCompute(provider)

	response, err := client.Get(fmt.Sprintf("%s/%s/%s/os-hypervisors/detail", client.Endpoint, client.Microversion, defaultProjectID), nil, nil)

	if err != nil {
		return result, err
	}

	msg, err := utils.ResponseUnmarshal(response, err)

	resources := msg["hypervisors"].([]interface{})
	if len(resources) > 0 {

		for _, computeNode := range resources {
			node := computeNode.(map[string]interface{})
			var nodeInfo models.NodeResources
			nodeInfo.ID = utils.TypeCheckerInt(node["id"]).(int)
			nodeInfo.Hostname = utils.TypeCheckerString(node["hypervisor_hostname"]).(string)
			nodeInfo.HostIP = utils.TypeCheckerString(node["host_ip"]).(string)
			nodeInfo.Type = utils.TypeCheckerString(node["hypervisor_type"]).(string)
			nodeInfo.VcpusMax = utils.TypeCheckerInt(node["vcpus"]).(int)
			nodeInfo.VcpusUsed = utils.TypeCheckerInt(node["vcpus_used"]).(int)
			nodeInfo.MemoryMbMax = utils.TypeCheckerInt(node["memory_mb"]).(int)
			nodeInfo.MemoryMbUsed = utils.TypeCheckerInt(node["memory_mb_used"]).(int)
			nodeInfo.MemoryMbFree = utils.TypeCheckerInt(node["free_ram_mb"]).(int)
			nodeInfo.DiskGbMax = utils.TypeCheckerInt(node["local_gb"]).(int)
			nodeInfo.DiskGbUsed = utils.TypeCheckerInt(node["local_gb_used"]).(int)
			nodeInfo.DiskGbFree = utils.TypeCheckerInt(node["free_disk_gb"]).(int)
			nodeInfo.DiskAvailableLeast = utils.TypeCheckerInt(node["disk_available_least"]).(int)
			nodeInfo.TotalVms = utils.TypeCheckerInt(node["running_vms"]).(int)
			nodeInfo.State = utils.TypeCheckerString(node["state"]).(string)
			nodeInfo.Status = utils.TypeCheckerString(node["status"]).(string)

			result = append(result, nodeInfo)
		}
	}
	return result, nil
}

// GetProjectResourcesLimit ...
/**
Description : Get project resources limit metadata(include network)
*/
func (n *Nova) GetProjectResourcesLimit(defaultProjectID, ProjectID string) (result models.ProjectResourcesLimit, err error) {
	var data interface{}
	/*provider, err := utils.GetAdminToken(n.OpenstackProvider)
	if err != nil {
		return result, err
	}*/

	//client for Compute API operation
	provider := models.GetProvider(n.Username)
	client := models.GetClientCompute(provider)

	response, err := client.Get(fmt.Sprintf("%s/%s/%s/os-quota-sets/%s", client.Endpoint, client.Microversion, defaultProjectID, ProjectID), nil, nil)
	if err != nil {
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}

	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})
	resources := msg["quota_set"].(map[string]interface{})
	if len(resources) > 0 {
		result.InstancesLimit = utils.TypeCheckerInt(resources["instances"]).(int)
		result.MemoryMbLimit = utils.TypeCheckerInt(resources["ram"]).(int)
		result.CoresLimit = utils.TypeCheckerInt(resources["cores"]).(int)
		result.ServerGroupsLimit = utils.TypeCheckerInt(resources["server_groups"]).(int)
		result.KeyPairsLimit = utils.TypeCheckerInt(resources["key_pairs"]).(int)
	}
	return result, nil

}

// GetProjectInstancesList ...
func (n *Nova) GetProjectInstancesList(defaultProjectID string, apiRequest models.ProjectReq) (result []models.InstanceInfo, err error) {

	//var instanceList []models.InstanceInfo
	var data interface{}

	/*provider, err := utils.GetAdminToken(n.OpenstackProvider)
	if err != nil {
		return result, err
	}*/

	//client for Compute API operation
	provider := models.GetProvider(n.Username)
	client := models.GetClientCompute(provider)

	var response *http.Response
	var apiError error

	if apiRequest.Marker == "" {

		if apiRequest.HostName == "" {
			response, apiError = client.Get(fmt.Sprintf("%s/%s/%s/servers/detail?all_tenants=0&limit=%s&ProjectID=%s",
				beego.AppConfig.String("nova.target.url"), beego.AppConfig.String("nova.target.version"), defaultProjectID, apiRequest.Limit, apiRequest.ProjectID), nil, nil)
		} else {
			response, apiError = client.Get(fmt.Sprintf("%s/%s/%s/servers/detail?all_tenants=0&limit=%s&hostname=%s&ProjectID=%s",
				beego.AppConfig.String("nova.target.url"), beego.AppConfig.String("nova.target.version"), defaultProjectID, apiRequest.Limit, apiRequest.HostName, apiRequest.ProjectID), nil, nil)
		}

	} else {

		if apiRequest.HostName == "" {
			response, apiError = client.Get(fmt.Sprintf("%s/%s/%s/servers/detail?all_tenants=0&limit=%s&marker=%s&ProjectID=%s",
				beego.AppConfig.String("nova.target.url"), beego.AppConfig.String("nova.target.version"), defaultProjectID, apiRequest.Limit, apiRequest.Marker, apiRequest.ProjectID), nil, nil)
		} else {
			response, apiError = client.Get(fmt.Sprintf("%s/%s/%s/servers/detail?all_tenants=0&limit=%s&marker=%s&hostname=%s&ProjectID=%s",
				beego.AppConfig.String("nova.target.url"), beego.AppConfig.String("nova.target.version"), defaultProjectID, apiRequest.Limit, apiRequest.Marker, apiRequest.HostName, apiRequest.ProjectID), nil, nil)
		}

	}

	//fmt.Printf("%s/%s/%s/server/details/%s?limit=5", models.NovaURL, models.NovaVersion, models.defaultProjectID, ProjectID)
	if apiError != nil {
		fmt.Println("Err :", apiError)
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)

	if err != nil {
		fmt.Println("Err :", err)
		return result, err
	}

	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})

	servers := msg["servers"].([]interface{})

	for _, v := range servers {
		var instanceInfo models.InstanceInfo
		inst := v.(map[string]interface{})

		instanceInfo.Zone = utils.TypeCheckerString(inst["OS-EXT-AZ:availability_zone"]).(string)
		instanceInfo.Name = utils.TypeCheckerString(inst["name"]).(string)
		instanceInfo.InstanceID = utils.TypeCheckerString(inst["id"]).(string)
		instanceInfo.State = utils.TypeCheckerString(inst["status"]).(string)
		instanceInfo.Volumes = inst["os-extended-volumes:volumes_attached"].([]interface{})
		instanceInfo.Fault = utils.TypeCheckerString(inst["fault"]).(string)
		addInfos := inst["addresses"].(map[string]interface{})
		var ipAddress []string
		for _, addrDetail := range addInfos {

			addrList := addrDetail.([]interface{})
			for _, address := range addrList {
				instanceAddr := address.(map[string]interface{})

				ipAddress = append(ipAddress, utils.TypeCheckerString(instanceAddr["addr"]).(string))
			}

		}

		instanceInfo.Address = ipAddress

		result = append(result, instanceInfo)
	}

	return result, nil
}

// GetProjectInstances ...
/**
Description : Get Project's created instance list
*/
func (n *Nova) GetProjectInstances(defaultProjectID, ProjectID string) (result []models.InstanceInfo, err error) {
	//var return_value models.HypervisorResources
	var data interface{}

	/*provider, err := utils.GetAdminToken(n.OpenstackProvider)
	if err != nil {
		return result, err
	}*/

	//client for Compute API operation
	provider := models.GetProvider(n.Username)
	client := models.GetClientCompute(provider)

	response, err := client.Get(fmt.Sprintf("%s/%s/%s/os-simple-tenant-usage/%s", beego.AppConfig.String("nova.target.url"), beego.AppConfig.String("nova.target.version"), defaultProjectID, ProjectID), nil, nil)

	if err != nil {
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})
	mapTenantUsage := msg["tenant_usage"].(map[string]interface{})
	if len(mapTenantUsage) < 1 {
		return result, err
	}
	resources := mapTenantUsage["server_usages"].([]interface{})

	if len(resources) > 0 {
		for _, v := range resources {
			inst := v.(map[string]interface{})
			var instance models.InstanceInfo
			instance.TenantID = utils.TypeCheckerString(inst["tenant_id"]).(string)
			instance.InstanceID = utils.TypeCheckerString(inst["instance_id"]).(string)
			instance.Name = utils.TypeCheckerString(inst["name"]).(string)
			instance.Flavor = utils.TypeCheckerString(inst["flavor"]).(string)
			instance.Vcpus = utils.TypeCheckerFloat64(inst["vcpus"]).(float64)
			instance.DiskGb = utils.TypeCheckerFloat64(inst["local_gb"]).(float64)
			instance.MemoryMb = utils.TypeCheckerFloat64(inst["memory_mb"]).(float64)
			instance.State = utils.TypeCheckerString(inst["state"]).(string)
			instance.StartedAt = utils.TypeCheckerString(inst["started_at"]).(string)
			if len(instance.StartedAt) > 19 {
				instance.StartedAt = instance.StartedAt[0:19]
			}
			instance.EndedAt = utils.TypeCheckerString(inst["ended_at"]).(string)
			if len(instance.EndedAt) > 19 {
				instance.EndedAt = instance.EndedAt[0:19]
			}
			instance.Uptime = utils.TypeCheckerFloat64(inst["uptime"]).(float64)
			result = append(result, instance)
		}
	}
	return result, nil
}

// GetInstanceDetail ...
/**
Description : Nova API Test
*/
func (n *Nova) GetInstanceDetail(instanceID, ProjectID string) (result models.InstanceDetail, err error) {
	var data interface{}

	/*provider, err := utils.GetAdminToken(n.OpenstackProvider)
	if err != nil {
		return result, err
	}*/

	//client for Compute API operation

	provider := models.GetProvider(n.Username)
	client := models.GetClientCompute(provider)

	response, err := client.Get(fmt.Sprintf("%s/%s/%s/servers/%s", client.Endpoint, client.Microversion, ProjectID, instanceID), nil, nil)
	if err != nil {
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})

	instanceInfo := msg["server"].(map[string]interface{})
	//########### Instance Bastic Info ##############
	result.ID = utils.TypeCheckerString(instanceInfo["id"]).(string)
	result.Name = utils.TypeCheckerString(instanceInfo["name"]).(string)
	result.ProcessName = utils.TypeCheckerString(instanceInfo["OS-EXT-SRV-ATTR:instance_name"]).(string)
	result.AvailabilityZone = utils.TypeCheckerString(instanceInfo["OS-EXT-AZ:availability_zone"]).(string)
	result.CreatedDate = utils.TypeCheckerString(instanceInfo["created"]).(string)

	metadataInfo := instanceInfo["metadata"].(map[string]interface{})
	if len(metadataInfo) > 0 {
		result.Deployment.Deployment = utils.TypeCheckerString(metadataInfo["deployment"]).(string)
		result.Deployment.Director = utils.TypeCheckerString(metadataInfo["director"]).(string)
		result.Deployment.Job = utils.TypeCheckerString(metadataInfo["job"]).(string)
		result.Deployment.Name = utils.TypeCheckerString(metadataInfo["name"]).(string)
	}

	addressInfo := instanceInfo["addresses"].(map[string]interface{})
	if len(addressInfo) > 0 {
		instanceNetwork := make([]models.NetworkInfo, len(addressInfo["private_net"].([]interface{})))
		var index int
		for _, network := range addressInfo["private_net"].([]interface{}) {
			instanceNetwork[index].IP = utils.TypeCheckerString(network.(map[string]interface{})["addr"]).(string)
			instanceNetwork[index].Type = utils.TypeCheckerString(network.(map[string]interface{})["OS-EXT-IPS:type"]).(string)
			instanceNetwork[index].MacAddr = utils.TypeCheckerString(network.(map[string]interface{})["mac_addr"]).(string)

			index = index + 1
		}
		result.Network = instanceNetwork
	}

	//########### Instance Image Info ############
	imageInfo := instanceInfo["image"].(map[string]interface{})

	//########### Instacne Flavor Info ###########
	flavorInfo := instanceInfo["flavor"].(map[string]interface{})

	//########### Instance Security Group ########
	securityGroups := instanceInfo["securityGroups"].([]interface{})

	if len(securityGroups) > 0 {
		var sGroups string
		for _, securityGroup := range securityGroups {
			sG := securityGroup.(map[string]interface{})
			sGroups = sGroups + sG["name"].(string) + " "
		}
		result.SecurityGroups = sGroups
	}

	response, err = client.Get(fmt.Sprintf("%s/%s/%s/flavors/%s", models.NovaURL, models.NovaVersion, ProjectID, flavorInfo["id"]), nil, nil)
	if err != nil {
		return result, err
	}
	rawdata, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg = data.(map[string]interface{})

	flavorDetail := msg["flavor"].(map[string]interface{})
	if len(flavorDetail) > 0 {
		result.Flavor.Name = utils.TypeCheckerString(flavorDetail["name"]).(string)
		result.Flavor.Vcpu = utils.TypeCheckerInt(flavorDetail["vcpus"]).(int)
		result.Flavor.Memory = utils.TypeCheckerInt(flavorDetail["ram"]).(int)
		result.Flavor.Disk = utils.TypeCheckerInt(flavorDetail["disk"]).(int)
	}

	response, err = client.Get(fmt.Sprintf("%s/%s/%s/images/%s", models.NovaURL, models.NovaVersion, ProjectID, imageInfo["id"].(string)), nil, nil)
	if err != nil {
		return result, err
	}
	rawdata, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg = data.(map[string]interface{})
	iInfo := msg["image"].(map[string]interface{})

	if len(iInfo) > 0 {
		result.Image.ID = utils.TypeCheckerString(iInfo["id"]).(string)
		result.Image.Name = utils.TypeCheckerString(iInfo["name"]).(string)

		resources := iInfo["metadata"].(map[string]interface{})
		if len(resources) > 0 {
			result.Image.Version = utils.TypeCheckerString(iInfo["version"]).(string)
			result.Image.OsType = utils.TypeCheckerString(iInfo["os_type"]).(string)
			result.Image.OsKind = utils.TypeCheckerString(iInfo["os_distro"]).(string)
			result.Image.HypervisorType = utils.TypeCheckerString(iInfo["hypervisor_type"]).(string)
		}
	}
	return result, err
}
