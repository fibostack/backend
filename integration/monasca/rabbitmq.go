package monasca

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// RabbitMq ...
type RabbitMq struct {
	OpenstackProvider models.OpenstackProvider
}

// GetRabbitMq ...
func GetRabbitMq(openstackProvider models.OpenstackProvider) *RabbitMq {
	return &RabbitMq{
		OpenstackProvider: openstackProvider,
	}
}

// GetRabbitMQOverview ...
/**
Description : Get project Storage Max & Used information
*/
//func (r *RabbitMq) GetRabbitMQOverview() (result map[string]interface{}, err error) {
func (r *RabbitMq) GetRabbitMQOverview() (rabbitmqOverview models.RabbitMQGlobalResource, err error) {
	//var rabbitmqOverview models.RabbitMQGlobalCounts
	client := &http.Client{
		CheckRedirect: func(req *http.Request, _ []*http.Request) error {
			return errors.New("No redirects")
		},
		Timeout: 30 * time.Second,
		Transport: &http.Transport{
			DisableKeepAlives:   true,
			TLSHandshakeTimeout: 10 * time.Second,
		},
	}

	overviewURL := fmt.Sprintf("http://%s:%s@%s:%s/api/overview", r.OpenstackProvider.RabbitmqUser, r.OpenstackProvider.RabbitmqPass, models.RabbitMqIP, models.RabbitMqPort)

	req, err := http.NewRequest("GET", overviewURL, nil)
	if err != nil {
		return rabbitmqOverview, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return rabbitmqOverview, err
	}

	var data interface{}
	rawdata, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return rabbitmqOverview, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})
	resources := msg["object_totals"].(map[string]interface{})
	if len(resources) > 0 {
		rabbitmqOverview.Connections = utils.TypeCheckerInt(resources["connections"]).(int)
		rabbitmqOverview.Channels = utils.TypeCheckerInt(resources["channels"]).(int)
		rabbitmqOverview.Queues = utils.TypeCheckerInt(resources["queues"]).(int)
		rabbitmqOverview.Consumers = utils.TypeCheckerInt(resources["consumers"]).(int)
		rabbitmqOverview.Exchanges = utils.TypeCheckerInt(resources["exchanges"]).(int)
	}

	var rabbitmqNodeResources models.RabbitMQNodeResources
	overviewURL = fmt.Sprintf("http://%s:%s@%s:%s/api/nodes/%s", r.OpenstackProvider.RabbitmqUser, r.OpenstackProvider.RabbitmqPass, models.RabbitMqIP, models.RabbitMqPort, r.OpenstackProvider.RabbitmqTargetNode)

	req, err = http.NewRequest("GET", overviewURL, nil)
	if err != nil {
		fmt.Println("Err Rabbit:", err.Error())
		return rabbitmqOverview, err
	}
	resp, err = client.Do(req)
	if err != nil {
		return rabbitmqOverview, err
	}
	rawdata, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return rabbitmqOverview, err
	}

	var rabbitResource interface{}
	json.Unmarshal(rawdata, &rabbitResource)
	rabbitResourceMap := rabbitResource.(map[string]interface{})

	rabbitmqNodeResources.FileDescriptorLimit = utils.TypeCheckerFloat64(rabbitResourceMap["fd_total"]).(float64)
	rabbitmqNodeResources.FileDescriptorUsed = utils.TypeCheckerFloat64(rabbitResourceMap["fd_used"]).(float64)
	rabbitmqNodeResources.SocketLimit = utils.TypeCheckerFloat64(rabbitResourceMap["sockets_total"]).(float64)
	rabbitmqNodeResources.SocketUsed = utils.TypeCheckerFloat64(rabbitResourceMap["sockets_used"]).(float64)
	rabbitmqNodeResources.ErlangProcLimit = utils.TypeCheckerInt(rabbitResourceMap["proc_total"]).(int)
	rabbitmqNodeResources.ErlangProcUsed = utils.TypeCheckerInt(rabbitResourceMap["proc_used"]).(int)
	rabbitmqNodeResources.MemoryMbLimit = utils.TypeCheckerFloat64(rabbitResourceMap["mem_limit"]).(float64) / 1024 / 1024
	rabbitmqNodeResources.MemoryMbUsed = utils.TypeCheckerFloat64(rabbitResourceMap["mem_used"]).(float64) / 1024 / 1024
	rabbitmqNodeResources.DiskMbLimit = utils.TypeCheckerFloat64(rabbitResourceMap["disk_free_limit"]).(float64) / 1024 / 1024
	rabbitmqNodeResources.DiskMbFree = utils.TypeCheckerFloat64(rabbitResourceMap["disk_free"]).(float64) / 1024 / 1024

	rabbitmqOverview.NodeResources = rabbitmqNodeResources

	return rabbitmqOverview, nil
}
