package monasca

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// Neutron ...
type Neutron struct {
	Username string
}

// GetNeutron ...
func GetNeutron(username string) *Neutron {
	return &Neutron{
		Username: username,
	}
}

// GetProjectNetworkLimit ...
/**
Description : Get project Network Limit metadata
*/
func (n *Neutron) GetProjectNetworkLimit(projectID string) (result models.ProjectNetworkLimit, err error) {

	var data interface{}
	//client for Neutron API operation
	provider := models.GetProvider(n.Username)
	client := models.GetClientNetwork(provider)
	//Neutron Project Network Quota Information
	response, err := client.Get(fmt.Sprintf("%s/%s/quotas/%s", client.Endpoint, client.Microversion, projectID), nil, nil)

	if err != nil {
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})
	resources := msg["quota"].(map[string]interface{})
	if len(resources) > 0 {
		result.RouterLimit = utils.TypeCheckerInt(resources["router"]).(int)
		result.PortLimit = utils.TypeCheckerInt(resources["port"]).(int)
		result.SecurityGroupRuleLimit = utils.TypeCheckerInt(resources["security_group_rule"]).(int)
		result.SecurityGroupLimit = utils.TypeCheckerInt(resources["security_group"]).(int)
		result.FloatingIpsLimit = utils.TypeCheckerInt(resources["FloatingIP"]).(int)
		result.SubnetLimit = utils.TypeCheckerInt(resources["subnet"]).(int)
		result.NetworkLimit = utils.TypeCheckerInt(resources["network"]).(int)
	}
	return result, nil
}

// GetProjectSecurityGroups ...
/**
Description : Get project Generated Security Groups Information - Only return number of security groups.
*/
func (n *Neutron) GetProjectSecurityGroups(projectID string) (result int, err error) {

	var data interface{}
	//client for Neutron API operation
	provider := models.GetProvider(n.Username)
	client := models.GetClientNetwork(provider)

	//Neutron Project Floating IPs Information
	response, err := client.Get(fmt.Sprintf("%s/%s/security-groups.json?tenant_id=%s", client.Endpoint, client.Microversion, projectID), nil, nil)
	if err != nil {
		return 0, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})
	resources := msg["security_groups"].([]interface{})
	/**
	It's complicated response. If you need details of security groups, parse response.
	Monitoring system just needs number of security groups .
	*/

	return len(resources), nil
}

// GetProjectFloatingIPs ...
/**
Description : Get project Generated Floating IPs Information
*/
func (n *Neutron) GetProjectFloatingIPs(projectID string) (result []models.FloatingIPInfo, err error) {

	var data interface{}
	//client for Neutron API operation
	provider := models.GetProvider(n.Username)
	client := models.GetClientNetwork(provider)

	//Neutron Project Floating IPs Information
	response, err := client.Get(fmt.Sprintf("%s/%s/FloatingIPs.json?tenant_id=%s", client.Endpoint, client.Microversion, projectID), nil, nil)
	if err != nil {
		return result, err
	}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})
	resources := msg["FloatingIPs"].([]interface{})
	if len(resources) > 0 {
		for _, v := range resources {
			FloatingIP := v.(map[string]interface{})
			var floatingInfo models.FloatingIPInfo
			floatingInfo.RouterID = utils.TypeCheckerString(FloatingIP["router_id"]).(string)
			floatingInfo.TenantID = utils.TypeCheckerString(FloatingIP["tenant_id"]).(string)
			floatingInfo.FloatingNetworkID = utils.TypeCheckerString(FloatingIP["floating_network_id"]).(string)
			floatingInfo.InnerIP = utils.TypeCheckerString(FloatingIP["fixed_ip_address"]).(string)
			floatingInfo.FloatingIP = utils.TypeCheckerString(FloatingIP["floating_ip_address"]).(string)
			floatingInfo.PortID = utils.TypeCheckerString(FloatingIP["port_id"]).(string)
			floatingInfo.Status = utils.TypeCheckerString(FloatingIP["status"]).(string)
			floatingInfo.Description = utils.TypeCheckerString(FloatingIP["description"]).(string)
			result = append(result, floatingInfo)
		}
	}
	return result, nil
}
