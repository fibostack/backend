package monasca

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/identity/v3/tokens"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// Keystone ...
type Keystone struct {
	Username string
}

// GetKeystone ...
func GetKeystone(username string) *Keystone {
	return &Keystone{
		Username: username,
	}
}

// GetProjectList ...
/**
Description : Get openstack's registered project lists
*/
func (c *Keystone) GetProjectList() (projectLists []models.ProjectInfo, err error) {

	//client for Keystone API operation
	provider := models.GetProvider(c.Username)
	client := models.GetClientIdentity(provider,  c.Username)
	response, err := client.Get(fmt.Sprintf("%s/%s/projects", client.Endpoint, client.Microversion), nil, nil)

	msg, err := utils.ResponseUnmarshal(response, err)

	for _, v := range msg["projects"].([]interface{}) {
		var projectInfo models.ProjectInfo
		vDetail := v.(map[string]interface{})
		projectInfo.ID = utils.TypeCheckerString(vDetail["id"]).(string)
		projectInfo.Name = utils.TypeCheckerString(vDetail["name"]).(string)
		projectInfo.IsDomain = vDetail["is_domain"].(bool)
		projectInfo.Description = utils.TypeCheckerString(vDetail["description"]).(string)
		projectInfo.Enabled = vDetail["enabled"].(bool)
		projectInfo.ParentID = utils.TypeCheckerString(vDetail["parent_id"]).(string)
		projectInfo.Links = vDetail["links"].(map[string]interface{})

		projectLists = append(projectLists, projectInfo)
	}
	return projectLists, nil
}

// GetuserIDByName ...
func (c *Keystone) GetuserIDByName() (userID string, err error) {

	/*provider, err := utils.GetAdminToken(k.OpenstackProvider)
	if err != nil {
		return userID, err
	}*/
	//client for Keystone API operation
	provider := models.GetProvider(c.Username)
	client := models.GetClientIdentity(provider,  c.Username)

	response, err := client.Get(fmt.Sprintf("%s/%s/users?name=%s", client.Endpoint, client.Microversion, c.Username), nil, nil)
	msg, err := utils.ResponseUnmarshal(response, err)

	for _, v := range msg["users"].([]interface{}) {
		vDetail := v.(map[string]interface{})
		userID = utils.TypeCheckerString(vDetail["id"]).(string)
	}
	return userID, err
}

// GetUserProjectList ...
func (c *Keystone) GetUserProjectList(userID string) (projectLists []models.ProjectInfo, err error) {

	provider := models.GetProvider(c.Username)
	client := models.GetClientIdentity(provider,  c.Username)

	response, err := client.Get(fmt.Sprintf("%s/%s/users/%s/projects", client.Endpoint, client.Microversion, userID), nil, nil)
	msg, err := utils.ResponseUnmarshal(response, err)

	for _, v := range msg["projects"].([]interface{}) {
		var projectInfo models.ProjectInfo
		vDetail := v.(map[string]interface{})
		projectInfo.ID = utils.TypeCheckerString(vDetail["id"]).(string)
		projectInfo.Name = utils.TypeCheckerString(vDetail["name"]).(string)
		projectInfo.IsDomain = vDetail["is_domain"].(bool)
		projectInfo.Description = utils.TypeCheckerString(vDetail["description"]).(string)
		projectInfo.Enabled = vDetail["enabled"].(bool)
		projectInfo.ParentID = utils.TypeCheckerString(vDetail["parent_id"]).(string)
		projectInfo.Links = vDetail["links"].(map[string]interface{})

		projectLists = append(projectLists, projectInfo)
	}
	return projectLists, nil
}

// RevokeToken ...
func (c *Keystone) RevokeToken() tokens.RevokeResult {
	provider := models.GetProvider(c.Username)
	client := models.GetClientIdentity(provider,  c.Username)
	result := tokens.Revoke(client, client.TokenID)

	return result
}
