package networking

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/subnets"
	"gitlab.com/fibo-stack/backend/models"
)

// ListSubnets ...
func (c *Networking) ListSubnets(networkID string) ([]subnets.Subnet, error) {
	provider := models.GetProvider(c.Username)
	listOpts := subnets.ListOpts{
		IPVersion: 4,
		NetworkID: networkID,
	}

	allPages, err := subnets.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allSubnets, err := subnets.ExtractSubnets(allPages)
	if err != nil {
		return nil, err
	}
	return allSubnets, err
}

// CreateSubnet ...
func (c *Networking) CreateSubnet(name, gatewayIP, networkID, cidr, desc string, allocationPools []subnets.AllocationPool, dnsNameservers []string, hostRoutes []subnets.HostRoute) (*subnets.Subnet, error) {
	provider := models.GetProvider(c.Username)
	createOpts := subnets.CreateOpts{
		Name:            name,
		NetworkID:       networkID,
		IPVersion:       4,
		CIDR:            cidr,
		GatewayIP:       &gatewayIP,
		AllocationPools: allocationPools,
		DNSNameservers:  dnsNameservers,
		HostRoutes:      hostRoutes,
		EnableDHCP:      gophercloud.Enabled,
		Description:     desc,
	}

	subnet, err := subnets.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return subnet, err
}

// UpdateSubnet ...
func (c *Networking) UpdateSubnet(subnetID, name, desc, gatewayIP string, allocationPools []subnets.AllocationPool, hostRoutes []subnets.HostRoute, dnsNameservers []string, enableDHCP bool) (*subnets.Subnet, error) {
	provider := models.GetProvider(c.Username)

	updateOpts := subnets.UpdateOpts{
		Name:            &name,
		Description:     &desc,
		DNSNameservers:  &dnsNameservers,
		AllocationPools: allocationPools,
		EnableDHCP:      &enableDHCP,
		GatewayIP:       &gatewayIP,
		HostRoutes:      &hostRoutes,
	}
	subnet, err := subnets.Update(models.GetClientNetwork(provider), subnetID, updateOpts).Extract()
	return subnet, err
}

// RemoveGatewayFromSubnet ...
func (c *Networking) RemoveGatewayFromSubnet(subnetID string) (*subnets.Subnet, error) {
	provider := models.GetProvider(c.Username)
	var noGateway = ""

	updateOpts := subnets.UpdateOpts{
		GatewayIP: &noGateway,
	}

	subnet, err := subnets.Update(models.GetClientNetwork(provider), subnetID, updateOpts).Extract()
	return subnet, err
}

// DeleteSubnet ...
func (c *Networking) DeleteSubnet(subnetID string) error {
	provider := models.GetProvider(c.Username)
	err := subnets.Delete(models.GetClientNetwork(provider), subnetID).ExtractErr()
	return err
}

// GetSubnet ...
func (c *Networking) GetSubnet(subnetID string) (subnet *subnets.Subnet, err error) {
	provider := models.GetProvider(c.Username)
	subnet, err = subnets.Get(models.GetClientNetwork(provider), subnetID).Extract()
	return
}
