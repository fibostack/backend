package networking

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/addressscopes"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/addressscopes

// ListAddressScopes ...
func (c *Networking) ListAddressScopes() ([]addressscopes.AddressScope, error) {
	provider := models.GetProvider(c.Username)
	listOpts := addressscopes.ListOpts{
		IPVersion: 4,
	}

	allPages, err := addressscopes.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allAddressScopes, err := addressscopes.ExtractAddressScopes(allPages)
	if err != nil {
		return nil, err
	}

	for _, addressScope := range allAddressScopes {
		fmt.Printf("%+v\n", addressScope)
	}
	return allAddressScopes, err
}

// GetAddressScope ...
func (c *Networking) GetAddressScope(addressScopeID string) (*addressscopes.AddressScope, error) {
	provider := models.GetProvider(c.Username)
	addressScope, err := addressscopes.Get(models.GetClientNetwork(provider), addressScopeID).Extract()
	return addressScope, err
}

// CreateAddressScope ...
func (c *Networking) CreateAddressScope(name string, ipVersion int) (*addressscopes.AddressScope, error) {
	provider := models.GetProvider(c.Username)
	addressScopeOpts := addressscopes.CreateOpts{
		Name:      name,
		IPVersion: ipVersion,
	}
	addressScope, err := addressscopes.Create(models.GetClientNetwork(provider), addressScopeOpts).Extract()
	return addressScope, err
}

// UpdateAddressScope ...
func (c *Networking) UpdateAddressScope(addressScopeID, newName string) (*addressscopes.AddressScope, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := addressscopes.UpdateOpts{
		Name: &newName,
	}

	addressScope, err := addressscopes.Update(models.GetClientNetwork(provider), addressScopeID, updateOpts).Extract()
	return addressScope, err
}

// DeleteAddressScope ...
func (c *Networking) DeleteAddressScope(addressScopeID string) error {
	provider := models.GetProvider(c.Username)
	err := addressscopes.Delete(models.GetClientNetwork(provider), addressScopeID).ExtractErr()
	return err
}
