package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/subnetpools"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/subnetpools

// ListSubnetpools ...
func (c *Networking) ListSubnetpools() ([]subnetpools.SubnetPool, error) {
	provider := models.GetProvider(c.Username)
	listOpts := subnetpools.ListOpts{
		IPVersion: 4,
	}

	allPages, err := subnetpools.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allSubnetpools, err := subnetpools.ExtractSubnetPools(allPages)
	if err != nil {
		return nil, err
	}

	return allSubnetpools, err
}

// GetSubnetpool ...
func (c *Networking) GetSubnetpool(subnetPoolID string) (*subnetpools.SubnetPool, error) {
	provider := models.GetProvider(c.Username)
	subnetPool, err := subnetpools.Get(models.GetClientNetwork(provider), subnetPoolID).Extract()
	return subnetPool, err
}

// CreateSubnetpool ...
func (c *Networking) CreateSubnetpool(subnetPoolName string, subnetPoolPrefixes []string) (*subnetpools.SubnetPool, error) {
	provider := models.GetProvider(c.Username)

	subnetPoolOpts := subnetpools.CreateOpts{
		Name:     subnetPoolName,
		Prefixes: subnetPoolPrefixes,
	}
	subnetPool, err := subnetpools.Create(models.GetClientNetwork(provider), subnetPoolOpts).Extract()
	return subnetPool, err
}

// UpdateSubnetpool ...
func (c *Networking) UpdateSubnetpool(subnetPoolID string, prefixes []string, maxPrefix int) (*subnetpools.SubnetPool, error) {
	provider := models.GetProvider(c.Username)
	// subnetPoolID := "099546ca-788d-41e5-a76d-17d8cd282d3e"
	updateOpts := subnetpools.UpdateOpts{
		Prefixes:     prefixes,
		MaxPrefixLen: maxPrefix,
	}

	subnetPool, err := subnetpools.Update(models.GetClientNetwork(provider), subnetPoolID, updateOpts).Extract()
	return subnetPool, err
}

// DeleteSubnetpool ...
func (c *Networking) DeleteSubnetpool(subnetPoolID string) error {
	provider := models.GetProvider(c.Username)
	err := subnetpools.Delete(models.GetClientNetwork(provider), subnetPoolID).ExtractErr()
	return err
}
