package networking

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/firewalls"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/routerinsertion"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/routerinsertion

// FirewallsWithRouters ...
type FirewallsWithRouters struct {
	firewalls.Firewall
	routerinsertion.FirewallExt
}

// ListFirewallsWithRouter ...
func (c *Networking) ListFirewallsWithRouter() ([]FirewallsWithRouters, error) {
	provider := models.GetProvider(c.Username)
	var allFirewalls []FirewallsWithRouters

	allPages, err := firewalls.List(models.GetClientNetwork(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	err = firewalls.ExtractFirewallsInto(allPages, &allFirewalls)
	if err != nil {
		return nil, err
	}

	for _, fw := range allFirewalls {
		fmt.Printf("%+v\n", fw)
	}
	return allFirewalls, err
}

// CreateFirewallWithRouter ...
func (c *Networking) CreateFirewallWithRouter(name, policyID string, routerIDs []string) (*firewalls.Firewall, error) {
	provider := models.GetProvider(c.Username)
	firewallCreateOpts := firewalls.CreateOpts{
		Name:     name,
		PolicyID: policyID,
	}

	createOpts := routerinsertion.CreateOptsExt{
		CreateOptsBuilder: firewallCreateOpts,
		RouterIDs:         routerIDs,
	}

	firewall, err := firewalls.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return firewall, err
}

// UpdateFirewallWithRouter ...
func (c *Networking) UpdateFirewallWithRouter(firewallID, description, policyID string, routerIDs []string) (*firewalls.Firewall, error) {
	provider := models.GetProvider(c.Username)
	firewallUpdateOpts := firewalls.UpdateOpts{
		Description: &description,
		PolicyID:    policyID,
	}

	updateOpts := routerinsertion.UpdateOptsExt{
		UpdateOptsBuilder: firewallUpdateOpts,
		RouterIDs:         routerIDs,
	}

	firewall, err := firewalls.Update(models.GetClientNetwork(provider), firewallID, updateOpts).Extract()
	return firewall, err
}
