package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/ikepolicies"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/ikepolicies

// CreateIKEPolicy ...
func (c *Networking) CreateIKEPolicy(name, description string, encryptionAlgorithm ikepolicies.EncryptionAlgorithm, pfs ikepolicies.PFS) (*ikepolicies.Policy, error) {
	provider := models.GetProvider(c.Username)
	createOpts := ikepolicies.CreateOpts{
		Name:                name,
		Description:         description,
		EncryptionAlgorithm: encryptionAlgorithm,
		PFS:                 pfs,
	}

	policy, err := ikepolicies.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return policy, err
}

// DeleteIKEPolicy ...
func (c *Networking) DeleteIKEPolicy(ikePolicyID string) error {
	provider := models.GetProvider(c.Username)
	err := ikepolicies.Delete(models.GetClientNetwork(provider), ikePolicyID).ExtractErr()
	return err
}

// UpdateIKEPolicy ...
func (c *Networking) UpdateIKEPolicy(ikePolicyID, name, description string) (*ikepolicies.Policy, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := ikepolicies.UpdateOpts{
		Name:        &name,
		Description: &description,
		Lifetime: &ikepolicies.LifetimeUpdateOpts{
			Value: 7000,
		},
	}
	updatedPolicy, err := ikepolicies.Update(models.GetClientNetwork(provider), ikePolicyID, updateOpts).Extract()
	return updatedPolicy, err
}

// ListIKEPolicies ...
func (c *Networking) ListIKEPolicies() ([]ikepolicies.Policy, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := ikepolicies.List(models.GetClientNetwork(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	allPolicies, err := ikepolicies.ExtractPolicies(allPages)
	if err != nil {
		return nil, err
	}

	return allPolicies, err
}
