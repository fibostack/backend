package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/security/rules"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/security/rules

// ListSecGroupRules ...
func (c *Networking) ListSecGroupRules(secgroupID string) ([]rules.SecGroupRule, error) {
	provider := models.GetProvider(c.Username)
	listOpts := rules.ListOpts{
		SecGroupID: secgroupID,
	}

	allPages, err := rules.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRules, err := rules.ExtractRules(allPages)
	if err != nil {
		return nil, err
	}

	return allRules, err
}

// CreateSecGroupRule ...
func (c *Networking) CreateSecGroupRule(direction, remoteGroupID, secGroupID, desc string, portRangeMin, portRangeMax int, protocol string) (*rules.SecGroupRule, error) {
	provider := models.GetProvider(c.Username)
	var createOpts rules.CreateOpts
	var prot rules.RuleProtocol
	switch protocol {
	case "ah":
		prot = rules.ProtocolAH
	case "dccp":
		prot = rules.ProtocolDCCP
	case "egp":
		prot = rules.ProtocolEGP
	case "esp":
		prot = rules.ProtocolESP
	case "gre":
		prot = rules.ProtocolGRE
	case "icmp":
		prot = rules.ProtocolICMP
	case "igmp":
		prot = rules.ProtocolIGMP
	case "ipv6-encap":
		prot = rules.ProtocolIPv6Encap
	case "ipv6-frag":
		prot = rules.ProtocolIPv6Frag
	case "ipv6-icmp":
		prot = rules.ProtocolIPv6ICMP
	case "ipv6-nonxt":
		prot = rules.ProtocolIPv6NoNxt
	case "ipv6-opts":
		prot = rules.ProtocolIPv6Opts
	case "ipv6-route":
		prot = rules.ProtocolIPv6Route
	case "ospf":
		prot = rules.ProtocolOSPF
	case "pgm":
		prot = rules.ProtocolPGM
	case "rsvp":
		prot = rules.ProtocolRSVP
	case "sctp":
		prot = rules.ProtocolSCTP
	case "tcp":
		prot = rules.ProtocolTCP
	case "udp":
		prot = rules.ProtocolUDP
	case "udplite":
		prot = rules.ProtocolUDPLite
	case "vrrp":
		prot = rules.ProtocolVRRP
	default:
		prot = rules.ProtocolICMP
	}
	if direction == "ingress" {
		createOpts = rules.CreateOpts{
			Direction:     rules.DirIngress,
			PortRangeMin:  portRangeMin,
			EtherType:     rules.EtherType4,
			PortRangeMax:  portRangeMax,
			Protocol:      prot,
			RemoteGroupID: remoteGroupID,
			SecGroupID:    secGroupID,
			Description:   desc,
		}
	} else {
		createOpts = rules.CreateOpts{
			Direction:     rules.DirEgress,
			PortRangeMin:  portRangeMin,
			EtherType:     rules.EtherType4,
			PortRangeMax:  portRangeMax,
			Protocol:      prot,
			RemoteGroupID: remoteGroupID,
			SecGroupID:    secGroupID,
			Description:   desc,
		}
	}

	rule, err := rules.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return rule, err
}

// DeleteSecGroupRule ...
func (c *Networking) DeleteSecGroupRule(ruleID string) error {
	provider := models.GetProvider(c.Username)
	err := rules.Delete(models.GetClientNetwork(provider), ruleID).ExtractErr()
	return err
}
