package networking

import (
	"fmt"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/firewalls"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/firewalls

// ListFirewalls ...
func (c *Networking) ListFirewalls(projectID string) ([]firewalls.Firewall, error) {
	provider := models.GetProvider(c.Username)
	listOpts := firewalls.ListOpts{
		TenantID: projectID,
	}

	allPages, err := firewalls.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allFirewalls, err := firewalls.ExtractFirewalls(allPages)
	if err != nil {
		return nil, err
	}

	for _, fw := range allFirewalls {
		fmt.Printf("%+v\n", fw)
	}
	return allFirewalls, err
}

// CreateFirewall ...
func (c *Networking) CreateFirewall(name, description, policyID string) (*firewalls.Firewall, error) {
	provider := models.GetProvider(c.Username)
	createOpts := firewalls.CreateOpts{
		Name:         name,
		Description:  description,
		PolicyID:     policyID,
		AdminStateUp: gophercloud.Enabled,
	}

	firewall, err := firewalls.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return firewall, err
}

// UpdateFirewall ...
func (c *Networking) UpdateFirewall(firewallID string) (*firewalls.Firewall, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := firewalls.UpdateOpts{
		AdminStateUp: gophercloud.Disabled,
	}

	firewall, err := firewalls.Update(models.GetClientNetwork(provider), firewallID, updateOpts).Extract()
	return firewall, err
}

// DeleteFirewall ...
func (c *Networking) DeleteFirewall(firewallID string) error {
	provider := models.GetProvider(c.Username)
	err := firewalls.Delete(models.GetClientNetwork(provider), firewallID).ExtractErr()
	return err
}

// GetFirewall ...
func (c *Networking) GetFirewall(firewallID string) (*firewalls.Firewall, error) {
	provider := models.GetProvider(c.Username)
	firewall, err := firewalls.Get(models.GetClientNetwork(provider), firewallID).Extract()
	return firewall, err
}
