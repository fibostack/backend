package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/services"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/services

// ListVPNServices ...
func (c *Networking) ListVPNServices(tenantID string) ([]services.Service, error) {
	provider := models.GetProvider(c.Username)
	listOpts := services.ListOpts{
		TenantID: tenantID,
	}

	allPages, err := services.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allServices, err := services.ExtractServices(allPages)
	if err != nil {
		return nil, err
	}

	return allServices, err
}

// CreateVPNService ...
func (c *Networking) CreateVPNService(name, description, routerID string, adminState bool) (*services.Service, error) {
	provider := models.GetProvider(c.Username)
	createOpts := services.CreateOpts{
		Name:         name,
		Description:  description,
		RouterID:     routerID,
		AdminStateUp: &adminState,
	}

	service, err := services.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return service, err
}

// UpdateVPNService ...
func (c *Networking) UpdateVPNService(serviceID, newName, description string) (*services.Service, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := services.UpdateOpts{
		Name:        &newName,
		Description: &description,
	}

	service, err := services.Update(models.GetClientNetwork(provider), serviceID, updateOpts).Extract()
	return service, err
}

// DeleteVPNService ...
func (c *Networking) DeleteVPNService(serviceID string) error {
	provider := models.GetProvider(c.Username)
	err := services.Delete(models.GetClientNetwork(provider), serviceID).ExtractErr()
	return err
}

// GetVPNService ...
func (c *Networking) GetVPNService(serviceID string) (*services.Service, error) {
	provider := models.GetProvider(c.Username)
	service, err := services.Get(models.GetClientNetwork(provider), serviceID).Extract()
	return service, err
}
