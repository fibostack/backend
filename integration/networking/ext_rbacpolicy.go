package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/rbacpolicies"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/rbacpolicies

// CreateRBACPolicy ...
func (c *Networking) CreateRBACPolicy(objectType, targetTenant, objectID string) (*rbacpolicies.RBACPolicy, error) {
	provider := models.GetProvider(c.Username)
	createOpts := rbacpolicies.CreateOpts{
		Action:       rbacpolicies.ActionAccessShared,
		ObjectType:   objectType,
		TargetTenant: targetTenant,
		ObjectID:     objectID,
	}

	rbacPolicy, err := rbacpolicies.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return rbacPolicy, err
}

// ListRBACPolicies ...
func (c *Networking) ListRBACPolicies(tenantID string) ([]rbacpolicies.RBACPolicy, error) {
	provider := models.GetProvider(c.Username)
	listOpts := rbacpolicies.ListOpts{
		TenantID: tenantID,
	}

	allPages, err := rbacpolicies.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRBACPolicies, err := rbacpolicies.ExtractRBACPolicies(allPages)
	if err != nil {
		return nil, err
	}

	return allRBACPolicies, err
}

// DeleteRBACPolicy ...
func (c *Networking) DeleteRBACPolicy(rbacPolicyID string) error {
	provider := models.GetProvider(c.Username)
	err := rbacpolicies.Delete(models.GetClientNetwork(provider), rbacPolicyID).ExtractErr()
	return err
}

// GetRBACPolicy ...
func (c *Networking) GetRBACPolicy(rbacPolicyID string) (*rbacpolicies.RBACPolicy, error) {
	provider := models.GetProvider(c.Username)
	rbacpolicy, err := rbacpolicies.Get(models.GetClientNetwork(provider), rbacPolicyID).Extract()
	return rbacpolicy, err
}

// UpdatRBACPolicy ...
func (c *Networking) UpdatRBACPolicy(rbacPolicyID, targetTenant string) (*rbacpolicies.RBACPolicy, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := rbacpolicies.UpdateOpts{
		TargetTenant: targetTenant,
	}
	rbacPolicy, err := rbacpolicies.Update(models.GetClientNetwork(provider), rbacPolicyID, updateOpts).Extract()
	return rbacPolicy, err
}
