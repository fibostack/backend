package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/siteconnections"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/siteconnections

// CreateIPSecSiteconnection ...
func (c *Networking) CreateIPSecSiteconnection(name, psk, ipSecPolicyID, peerEPGroupID, ikePolicID, vpnServiceID, localIPGroupID, peerAddress, peerID string, mtu int, initiator siteconnections.Initiator, adminState bool) (*siteconnections.Connection, error) {
	provider := models.GetProvider(c.Username)
	createOpts := siteconnections.CreateOpts{
		Name:           name,
		PSK:            psk,
		Initiator:      initiator,
		AdminStateUp:   &adminState,
		IPSecPolicyID:  ipSecPolicyID,
		PeerEPGroupID:  peerEPGroupID,
		IKEPolicyID:    ikePolicID,
		VPNServiceID:   vpnServiceID,
		LocalEPGroupID: localIPGroupID,
		PeerAddress:    peerAddress,
		PeerID:         peerID,
		MTU:            mtu,
	}
	connection, err := siteconnections.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return connection, err
}

// GetIPSecSiteconnection ...
func (c *Networking) GetIPSecSiteconnection(connID string) (*siteconnections.Connection, error) {
	provider := models.GetProvider(c.Username)
	conn, err := siteconnections.Get(models.GetClientNetwork(provider), connID).Extract()
	return conn, err
}

// DeleteIPSecSiteconnection ...
func (c *Networking) DeleteIPSecSiteconnection(connID string) error {
	provider := models.GetProvider(c.Username)
	err := siteconnections.Delete(models.GetClientNetwork(provider), connID).ExtractErr()
	return err
}

// ListIPSecSiteconnections ...
func (c *Networking) ListIPSecSiteconnections() ([]siteconnections.Connection, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := siteconnections.List(models.GetClientNetwork(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	allConnections, err := siteconnections.ExtractConnections(allPages)
	if err != nil {
		return nil, err
	}
	return allConnections, err
}

// UpdateIPSecSiteConnection ...
func (c *Networking) UpdateIPSecSiteConnection(connID, name, description string) (*siteconnections.Connection, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := siteconnections.UpdateOpts{
		Name:        &name,
		Description: &description,
	}
	updatedConnection, err := siteconnections.Update(models.GetClientNetwork(provider), connID, updateOpts).Extract()
	return updatedConnection, err
}
