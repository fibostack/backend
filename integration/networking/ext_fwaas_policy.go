package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/policies"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/policies

// ListPolicies ...
func (c *Networking) ListPolicies(tenantID string) ([]policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	listOpts := policies.ListOpts{
		TenantID: tenantID,
	}

	allPages, err := policies.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allPolicies, err := policies.ExtractPolicies(allPages)
	if err != nil {
		return nil, err
	}
	return allPolicies, err
}

// CreatePolicy ...
func (c *Networking) CreatePolicy(name, description string, rules []string) (*policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	createOpts := policies.CreateOpts{
		Name:        name,
		Description: description,
		Rules:       rules,
	}

	policy, err := policies.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return policy, err
}

// UpdatePolicy ...
func (c *Networking) UpdatePolicy(policyID, description string) (*policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := policies.UpdateOpts{
		Description: &description,
	}

	policy, err := policies.Update(models.GetClientNetwork(provider), policyID, updateOpts).Extract()

	return policy, err
}

// DeletePolicy ...
func (c *Networking) DeletePolicy(policyID string) error {
	provider := models.GetProvider(c.Username)
	err := policies.Delete(models.GetClientNetwork(provider), policyID).ExtractErr()
	return err
}

// AddRuleToPolicy ...
func (c *Networking) AddRuleToPolicy(policyID, ruleID string) (*policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	ruleOpts := policies.InsertRuleOpts{
		ID: ruleID,
	}

	policy, err := policies.AddRule(models.GetClientNetwork(provider), policyID, ruleOpts).Extract()
	return policy, err
}

// DeleteRuleFromPolicy ...
func (c *Networking) DeleteRuleFromPolicy(policyID, ruleID string) (*policies.Policy, error) {
	provider := models.GetProvider(c.Username)
	policy, err := policies.RemoveRule(models.GetClientNetwork(provider), policyID, ruleID).Extract()

	return policy, err
}
