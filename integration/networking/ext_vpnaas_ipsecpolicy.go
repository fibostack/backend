package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/ipsecpolicies"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/ipsecpolicies

// CreateIPSecPolicy ...
func (c *Networking) CreateIPSecPolicy(name string) (*ipsecpolicies.Policy, error) {
	provider := models.GetProvider(c.Username)
	createOpts := ipsecpolicies.CreateOpts{
		Name: name,
	}

	policy, err := ipsecpolicies.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return policy, err
}

// DeleteIPSecPolicy ...
func (c *Networking) DeleteIPSecPolicy(ipSecPolicyID string) error {
	provider := models.GetProvider(c.Username)
	err := ipsecpolicies.Delete(models.GetClientNetwork(provider), ipSecPolicyID).ExtractErr()
	return err
}

// GetIPSecPolicy ...
func (c *Networking) GetIPSecPolicy(ipSecPolicyID string) (*ipsecpolicies.Policy, error) {
	provider := models.GetProvider(c.Username)
	policy, err := ipsecpolicies.Get(models.GetClientNetwork(provider), ipSecPolicyID).Extract()
	return policy, err
}

// UpdateIPSecPolicy ...
func (c *Networking) UpdateIPSecPolicy(ipSecPolicyID, name, description string) (*ipsecpolicies.Policy, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := ipsecpolicies.UpdateOpts{
		Name:        &name,
		Description: &description,
	}
	updatedPolicy, err := ipsecpolicies.Update(models.GetClientNetwork(provider), ipSecPolicyID, updateOpts).Extract()
	return updatedPolicy, err
}

// ListIPSecPolicies ...
func (c *Networking) ListIPSecPolicies() ([]ipsecpolicies.Policy, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := ipsecpolicies.List(models.GetClientNetwork(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	allPolicies, err := ipsecpolicies.ExtractPolicies(allPages)
	if err != nil {
		return nil, err
	}
	return allPolicies, err
}
