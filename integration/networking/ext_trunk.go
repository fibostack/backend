package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/trunks"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/trunks

// CreateTrunk ...
func (c *Networking) CreateTrunk(name, description, portID string, adminState bool) (*trunks.Trunk, error) {
	provider := models.GetProvider(c.Username)
	createOpts := trunks.CreateOpts{
		Name:         name,
		Description:  description,
		AdminStateUp: &adminState,
		PortID:       portID,
	}

	trunk, err := trunks.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return trunk, err
}

// CreateTrunkWith2subports ...
func (c *Networking) CreateTrunkWith2subports(name, description, portID string, adminState bool, segID1, segID2 int, segType1, segType2, portID1, portID2 string) (*trunks.Trunk, error) {
	provider := models.GetProvider(c.Username)
	createOpts := trunks.CreateOpts{
		Name:         name,
		Description:  description,
		AdminStateUp: &adminState,
		PortID:       portID,
		Subports: []trunks.Subport{
			{
				SegmentationID:   segID1,
				SegmentationType: segType1,
				PortID:           portID1,
			},
			{
				SegmentationID:   segID2,
				SegmentationType: segType2,
				PortID:           portID2,
			},
		},
	}

	trunk, err := trunks.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return trunk, err
}

// DeleteTrunk ...
func (c *Networking) DeleteTrunk(trunkID string) error {
	provider := models.GetProvider(c.Username)
	err := trunks.Delete(models.GetClientNetwork(provider), trunkID).ExtractErr()
	return err
}

// ListTrunks ...
func (c *Networking) ListTrunks() ([]trunks.Trunk, error) {
	provider := models.GetProvider(c.Username)
	listOpts := trunks.ListOpts{}
	allPages, err := trunks.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}
	allTrunks, err := trunks.ExtractTrunks(allPages)
	if err != nil {
		return nil, err
	}

	return allTrunks, err
}

// GetTrunk ...
func (c *Networking) GetTrunk(trunkID string) (*trunks.Trunk, error) {
	provider := models.GetProvider(c.Username)
	trunk, err := trunks.Get(models.GetClientNetwork(provider), trunkID).Extract()
	return trunk, err
}

// UpdateTrunk ...
func (c *Networking) UpdateTrunk(trunkID, newName, description string, adminState bool) (*trunks.Trunk, error) {
	provider := models.GetProvider(c.Username)
	// subports, err := trunks.GetSubports(models.GetClientNetwork(provider), trunkID).Extract()
	updateOpts := trunks.UpdateOpts{
		AdminStateUp: &adminState,
		Name:         &newName,
		Description:  &description,
	}
	trunk, err := trunks.Update(models.GetClientNetwork(provider), trunkID, updateOpts).Extract()
	return trunk, err
}

// GetSubportOfTrunk ...
func (c *Networking) GetSubportOfTrunk(trunkID string) ([]trunks.Subport, error) {
	provider := models.GetProvider(c.Username)
	subports, err := trunks.GetSubports(models.GetClientNetwork(provider), trunkID).Extract()
	return subports, err
}

// Add2SubportsToTrunk ...
func (c *Networking) Add2SubportsToTrunk(trunkID string, segID1, segID2 int, segType1, segType2, portID1, portID2 string) (*trunks.Trunk, error) {
	provider := models.GetProvider(c.Username)
	addSubportsOpts := trunks.AddSubportsOpts{
		Subports: []trunks.Subport{
			{
				SegmentationID:   segID1,
				SegmentationType: segType1,
				PortID:           portID1,
			},
			{
				SegmentationID:   segID2,
				SegmentationType: segType2,
				PortID:           portID2,
			},
		},
	}
	trunk, err := trunks.AddSubports(models.GetClientNetwork(provider), trunkID, addSubportsOpts).Extract()
	return trunk, err
}

// DeleteSubportsFromTrunk ...
func (c *Networking) DeleteSubportsFromTrunk(trunkID, portID1, portID2 string) (*trunks.Trunk, error) {
	provider := models.GetProvider(c.Username)
	removeSubportsOpts := trunks.RemoveSubportsOpts{
		Subports: []trunks.RemoveSubport{
			{PortID: portID1},
			{PortID: portID2},
		},
	}
	trunk, err := trunks.RemoveSubports(models.GetClientNetwork(provider), trunkID, removeSubportsOpts).Extract()
	return trunk, err
}
