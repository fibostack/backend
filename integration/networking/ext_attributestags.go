package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/attributestags"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/attributestags

// ReplaceAllResTags ...
func (c *Networking) ReplaceAllResTags(tagKey, tagValue string) ([]string, error) {
	provider := models.GetProvider(c.Username)
	createOpts := networks.CreateOpts{}
	network, err := networks.Create(models.GetClientNetwork(provider), createOpts).Extract()
	if err != nil {
		return nil, err
	}

	tagReplaceAllOpts := attributestags.ReplaceAllOpts{
		Tags: []string{tagKey, tagValue},
	}
	replace, err := attributestags.ReplaceAll(models.GetClientNetwork(provider), "networks", network.ID, tagReplaceAllOpts).Extract()
	return replace, err
}

// ListResTags ...
func (c *Networking) ListResTags(networkID string) ([]string, error) {
	provider := models.GetProvider(c.Username)
	tags, err := attributestags.List(models.GetClientNetwork(provider), "networks", networkID).Extract()
	return tags, err
}

// DeleteAllResTags ...
func (c *Networking) DeleteAllResTags(networkID string) error {
	provider := models.GetProvider(c.Username)
	err := attributestags.DeleteAll(models.GetClientNetwork(provider), "networks", networkID).ExtractErr()
	return err
}

// AddTagToRes ...
func (c *Networking) AddTagToRes(networkID, atag string) error {
	provider := models.GetProvider(c.Username)
	err := attributestags.Add(models.GetClientNetwork(provider), "networks", networkID, atag).ExtractErr()
	return err
}

// DeleteTagFromRes ...
func (c *Networking) DeleteTagFromRes(networkID, atag string) error {
	provider := models.GetProvider(c.Username)
	err := attributestags.Delete(models.GetClientNetwork(provider), "networks", networkID, atag).ExtractErr()
	return err
}

// ConfirmTagExistOnRes ...
func (c *Networking) ConfirmTagExistOnRes(networkID, atag string) (bool, error) {
	provider := models.GetProvider(c.Username)
	exists, err := attributestags.Confirm(models.GetClientNetwork(provider), "networks", networkID, atag).Extract()
	return exists, err
}
