package networking

import (
	"github.com/gophercloud/gophercloud/openstack/common/extensions"
	"gitlab.com/fibo-stack/backend/models"
)

// ListExtensions ...
func (c *Networking) ListExtensions() ([]extensions.Extension, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := extensions.List(models.GetClientNetwork(provider)).AllPages()
	if err != nil {
		panic(err)
	}
	allExtensions, err := extensions.ExtractExtensions(allPages)

	return allExtensions, err
}

// GetExtension ...
func (c *Networking) GetExtension(alias string) *extensions.Extension {
	provider := models.GetProvider(c.Username)
	extension, err := extensions.Get(models.GetClientNetwork(provider), alias).Extract()
	if err != nil {
		panic(err)
	}
	return extension
}
