package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vlantransparent"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vlantransparent

// NetworkWithVLANTransparentExt ...
type NetworkWithVLANTransparentExt struct {
	networks.Network
	vlantransparent.TransparentExt
}

// ListNetworksWithVLANtransparentExt ...
func (c *Networking) ListNetworksWithVLANtransparentExt() ([]NetworkWithVLANTransparentExt, error) {
	provider := models.GetProvider(c.Username)
	iTrue := true
	networkListOpts := networks.ListOpts{}
	listOpts := vlantransparent.ListOptsExt{
		ListOptsBuilder: networkListOpts,
		VLANTransparent: &iTrue,
	}

	var allNetworks []NetworkWithVLANTransparentExt

	allPages, err := networks.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	err = networks.ExtractNetworksInto(allPages, &allNetworks)
	if err != nil {
		return nil, err
	}

	return allNetworks, err
}

// GetNetworkWithVLANtransparentExt ...
func (c *Networking) GetNetworkWithVLANtransparentExt(networkID string) (NetworkWithVLANTransparentExt, error) {
	provider := models.GetProvider(c.Username)
	var network NetworkWithVLANTransparentExt

	err := networks.Get(models.GetClientNetwork(provider), networkID).ExtractInto(&network)
	return network, err
}

// CreateNetworkWithVLANtransparentExt ...
func (c *Networking) CreateNetworkWithVLANtransparentExt(vlanTransparent bool, name string) (NetworkWithVLANTransparentExt, error) {
	provider := models.GetProvider(c.Username)
	networkCreateOpts := networks.CreateOpts{
		Name: name,
	}

	createOpts := vlantransparent.CreateOptsExt{
		CreateOptsBuilder: &networkCreateOpts,
		VLANTransparent:   &vlanTransparent,
	}

	var network NetworkWithVLANTransparentExt

	err := networks.Create(models.GetClientNetwork(provider), createOpts).ExtractInto(&network)
	return network, err
}
