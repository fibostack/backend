package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/provider"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/provider

// NetworkWithProvider ...
type NetworkWithProvider struct {
	networks.Network
	provider.NetworkProviderExt
}

// ListNetworksWithProviderInfo ...
func (c *Networking) ListNetworksWithProviderInfo() ([]NetworkWithProvider, error) {
	provider := models.GetProvider(c.Username)
	var allNetworks []NetworkWithProvider

	allPages, err := networks.List(models.GetClientNetwork(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	err = networks.ExtractNetworksInto(allPages, &allNetworks)
	if err != nil {
		return nil, err
	}

	return allNetworks, err
}

// CreateProviderNetwork ...
func (c *Networking) CreateProviderNetwork(networkType, physicalNetwork, name string, segmentationID int, shared bool) (*networks.Network, error) {

	segments := []provider.Segment{
		provider.Segment{
			NetworkType:     networkType,
			PhysicalNetwork: physicalNetwork,
			SegmentationID:  segmentationID,
		},
	}

	iTrue := true
	networkCreateOpts := networks.CreateOpts{
		Name:         name,
		AdminStateUp: &iTrue,
		Shared:       &shared,
	}

	createOpts := provider.CreateOptsExt{
		CreateOptsBuilder: networkCreateOpts,
		Segments:          segments,
	}
	provider := models.GetProvider(c.Username)
	network, err := networks.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return network, err
}
