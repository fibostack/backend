package networking

import (
	"time"

	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/agents"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/agents

// RetDetail ...
type RetDetail struct {
	ID                 string                 `json:"id"`
	AdminStateUp       bool                   `json:"admin_state_up"`
	AgentType          string                 `json:"agent_type"`
	Alive              bool                   `json:"alive"`
	AvailabilityZone   string                 `json:"availability_zone"`
	Binary             string                 `json:"binary"`
	Configurations     map[string]interface{} `json:"configurations"`
	CreatedAt          time.Time              `json:"created_at"`
	StartedAt          time.Time              `json:"started_at"`
	HeartbeatTimestamp time.Time              `json:"heartbeat_timestamp"`
	Description        string                 `json:"description"`
	Host               string                 `json:"host"`
	Topic              string                 `json:"topic"`
}

// ListAgents ...
func (c *Networking) ListAgents() ([]RetDetail, error) {
	provider := models.GetProvider(c.Username)
	listOpts := agents.ListOpts{
		// AgentType: "Open vSwitch agent",
	}

	allPages, err := agents.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allAgents, err := agents.ExtractAgents(allPages)

	var retValues []RetDetail

	for _, i := range allAgents {
		var ret RetDetail
		ret.ID = i.ID
		ret.AdminStateUp = i.AdminStateUp
		ret.AgentType = i.AgentType
		ret.Alive = i.Alive
		ret.AvailabilityZone = i.AvailabilityZone
		ret.Binary = i.Binary
		ret.Configurations = i.Configurations
		ret.CreatedAt = i.CreatedAt
		ret.StartedAt = i.StartedAt
		ret.HeartbeatTimestamp = i.HeartbeatTimestamp
		ret.Description = i.Description
		ret.Host = i.Host
		ret.Topic = i.Topic
		retValues = append(retValues, ret)
	}

	return retValues, err
}

// GetAgent ...
func (c *Networking) GetAgent(agentID string) (*agents.Agent, error) {
	provider := models.GetProvider(c.Username)
	agent, err := agents.Get(models.GetClientNetwork(provider), agentID).Extract()
	return agent, err
}
