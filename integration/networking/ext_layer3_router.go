package networking

import (
	"reflect"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/routers"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/routers

// ListRouters ...
func (c *Networking) ListRouters() ([]routers.Router, error) {
	provider := models.GetProvider(c.Username)
	listOpts := routers.ListOpts{}
	allPages, err := routers.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRouters, err := routers.ExtractRouters(allPages)
	if err != nil {
		return nil, err
	}
	return allRouters, err
}

// CreateRouter ...
func (c *Networking) CreateRouter(extNetworkID, name, desc string) (*routers.Router, error) {
	provider := models.GetProvider(c.Username)

	gwi := routers.GatewayInfo{
		NetworkID: extNetworkID,
	}

	createOpts := routers.CreateOpts{
		Name:         name,
		Description:  desc,
		AdminStateUp: gophercloud.Enabled,
		GatewayInfo:  &gwi,
	}

	router, err := routers.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return router, err
}

// UpdateRouter ...
func (c *Networking) UpdateRouter(name, routerID, extNetworkID, desc string, adminadminStateUp, distributed bool) (*routers.Router, error) {
	provider := models.GetProvider(c.Username)

	gwi := routers.GatewayInfo{
		NetworkID: extNetworkID,
		// EnableSNAT: &enableSNAT,
	}
	updateOpts := routers.UpdateOpts{
		Name:         name,
		Description:  &desc,
		GatewayInfo:  &gwi,
		Distributed:  &distributed,
		AdminStateUp: &adminadminStateUp,
	}

	router, err := routers.Update(models.GetClientNetwork(provider), routerID, updateOpts).Extract()
	return router, err
}

// GetRouter ...
func (c *Networking) GetRouter(routerID string) (*routers.Router, error) {
	provider := models.GetProvider(c.Username)
	router, err := routers.Get(models.GetClientNetwork(provider), routerID).Extract()
	return router, err
}

// AddRoutesToRouter ...
func (c *Networking) AddRoutesToRouter(routerID string, routes []routers.Route) (*routers.Router, error) {
	provider := models.GetProvider(c.Username)
	addOpts := routers.UpdateOpts{
		Routes: routes,
	}
	router, err := routers.Update(models.GetClientNetwork(provider), routerID, addOpts).Extract()
	return router, err
}

// RemoveRoutesFromRouter ...
func (c *Networking) RemoveRoutesFromRouter(routerID string, routes []routers.Route) (*routers.Router, error) {
	provider := models.GetProvider(c.Username)
	router, err := routers.Get(models.GetClientNetwork(provider), routerID).Extract()
	if err != nil {
		return nil, err
	}
	rRoutes := []routers.Route{}
	for _, r := range router.Routes {
		tmp := false
		for _, ri := range routes {
			if reflect.DeepEqual(r, ri) {
				tmp = true
				break
			}
		}
		if tmp == false {
			rRoutes = append(rRoutes, r)
		}
	}
	removeOpts := routers.UpdateOpts{
		Routes: rRoutes,
	}
	router, err = routers.Update(models.GetClientNetwork(provider), routerID, removeOpts).Extract()
	return router, err
}

// RemoveAllRoutesFromRouter ...
func (c *Networking) RemoveAllRoutesFromRouter(routerID string) (*routers.Router, error) {
	provider := models.GetProvider(c.Username)
	routes := []routers.Route{}

	updateOpts := routers.UpdateOpts{
		Routes: routes,
	}
	router, err := routers.Update(models.GetClientNetwork(provider), routerID, updateOpts).Extract()
	return router, err
}

// DeleteRouter ...
func (c *Networking) DeleteRouter(routerID string) error {
	provider := models.GetProvider(c.Username)
	err := routers.Delete(models.GetClientNetwork(provider), routerID).ExtractErr()
	return err
}

// AddInterfaceSubnetToRouter ...
func (c *Networking) AddInterfaceSubnetToRouter(routerID, subnetID string) (*routers.InterfaceInfo, error) {
	provider := models.GetProvider(c.Username)
	intOpts := routers.AddInterfaceOpts{
		SubnetID: subnetID,
	}

	interfac, err := routers.AddInterface(models.GetClientNetwork(provider), routerID, intOpts).Extract()
	return interfac, err
}

// AddInterfacePortToRouter ...
func (c *Networking) AddInterfacePortToRouter(routerID, portID string) (*routers.InterfaceInfo, error) {
	provider := models.GetProvider(c.Username)
	intOpts := routers.AddInterfaceOpts{
		PortID: portID,
	}

	interfac, err := routers.AddInterface(models.GetClientNetwork(provider), routerID, intOpts).Extract()
	return interfac, err
}

// RemoveInterfaceSubnetFromRouter ...
func (c *Networking) RemoveInterfaceSubnetFromRouter(routerID, subnetID string) (*routers.InterfaceInfo, error) {
	provider := models.GetProvider(c.Username)
	intOpts := routers.RemoveInterfaceOpts{
		SubnetID: subnetID,
	}

	interfac, err := routers.RemoveInterface(models.GetClientNetwork(provider), routerID, intOpts).Extract()
	return interfac, err
}

// RemoveInterfacePortFromRouter ...
func (c *Networking) RemoveInterfacePortFromRouter(routerID, portID string) (*routers.InterfaceInfo, error) {
	provider := models.GetProvider(c.Username)
	intOpts := routers.RemoveInterfaceOpts{
		PortID: portID,
	}

	interfac, err := routers.RemoveInterface(models.GetClientNetwork(provider), routerID, intOpts).Extract()
	return interfac, err
}
