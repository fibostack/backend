package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/vpnaas/endpointgroups"
	"gitlab.com/fibo-stack/backend/models"
)

// CreateEndpointGroup ...
func (c *Networking) CreateEndpointGroup(groupName string, typ endpointgroups.EndpointType, endpoints []string) (*endpointgroups.EndpointGroup, error) {
	provider := models.GetProvider(c.Username)
	createOpts := endpointgroups.CreateOpts{
		Name:      groupName,
		Type:      typ,
		Endpoints: endpoints,
	}
	group, err := endpointgroups.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return group, err
}

// GetEndpointGroup ...
func (c *Networking) GetEndpointGroup(endpointGID string) (*endpointgroups.EndpointGroup, error) {
	provider := models.GetProvider(c.Username)
	group, err := endpointgroups.Get(models.GetClientNetwork(provider), endpointGID).Extract()
	return group, err
}

// DeleteEndpointGroup ...
func (c *Networking) DeleteEndpointGroup(endpointGID string) error {
	provider := models.GetProvider(c.Username)
	err := endpointgroups.Delete(models.GetClientNetwork(provider), endpointGID).ExtractErr()
	return err
}

// ListEndpointGroups ...
func (c *Networking) ListEndpointGroups() ([]endpointgroups.EndpointGroup, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := endpointgroups.List(models.GetClientNetwork(provider), nil).AllPages()
	if err != nil {
		return nil, err
	}

	allGroups, err := endpointgroups.ExtractEndpointGroups(allPages)
	if err != nil {
		return nil, err
	}
	return allGroups, err
}

// UpdateEndpointGroup ...
func (c *Networking) UpdateEndpointGroup(name, description, endpointGID string) (*endpointgroups.EndpointGroup, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := endpointgroups.UpdateOpts{
		Name:        &name,
		Description: &description,
	}
	endpointGroup, err := endpointgroups.Update(models.GetClientNetwork(provider), endpointGID, updateOpts).Extract()
	return endpointGroup, err
}
