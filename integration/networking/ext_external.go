package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/external"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/external

// NetworkWithExternalExt ...
type NetworkWithExternalExt struct {
	networks.Network
	external.NetworkExternalExt
}

// ListExternalInfos ...
func (c *Networking) ListExternalInfos() ([]NetworkWithExternalExt, error) {
	provider := models.GetProvider(c.Username)
	iTrue := true
	networkListOpts := networks.ListOpts{}
	listOpts := external.ListOptsExt{
		ListOptsBuilder: networkListOpts,
		External:        &iTrue,
	}
	var allNetworks []NetworkWithExternalExt

	allPages, err := networks.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	err = networks.ExtractNetworksInto(allPages, &allNetworks)
	if err != nil {
		return nil, err
	}

	return allNetworks, err
}

// CreateNetworkExternalInfo ...
func (c *Networking) CreateNetworkExternalInfo(name string, ext bool) (*networks.Network, error) {
	provider := models.GetProvider(c.Username)
	networkCreateOpts := networks.CreateOpts{
		Name:         name,
		AdminStateUp: &ext,
	}

	createOpts := external.CreateOptsExt{
		networkCreateOpts,
		&ext,
	}

	network, err := networks.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return network, err
}
