package networking

import (
	"fmt"
	"strconv"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/networkipavailabilities"
	"gitlab.com/fibo-stack/backend/models"
)

// RetIPStatus ...
type RetIPStatus struct {
	TotalIPs int `json:"total_ips"`
	UsedIPs  int `json:"used_ips"`
}

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/networkipavailabilities

// ListNetworkIPAvailabilities ...
func (c *Networking) ListNetworkIPAvailabilities() ([]networkipavailabilities.NetworkIPAvailability, error) {
	provider := models.GetProvider(c.Username)
	allPages, err := networkipavailabilities.List(models.GetClientNetwork(provider), networkipavailabilities.ListOpts{}).AllPages()
	if err != nil {
		return nil, err
	}

	allAvailabilities, err := networkipavailabilities.ExtractNetworkIPAvailabilities(allPages)
	if err != nil {
		return nil, err
	}

	for _, availability := range allAvailabilities {
		fmt.Printf("%+v\n", availability)
	}
	return allAvailabilities, err
}

// GetNetworkIPAvailability ...
func (c *Networking) GetNetworkIPAvailability(networkIPAvailabilityID string) (*networkipavailabilities.NetworkIPAvailability, error) {
	provider := models.GetProvider(c.Username)
	availability, err := networkipavailabilities.Get(models.GetClientNetwork(provider), networkIPAvailabilityID).Extract()
	return availability, err
}

// ListNetworkIPAvailabilitiesTotal ...
func (c *Networking) ListNetworkIPAvailabilitiesTotal(adminProvider *gophercloud.ProviderClient, networkID string) (retVal RetIPStatus, err error) {
	listOpts := networkipavailabilities.ListOpts{
		NetworkID: networkID,
	}

	// provider := models.GetProvider(c.Username)
	allPages, err := networkipavailabilities.List(models.GetClientNetwork(adminProvider), listOpts).AllPages()
	if err != nil {
		return retVal, err
	}

	allAvailabilities, err := networkipavailabilities.ExtractNetworkIPAvailabilities(allPages)
	if err != nil {
		return retVal, err
	}

	for _, availability := range allAvailabilities {
		fmt.Println("*******")
		fmt.Println("TotalIPs", availability.TotalIPs)
		fmt.Println("UsedIPs", availability.UsedIPs)

		tIps, _ := strconv.Atoi(availability.TotalIPs)
		uIps, _ := strconv.Atoi(availability.UsedIPs)
		retVal.TotalIPs += tIps
		retVal.UsedIPs += uIps
		fmt.Println("*******")
	}
	return retVal, err
}
