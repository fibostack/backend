package networking

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/rules"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/fwaas/rules

// ListRules ...
func (c *Networking) ListRules(protocol rules.Protocol) ([]rules.Rule, error) {
	provider := models.GetProvider(c.Username)
	listOpts := rules.ListOpts{
		Protocol: string(protocol),
	}

	allPages, err := rules.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allRules, err := rules.ExtractRules(allPages)
	if err != nil {
		return nil, err
	}

	for _, rule := range allRules {
		fmt.Printf("%+v\n", rule)
	}
	return allRules, err
}

// CreateRule ...
func (c *Networking) CreateRule(action, description, destPort, destIPaddr string) (*rules.Rule, error) {
	provider := models.GetProvider(c.Username)
	createOpts := rules.CreateOpts{
		Action:               action,
		Protocol:             rules.ProtocolTCP,
		Description:          description,
		DestinationPort:      destPort,
		DestinationIPAddress: destIPaddr,
	}

	rule, err := rules.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return rule, err
}

// UpdateRule ...
func (c *Networking) UpdateRule(ruleID, newPort, newDescription string) (*rules.Rule, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := rules.UpdateOpts{
		Description:     &newDescription,
		DestinationPort: &newPort,
	}

	rule, err := rules.Update(models.GetClientNetwork(provider), ruleID, updateOpts).Extract()
	return rule, err
}

// DeleteRule ...
func (c *Networking) DeleteRule(ruleID string) error {
	provider := models.GetProvider(c.Username)
	err := rules.Delete(models.GetClientNetwork(provider), ruleID).ExtractErr()
	return err
}
