package networking

import (
	"fmt"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/floatingips"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/floatingips

// ListFLoatingIPs ...
func (c *Networking) ListFLoatingIPs(tenantID string) ([]floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)
	listOpts := floatingips.ListOpts{}
	if tenantID != "" {
		listOpts = floatingips.ListOpts{
			// FloatingNetworkID: networkID,
			TenantID: tenantID,
		}
	}

	allPages, err := floatingips.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allFIPs, err := floatingips.ExtractFloatingIPs(allPages)
	if err != nil {
		return nil, err
	}

	for _, fip := range allFIPs {
		fmt.Printf("%+v\n", fip)
	}
	return allFIPs, err
}

// GetFloatingIP ...
func (c *Networking) GetFloatingIP(FloatingIP string) (*floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)

	fip, err := floatingips.Get(models.GetClientNetwork(provider), FloatingIP).Extract()
	return fip, err
}

// CreateFloatingIP ...
func (c *Networking) CreateFloatingIP(networkID, description string) (*floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)
	createOpts := floatingips.CreateOpts{
		FloatingNetworkID: networkID,
		Description:       description,
	}

	fip, err := floatingips.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return fip, err
}

// GetNetworkFloatingIP ...
func (c *Networking) GetNetworkFloatingIP(adminProvider *gophercloud.ProviderClient, networkID string) ([]floatingips.FloatingIP, error) {
	listOpts := floatingips.ListOpts{
		FloatingNetworkID: networkID,
	}

	allPages, err := floatingips.List(models.GetClientNetwork(adminProvider), listOpts).AllPages()

	if err != nil {
		panic(err)
	}

	allFIPs, err := floatingips.ExtractFloatingIPs(allPages)
	if err != nil {
		panic(err)
	}

	fmt.Println(allFIPs)
	return allFIPs, err
}

// AssociatFloatingIP ...
func (c *Networking) AssociatFloatingIP(fipID, portID string) (*floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := floatingips.UpdateOpts{
		PortID: &portID,
	}

	fip, err := floatingips.Update(models.GetClientNetwork(provider), fipID, updateOpts).Extract()
	return fip, err
}

// DisassociateFloatingIP ...
func (c *Networking) DisassociateFloatingIP(fipID string) (*floatingips.FloatingIP, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := floatingips.UpdateOpts{
		PortID: new(string),
	}

	fip, err := floatingips.Update(models.GetClientNetwork(provider), fipID, updateOpts).Extract()
	return fip, err
}

// DeleteFloatingIP ...
func (c *Networking) DeleteFloatingIP(fipID string) error {
	provider := models.GetProvider(c.Username)
	err := floatingips.Delete(models.GetClientNetwork(provider), fipID).ExtractErr()
	return err
}
