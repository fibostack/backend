package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/portsecurity"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/ports"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/portsecurity

// NetworkWithPortSecurityExt ...
type NetworkWithPortSecurityExt struct {
	networks.Network
	portsecurity.PortSecurityExt
}

// ListPortSecurities ...
func (c *Networking) ListPortSecurities(name string) ([]NetworkWithPortSecurityExt, error) {
	provider := models.GetProvider(c.Username)
	var allNetworks []NetworkWithPortSecurityExt

	listOpts := networks.ListOpts{
		Name: name,
	}

	allPages, err := networks.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	err = networks.ExtractNetworksInto(allPages, &allNetworks)
	if err != nil {
		return nil, err
	}

	return allNetworks, err
}

// CreateNetworkWithoutPortsecurity ...
func (c *Networking) CreateNetworkWithoutPortsecurity(name string, portsecurityEnabled bool) (NetworkWithPortSecurityExt, error) {
	provider := models.GetProvider(c.Username)
	var networkWithPortSecurityExt NetworkWithPortSecurityExt

	networkCreateOpts := networks.CreateOpts{
		Name: name,
	}

	createOpts := portsecurity.NetworkCreateOptsExt{
		CreateOptsBuilder:   networkCreateOpts,
		PortSecurityEnabled: &portsecurityEnabled,
	}

	err := networks.Create(models.GetClientNetwork(provider), createOpts).ExtractInto(&networkWithPortSecurityExt)
	return networkWithPortSecurityExt, err
}

// DisablePortsecurityOnExistingNetwork ...
func (c *Networking) DisablePortsecurityOnExistingNetwork(networkID string) (NetworkWithPortSecurityExt, error) {
	provider := models.GetProvider(c.Username)
	var networkWithPortSecurityExt NetworkWithPortSecurityExt
	networkUpdateOpts := networks.UpdateOpts{}
	iFalse := false
	updateOpts := portsecurity.NetworkUpdateOptsExt{
		UpdateOptsBuilder:   networkUpdateOpts,
		PortSecurityEnabled: &iFalse,
	}

	err := networks.Update(models.GetClientNetwork(provider), networkID, updateOpts).ExtractInto(&networkWithPortSecurityExt)
	return networkWithPortSecurityExt, err
}

// PortWithPortSecurityExtensions ...
type PortWithPortSecurityExtensions struct {
	ports.Port
	portsecurity.PortSecurityExt
}

// GetPortWithPortsecurityInfo ...
func (c *Networking) GetPortWithPortsecurityInfo(portID string) (PortWithPortSecurityExtensions, error) {
	provider := models.GetProvider(c.Username)
	var portWithPortSecurityExtensions PortWithPortSecurityExtensions
	err := ports.Get(models.GetClientNetwork(provider), portID).ExtractInto(&portWithPortSecurityExtensions)
	return portWithPortSecurityExtensions, err
}

// CreatePortWithoutPortsecurity ...
func (c *Networking) CreatePortWithoutPortsecurity(networkID, subnetID string, portsecurityEnabled bool) (PortWithPortSecurityExtensions, error) {
	provider := models.GetProvider(c.Username)
	var portWithPortSecurityExtensions PortWithPortSecurityExtensions

	portCreateOpts := ports.CreateOpts{
		NetworkID: networkID,
		FixedIPs:  []ports.IP{ports.IP{SubnetID: subnetID}},
	}

	createOpts := portsecurity.PortCreateOptsExt{
		CreateOptsBuilder:   portCreateOpts,
		PortSecurityEnabled: &portsecurityEnabled,
	}

	err := ports.Create(models.GetClientNetwork(provider), createOpts).ExtractInto(&portWithPortSecurityExtensions)
	return portWithPortSecurityExtensions, err
}

// DisablePortsecurityOnExistingPort ...
func (c *Networking) DisablePortsecurityOnExistingPort(portID string) (PortWithPortSecurityExtensions, error) {
	provider := models.GetProvider(c.Username)
	var portWithPortSecurityExtensions PortWithPortSecurityExtensions
	iFalse := false

	portUpdateOpts := ports.UpdateOpts{}
	updateOpts := portsecurity.PortUpdateOptsExt{
		UpdateOptsBuilder:   portUpdateOpts,
		PortSecurityEnabled: &iFalse,
	}

	err := ports.Update(models.GetClientNetwork(provider), portID, updateOpts).ExtractInto(&portWithPortSecurityExtensions)
	return portWithPortSecurityExtensions, err
}
