package networking

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/portsbinding"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/ports"
	"gitlab.com/fibo-stack/backend/models"
)

// PortWithBinding struct
type PortWithBinding struct {
	ports.Port
	portsbinding.PortsBindingExt
}

// ListPorts ...
func (c *Networking) ListPorts(network string) ([]PortWithBinding, error) {
	var portWithBinding []PortWithBinding
	provider := models.GetProvider(c.Username)
	listOpts := ports.ListOpts{
		NetworkID: network,
	}

	allPages, err := ports.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	err = ports.ExtractPortsInto(allPages, &portWithBinding)
	if err != nil {
		return nil, err
	}

	return portWithBinding, err
}

// CreatePort ...
func (c *Networking) CreatePort(networkID, name, desc string, secGroups []string) (*ports.Port, error) {
	provider := models.GetProvider(c.Username)
	createOtps := ports.CreateOpts{
		Name:           name,
		AdminStateUp:   gophercloud.Enabled,
		NetworkID:      networkID,
		SecurityGroups: &secGroups,
		Description:    desc,
	}

	port, err := ports.Create(models.GetClientNetwork(provider), createOtps).Extract()
	return port, err
}

// UpdatePort ...
func (c *Networking) UpdatePort(portID, name, desc string, secGroups []string, state bool) (*ports.Port, error) {
	provider := models.GetProvider(c.Username)
	updateOpts := ports.UpdateOpts{
		Name:           &name,
		Description:    &desc,
		SecurityGroups: &secGroups,
		AdminStateUp:   &state,
	}

	port, err := ports.Update(models.GetClientNetwork(provider), portID, updateOpts).Extract()
	return port, err
}

// DeletePort ...
func (c *Networking) DeletePort(portID string) error {
	provider := models.GetProvider(c.Username)
	err := ports.Delete(models.GetClientNetwork(provider), portID).ExtractErr()
	return err
}

// GetPort ...
func (c *Networking) GetPort(portID string) (port *ports.Port, err error) {
	provider := models.GetProvider(c.Username)
	port, err = ports.Get(models.GetClientNetwork(provider), portID).Extract()
	return
}

// Bind port ...
func (c *Networking) BindPort() {

}

// ListPortsDevice ...
func (c *Networking) ListPortsDevice(deviceID string) ([]PortWithBinding, error) {
	var portWithBinding []PortWithBinding
	provider := models.GetProvider(c.Username)
	listOpts := ports.ListOpts{
		DeviceID: deviceID,
	}

	allPages, err := ports.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	err = ports.ExtractPortsInto(allPages, &portWithBinding)
	if err != nil {
		return nil, err
	}

	return portWithBinding, err
}
