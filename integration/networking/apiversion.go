package networking

import (
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/networking/v2/apiversions"
	"gitlab.com/fibo-stack/backend/models"
)

//https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/apiversions

// ListAPIVersions ...
func (c *Networking) ListAPIVersions() []apiversions.APIVersion {
	provider := models.GetProvider(c.Username)
	allPages, err := apiversions.ListVersions(models.GetClientNetwork(provider)).AllPages()
	if err != nil {
		fmt.Println(err)
	}

	allVersions, err := apiversions.ExtractAPIVersions(allPages)
	if err != nil {
		fmt.Println(err)
	}

	for _, version := range allVersions {
		fmt.Printf("%+v\n", version)
	}
	return allVersions
}
