package networking

import (
	"github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/extradhcpopts"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/ports"
	"gitlab.com/fibo-stack/backend/models"
)

// https://godoc.org/github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/extradhcpopts

// PortWithExtraDHCPExt ...
type PortWithExtraDHCPExt struct {
	ports.Port
	extradhcpopts.ExtraDHCPOptsExt
}

// GetPortWithExtraDHCPOpts ...
func (c *Networking) GetPortWithExtraDHCPOpts(portID string) (PortWithExtraDHCPExt, error) {
	provider := models.GetProvider(c.Username)
	var s PortWithExtraDHCPExt
	err := ports.Get(models.GetClientNetwork(provider), portID).ExtractInto(&s)
	return s, err
}

// CreatePortWithExtraDHCPOpt ...
func (c *Networking) CreatePortWithExtraDHCPOpt(name, networkID, subnetID, ipAddress string, optionName, optionValue string) (PortWithExtraDHCPExt, error) {
	provider := models.GetProvider(c.Username)
	var s PortWithExtraDHCPExt

	adminStateUp := true
	portCreateOpts := ports.CreateOpts{
		Name:         name,
		AdminStateUp: &adminStateUp,
		NetworkID:    networkID,
		FixedIPs: []ports.IP{
			{SubnetID: subnetID, IPAddress: ipAddress},
		},
	}

	createOpts := extradhcpopts.CreateOptsExt{
		CreateOptsBuilder: portCreateOpts,
		ExtraDHCPOpts: []extradhcpopts.CreateExtraDHCPOpt{
			{
				OptName:  optionName,
				OptValue: optionValue,
			},
		},
	}

	err := ports.Create(models.GetClientNetwork(provider), createOpts).ExtractInto(&s)
	return s, err
}

// UpdatePortWithExtraDHCPOpt ...
func (c *Networking) UpdatePortWithExtraDHCPOpt(name, subnetID, ipAddress, portID string, optionValue, optionName string) (PortWithExtraDHCPExt, error) {
	provider := models.GetProvider(c.Username)
	var s PortWithExtraDHCPExt

	portUpdateOpts := ports.UpdateOpts{
		Name: &name,
		FixedIPs: []ports.IP{
			{SubnetID: subnetID, IPAddress: ipAddress},
		},
	}
	updateOpts := extradhcpopts.UpdateOptsExt{
		UpdateOptsBuilder: portUpdateOpts,
		ExtraDHCPOpts: []extradhcpopts.UpdateExtraDHCPOpt{
			{
				OptName:  optionName,
				OptValue: &optionValue,
			},
		},
	}

	err := ports.Update(models.GetClientNetwork(provider), portID, updateOpts).ExtractInto(&s)
	return s, err
}
