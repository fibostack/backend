package networking

import (
	"time"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	"gitlab.com/fibo-stack/backend/models"
)

// Networking ...
type Networking struct {
	Username string
}

// NetworkDetail ...
type NetworkDetail struct {
	ID                    string    `json:"id"`
	Name                  string    `json:"name"`
	Description           string    `json:"description"`
	AdminStateUp          bool      `json:"admin_state_up"`
	Status                string    `json:"status"`
	Subnets               []string  `json:"subnets"`
	TenantID              string    `json:"tenant_id"`
	UpdatedAt             time.Time `json:"updated_at"`
	CreatedAt             time.Time `json:"created_at"`
	ProjectID             string    `json:"project_id"`
	Shared                bool      `json:"shared"`
	AvailabilityZoneHints []string  `json:"availability_zone_hints"`
	Tags                  []string  `json:"tags"`
}

// GetNetworking ...
func GetNetworking(username string) *Networking {
	return &Networking{
		Username: username,
	}
}

// ListNetworks ...
func (c *Networking) ListNetworks() ([]networks.Network, error) {
	provider := models.GetProvider(c.Username)

	iFalse := false
	listOpts := networks.ListOpts{
		Shared: &iFalse,
	}

	allPages, err := networks.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allNetworks, err := networks.ExtractNetworks(allPages)
	if err != nil {
		return nil, err
	}
	return allNetworks, err
}

// ListNetworksAll ...
func (c *Networking) ListNetworksAll(tenantID string) ([]networks.Network, error) {
	provider := models.GetProvider(c.Username)
	listOpts := networks.ListOpts{
		TenantID: tenantID,
	}

	allPages, err := networks.List(models.GetClientNetwork(provider), listOpts).AllPages()
	if err != nil {
		return nil, err
	}

	allNetworks, err := networks.ExtractNetworks(allPages)
	if err != nil {
		return nil, err
	}

	return allNetworks, err
}

// CreateNetwork ...
func (c *Networking) CreateNetwork(networkName, desc string, isShared bool) (*networks.Network, error) {
	provider := models.GetProvider(c.Username)
	createOpts := networks.CreateOpts{
		Name:         networkName,
		AdminStateUp: gophercloud.Enabled,
		Description:  desc,
		Shared:       &isShared,
	}

	network, err := networks.Create(models.GetClientNetwork(provider), createOpts).Extract()
	return network, err
}

// UpdateNetwork ...
func (c *Networking) UpdateNetwork(networkID, name, desc string, isShared, state bool) (*networks.Network, error) {
	provider := models.GetProvider(c.Username)

	updateOpts := networks.UpdateOpts{
		Name:         &name,
		AdminStateUp: &state,
		Description:  &desc,
		Shared:       &isShared,
	}

	network, err := networks.Update(models.GetClientNetwork(provider), networkID, updateOpts).Extract()
	return network, err
}

// DeleteNetwork ...
func (c *Networking) DeleteNetwork(networkID string) error {
	provider := models.GetProvider(c.Username)
	err := networks.Delete(models.GetClientNetwork(provider), networkID).ExtractErr()
	return err
}

// GetNetwork ...
func (c *Networking) GetNetwork(networkID string) (retNetwork NetworkDetail, err error) {
	provider := models.GetProvider(c.Username)
	network, err := networks.Get(models.GetClientNetwork(provider), networkID).Extract()
	retNetwork.ID = network.ID
	retNetwork.Name = network.Name
	retNetwork.Description = network.Description
	retNetwork.AdminStateUp = network.AdminStateUp
	retNetwork.Status = network.Status
	retNetwork.Subnets = network.Subnets
	retNetwork.TenantID = network.TenantID
	retNetwork.UpdatedAt = network.UpdatedAt
	retNetwork.CreatedAt = network.CreatedAt
	retNetwork.ProjectID = network.ProjectID
	retNetwork.Shared = network.Shared
	retNetwork.AvailabilityZoneHints = network.AvailabilityZoneHints
	retNetwork.Tags = network.Tags
	return retNetwork, err
}
