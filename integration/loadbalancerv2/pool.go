package loadbalancerv2

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/pools"
	"gitlab.com/fibo-stack/backend/models"
)

// ListPools ...
func (l *LoadBalancerV2) ListPools(lbID string) (poolss []pools.Pool, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := pools.ListOpts{
		LoadbalancerID: lbID,
	}

	allPages, err := pools.List(models.GetLoadBalancerV2(provider), listOpts).AllPages()
	if err != nil {
		return poolss, err
	}

	poolss, err = pools.ExtractPools(allPages)
	return poolss, err
}

// CreatePool ...
func (l *LoadBalancerV2) CreatePool(name, lstrID, plDesc string, lbMethod pools.LBMethod, protocol pools.Protocol) (pool *pools.Pool, err error) {
	provider := models.GetProvider(l.Username)
	createOpts := pools.CreateOpts{
		Description:  plDesc,
		LBMethod:     lbMethod,
		Protocol:     protocol,
		Name:         name,
		ListenerID:   lstrID,
		AdminStateUp: gophercloud.Enabled,
	}

	pool, err = pools.Create(models.GetLoadBalancerV2(provider), createOpts).Extract()
	return pool, err
}

// UpdatePool ...
func (l *LoadBalancerV2) UpdatePool(poolID, name string, status bool) (pool *pools.Pool, err error) {
	provider := models.GetProvider(l.Username)
	updateOpts := pools.UpdateOpts{
		Name:         &name,
		AdminStateUp: &status,
	}

	pool, err = pools.Update(models.GetLoadBalancerV2(provider), poolID, updateOpts).Extract()
	return pool, err
}

// DeletePool ...
func (l *LoadBalancerV2) DeletePool(poolID string) error {
	provider := models.GetProvider(l.Username)
	err := pools.Delete(models.GetLoadBalancerV2(provider), poolID).ExtractErr()
	return err
}

// GetPool ...
func (l *LoadBalancerV2) GetPool(poolID string) (*pools.Pool, error) {
	provider := models.GetProvider(l.Username)
	pool, err := pools.Get(models.GetLoadBalancerV2(provider), poolID).Extract()
	return pool, err
}

// ListPoolMembers ...
func (l *LoadBalancerV2) ListPoolMembers(poolID string) (members []pools.Member, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := pools.ListMembersOpts{}

	allPages, err := pools.ListMembers(models.GetLoadBalancerV2(provider), poolID, listOpts).AllPages()
	if err != nil {
		return members, err
	}

	members, err = pools.ExtractMembers(allPages)
	return members, err
}

// GetPoolMembers ...
func (l *LoadBalancerV2) GetPoolMembers(poolID, memberID string) (member *pools.Member, err error) {
	provider := models.GetProvider(l.Username)
	member, err = pools.GetMember(models.GetLoadBalancerV2(provider), poolID, memberID).Extract()
	return
}

// CreatePoolMember ...
func (l *LoadBalancerV2) CreatePoolMember(poolID, ip, name, subnetID string, protocolPort, weight int) (member *pools.Member, err error) {
	provider := models.GetProvider(l.Username)
	createOpts := pools.CreateMemberOpts{
		Name:         name,
		Address:      ip,
		ProtocolPort: protocolPort,
		SubnetID:     subnetID,
		Weight:       &weight,
	}

	member, err = pools.CreateMember(models.GetLoadBalancerV2(provider), poolID, createOpts).Extract()
	return member, err
}

// UpdatePoolMember ...
func (l *LoadBalancerV2) UpdatePoolMember(poolID, memberID, name string, weight int, status bool) (member *pools.Member, err error) {
	provider := models.GetProvider(l.Username)
	updateOpts := pools.UpdateMemberOpts{
		Name:         &name,
		Weight:       &weight,
		AdminStateUp: &status,
	}

	member, err = pools.UpdateMember(models.GetLoadBalancerV2(provider), poolID, memberID, updateOpts).Extract()
	return member, err
}

// DeletePoolMember ...
func (l *LoadBalancerV2) DeletePoolMember(poolID, memberID string) error {
	provider := models.GetProvider(l.Username)
	err := pools.DeleteMember(models.GetLoadBalancerV2(provider), poolID, memberID).ExtractErr()
	return err
}
