package loadbalancerv2

import (
	"fmt"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/loadbalancers"
	"gitlab.com/fibo-stack/backend/models"
)

// ListLBs ...
func (l *LoadBalancerV2) ListLBs() (lbs []loadbalancers.LoadBalancer, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := loadbalancers.ListOpts{}

	allPages, err := loadbalancers.List(models.GetLoadBalancerV2(provider), listOpts).AllPages()
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}

	lbs, err = loadbalancers.ExtractLoadBalancers(allPages)
	return lbs, err
}

// CreateLB ...
func (l *LoadBalancerV2) CreateLB(name, desc, vipSubnetID string, adminState gophercloud.EnabledState) (lb *loadbalancers.LoadBalancer, err error) {
	provider := models.GetProvider(l.Username)
	createOpts := loadbalancers.CreateOpts{
		Name:         name,
		AdminStateUp: adminState,
		VipSubnetID:  vipSubnetID,
		Description:  desc,
	}

	lb, err = loadbalancers.Create(models.GetLoadBalancerV2(provider), createOpts).Extract()
	return lb, err
}

// UpdateLB ...
func (l *LoadBalancerV2) UpdateLB(lbID, name string, adminState bool) (lb *loadbalancers.LoadBalancer, err error) {
	provider := models.GetProvider(l.Username)
	updateOpts := loadbalancers.UpdateOpts{
		Name:         &name,
		AdminStateUp: &adminState,
	}
	lb, err = loadbalancers.Update(models.GetLoadBalancerV2(provider), lbID, updateOpts).Extract()
	return lb, err
}

// DeleteLB ...
func (l *LoadBalancerV2) DeleteLB(lbID string) error {
	provider := models.GetProvider(l.Username)
	deleteOpts := loadbalancers.DeleteOpts{
		Cascade: true,
	}

	err := loadbalancers.Delete(models.GetLoadBalancerV2(provider), lbID, deleteOpts).ExtractErr()
	return err
}

// GetLBStatus ...
func (l *LoadBalancerV2) GetLBStatus(lbID string) (status *loadbalancers.StatusTree, err error) {
	provider := models.GetProvider(l.Username)
	status, err = loadbalancers.GetStatuses(models.GetLoadBalancerV2(provider), lbID).Extract()
	return status, err
}

// GetLB ...
func (l *LoadBalancerV2) GetLB(lbID string) (lb *loadbalancers.LoadBalancer, err error) {
	provider := models.GetProvider(l.Username)
	lb, err = loadbalancers.Get(models.GetLoadBalancerV2(provider), lbID).Extract()
	return lb, err
}

// GetLBStats ...
func (l *LoadBalancerV2) GetLBStats(lbID string) (stats *loadbalancers.Stats, err error) {
	provider := models.GetProvider(l.Username)
	stats, err = loadbalancers.GetStats(models.GetLoadBalancerV2(provider), lbID).Extract()
	return stats, err
}

// FailOverLB ...
func (l *LoadBalancerV2) FailOverLB(lbID string) error {
	provider := models.GetProvider(l.Username)
	err := loadbalancers.Failover(models.GetLoadBalancerV2(provider), lbID).ExtractErr()
	return err
}
