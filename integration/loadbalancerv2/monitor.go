package loadbalancerv2

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/monitors"
	"gitlab.com/fibo-stack/backend/models"
)

// ListMonitors ...
func (l *LoadBalancerV2) ListMonitors(poolID string) (monitorss []monitors.Monitor, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := monitors.ListOpts{
		PoolID: poolID,
	}

	allPages, err := monitors.List(models.GetLoadBalancerV2(provider), listOpts).AllPages()
	if err != nil {
		return monitorss, err
	}

	monitorss, err = monitors.ExtractMonitors(allPages)
	return monitorss, err
}

// CreateMonitor ...
func (l *LoadBalancerV2) CreateMonitor(poolID, name, typ, urlPath, expCodes, httpMethod string, maxRetriesDown, delay, maxRetries, timeOut int) (monit *monitors.Monitor, err error) {
	provider := models.GetProvider(l.Username)
	createOpts := monitors.CreateOpts{
		Type:          typ,
		Name:          name,
		PoolID:        poolID,
		AdminStateUp:  gophercloud.Enabled,
		Delay:         delay,
		Timeout:       timeOut,
		MaxRetries:    maxRetries,
		HTTPMethod:    httpMethod,
		URLPath:       urlPath,
		ExpectedCodes: expCodes,
	}

	monit, err = monitors.Create(models.GetLoadBalancerV2(provider), createOpts).Extract()
	return monit, err
}

// UpdateMonitor ...
func (l *LoadBalancerV2) UpdateMonitor(monitorID, name, urlPath, expCodes, httpMethod string, delay, timeOut, maxRetries int, status bool) (monit *monitors.Monitor, err error) {
	provider := models.GetProvider(l.Username)
	updateOpts := monitors.UpdateOpts{
		Name:          &name,
		Delay:         delay,
		Timeout:       timeOut,
		MaxRetries:    maxRetries,
		URLPath:       urlPath,
		ExpectedCodes: expCodes,
		AdminStateUp:  &status,
		HTTPMethod:    httpMethod,
	}

	monit, err = monitors.Update(models.GetLoadBalancerV2(provider), monitorID, updateOpts).Extract()
	return monit, err
}

// DeleteMonitor ...
func (l *LoadBalancerV2) DeleteMonitor(monitorID string) error {
	provider := models.GetProvider(l.Username)
	err := monitors.Delete(models.GetLoadBalancerV2(provider), monitorID).ExtractErr()
	return err
}

// GetMonitor ...
func (l *LoadBalancerV2) GetMonitor(monitorID string) (monit *monitors.Monitor, err error) {
	provider := models.GetProvider(l.Username)
	monit, err = monitors.Get(models.GetLoadBalancerV2(provider), monitorID).Extract()
	return monit, err
}
