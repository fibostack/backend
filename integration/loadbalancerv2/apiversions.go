package loadbalancerv2

import (
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/apiversions"
	"gitlab.com/fibo-stack/backend/models"
)

// ListAPIs ...
func (l *LoadBalancerV2) ListAPIs() (apis []apiversions.APIVersion, err error) {
	provider := models.GetProvider(l.Username)
	allPages, err := apiversions.List(models.GetLoadBalancerV2(provider)).AllPages()
	if err != nil {
		return apis, err
	}

	apis, err = apiversions.ExtractAPIVersions(allPages)
	return apis, err
}
