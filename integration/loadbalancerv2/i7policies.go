package loadbalancerv2

import (
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/l7policies"
	"gitlab.com/fibo-stack/backend/models"
)

// CreateL7Policy ...
func (l *LoadBalancerV2) CreateL7Policy(name, listenerID, action, redirectURL string) (l7policy *l7policies.L7Policy, err error) {
	provider := models.GetProvider(l.Username)
	createOpts := l7policies.CreateOpts{
		Name:        name,
		ListenerID:  listenerID,
		Action:      l7policies.ActionRedirectToURL,
		RedirectURL: redirectURL,
	}
	l7policy, err = l7policies.Create(models.GetLoadBalancerV2(provider), createOpts).Extract()
	return l7policy, err
}

// ListL7Rules ...
func (l *LoadBalancerV2) ListL7Rules(l7policyID string) (l7rules []l7policies.Rule, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := l7policies.ListRulesOpts{
		RuleType: l7policies.TypePath,
	}
	allPages, err := l7policies.ListRules(models.GetLoadBalancerV2(provider), l7policyID, listOpts).AllPages()
	if err != nil {
		return l7rules, err
	}
	l7rules, err = l7policies.ExtractRules(allPages)
	return l7rules, err
}

// GetL7Rule ...
func (l *LoadBalancerV2) GetL7Rule(l7policyID, l7ruleID string) (l7rule *l7policies.Rule, err error) {
	provider := models.GetProvider(l.Username)
	l7rule, err = l7policies.GetRule(models.GetLoadBalancerV2(provider), l7policyID, l7ruleID).Extract()
	return l7rule, err
}

// DeleteL7Rule ...
func (l *LoadBalancerV2) DeleteL7Rule(l7policyID, l7ruleID string) error {
	provider := models.GetProvider(l.Username)
	err := l7policies.DeleteRule(models.GetLoadBalancerV2(provider), l7policyID, l7ruleID).ExtractErr()
	return err
}

// UpdateL7Rule ...
//Eniig dahin uzne
func (l *LoadBalancerV2) UpdateL7Rule(l7policyID, l7ruleID string) (l7rule *l7policies.Rule, err error) {
	provider := models.GetProvider(l.Username)
	updateOpts := l7policies.UpdateRuleOpts{
		RuleType:    l7policies.TypePath,
		CompareType: l7policies.CompareTypeRegex,
		Value:       "/images/special*",
	}
	l7rule, err = l7policies.UpdateRule(models.GetLoadBalancerV2(provider), l7policyID, l7ruleID, updateOpts).Extract()
	return l7rule, err
}
