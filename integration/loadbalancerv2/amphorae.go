package loadbalancerv2

import (
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/amphorae"
	"gitlab.com/fibo-stack/backend/models"
)

// LoadBalancerV2 ...
type LoadBalancerV2 struct {
	Username string
}

// GetLoadBalancerV2 ...
func GetLoadBalancerV2(username string) *LoadBalancerV2 {
	return &LoadBalancerV2{
		Username: username,
	}
}

// ListAmphorae ...
func (l *LoadBalancerV2) ListAmphorae(lbID string) (amp []amphorae.Amphora, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := amphorae.ListOpts{
		LoadbalancerID: lbID,
	}

	allPages, err := amphorae.List(models.GetLoadBalancerV2(provider), listOpts).AllPages()
	if err != nil {
		return amp, err
	}

	amp, err = amphorae.ExtractAmphorae(allPages)
	return amp, err
}

// GetAmphorae ...
func (l *LoadBalancerV2) GetAmphorae(lbID string) (amp amphorae.Amphora, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := amphorae.ListOpts{
		LoadbalancerID: lbID,
	}

	allPages, err := amphorae.List(models.GetLoadBalancerV2(provider), listOpts).AllPages()
	if err != nil {
		return amp, err
	}

	ampList, err := amphorae.ExtractAmphorae(allPages)
	if len(ampList) > 0 {
		amp = ampList[0]
	}
	return amp, err
}
