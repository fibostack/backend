package loadbalancerv2

import (
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/listeners"
	"gitlab.com/fibo-stack/backend/models"
)

// ListListeners ...
func (l *LoadBalancerV2) ListListeners(lbID string) (listenerss []listeners.Listener, err error) {
	provider := models.GetProvider(l.Username)
	listOpts := listeners.ListOpts{
		LoadbalancerID: lbID,
	}

	allPages, err := listeners.List(models.GetLoadBalancerV2(provider), listOpts).AllPages()
	if err != nil {
		return listenerss, err
	}

	listenerss, err = listeners.ExtractListeners(allPages)
	return listenerss, err
}

// CreateListener ...
func (l *LoadBalancerV2) CreateListener(lbID, name, desc string, ltrProtocol listeners.Protocol, adminState gophercloud.EnabledState, port int, ltrClientTimeout, ltrTCPInspectTimeout, ltrMemberConnectTimeout, ltrMemberDataTimeout, ltrConnLimit int) (listener *listeners.Listener, err error) {
	provider := models.GetProvider(l.Username)
	// con := -1
	createOpts := listeners.CreateOpts{
		Name:           name,
		LoadbalancerID: lbID,
		AdminStateUp:   adminState,
		// DefaultPoolID:  defPoolID,
		ProtocolPort: port,
		Description:  desc,
		Protocol:     ltrProtocol,
		// ConnLimit:    &con,
		ConnLimit:            &ltrClientTimeout,
		TimeoutClientData:    &ltrClientTimeout,
		TimeoutMemberData:    &ltrMemberDataTimeout,
		TimeoutTCPInspect:    &ltrTCPInspectTimeout,
		TimeoutMemberConnect: &ltrMemberConnectTimeout,
	}

	listener, err = listeners.Create(models.GetLoadBalancerV2(provider), createOpts).Extract()
	return listener, err
}

// UpdateListener ...
func (l *LoadBalancerV2) UpdateListener(listenerID string, connLimit int, status bool) (listener *listeners.Listener, err error) {
	provider := models.GetProvider(l.Username)
	updateOpts := listeners.UpdateOpts{
		ConnLimit:    &connLimit,
		AdminStateUp: &status,
	}

	listener, err = listeners.Update(models.GetLoadBalancerV2(provider), listenerID, updateOpts).Extract()
	return listener, err
}

// DeleteListener ...
func (l *LoadBalancerV2) DeleteListener(listenerID string) error {
	provider := models.GetProvider(l.Username)
	err := listeners.Delete(models.GetLoadBalancerV2(provider), listenerID).ExtractErr()
	return err
}

// GetListenerStat ...
func (l *LoadBalancerV2) GetListenerStat(listenerID string) (stats *listeners.Stats, err error) {
	provider := models.GetProvider(l.Username)
	stats, err = listeners.GetStats(models.GetLoadBalancerV2(provider), listenerID).Extract()
	return stats, err
}

// GetListener ...
func (l *LoadBalancerV2) GetListener(listenerID string) (listner *listeners.Listener, err error) {
	provider := models.GetProvider(l.Username)
	listner, err = listeners.Get(models.GetLoadBalancerV2(provider), listenerID).Extract()
	return listner, err
}
