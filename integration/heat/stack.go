package heat

import (
	"bytes"
	"fmt"
	"io/ioutil"

	"gitlab.com/fibo-stack/backend/database"

	"github.com/gophercloud/gophercloud/openstack/orchestration/v1/stacks"
	"gitlab.com/fibo-stack/backend/models"
)

// CreateStack ...
func CreateStack(username, instName, instNetworkName, pubNetworkName, ipAddress string) (result stacks.CreateResult, err error) {
	temp := []byte("")
	if instNetworkName == pubNetworkName {
		if temp, err = ioutil.ReadFile("files/pub_waf.yaml"); err != nil {
			return
		}
	} else {
		if temp, err = ioutil.ReadFile("files/waf.yaml"); err != nil {
			return
		}
	}
	name := fmt.Sprintf("%s_waf", instName)
	temp = bytes.Replace(temp, []byte("[int_net_id]"), []byte(instNetworkName), -1)
	temp = bytes.Replace(temp, []byte("[pub_net_id]"), []byte(pubNetworkName), -1)
	temp = bytes.Replace(temp, []byte("[ipAddress]"), []byte(ipAddress), -1)
	temp = bytes.Replace(temp, []byte("[name]"), []byte(name), -1)
	result = stacks.Create(models.GetClientService(models.GetProvider(username)), stacks.CreateOpts{
		Name: name,
		TemplateOpts: &stacks.Template{
			TE: stacks.TE{
				Bin: temp,
			},
		},
	})
	return
}

// DeleteStack ...
func DeleteStack(username string, waf database.WAFs) stacks.DeleteResult {
	return stacks.Delete(models.GetClientService(models.GetProvider(username)), waf.Name, waf.WafID)
}
