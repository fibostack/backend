package database

import (
	"time"
)

type (
	// Base struct
	Base struct {
		ID        uint32    `orm:"auto;column(id)" json:"id"`
		CreatedAt time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
		UpdatedAt time.Time `orm:"auto_now;type(datetime)" json:"-"`
	}

	// SysUser struct
	SysUser struct {
		Base
		Username      string    `orm:"column(username)" json:"username"`
		Password      string    `orm:"column(password)" json:"-"`
		Firstname     string    `orm:"column(firstname)" json:"firstname"`
		Lastname      string    `orm:"column(lastname)" json:"lastname"`
		MobileNum     string    `orm:"column(mobile_num)" json:"phone"`
		Email         string    `orm:"column(email)" json:"email"`
		OsUserID      string    `orm:"column(os_user_id);unique;size(100)" json:"os_user_id"`
		OsPwd         string    `orm:"column(os_pwd)" json:"-"`
		OsTenantID    string    `orm:"column(os_tenant_id)" json:"os_tenant_id"`
		Role          string    `orm:"column(role)" json:"role"`
		LastLoginDate time.Time `orm:"column(last_login_date);null" json:"last_login_date"`
		QuotaPlan     string    `orm:"column(quota_plan)" json:"quota_plan"`
		IsActive      bool      `orm:"column(is_active)" json:"is_active"`
	}

	// SysNotif struct
	SysNotif struct {
		Base
		OsUserID   string `orm:"column(os_user_id)" json:"-"`
		Title      string `json:"TopicName"`
		Text       string `json:"NotifName"`
		IsErrored  string `json:"IsError"`
		OsTenantID string `orm:"column(os_tenant_id)" json:"-"`
		IsViewed   int    `json:"IsNew"`
		URL        string `orm:"column(url)" json:"-"`
	}

	// SysForgotPwd sturct
	SysForgotPwd struct {
		Base
		Token    string
		Email    string
		IsActive bool
	}

	// SysMobileVal struct
	SysMobileVal struct {
		Base
		Code      string
		OsUserID  string `orm:"column(os_user_id)" json:"os_user_id"`
		MobileNum string `orm:"column(mobile_num)" json:"mobile_num"`
		IsUsed    bool   `orm:"column(is_used)" json:"is_used"`
	}

	// LappMaster struct
	LappMaster struct {
		Base
		Name      string
		PathYml   string
		PathImage string
	}

	// LogAction struct
	LogAction struct {
		Base
		OsUserID      string    `orm:"column(os_user_id)" json:"os_user_id"`
		OsTenantID    string    `orm:"column(os_tenant_id)" json:"os_tenant_id"`
		CloudProvider string    `orm:"column(cloud_provider)" json:"cloud_provider"`                 // OPENSTACK
		Service       string    `orm:"column(service)" json:"service"`                               // INTSANCE, NETWORK
		ResourceName  string    `orm:"column(resource_name)" json:"resource_name"`                   // instance-name
		ResourceID    string    `orm:"column(resource_id)" json:"resource_id"`                       // instance-id
		EventName     string    `orm:"column(event_name)" json:"event_name"`                         // start
		EventDate     time.Time `orm:"column(event_date)" json:"event_date"`                         // 2020-10-10 10:10:10
		ErrorMsg      string    `orm:"column(error_msg);type(text);null" json:"error_msg"`           // if error
		SourceIP      string    `orm:"column(source_ip);null" json:"source_ip"`                      // xxx.xxx.xxx.xxx
		RequestBody   string    `orm:"column(request_body);type(text);null" json:"request_body"`     // [{}]
		RequestHeader string    `orm:"column(request_header);type(text);null" json:"request_header"` // [{}]
	}

	// UsgHistory struct
	UsgHistory struct {
		Base
		SysUserID       uint32 `orm:"column(sys_user_id)" json:"sys_user_id"`
		OsUserID        string `orm:"column(os_user_id)" json:"os_user_id"`
		Type            string `orm:"column(type);null" json:"type"`
		Hostname        string `orm:"column(hostname);null" json:"hostname"`
		OsResourceID    string `orm:"column(os_resource_id);null" json:"os_resource_id"`
		OsInstanceID    string `orm:"column(os_instance_id);null" json:"os_instance_id"`
		Flavor          string `orm:"column(flavor);null" json:"flavor"`
		DiskSize        int    `orm:"column(disk_size);null" json:"disk_size"`
		CPU             int    `orm:"column(cpu);null" json:"cpu"`
		RAM             int    `orm:"column(ram);null" json:"ram"`
		Status          string `orm:"column(status)" json:"status"`
		StartDate       string `orm:"column(start_date)" json:"start_date"`
		EndDate         string `orm:"column(end_date);null" json:"end_date"`
		IP              string `orm:"column(ip);null" json:"ip"`
		LastLogActionID uint32 `orm:"column(last_log_action_id)" json:"last_log_action_id"`
	}

	// SysMsg struct
	SysMsg struct {
		Base
		Body     string `orm:"column(body)" json:"body"`
		Type     string `orm:"column(type)" json:"type"`
		OsUserID string `orm:"column(os_user_id)" json:"os_user_id"`
		ToNumber string `orm:"column(to_number)" json:"to_number"`
		IsError  bool   `orm:"column(is_error)" json:"is_error"`
		ErrorTxt string `orm:"column(error_txt)" json:"error_txt"`
	}

	// SysUserPermission struct
	SysUserPermission struct {
		ID        uint32          `orm:"column(id)" json:"id"`
		CreatedAt time.Time       `orm:"auto_now_add;type(datetime)" json:"created_at"`
		UpdatedAt time.Time       `orm:"auto_now;type(datetime)" json:"-"`
		Entity    string          `orm:"column(entity)" json:"entity"`
		API       string          `orm:"column(api)" json:"api"`
		Roles     []*SysUserRoles `orm:"reverse(many);null"`
	}

	// SysUserRoles struct
	SysUserRoles struct {
		ID          uint32               `orm:"column(id)" json:"id"`
		CreatedAt   time.Time            `orm:"auto_now_add;type(datetime)" json:"created_at"`
		UpdatedAt   time.Time            `orm:"auto_now;type(datetime)" json:"-"`
		Name        string               `orm:"column(name);unique" json:"name"`
		Permissions []*SysUserPermission `orm:"rel(m2m);null"`
	}

	// SysUserQuota struct
	SysUserQuota struct {
		Base
		Instances         int `orm:"column(instances)" json:"instances"`
		UsedInstances     int `orm:"column(used_instances)" json:"used_instances"`
		CPU               int `orm:"column(cpu)" json:"cpu"`
		UsedCPU           int `orm:"column(used_cpu)" json:"used_cpu"`
		RAM               int `orm:"column(ram)" json:"ram"`
		UsedRAM           int `orm:"column(used_ram)" json:"used_ram"`
		Keypair           int `orm:"column(keypair)" json:"keypair"`
		UsedKeypair       int `orm:"column(used_keypair)" json:"used_keypair"`
		Volume            int `orm:"column(volume)" json:"volume"`
		UsedVolume        int `orm:"column(used_volume)" json:"used_volume"`
		Snapshot          int `orm:"column(snapshot)" json:"snapshot"`
		UsedSnapshot      int `orm:"column(used_snapshot)" json:"used_snapshot"`
		VolumeSize        int `orm:"column(volume_size)" json:"volume_size"`
		UsedVolumeSize    int `orm:"column(used_volume_size)" json:"used_volume_size"`
		ExternalIP        int `orm:"column(external_ip)" json:"external_ip"`
		UsedExternalIP    int `orm:"column(used_external_ip)" json:"used_external_ip"`
		SecurityGroup     int `orm:"column(security_group)" json:"security_group"`
		UsedSecurityGroup int `orm:"column(used_security_group)" json:"used_security_group"`
	}

	// UserProjectRole ...
	UserProjectRole struct {
		Base
		Email        string `orm:"column(email);size(100)" json:"email"`
		OsUserID     string `orm:"column(os_user_id);size(100)" json:"os_user_id"`
		OsTenantID   string `orm:"column(os_tenant_id)" json:"os_tenant_id"`
		OsTenantName string `orm:"column(os_tenant_name)" json:"os_tenant_name"`
		RoleID       uint32 `orm:"column(role_id)" json:"role_id"`
		QuotaID      uint32 `orm:"column(quota_id)" json:"quota_id"`
	}

	// SysSettings struct
	SysSettings struct {
		Base
		Key   string `orm:"column(key)" json:"key"`
		Value string `orm:"column(value);type(text)" json:"value"`
	}

	// WAFs struct
	WAFs struct {
		Base
		OsUserID   string `orm:"column(os_user_id)" json:"-"`
		OsTenantID string `orm:"column(os_tenant_id)" json:"os_tenant_id"`
		Name       string `orm:"column(name))" json:"name"`
		WafID      string `orm:"column(waf_id);" json:"waf_id"`
		InstanceID string `orm:"column(instance_id)" json:"instance_id"`
	}
)
