package utils

import (
	"fmt"

	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
	"gitlab.com/fibo-stack/backend/models"
)

// DBConfig ...
type DBConfig struct {
	DbType       string
	UserName     string
	UserPassword string
	Host         string
	Port         string
	DbName       string
}

// ConnectMonascaDB ...
func ConnectMonascaDB() {
	configDbCon := new(DBConfig)
	configDbCon.DbType = beego.AppConfig.String("monitoring.db.type")
	configDbCon.DbName = beego.AppConfig.String("monitoring.db.dbname")
	configDbCon.UserName = beego.AppConfig.String("monitoring.db.username")
	configDbCon.UserPassword = beego.AppConfig.String("monitoring.db.password")
	configDbCon.Host = beego.AppConfig.String("monitoring.db.host")
	configDbCon.Port = beego.AppConfig.String("monitoring.db.port")
	connectionString := GetConnectionString(configDbCon.Host, configDbCon.Port, configDbCon.UserName, configDbCon.UserPassword, configDbCon.DbName)
	fmt.Println("String:", connectionString)
	dbAccessObj, dbErr := gorm.Open(configDbCon.DbType, connectionString+"?charset=utf8&parseTime=true")
	if dbErr != nil {
		fmt.Println("err::", dbErr)
	}
	dbAccessObj.Debug().AutoMigrate(&models.AlarmActionHistory{})
}
