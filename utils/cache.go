package utils

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/cache"
)

var myCache UCache

// UCache struct
type UCache struct {
	Provider cache.Cache
	time.Duration
}

// UCacheInit cache provider
func UCacheInit() {
	bm, err := cache.NewCache("memory", fmt.Sprintf(`{"interval": %d}`, 600))
	if err == nil {
		myCache = UCache{
			Provider: bm,
			Duration: 600 * time.Second,
		}
	} else {
		fmt.Println(err.Error())
	}
}

// GetUCache return UCache
func GetUCache() *UCache {
	return &myCache
}

// Set cache element
func (uc UCache) Set(key, value string) error {
	return uc.Provider.Put(key, value, uc.Duration)
}

// Get cache element
func (uc UCache) Get(key string) string {
	return uc.Provider.Get(key).(string)
}

// GetMulti cache element
func (uc UCache) GetMulti(keys []string) []string {
	return InterfaceArrayToStringArray(uc.Provider.GetMulti(keys))
}

// Delete cache element
func (uc UCache) Delete(key string) error {
	return uc.Provider.Delete(key)
}

// Exist cache element
func (uc UCache) Exist(key string) bool {
	return uc.Provider.IsExist(key)
}

// Clear cache
func (uc UCache) Clear() error {
	return uc.Provider.ClearAll()
}
