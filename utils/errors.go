// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package utils

import (
	"fmt"
	"time"

	"go.uber.org/zap"

	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var lang string

// HTTP status codes as registered with IANA.
// See: https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
const (
	StatusOK                 = 0
	StatusBadRequest         = 1
	StatusUnauthorized       = 2
	StatusPaymentRequired    = 3
	StatusForbidden          = 4
	StatusNotFound           = 5
	StatusEmailNotActivated  = 6
	StatusWrongPassword      = 7
	StatusWrongEmail         = 8
	StatusWrongRole          = 9
	StatusWrongOpenstack     = 99
	StatusWrongEmailPassword = 100
	//instance

	StatusFlavorNotCreated            = 10
	StatusServerNotCreated            = 11
	StatusNotReturnServerList         = 12
	StatusNotGetServer                = 13
	StatusNotStopServer               = 14
	StatusNotRebootServer             = 15
	StatusNotStartServer              = 16
	StatusNotDeleteServer             = 17
	StatusNotResizeServer             = 18
	StatusNotConfirmResize            = 19
	StatusNotRevertResize             = 20
	StatusNotSnapshot                 = 21
	StatusNotSuspend                  = 22
	StatusNotResume                   = 23
	StatusNotVNC                      = 24
	StatutSnapshotError               = 25
	StatusAlreadyVerified             = 26
	StatusLBNotCreated                = 27
	StatusPoolNotCreated              = 28
	StatusListenerNotCreated          = 29
	StatusNotCreatePoolMember         = 30
	StatusNotCreateMonitor            = 31
	StatusNotUpdateLoadBalancer       = 32
	StatusNotDeleteLoadBalancer       = 33
	StatusNotGetLoadBalancer          = 34
	StatusNotGetLoadBalancerStatistic = 35
	StatusPoolsNotGet                 = 36
	StatusNotDeletePool               = 37
	StatusNotUpdatePool               = 38
	StatusNotListPoolMembers          = 39
	StatusNotUpdatePoolMember         = 40
	StatusNotListListener             = 41
	StatusNotUpdateListener           = 42
	StatusNotGetListener              = 43
	StatusNotListMonitor              = 44
	StatusNotUpdateMonitor            = 45
	StatusNotDeleteMonitor            = 46
	StatusNotGetMonitor               = 47
	StatusNotListSubnet               = 48
	// StatusConflict                     = 10
	// StatusGone                         = 11
	// StatusLengthRequired               = 12
	// StatusPreconditionFailed           = 13
	// StatusRequestEntityTooLarge        = 14
	// StatusRequestURITooLong            = 15
	// StatusUnsupportedMediaType         = 16
	// StatusRequestedRangeNotSatisfiable = 17
	// StatusExpectationFailed            = 18
	// StatusTeapot                       = 19
	// StatusMisdirectedRequest           = 20
	// StatusUnprocessableEntity          = 21
	// StatusLocked                       = 22
	// StatusFailedDependency             = 23
	// StatusTooEarly                     = 24
	// StatusUpgradeRequired              = 25
	// StatusPreconditionRequired         = 26
	// StatusTooManyRequests              = 27
	// StatusRequestHeaderFieldsTooLarge  = 28
	// StatusUnavailableForLegalReasons   = 29

	//volume
)

var statusTextENG = map[int]string{
	StatusOK:                 "OK",
	StatusBadRequest:         "Bad Request",
	StatusUnauthorized:       "Unauthorized",
	StatusPaymentRequired:    "Payment Required",
	StatusForbidden:          "Forbidden",
	StatusNotFound:           "Not Found",
	StatusEmailNotActivated:  "Email not activated",
	StatusWrongPassword:      "Password incorrect",
	StatusWrongEmail:         "Email incorrect",
	StatusWrongRole:          "Wrong role",
	StatusWrongOpenstack:     "Can't connect cloud service",
	StatusWrongEmailPassword: "Wrong email or Password",
	StatusAlreadyVerified:    "Your email address is already verified",

	//instance
	StatusFlavorNotCreated:            "Custom Flavor can't Created",
	StatusServerNotCreated:            "Server not created",
	StatusNotReturnServerList:         "Not return lists of server",
	StatusNotGetServer:                "Can't get Server Details",
	StatusNotStopServer:               "Can't stop server",
	StatusNotRebootServer:             "Can't reboot server",
	StatusNotStartServer:              "Can't start server",
	StatusNotDeleteServer:             "Can't delete server",
	StatusNotResizeServer:             "Can't resize server",
	StatusNotConfirmResize:            "Can't confirm resize",
	StatusNotRevertResize:             "Can't revert resize",
	StatusNotSnapshot:                 "Can't take snapshot",
	StatusNotSuspend:                  "Can't suspend server",
	StatusNotResume:                   "Can't resume server",
	StatusNotVNC:                      "Can't create VNC of server",
	StatutSnapshotError:               "Can't create server snapshot",
	StatusLBNotCreated:                "Can't create LoadBalancer",
	StatusPoolNotCreated:              "Can't create Pool",
	StatusListenerNotCreated:          "Can't create Listener",
	StatusNotCreatePoolMember:         "Can't create pool member",
	StatusNotCreateMonitor:            "Can't create monitor",
	StatusNotUpdateLoadBalancer:       "Can't update loadbalancer",
	StatusNotDeleteLoadBalancer:       "Can't delete loadbalancer",
	StatusNotGetLoadBalancer:          "Can't get loadbalancer status",
	StatusNotGetLoadBalancerStatistic: "Can't get loadbalancer statistic",
	StatusPoolsNotGet:                 "Can't return list of pools",
	StatusNotDeletePool:               "Can't delete pool",
	StatusNotUpdatePool:               "Can't update pool",
	StatusNotListPoolMembers:          "Can't get pool members list",
	StatusNotUpdatePoolMember:         "Can't update pool member",
	StatusNotListListener:             "Can't get listener list",
	StatusNotUpdateListener:           "Can't update listener",
	StatusNotGetListener:              "Can't get listener",
	StatusNotListMonitor:              "Can't get monitor list",
	StatusNotUpdateMonitor:            "Can't update monitor",
	StatusNotDeleteMonitor:            "Can't delete monitor",
	StatusNotGetMonitor:               "Can't get monitor",
	StatusNotListSubnet:               "Can't get subnet list",
	// StatusConflict:                     "Conflict",
	// StatusGone:                         "Gone",
	// StatusLengthRequired:               "Length Required",
	// StatusPreconditionFailed:           "Precondition Failed",
	// StatusRequestEntityTooLarge:        "Request Entity Too Large",
	// StatusRequestURITooLong:            "Request URI Too Long",
	// StatusUnsupportedMediaType:         "Unsupported Media Type",
	// StatusRequestedRangeNotSatisfiable: "Requested Range Not Satisfiable",
	// StatusExpectationFailed:            "Expectation Failed",
	// StatusTeapot:                       "I'm a teapot",
	// StatusMisdirectedRequest:           "Misdirected Request",
	// StatusUnprocessableEntity:          "Unprocessable Entity",
	// StatusLocked:                       "Locked",
	// StatusFailedDependency:             "Failed Dependency",
	// StatusTooEarly:                     "Too Early",
	// StatusUpgradeRequired:              "Upgrade Required",
	// StatusPreconditionRequired:         "Precondition Required",
	// StatusTooManyRequests:              "Too Many Requests",
	// StatusRequestHeaderFieldsTooLarge:  "Request Header Fields Too Large",
	// StatusUnavailableForLegalReasons:   "Unavailable For Legal Reasons",
}

var statusTextMN = map[int]string{
	StatusOK:                 "OK",
	StatusBadRequest:         "Буруу хүсэлт",
	StatusUnauthorized:       "Нэвтрэх эрх алга",
	StatusPaymentRequired:    "Payment Required",
	StatusForbidden:          "Хандах эрх байхгүй",
	StatusNotFound:           "Үр дүн олдсонгүй",
	StatusEmailNotActivated:  "Имэйл хаяг баталгаажаагүй байна",
	StatusWrongPassword:      "Нууц үг буруу байна",
	StatusWrongEmail:         "Имэйл хаяг бүртгэлгүй байна",
	StatusWrongRole:          "Хэрэглэгчийн эрхээр нэвтрэх боломжгүй",
	StatusWrongOpenstack:     "Cloud service-д нэвтрэх боломжгүй",
	StatusWrongEmailPassword: "Имэйл эсвэл нууц үг буруу байна",
	StatusAlreadyVerified:    "Таны имэйл хаяг баталгаажсан байна",
	// StatusProxyAuthRequired: "",
	// StatusRequestTimeout: "Хүсэлтийн хугацаа дуусав",

	//instance
	StatusFlavorNotCreated:            "Хэрэглэгчийн flavor үүсч байх үед алдаа гарлаа",
	StatusServerNotCreated:            "Сервер үүсгэх үед алдаа гарлаа",
	StatusNotReturnServerList:         "Серверийн жагсаалтуудыг дуудах үед алдаа гарлаа",
	StatusNotGetServer:                "Серверийн мэдээллийг авах үед алдаа гарлаа",
	StatusNotStopServer:               "Серверийг унртаах үед алдаа гарлаа",
	StatusNotRebootServer:             "Серверийг дахин ачаалах үед алдаа гарлаа",
	StatusNotStartServer:              "Серверийг асааж үед алдаа гарлаа",
	StatusNotDeleteServer:             "Серверийг унтраах үед алдаа гарлаа",
	StatusNotResizeServer:             "Серверийн хэмжээг өөрчлөх үед алдаа гарлаа",
	StatusNotConfirmResize:            "Серверийн өөрчлөлтийг зөвшөөрөх үед алдаа гарлаа",
	StatusNotRevertResize:             "Серверийн өөрчлөлтийг буцаах үед алдаа гарлаа",
	StatusNotSnapshot:                 "Серверийн снапшот хийх үед алдаа гарлаа",
	StatusNotSuspend:                  "Серверийг suspend хийх үед гарлаа",
	StatusNotResume:                   "Серверийн resume хийх үед алдаа гарлаа",
	StatusNotVNC:                      "Серверийн VNC үүсгэх үед гарлаа",
	StatutSnapshotError:               "Серверийн снапшот үүсгэхэд алдаа гарлаа",
	StatusLBNotCreated:                "Ачаалал тэнцвэржүүлэгч үүсгэж чадсангүй",
	StatusPoolNotCreated:              "Пүүл үүсгэж чадсангүй",
	StatusListenerNotCreated:          "Листенер үүсгэж чадсангүй",
	StatusNotCreatePoolMember:         "Пүүлийн гишүүн үүсгэж чадсангүй",
	StatusNotCreateMonitor:            "Монитор үүсгэж чадсангүй",
	StatusNotUpdateLoadBalancer:       "Ачаалал тэнцвэржүүлэгчийг шинэчлэж чадсангүй",
	StatusNotDeleteLoadBalancer:       "Ачаалал тэнцвэржүүлэгчийг устгаж чадсангүй",
	StatusNotGetLoadBalancer:          "Ачаалал тэнцвэржүүлэгчийн мэдээллийг авч чадсангүй",
	StatusNotGetLoadBalancerStatistic: "Ачаалал тэнцвэржүүлэгчийн статистикийг авч чадсангүй",
	StatusPoolsNotGet:                 "Пүүлийн жагсаалт авч чадсангүй",
	StatusNotDeletePool:               "Пүүлийг устгаж чадсангүй",
	StatusNotUpdatePool:               "Пүүлийг шинэчлэж чадсангүй",
	StatusNotListPoolMembers:          "Пүүлийн гишүүдийн жагсаалыг авч чадсангүй",
	StatusNotUpdatePoolMember:         "Пүүлийн гишүүнийг шинэчлэж чадсангүй",
	StatusNotListListener:             "Листенерийн жагсаалтыг дуудаж чадсангүй",
	StatusNotUpdateListener:           "Листенерийг шинэчлэж чадсангүй",
	StatusNotGetListener:              "Листенерийн мэдээллийг авч чадсангүй",
	StatusNotListMonitor:              "Мониторыг жагсаалтыг авч чадсангүй",
	StatusNotUpdateMonitor:            "Мониторын шинэчлэж чадсангүй",
	StatusNotDeleteMonitor:            "Мониторыг устгаж чадсангүй",
	StatusNotGetMonitor:               "Мониторын мэдээллийг авч чадсангүй",
	StatusNotListSubnet:               "Subnet-ийн жагсаалтын авч чадсангүй",
	// StatusConflict:                     "Conflict",
	// StatusGone:                         "Gone",
	// StatusLengthRequired:               "Length Required",
	// StatusPreconditionFailed:           "Precondition Failed",
	// StatusRequestEntityTooLarge:        "Request Entity Too Large",
	// StatusRequestURITooLong:            "Request URI Too Long",
	// StatusUnsupportedMediaType:         "Unsupported Media Type",
	// StatusRequestedRangeNotSatisfiable: "Requested Range Not Satisfiable",
	// StatusExpectationFailed:            "Expectation Failed",
	// StatusTeapot:                       "I'm a teapot",
	// StatusMisdirectedRequest:           "Misdirected Request",
	// StatusUnprocessableEntity:          "Unprocessable Entity",
	// StatusLocked:                       "Locked",
	// StatusFailedDependency:             "Failed Dependency",
	// StatusTooEarly:                     "Too Early",
	// StatusUpgradeRequired:              "Upgrade Required",
	// StatusPreconditionRequired:         "Precondition Required",
	// StatusTooManyRequests:              "Too Many Requests",
	// StatusRequestHeaderFieldsTooLarge:  "Request Header Fields Too Large",
	// StatusUnavailableForLegalReasons:   "Unavailable For Legal Reasons",
}

// StatusText returns a text for the HTTP status code. It returns the empty
// string if the code is unknown.
func StatusText(code int) (message string) {
	if lang == "en" {
		return statusTextENG[code]
	} else if lang == "mn" {
		return statusTextMN[code]
	}
	message = ""
	return
}

// SetLang ...
func SetLang(headerLang string) {
	lang = headerLang
}

var today = time.Now()

var lumlog = &lumberjack.Logger{
	// Filename:   "log/" + today.Format("2006-01-02"),
	MaxSize:    10,
	MaxBackups: 3,
	MaxAge:     3,
}

// SetLumlog ...
func SetLumlog(userID string) {
	lumlog.Filename = "log/" + userID + "_" + today.Format("2006-01-02") + ".log"
}

func lumberjackZapHook(e zapcore.Entry) error {
	lumlog.Write([]byte(fmt.Sprintf("\n\n%+v", e)))
	return nil
}

// GetLogger ...
func GetLogger() *zap.Logger {
	logger, _ := zap.NewProduction(zap.Hooks(lumberjackZapHook))
	return logger
}
