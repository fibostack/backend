package dao

import (
	"fmt"

	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// NodeDao struct
type NodeDao struct {
	influxClient client.Client
}

// GetNodeDao ...
func GetNodeDao(influxClient client.Client) *NodeDao {
	return &NodeDao{
		influxClient: influxClient,
	}
}

// GetNodeCPUUsage ...
//Node의 현재 CPU사용률을 조회한다.
func (d NodeDao) GetNodeCPUUsage(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuUsageSQL := "select value from \"cpu.percent\"  where time > now() - 2m and hostname = '%s' order by time desc limit 1"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(cpuUsageSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeCpuUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeCPUUsageList ...
//Node의 현재 CPU사용률을 조회한다.
func (d NodeDao) GetNodeCPUUsageList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuUsageSQL := "select mean(value) as usage from \"cpu.percent\"  where hostname = '%s' "

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		cpuUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.HostName, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		cpuUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.HostName, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetNodeCpuUsageList Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeMemoryUsageList ...
//Node의 현재 Memory 사용률을 조회한다.
func (d NodeDao) GetNodeMemoryUsageList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	memoryTotalSQL := "select mean(value) as usage from \"mem.usable_perc\"  where hostname = '%s' "
	//memoryFreeSql := "select mean(value) as usage from \"mem.free_mb\"  where hostname = '%s' ";

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		memoryTotalSQL += " and time > now() - %s  group by time(%s);"
		//memoryFreeSql += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(memoryTotalSQL,
				request.HostName, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		memoryTotalSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(memoryTotalSQL,
				request.HostName, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetNodeMemoryUsageList Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeSwapMemoryFreeUsageList ...
//Node의 현재 CPU사용률을 조회한다.
func (d NodeDao) GetNodeSwapMemoryFreeUsageList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	swapFreeUsageSQL := "select mean(value) as usage from \"mem.swap_free_perc\"  where hostname = '%s' "

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		swapFreeUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(swapFreeUsageSQL,
				request.HostName, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		swapFreeUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(swapFreeUsageSQL,
				request.HostName, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetNodeSwapMemoryFreeUsageList Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeCPULoadList ...
//Node의 현재 CPU사용률을 조회한다.
func (d NodeDao) GetNodeCPULoadList(request models.DetailReq, minute string) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var cpuUsageSQL string
	if minute == "1m" {
		cpuUsageSQL = "select mean(value) as usage from \"load.avg_1_min\"  where hostname = '%s' "
	} else if minute == "5m" {
		cpuUsageSQL = "select mean(value) as usage from \"load.avg_5_min\"  where hostname = '%s' "
	} else if minute == "15m" {
		cpuUsageSQL = "select mean(value) as usage from \"load.avg_15_min\"  where hostname = '%s' "
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		cpuUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.HostName, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		cpuUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.HostName, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetNodeCpuLoadList Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeMemoryUsage ...
//Node의 현재 Total Memory을 조회한다.
func (d NodeDao) GetNodeMemoryUsage(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"mem.usable_perc\" where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeMemoryUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeTotalMemoryUsage ...
//Node의 현재 Total Memory을 조회한다.
func (d NodeDao) GetNodeTotalMemoryUsage(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"mem.total_mb\" where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeCpuUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeFreeMemoryUsage ...
//Node의 현재 Total Memory을 조회한다.
func (d NodeDao) GetNodeFreeMemoryUsage(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	freeMemSQL := "select value from \"mem.free_mb\"  where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(freeMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeCpuUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeTotalDisk ...
//Node의 현재 Total Memory을 조회한다.
func (d NodeDao) GetNodeTotalDisk(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"disk.total_space_mb\" where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeTotalDisk Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeUsedDisk ...
//Node의 현재 Total Memory을 조회한다.
func (d NodeDao) GetNodeUsedDisk(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"disk.total_used_space_mb\" where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeUsedDisk Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetAgentProcessStatus ...
//Monasca Agent Forwarder 현재 상태 조회
func (d NodeDao) GetAgentProcessStatus(request models.NodeReq, processName string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	agentStatusSQL := "select value from \"supervisord.process.status\" where hostname = '%s' and supervisord_process = '%s' and time > now() - 2m order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(agentStatusSQL,
			request.HostName, processName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("AgentProcess Status Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetAliveInstanceListByNodename ...
//VM Instance가 Running인 VM만 조회
func (d NodeDao) GetAliveInstanceListByNodename(request models.NodeReq, allStatus bool) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	/*     VM Instance Status
	       -1 : no status,
		0 : Running / OK,
		1 : Idle / blocked,
		2 : Paused,
		3 : Shutting down,
		4 : Shut off or Nova suspend
		5 : Crashed,
		6 : Power management suspend (S3 state)
	*/
	var instanceListStatusSQL string
	if allStatus == true {
		instanceListStatusSQL = "select resource_id, value from \"vm.host_alive_status\" where time > now() - 2m and hostname = '%s' ;"
	} else {
		instanceListStatusSQL = "select resource_id, value from \"vm.host_alive_status\" where time > now() - 2m and hostname = '%s' and value = 0 ;"
	}

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(instanceListStatusSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetInstanceList Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetMountPointList ...
func (d NodeDao) GetMountPointList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	mountPointListSQL := "select  mount_point, value from \"disk.space_used_perc\"  where time > now() - 150s and hostname = '%s'"
	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(mountPointListSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetServiceFileSystems Sql======>", q)
	resp, err := d.influxClient.Query(q)

	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeDiskUsage ...
//Node의 현재 Total Memory을 조회한다.
func (d NodeDao) GetNodeDiskUsage(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	diskUsageSQL := "select mean(value) as usage from \"disk.space_used_perc\"  where hostname = '%s' and mount_point = '%s' "
	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, request.MountPoint, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, request.MountPoint, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetNodeDiskUsage Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetNodeDiskIoReadKbyte ...
//Node의 disk io read Kbyte를 조회한다.
func (d NodeDao) GetNodeDiskIoReadKbyte(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	diskUsageSQL := "select mean(value) as usage from \"io.read_kbytes_sec\"  where hostname = '%s' and mount_point = '%s' "
	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, request.MountPoint, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, request.MountPoint, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetNodeDiskIoReadKbyte Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetNodeDiskIoWriteKbyte ...
//Node의 disk io read Kbyte를 조회한다.
func (d NodeDao) GetNodeDiskIoWriteKbyte(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	diskUsageSQL := "select mean(value) as usage from \"io.write_kbytes_sec\"  where hostname = '%s' and mount_point = '%s' "
	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, request.MountPoint, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, request.MountPoint, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetNodeDiskIoReadKbyte Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetNodeNetworkKbyte ...
//Node의 disk io read Kbyte를 조회한다.
func (d NodeDao) GetNodeNetworkKbyte(request models.DetailReq, inOut, device string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var diskUsageSQL string
	if inOut == "in" {
		diskUsageSQL = "select sum(value)/1024 as usage from \"net.in_bytes_sec\"  where hostname = '%s' and device =~ /%s/"
	} else if inOut == "out" {
		diskUsageSQL = "select sum(value)/1024 as usage from \"net.out_bytes_sec\"  where hostname = '%s' and device =~ /%s/"
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, device, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, device, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetNodeNetworkInOutKbyte Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetNodeNetworkError ...
//Node의 Network Error를 조회한다.
func (d NodeDao) GetNodeNetworkError(request models.DetailReq, inOut, device string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var diskUsageSQL string
	if inOut == "in" {
		diskUsageSQL = "select sum(value) as usage from \"net.in_errors_sec\"  where hostname = '%s' and device =~ /%s/"
	} else if inOut == "out" {
		diskUsageSQL = "select sum(value) as usage from \"net.out_errors_sec\"  where hostname = '%s' and device =~ /%s/"
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, device,
				request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.HostName, device,
				request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetNodeNetworkError Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetNodeNetworkDropPacket ...
//Node의 Network Dropped packets를 조회한다.
func (d NodeDao) GetNodeNetworkDropPacket(request models.DetailReq, inOut, device string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var diskUsageSQL string
	if inOut == "in" {
		diskUsageSQL = "select sum(value) as usage from \"net.in_packets_dropped_sec\"  where hostname = '%s' and device =~ /%s/ "
	} else if inOut == "out" {
		diskUsageSQL = "select sum(value) as usage from \"net.out_packets_dropped_sec\"  where hostname = '%s' and device =~ /%s/"
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {
		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL, request.HostName, device,
				request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {
		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL, request.HostName, device,
				request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetNodeNetworkDropPacket Sql==>", q)
	resp, err := d.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeTopProcessByCPU ...
//Node의 disk io read Kbyte를 조회한다.
func (d NodeDao) GetNodeTopProcessByCPU(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuTopProcessSQL := "select mean(value) as usage from \"process.cpu_perc\"  where time > now() - 2m and hostname = '%s' group by process_name "

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(cpuTopProcessSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeTopProcessByCpu Sql==>", q)

	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)

}

// GetNodeTopProcessByMemory ...
//Node의 disk io read Kbyte를 조회한다.
func (d NodeDao) GetNodeTopProcessByMemory(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuTopProcessSQL := "select mean(value) as usage from \"process.mem.rss_mbytes\"  where time > now() - 2m and hostname = '%s' group by process_name "

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(cpuTopProcessSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeTopProcessByMemory Sql==>", q)

	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}
