package dao

import (
	"fmt"

	"github.com/astaxie/beego"

	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

var influxConn client.Client

// GetInfluxConn ...
func GetInfluxConn() client.Client {
	return influxConn
}

// NewInfluxConn ...
// Influx info
func NewInfluxConn() {
	url := beego.AppConfig.String("metric.db.url")
	userName := beego.AppConfig.String("metric.db.username")
	password := beego.AppConfig.String("metric.db.password")

	influxConn, _ = client.NewHTTPClient(client.HTTPConfig{
		Addr:     url,
		Username: userName,
		Password: password,
	})
}

// GetInstanceCPUUsageList ...
//Instance의 현재 CPU사용률을 조회한다.
func GetInstanceCPUUsageList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {
			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuUsageSQL := "select mean(value) as usage from \"vm.cpu.utilization_norm_perc\"  where resource_id = '%s' "

	var q client.Query
	if request.DefaultTimeRange != "" {

		cpuUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		cpuUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(cpuUsageSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetInstanceCpuUsageList Sql==>", q)
	resp, err := influxConn.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceMemoryUsageList ...
//Instance의 현재 CPU사용률을 조회한다.
func GetInstanceMemoryUsageList(request models.DetailReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	memoryTotalSQL := "select mean(value) as usage from \"vm.mem.total_gb\"  where resource_id = '%s' "
	memoryFreeSQL := "select mean(value) as usage from \"vm.mem.free_gb\"  where resource_id = '%s' "

	models.MonitLogger.Debug("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		memoryTotalSQL += " and time > now() - %s  group by time(%s);"
		memoryFreeSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(memoryTotalSQL+memoryFreeSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		memoryTotalSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"
		memoryFreeSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(memoryTotalSQL+memoryFreeSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetInstanceMemoryUsageList Sql==>", q)
	resp, err := influxConn.Query(q)

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceCPUUsage ...
//Instance의 현재 CPU사용률을 조회한다.
func GetInstanceCPUUsage(request models.InstanceReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuUsageSQL := "select value from \"vm.cpu.utilization_norm_perc\"  where time > now() - 2m and resource_id = '%s' order by time desc limit 1"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(cpuUsageSQL,
			request.InstanceID),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetInstanceCpuUsage Sql======>", q)
	resp, err := influxConn.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceTotalMemoryUsage ...
//Node의 현재 Total Memory을 조회한다.
func GetInstanceTotalMemoryUsage(request models.InstanceReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"vm.mem.total_gb\" where time > now() - 2m and resource_id = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.InstanceID),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debugf("GetInstanceTotalMemoryUsage Sql======>", q)
	resp, err := influxConn.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceFreeMemoryUsage ...
//Node의 현재 Total Memory을 조회한다.
func GetInstanceFreeMemoryUsage(request models.InstanceReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	freeMemSQL := "select value from \"vm.mem.free_gb\"  where time > now() - 2m and resource_id = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(freeMemSQL,
			request.InstanceID),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debugf("GetInstanceFreeMemoryUsage Sql======>", q)
	resp, err := influxConn.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetInstanceDiskIoKbyte ...
//Node의 disk io read Kbyte를 조회한다.
func GetInstanceDiskIoKbyte(request models.DetailReq, gubun string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var diskUsageSQL string

	if gubun == "read" {
		diskUsageSQL = "select sum(value)/1024 as usage from \"vm.io.read_bytes_sec\"  where resource_id = '%s' "
	} else if gubun == "write" {
		diskUsageSQL = "select sum(value)/1024 as usage from \"vm.io.write_bytes_sec\"  where resource_id = '%s' "
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		diskUsageSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		diskUsageSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(diskUsageSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetInstanceDiskIoReadKbyte Sql==>", q)
	resp, err := influxConn.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetInstanceNetworkKbyte ...
//Instance의 network io read Kbyte를 조회한다.
func GetInstanceNetworkKbyte(request models.DetailReq, inOut string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var networkSQL string
	if inOut == "in" {
		networkSQL = "select sum(value)/1024 as usage from \"vm.net.in_bytes_sec\"  where resource_id = '%s' "
	} else if inOut == "out" {
		networkSQL = "select sum(value)/1024 as usage from \"vm.net.out_bytes_sec\"  where resource_id = '%s'"
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		networkSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		networkSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetInstanceNetworkKbyte Sql==>", q)
	resp, err := influxConn.Query(q)

	return utils.GetError().CheckError(*resp, err)

}

// GetInstanceNetworkPackets ...
//Instance의 network io read Kbyte를 조회한다.
func GetInstanceNetworkPackets(request models.DetailReq, inOut string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	var networkSQL string
	if inOut == "in" {
		networkSQL = "select sum(value) as usage from \"vm.net.in_packets_sec\"  where resource_id = '%s' "
	} else if inOut == "out" {
		networkSQL = "select sum(value) as usage from \"vm.net.out_packets_sec\"  where resource_id = '%s'"
	}

	models.MonitLogger.Debugf("defaultTimeRange: %s, timeRangeFrom: %s, timeRangeTo:%s", request.DefaultTimeRange, request.TimeRangeFrom, request.TimeRangeTo)

	var q client.Query
	if request.DefaultTimeRange != "" {

		networkSQL += " and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.DefaultTimeRange, request.GroupBy),
			Database: models.MetricDBName,
		}
	} else {

		networkSQL += " and time < now() - %s and time > now() - %s  group by time(%s);"

		q = client.Query{
			Command: fmt.Sprintf(networkSQL,
				request.InstanceID, request.TimeRangeFrom, request.TimeRangeTo, request.GroupBy),
			Database: models.MetricDBName,
		}
	}
	models.MonitLogger.Debug("GetInstanceNetworkPackets Sql==>", q)
	resp, err := influxConn.Query(q)

	return utils.GetError().CheckError(*resp, err)

}
