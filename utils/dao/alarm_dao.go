package dao

import (
	"fmt"
	"strconv"
	"time"

	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/jinzhu/gorm"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// AlarmDao struct
type AlarmDao struct {
	influxClient client.Client
	txn          *gorm.DB
}

// GetAlarmDao ...
func GetAlarmDao(influxClient client.Client, txn *gorm.DB) *AlarmDao {
	return &AlarmDao{
		influxClient: influxClient,
		txn:          txn,
	}
}

// GetAlarmsActionHistoryList ...
func (a *AlarmDao) GetAlarmsActionHistoryList(request models.AlarmActionRequest) ([]models.AlarmActionHistory, error) {

	fmt.Println("alarmRequest.AlarmID::", request.AlarmID)
	t := []models.AlarmActionHistory{}
	status := a.txn.Debug().Table("alarm_action_histories").
		Select("ID, alarm_ID,  alarm_action_desc, reg_date + INTERVAL "+strconv.Itoa(models.GmtTimeGap)+" HOUR  as reg_date , reg_user, modi_date + INTERVAL "+strconv.Itoa(models.GmtTimeGap)+" HOUR  as modi_date  , modi_user").
		Where("alarm_ID = ?", request.AlarmID).
		Order("reg_date desc").
		Find(&t)

	if status.Error != nil {
		return t, status.Error
	}
	return t, nil
}

// CreateAlarmAction ...
func (a *AlarmDao) CreateAlarmAction(request models.AlarmActionRequest) error {

	actionData := models.AlarmActionHistory{AlarmID: request.AlarmID, AlarmActionDesc: request.AlarmActionDesc}

	status := a.txn.Debug().Create(&actionData)

	if status.Error != nil {
		return status.Error
	}
	return status.Error
}

// UpdateAlarmAction ...
func (a *AlarmDao) UpdateAlarmAction(request models.AlarmActionRequest) error {

	status := a.txn.Debug().Table("alarm_action_histories").Where("ID = ? ", request.ID).
		Updates(map[string]interface{}{"alarm_action_desc": request.AlarmActionDesc, "modi_date": time.Now()})
	if status.Error != nil {
		return status.Error
	}
	return status.Error
}

// DeleteAlarmAction ...
func (a *AlarmDao) DeleteAlarmAction(request models.AlarmActionRequest) error {

	status := a.txn.Debug().Table("alarm_action_histories").Where("ID = ? ", request.ID).Delete(&request)

	if status.Error != nil {
		return status.Error
	}
	return status.Error
}

// GetAlarmHistoryList ...
//Instance의 현재 CPU사용률을 조회한다.
func (a AlarmDao) GetAlarmHistoryList(request models.AlarmReq) (_ client.Response, errMsg models.ErrMessage) {
	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {
			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	alarmHistorySQL := "select new_state, old_state, reason from alarm_state_history where alarm_ID = '%s' "

	var q client.Query
	if request.TimeRange != "" {

		alarmHistorySQL += " and time > now() - %s  "

		if request.State != "" {
			alarmHistorySQL += " and new_state = '%s'"
		}

		q = client.Query{
			Command: fmt.Sprintf(alarmHistorySQL+" order by time desc ;",
				request.AlarmID, request.TimeRange),
			Database: models.MetricDBName,
		}
	}

	models.MonitLogger.Debug("GetAlarmHistoryList Sql==>", q)
	resp, err := a.influxClient.Query(q)

	return utils.GetError().CheckError(*resp, err)
}
