package dao

import (
	"fmt"

	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/fibo-stack/backend/models"
	"gitlab.com/fibo-stack/backend/utils"
)

// MainDao struct
type MainDao struct {
	influxClient client.Client
}

// GetMainDao ...
func GetMainDao(influxClient client.Client) *MainDao {
	return &MainDao{
		influxClient: influxClient,
	}
}

// GetNodeCPUUsage ...
//Node의 현재 CPU사용률을 조회한다.
func (d MainDao) GetNodeCPUUsage(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	cpuUsageSQL := "select value from \"cpu.percent\"  where time > now() - 2m and hostname = '%s' order by time desc limit 1"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(cpuUsageSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeCpuUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeTotalMemoryUsage ...
//Node의 현재 Total Memory을 조회한다.
func (d MainDao) GetNodeTotalMemoryUsage(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"mem.total_mb\" where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeCpuUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeFreeMemoryUsage ...
//Node의 현재 Total Memory을 조회한다.
func (d MainDao) GetNodeFreeMemoryUsage(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	freeMemSQL := "select value from \"mem.free_mb\"  where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(freeMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeCpuUsage Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeTotalDisk ...
//Node의 현재 Total Memory을 조회한다.
func (d MainDao) GetNodeTotalDisk(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"disk.total_space_mb\" where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeTotalDisk Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetNodeUsedDisk ...
//Node의 현재 Total Memory을 조회한다.
func (d MainDao) GetNodeUsedDisk(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	totalMemSQL := "select value from \"disk.total_used_space_mb\" where time > now() - 2m and hostname = '%s' order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(totalMemSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetNodeUsedDisk Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetAgentProcessStatus ...
//Monasca Agent Forwarder 현재 상태 조회
func (d MainDao) GetAgentProcessStatus(request models.NodeReq, processName string) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	agentStatusSQL := "select value from \"supervisord.process.status\" where hostname = '%s' and supervisord_process = '%s' and time > now() - 2m order by time desc limit 1;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(agentStatusSQL,
			request.HostName, processName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("AgentProcess Status Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetAliveInstanceListByNodename ...
//VM Instance가 Running인 VM만 조회
func (d MainDao) GetAliveInstanceListByNodename(request models.NodeReq) (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	/*     VM Instance Status
	       -1 : no status,
		0 : Running / OK,
		1 : Idle / blocked,
		2 : Paused,
		3 : Shutting down,
		4 : Shut off or Nova suspend
	5 : Crashed,
		6 : Power management suspend (S3 state)
	*/

	instanceListStatusSQL := "select resource_id, value from \"host_alive_status\" where time > now() - 2m and hostname = '%s' and value = 0 ;"

	var q client.Query

	q = client.Query{
		Command: fmt.Sprintf(instanceListStatusSQL,
			request.HostName),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("GetInstanceList Sql======>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}

// GetOpenstackNodeList ...
//Monasca Agent Forwarder 현재 상태 조회
func (d MainDao) GetOpenstackNodeList() (_ client.Response, errMsg models.ErrMessage) {

	var errLogMsg string
	defer func() {
		if r := recover(); r != nil {

			errMsg = models.ErrMessage{
				"Message": errLogMsg,
			}
		}
	}()

	nodeListSQL := "select hostname, value from \"cpu.percent\" where time > now() - 2m;"

	var q client.Query

	q = client.Query{
		Command:  fmt.Sprintf(nodeListSQL),
		Database: models.MetricDBName,
	}

	models.MonitLogger.Debug("NodeList Sql =====>", q)
	resp, err := d.influxClient.Query(q)
	if err != nil {
		errLogMsg = err.Error()
	}

	return utils.GetError().CheckError(*resp, err)
}
