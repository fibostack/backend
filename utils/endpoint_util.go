package utils

import (
	"strconv"

	"github.com/astaxie/beego"
	"gitlab.com/fibo-stack/backend/models"
)

// SetEndpoints ...
func SetEndpoints() {
	models.MetricDBName = beego.AppConfig.String("metric.db.name")
	models.NovaURL = beego.AppConfig.String("nova.target.Url")
	models.NovaVersion = beego.AppConfig.String("nova.target.version")
	models.NeutronURL = beego.AppConfig.String("neutron.target.Url")
	models.NeutronVersion = beego.AppConfig.String("neutron.target.version")
	models.KeystoneURL = beego.AppConfig.String("keystone.target.Url")
	models.KeystoneVersion = beego.AppConfig.String("keystone.target.version")
	models.CinderURL = beego.AppConfig.String("cinder.target.Url")
	models.CinderVersion = beego.AppConfig.String("cinder.target.version")
	models.GlanceURL = beego.AppConfig.String("glance.target.Url")
	models.GlanceVersion = beego.AppConfig.String("glance.target.version")
	models.DefaultProjectID = beego.AppConfig.String("default.project_id")
	models.RabbitMqIP = beego.AppConfig.String("rabbitmq.ip")
	models.RabbitMqPort = beego.AppConfig.String("rabbitmq.port")
	models.GMTTimeGap, _ = strconv.ParseInt(beego.AppConfig.String("gmt.time.gap"), 10, 64)
}
