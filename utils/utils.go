package utils

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"

	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/fibo-stack/backend/models"
)

// ErrorMessage struct
type ErrorMessage struct {
	models.ErrMessage
}

// GetError ...
func GetError() *ErrorMessage {
	return &ErrorMessage{}
}

// GetCheckErrorMessage ...
func (e ErrorMessage) GetCheckErrorMessage(err error) models.ErrMessage {

	if err != nil {
		errMessage := models.ErrMessage{
			"Message": err.Error(),
		}
		return errMessage
	}
	return nil
}

// Round ...
func Round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

// RoundFloat ...
func RoundFloat(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(Round(num*output)) / output
}

// RoundFloatDigit2 ...
func RoundFloatDigit2(num float64) float64 {
	return RoundFloat(num, 2)
}

// FloattostrDigit2 ...
func FloattostrDigit2(fv float64) string {
	return strconv.FormatFloat(RoundFloatDigit2(fv), 'f', 2, 64)
}

// GetConnectionString ...
func GetConnectionString(host, port, user, pass, dbname string) string {

	return fmt.Sprintf("%s:%s@%s([%s]:%s)/%s%s",
		user, pass, "tcp", host, port, dbname, "")

}

// StringArrayDistinct ...
func StringArrayDistinct(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// CheckError ...
//오류 체크 모듈로 오류 발생시 오류 메시지 리턴
func (e ErrorMessage) CheckError(resp client.Response, err error) (client.Response, models.ErrMessage) {

	if err != nil {
		errMessage := models.ErrMessage{
			"Message": err.Error(),
		}
		return resp, errMessage

	} else if resp.Error() != nil {
		errMessage := models.ErrMessage{
			"Message": resp.Err,
		}
		return resp, errMessage
	} else {

		return resp, nil
	}
}

// ResponseUnmarshal ...
func ResponseUnmarshal(response *http.Response, resErr error) (map[string]interface{}, error) {

	if resErr != nil {
		return nil, resErr
	}
	var data interface{}
	rawdata, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(rawdata, &data)
	msg := data.(map[string]interface{})

	return msg, nil

}

// TypeCheckerInt ...
func TypeCheckerInt(target interface{}) interface{} {
	switch target.(type) {
	case int:
		// v is an int here, so e.g. v + 1 is possible.
		return target.(int)
	case float64:
		// v is a float64 here, so e.g. v + 1.0 is possible.
		return int(target.(float64))
	case string:
		// v is a string here, so e.g. v + " Yeah!" is possible.
		i, _ := strconv.ParseInt(target.(string), 10, 0)
		return i
	case json.Number:
		jsonValue := target.(json.Number)
		f, _ := strconv.ParseInt(jsonValue.String(), 10, 0)

		//f, _ := strconv.ParseFloat(jsonValue.String(),64)
		return f
	case nil:
		// v is a string here, so e.g. v + " Yeah!" is possible.
		return int(0)
	default:
		// And here I'm feeling dumb. ;)
		return int(0)
	}
}

// TypeCheckerFloat64 ...
func TypeCheckerFloat64(target interface{}) interface{} {

	switch target.(type) {
	case int:
		// v is an int here, so e.g. v + 1 is possible.
		return float64(target.(int))
	case float64:
		// v is a float64 here, so e.g. v + 1.0 is possible.
		return target.(float64)
	case string:
		// v is a string here, so e.g. v + " Yeah!" is possible.
		f, _ := strconv.ParseFloat(target.(string), 64)
		return f
	case nil:
		// v is a string here, so e.g. v + " Yeah!" is possible.
		return float64(0)
	case json.Number:
		jsonValue := target.(json.Number)
		f, _ := strconv.ParseFloat(jsonValue.String(), 64)
		return f

	default:
		// And here I'm feeling dumb. ;)
		return float64(0)
	}
}

// TypeCheckerString ...
func TypeCheckerString(target interface{}) interface{} {
	switch target.(type) {
	case int:
		// v is an int here, so e.g. v + 1 is possible.
		return fmt.Sprintf("%d", target)
	case float64:
		// v is a float64 here, so e.g. v + 1.0 is possible.
		return fmt.Sprintf("%f", target)
	case string:
		// v is a string here, so e.g. v + " Yeah!" is possible.
		return target.(string)
	case nil:
		// v is a string here, so e.g. v + " Yeah!" is possible.
		return ""
	default:
		// And here I'm feeling dumb. ;)
		return ""
	}
}

// TypeCheckerBool ...
func TypeCheckerBool(target interface{}) interface{} {
	switch target.(type) {
	case bool:
		return target.(bool)
	case string:
		return fmt.Sprintf("%t", target)
	case nil:
		return false
	default:
		return false
	}
}

// GetVMStatusCount ...
func GetVMStatusCount(noStatusList, runningList, idleList, pausedList, shutDownList, shutOffList, crashedList, powerOffList []string) []models.VMState {

	var vmStatusList []models.VMState

	//if len(noStatusList) != 0 {
	var vmStatusNo models.VMState
	vmStatusNo.VMStateName = models.VMStatusNo
	vmStatusNo.VMCnt = len(noStatusList)
	vmStatusList = append(vmStatusList, vmStatusNo)
	//}

	//if len(runningList) != 0 {
	var vmStatusRunning models.VMState
	vmStatusRunning.VMStateName = models.VMStatusRunning
	vmStatusRunning.VMCnt = len(runningList)
	vmStatusList = append(vmStatusList, vmStatusRunning)
	//}

	//if len(idleList) != 0 {
	var vmStatusIdle models.VMState
	vmStatusIdle.VMStateName = models.VMStatusIdle
	vmStatusIdle.VMCnt = len(idleList)
	vmStatusList = append(vmStatusList, vmStatusIdle)
	//}

	//if len(pausedList) != 0 {
	var vmStatusPaused models.VMState
	vmStatusPaused.VMStateName = models.VMStatusPaused
	vmStatusPaused.VMCnt = len(pausedList)
	vmStatusList = append(vmStatusList, vmStatusPaused)
	//}

	//if len(shutDownList) != 0 {
	var vmStatusShutDown models.VMState
	vmStatusShutDown.VMStateName = models.VMStatusShutdown
	vmStatusShutDown.VMCnt = len(shutDownList)
	vmStatusList = append(vmStatusList, vmStatusShutDown)
	//}

	//if len(shutOffList) != 0 {
	var vmStatusShutOff models.VMState
	vmStatusShutOff.VMStateName = models.VMStatusShutoff
	vmStatusShutOff.VMCnt = len(shutOffList)
	vmStatusList = append(vmStatusList, vmStatusShutOff)
	//}

	//if len(crashedList) != 0 {
	var vmStatusCrash models.VMState
	vmStatusCrash.VMStateName = models.VMStatusCrashed
	vmStatusCrash.VMCnt = len(crashedList)
	vmStatusList = append(vmStatusList, vmStatusCrash)
	//}

	//if len(powerOffList) != 0 {
	var vmStatusPowerOff models.VMState
	vmStatusPowerOff.VMStateName = models.VMStatusPoewrOff
	vmStatusPowerOff.VMCnt = len(powerOffList)
	vmStatusList = append(vmStatusList, vmStatusPowerOff)
	//}

	return vmStatusList
}

// ErrRenderJSONResponse ...
func ErrRenderJSONResponse(data interface{}, w http.ResponseWriter) {

	var errorCode float64
	var errorStruct models.ErrorMessageStruct

	errData := data.(models.ErrMessage)
	ErrorMessage := errData["Message"]

	//Openstack Error 인경우
	if strings.Contains(ErrorMessage.(string), "instead") {
		//Error Message 정보가 instead 이후에 json data로 되어 있음.
		errorJSON := strings.SplitAfter(ErrorMessage.(string), "instead")

		var errorString interface{}
		json.Unmarshal([]byte(errorJSON[1]), &errorString)
		errorMsgJSON := errorString.(map[string]interface{})
		for _, v := range errorMsgJSON {
			errorDetail := v.(map[string]interface{})
			errorStruct.HTTPStatus = int(errorDetail["code"].(float64))
			errorStruct.Message = errorDetail["message"].(string)
		}
	} else {
		errorStruct.Message = ErrorMessage.(string)
		if errData["HTTPStatus"] != nil {
			errorStruct.HTTPStatus = errData["HTTPStatus"].(int)
			errorCode = float64(errData["HTTPStatus"].(int))
		} else {
			errorStruct.HTTPStatus = 500
			errorCode = float64(500)
		}
	}

	fmt.Println("===>", errorCode)
	js, err := json.Marshal(errorStruct)

	if err != nil {
		log.Fatalln("Error writing JSON:", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(errorStruct.HTTPStatus)
	w.Write(js)
	return
}

// RenderJSONUnAuthResponse ...
func RenderJSONUnAuthResponse(data interface{}, status int, w http.ResponseWriter) {

	var errorCode float64
	var errorStruct models.ErrorMessageStruct

	errData := data.(models.ErrMessage)
	ErrorMessage := errData["Message"]
	errorStruct.Message = ErrorMessage.(string)
	if errData["HTTPStatus"] != nil {
		errorStruct.HTTPStatus = errData["HTTPStatus"].(int)
		errorCode = float64(errData["HTTPStatus"].(int))
	} else {
		errorStruct.HTTPStatus = status
		errorCode = float64(status)
	}

	js, err := json.Marshal(errorStruct)

	if err != nil {
		log.Fatalln("Error writing JSON:", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(int(errorCode))
	w.Write(js)
	return
}

// RenderJSONResponse ...
func RenderJSONResponse(data interface{}, w http.ResponseWriter) {

	js, err := json.Marshal(data)
	if err != nil {
		log.Fatalln("Error writing JSON:", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(js)
	return
}

// RenderJSONLogoutResponse ...
func RenderJSONLogoutResponse(data interface{}, w http.ResponseWriter) {

	js, err := json.Marshal(data)
	if err != nil {
		log.Fatalln("Error writing JSON:", err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(js)

	return
}

// RenderJSONForbiddenResponse ...
func RenderJSONForbiddenResponse(data interface{}, w http.ResponseWriter) {

	js, err := json.Marshal(data)
	if err != nil {
		log.Fatalln("Error writing JSON:", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusForbidden)
	w.Write(js)
	return
}

// GenerateRandomBytes ...
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomString ...
// GenerateRandomString returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomString(s int) (string, error) {
	b, err := GenerateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

// InterfaceArrayToStringArray ...
func InterfaceArrayToStringArray(input []interface{}) []string {
	aString := make([]string, len(input))
	for i, v := range input {
		aString[i] = v.(string)
	}
	return aString
}
