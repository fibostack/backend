package utils

import (
	"fmt"
	"io"
	"os"
	"time"
)

// CreateFile ...
func CreateFile(path string) {
	// check if file exists
	var _, err = os.Stat(path)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if IsError(err) {
			return
		}
		defer file.Close()
	}

	fmt.Println("File Created Successfully", path)
}

// WriteFile ...
func WriteFile(osErr error, path string) {
	// Open file using READ & WRITE permission.
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if IsError(err) {
		return
	}
	defer file.Close()
	t := time.Now()
	// Write some text line-by-line to file.
	_, err = file.WriteString(t.Format(time.RFC3339) + osErr.Error() + " \n")
	if IsError(err) {
		return
	}
	// Save file changes.
	err = file.Sync()
	if IsError(err) {
		return
	}
	fmt.Println("File Updated Successfully.")
}

// ReadFile ...
func ReadFile(path string) (text []byte) {
	// Open file for reading.
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if IsError(err) {
		return
	}
	defer file.Close()

	// Read file, line by line
	// var text = make([]byte, 1024)
	for {
		_, err = file.Read(text)

		// Break if finally arrived at end of file
		if err == io.EOF {
			break
		}

		// Break if error occured
		if err != nil && err != io.EOF {
			IsError(err)
			break
		}
	}
	fmt.Println("Reading from file.")
	return text
}

// DeleteFile ...
func DeleteFile(path string) {
	// delete file
	var err = os.Remove(path)
	if IsError(err) {
		return
	}
	fmt.Println("File Deleted")
}

// IsError ...
func IsError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}
	return (err != nil)
}
