package utils

import (
	"fmt"
	"io/ioutil"

	"github.com/cihub/seelog"
)

// Logger interface
var Logger seelog.LoggerInterface

// UseLogger ...
func UseLogger(newLogger seelog.LoggerInterface) {
	Logger = newLogger
}

// ReadXMLConfig ...
func ReadXMLConfig(filename string) (string, error) {
	xmlFile, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return "", err
	}
	return string(xmlFile), nil
}
